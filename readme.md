## Fator Digital - CMS 2016

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## Seguir os passos a seguir após puxar os arquivos do git


0. Copiar o database que está no server (professorpiva)

1. Editar o arquivo .ENV que se encontra na raiz e ajustar as configurações básicas

2. Caso não tenha a pasta vendor utilizar o comando ** composer install **, caso tenha, usar o commando ** composer update **

2.1 No arquivo .ENV a URL precisa estar corretamente setada para não ter problema com perda de sessões e etc...

3. Para acessar o administrativo do sistema, utilizar o /fatorcms

4. Usuário desenvolvedor (Login: dev@fd.com.br, senha: 123)

5. Commando disponíveis para verificar e gerenciar o projeto

5.1 php artisan route:list [ Consegue visualizar todas as URL's disponíveis ]

5.2 php artisan module:make [ Visualiza o HELPER para criação de Controller, Models, Views e etc ]

5.2.1 Exemplo: php artisan module:make-model {nome da model singularize} {nome da pasta que está na pasta module}

5.2.2 Exemplo: php artisan module:make-controller {nome da controller camelcase} {nome da pasta está na pasta module}

5.3 Modulo e Controller ambos tem um extend padrão pra classe [BaseModel, BaseModuleController]
    
6. Todas as URl's precisam ser declaradas, nessa versão, as rotas ficam dentro de cada módulo/Http/routes.php

6.1 Caso não ache uma action, pode ser que ela está sendo mapeada com {resource} (index, create, store, edit, update, destroy}

7. Módulo FrontEnd manipula as ações públicas onde os usuários vão trabalhar.


## Documentação Oficial

Documentação do framework [Laravel website](https://laravel.com/docs/5.2).

## Segurança

Caso seja descoberta um vulnerabilidade no framework, informar Taylor Otwell taylor@laravel.com.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

Última atualização da documentação [08/01/2016]
