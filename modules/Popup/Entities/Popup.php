<?php namespace Modules\Popup\Entities;

use App\Events\ImageEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Popup extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'name', 'slug', 'image', 'url',
        'status', 'release_date', 'closed_date'
    ];
    
    public function setSlugAttribute($value)
    {
        if (!empty($value))
            $this->attributes['slug'] = str_slug($value);
    }

    public function setReleaseDateAttribute($value)
    {
        if (!empty($value) && typeDate($value))
            $this->attributes['release_date'] = dateToSql($value);
    }

    public function setClosedDateAttribute($value)
    {
        if (!empty($value) && typeDate($value))
            $this->attributes['closed_date'] = dateToSql($value);
    }

    public function setImageAttribute()
    {
        event(new ImageEvent(request(), ['path' => '/assets/popup/', 'sizes' => [['w' => '800', 'h' => '400'], ['w' => '100']]], 'image'));
        $this->attributes['image'] = json_encode(request('return_upload'));
    }

    public function getClosedDateAttribute($value)
    {
        if (!empty($value) && $value != '0000-00-00')
            return dateToSql($value, 'Y-m-d', 'd/m/Y');
        return null;
    }

    public function getReleaseDateAttribute($value)
    {
        if (!empty($value) && $value != '0000-00-00')
            return dateToSql($value, 'Y-m-d', 'd/m/Y');
        return null;
    }

    public function getImageAttribute($value)
    {
        if (!empty($value))
            return json_decode($value, true);
        return null;
    }
}