<div class="ibox">
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group{!! $errors->first('image') ? ' has-error' : '' !!}">
                    <label for="file">
                        Imagem <strong class="text-danger">*</strong>
                    </label>
                    @if (isset($edit) && !is_null($edit->image))
                        <br />
                        <img src="{!! url('/assets/popup/'.$edit->image['w']) !!}" alt=""/>
                        {!! Form::file('image', ['class' => 'form-control']) !!}
                    @else
                        {!! Form::file('image', ['class' => 'form-control', 'required']) !!}
                    @endif
                    {!! $errors->first('image', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                    <label for="name">
                        Nome <strong class="text-danger">*</strong>
                    </label>
                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nome', 'required']) !!}
                    {!! $errors->first('name', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('url') ? ' has-error' : '' !!}">
                    <label for="name">
                        URl <strong class="text-danger">*</strong>
                    </label>
                    {!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'URl', 'required']) !!}
                    {!! $errors->first('url', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('release_date') ? ' has-error' : '' !!}">
                    <label for="release_date">
                        Data de lançamento <strong class="text-success">(Opcional)</strong>
                    </label>
                    {!! Form::text('release_date', null, ['class' => 'form-control date-mask s-datepicker', 'placeholder' => 'Data de lançamento']) !!}
                    {!! $errors->first('release_date', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group{!! $errors->first('closed_date') ? ' has-error' : '' !!}">
                    <label for="closed_date">
                        Data de encerramento <strong class="text-success">(Opcional)</strong>
                    </label>
                    {!! Form::text('closed_date', null, ['class' => 'form-control date-mask s-datepicker', 'placeholder' => 'Data de encerramento']) !!}
                    {!! $errors->first('closed_date', '<span class="text-danger">:message</span>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

<hr/>

<div class="ibox">
    <div class="ibox-content">
        <div class="row">
            <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-check"></i> Salvar
                </button>
            </div>
        </div>
    </div>
</div>