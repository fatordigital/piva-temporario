@extends('layouts.cms')

@section('title')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Novo Popup</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
                </li>
                <li>
                    <a href="{!! route('fatorcms.course.index') !!}">Popup's</a>
                </li>
                <li class="active">
                    <strong>Novo Curso</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-12">
            <p>
                Todos os campos com <strong class="text-danger">(*)</strong> são obrigatórios.
            </p>
        </div>
    </div>

    <div class="row page-heading white-bg">
        <div class="col-lg-9">
            <br/>
            <a href="{!! route('fatorcms.popup.index') !!}"
               class="btn btn-warning">
                <i class="fa fa-arrow-left"></i> Voltar
            </a>
        </div>
    </div>
@endsection

@section('content')
    {!! Form::open(['route' => 'fatorcms.popup.store', 'files' => true]) !!}
    @include('popup::form')
    {!! Form::close() !!}
@endsection