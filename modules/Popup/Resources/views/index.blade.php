@extends('layouts.cms')

@section('title')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Popup</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Popup</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="row page-heading white-bg">
        <div class="col-lg-9">
            <br/>
            <a href="{!! URL::route('fatorcms.popup.create') !!}" class="btn btn-success">
                <i class="fa fa-plus"></i> Nova Pop UP
            </a>
        </div>
    </div>
@endsection

@section('content')

    <div class="col-lg-12">
        <div class="ibox-content">
            <table class="table table-hover table-stripped">
                <thead>
                <tr>
                    <td>#Cód</td>
                    <td>Nome</td>
                    <td>URl</td>
                    <td>Data de lançamento</td>
                    <td>Data de encerramento</td>
                    <td>#</td>
                </tr>
                </thead>
                <tbody>
                @foreach($rows as $row)
                    <tr>
                        <td>{!! $row->id !!}</td>
                        <td>{!! $row->name !!}</td>
                        <td>{!! $row->url !!}</td>
                        <td>{!! $row->release_date !!}</td>
                        <td>{!! $row->closed_date !!}</td>
                        <td>
                            <a href="{!! route('fatorcms.popup.edit', $row->id) !!}" class="btn btn-xs btn-primary margin-right pull-left">
                                <i class="fa fa-pencil"></i>
                            </a>
                            {!! Form::open(['route' => ['fatorcms.popup.destroy', $row->id], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deleta o popup: '. $row->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                            <button data-toggle="tooltip" title="Excluir curso" class="btn no-rounded btn-danger btn-xs">
                                <i class="fa fa-trash-o"></i>
                            </button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@stop