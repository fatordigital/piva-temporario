<?php

Route::group([
    'prefix' => 'fatorcms',
    'middleware' => 'cms',
    'namespace' => 'Modules\Popup\Http\Controllers'
], function () {
    Route::resource('popup', 'PopupController');
});