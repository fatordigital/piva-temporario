<?php namespace Modules\Popup\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Illuminate\Support\Facades\File;
use Modules\Popup\Entities\Popup;
use Modules\Popup\Http\Requests\PopupRequest;

class PopupController extends BaseModuleController
{

    public function index(Popup $popup)
    {
        $rows = $popup->paginate(20);
        return view('popup::index', compact('rows'));
    }

    public function create()
    {
        return view('popup::create');
    }

    public function store(PopupRequest $popupRequest, Popup $popup)
    {
        $popupRequest->merge(['slug' => $popupRequest->get('name')]);
        if (!$popup->create($popupRequest->all())) {
            return redirect()->back()
                ->with('error', 'Ops, algo aconteceu no servidor, não foi possível salvar o Popup, tente novamente.');
        } else {
            return redirect()->back()
                ->with('success', 'Popup ' . $popupRequest->get('name') . ', foi criado com sucesso.');
        }
    }

    public function edit($id, Popup $popup)
    {
        $edit = $popup->find($id);
        if (!$edit)
            return redirect()->route('fatorcms.popup.index')->with('error', 'Registro não encontrado.');
        return view('popup::edit', compact('edit'));
    }

    public function update($id, PopupRequest $popupRequest)
    {
        $popup = new Popup();
        $edit = $popup->find($id);
        if (!$edit)
            return redirect()->route('fatorcms.popup.index')->with('error', 'Registro não encontrado.');
        if ($popupRequest->hasFile('image')) {
            foreach ($edit->image as $item) {
                if (File::exists(public_path('/assets/popup/' . $item))) {
                    File::delete(public_path('/assets/popup/' . $item));
                }
            }
        }
        if (!$edit->update($popupRequest->all())) {
            return redirect()->back()
                ->with('error', 'Ops, algo aconteceu no servidor, não foi possível salvar o Popup, tente novamente.');
        } else {
            return redirect()->back()
                ->with('success', 'Popup ' . $popupRequest->get('name') . ', foi alterado com sucesso.');
        }
    }

    public function destroy($id, Popup $popup)
    {
        $destroy = $popup->find($id);
        if (!$destroy)
            return redirect()->route('fatorcms.popup.index')->with('error', 'Registro não encontrado.');

        foreach ($destroy->image as $item) {
            if (File::exists(public_path('/assets/popup/' . $item))) {
                File::delete(public_path('/assets/popup/' . $item));
            }
        }

        if (!$destroy->delete()) {
            return redirect()->route('fatorcms.popup.index')->with('error', 'Ops, algo aconteceu no servidor, não foi possível deletar o Popup, tente novamente.');
        } else {
            return redirect()->route('fatorcms.popup.index')->with('success', 'Popup deletada com sucesso.');
        }
    }

}