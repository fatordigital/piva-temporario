<?php namespace Modules\Popup\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PopupRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [
            'name' => 'required',
            'url' => 'required'
        ];
        if ($this->method() != 'PATCH')
            $return['image'] = 'required';
        return $return;
    }

    public function messages()
    {
        return [
            'name.required' => 'Preencha o campo nome, para futuramente ser identificado. Exemplo: Popup 01/Abril/2017',
            'url.required' => 'Preencha o campo de URl para que o usuário seja direcionado',
            'file.required' => 'Selecione uma imagem, seria ótimo ter uma largura maior que 800 pixels.'
        ];
    }

}
