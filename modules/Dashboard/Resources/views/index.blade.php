@extends('layouts.cms')

@section('elements')
@endsection

@section('content')

    @if (auth()->user()->isAdmin() || auth()->user()->group_id == 2)

    @if ($abandoned->count())
      <div class="col-lg-3">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <span class="label label-success pull-right">Total</span>
                  <h5>Abandonado</h5>
              </div>
              <div class="ibox-content">
                  <h1 class="no-margins">R$ {!! bcoToCoin($abandoned->sum('total')) !!}</h1>
                  <div class="stat-percent font-bold text-success">{!! $abandoned->count() !!} <i class="fa fa-bolt"></i></div>
                  <small>Total abandonado</small>
              </div>
          </div>
      </div>
    @endif

    @if ($order->count())
      <div class="col-lg-3">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <span class="label label-info pull-right">Total/Mês</span>
                  <h5>Finalizados</h5>
              </div>
              <div class="ibox-content">
                  <h1 class="no-margins">R$ {!! bcoToCoin($order->sum('net_amount')) !!}</h1>
                  <div class="stat-percent font-bold text-info">{!! $order->count() !!} <i class="fa fa-level-up"></i></div>
                  <small>Total finalizados</small>
              </div>
          </div>
      </div>
    @endif

    @if ($order->count())
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-danger pull-right">Total Taxas</span>
                    <h5>Total Taxas</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">R$ {!! bcoToCoin($order->sum('fee_amount')) !!}</h1>
                    <div class="stat-percent font-bold text-info">{!! $order->count() !!} <i class="fa fa-level-up"></i></div>
                    <small>Total Taxas PagSeguro</small>
                </div>
            </div>
        </div>
    @endif

    @if ($reports->count())
      <div class="col-lg-4">
          <div class="ibox float-e-margins">
              <div class="ibox-title">
                  <h5>Questões Reportadas</h5>
              </div>
              <div class="ibox-content">
                  <div>
                      <div class="feed-activity-list">
                          @foreach($reports as $report)
                            <div class="feed-element">
                                <a href="{!! route('fatorcms.user.edit', $report->user_id) !!}" class="pull-left">
                                    <img alt="image" class="img-circle" src="{!! url('img/profile.jpg') !!}">
                                </a>
                                <div class="media-body ">
                                    <strong>{!! $report->user->name !!}</strong> reportou um erro na questão : <strong>{!! $report->question->name !!}</strong> <br>
                                    <small class="text-muted">{!! $report->Created !!}</small>
                                    <div class="well">
                                        {!! $report->report !!}
                                    </div>
                                    <div class="pull-right">
                                        <a href="{!! route('fatorcms.question.edit', [$report->question_id, 'ok' => $report->user_id]) !!}" data-title="title"
                                           title="Ao clicar, você será direcionado a questão e automaticamente será considerado ajustado." class="btn btn-xs btn-white">
                                          <i class="fa fa-check"></i> Revisar
                                        </a>
                                    </div>
                                </div>
                            </div>
                          @endforeach
                      </div>
                  </div>
              </div>
          </div>
      </div>
    @endif


    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Últimos pedidos</h5>
        </div>
        <div class="ibox-content table-responsive">
            <table id="commerceTable"
                   class="table table-bordered">
                <thead>
                <tr>
                    <th data-ref="id">{!! trans('dictionary.order_id') !!}</th>
                    <th data-ref="user_id">{!! trans('dictionary.user') !!}</th>
                    <th data-ref="coupon">Status</th>
                    <th data-ref="discount">{!! trans('dictionary.discount') !!}</th>
                    <th data-ref="sub_total">{!! trans('dictionary.sub_total') !!}</th>
                    <th data-ref="total">{!! trans('dictionary.total') !!}</th>
                    <th data-ref="notification_email">{!! trans('dictionary.notified') !!}</th>
                    <th data-ref="created_at">{!! trans('dictionary.created_at') !!}</th>
                    <th data-ref="updated_at">{!! trans('dictionary.updated_at') !!}</th>
                    <th width="11%">{!! trans('dictionary.options') !!}</th>
                </tr>
                </thead>
                <tbody>
                @if ($last_orders->count())
                    @foreach($last_orders as $row)
                        <tr>
                            <td>{!! $row->id !!}</td>
                            <td>{!! (!is_null($row->user)) ? $row->user->name : 'USUÁRIO NÃO EXISTENTE' !!}</td>
                            <td>{!! (!is_null($row->orderStatus)) ? $row->orderStatus->name : 'DADOS INDISPONÍVEIS' !!}</td>
                            <td>{!! $row->discount !!}</td>
                            <td>{!! $row->sub_total !!}</td>
                            <td>{!! $row->total !!}</td>
                            <td>{!! $row->notification_email !!}</td>
                            <td>{!! \Carbon\Carbon::parse($row->created_at)->format('d/m/Y H:i:s') !!}</td>
                            <td>{!! \Carbon\Carbon::parse($row->updated_at)->format('d/m/Y H:i:s') !!}</td>
                            <td>
                                <a href="{!! URL::route('fatorcms.order.show', $row->id) !!}" class="btn btn-xs btn-success" title="{!! trans('dictionary.view') !!}">
                                    <i class="fa fa-align-justify"></i>
                                </a>
                            </td>
                        </tr>
                        @if ($row->OrderItems)
                            <tr class="bg-danger">
                                <td colspan="5">Curso</td>
                                <td colspan="4">Valor</td>
                                <td>Quantidade</td>
                            </tr>
                            @foreach($row->OrderItems as $item)
                                <tr class="bg-primary">
                                    <td colspan="5">{!! $item->name !!}</td>
                                    <td colspan="4">{!! $item->value !!}</td>
                                    <td>{!! $item->quantity !!}</td>
                                </tr>
                            @endforeach
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
      </div>
    </div>


    <div class="col-lg-12">
      <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5><i class="fa fa-exclamation-circle" data-title="title" title="Tente reconquistar o usuário"></i> Pedidos abandonados</h5>
        </div>
        <div class="ibox-content table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                  <th data-ref="id">{!! trans('dictionary.order_id') !!}</th>
                  <th data-ref="user_id">{!! trans('dictionary.user') !!}</th>
                  <th data-ref="user_email">E-mail</th>
                  <th data-ref="discount">{!! trans('dictionary.discount') !!}</th>
                  <th data-ref="sub_total">{!! trans('dictionary.sub_total') !!}</th>
                  <th data-ref="total">{!! trans('dictionary.total') !!}</th>
                  <th data-ref="created_at">{!! trans('dictionary.created_at') !!}</th>
                  <th data-ref="updated_at">{!! trans('dictionary.updated_at') !!}</th>
              </tr>
            </thead>
            <tbody>
              @if ($abandoned->count())
                @foreach($abandoned as $row)
                  <tr>
                    <td>#{!! $row->id !!}</td>
                    <td>{!! (!is_null($row->user)) ? $row->user->name : 'USUÁRIO NÃO CADASTRADO' !!}</td>
                    <td>{!! (!is_null($row->user)) ? $row->user->email : 'USUÁRIO NÃO CADASTRADO' !!}</td>
                    <td>R$ {!! bcoToCoin($row->discount) !!}</td>
                    <td>R$ {!! bcoToCoin($row->sub_total) !!}</td>
                    <td>R$ {!! bcoToCoin($row->total) !!}</td>
                    <td>{!! $row->Created !!}</td>
                    <td>{!! $row->Updated !!}</td>
                    <td></td>
                  </tr>
                @endforeach
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
    @endif

@endsection