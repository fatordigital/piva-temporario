<?php

    Route::group([
        'prefix'     => 'fatorcms',
        'middleware' => [
            'auth',
            'acl',
            'cms'
        ],
        'namespace'  => 'Modules\Dashboard\Http\Controllers'
    ], function () {
        Route::get('/', [
            'as'   => 'dashboard',
            'uses' => 'DashboardController@index'
        ]);
        Route::get(trans('routes.dashboard'), [
            'as'   => 'fatorcms.dashboard',
            'uses' => 'DashboardController@index'
        ]);
    });