<?php namespace Modules\Dashboard\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Modules\News\Entities\NewCategory;
use Modules\News\Entities\News;
use Modules\Order\Entities\AbandonedOrder;
use Modules\Order\Entities\Order;
use Modules\Test\Entities\QuestionReport;
use Modules\User\Entities\UserTest;

class DashboardController extends BaseModuleController
{


    public function loadAbandoned()
    {
        view()->share(['abandoned' => AbandonedOrder::whereFinished(false)->orderBy('created_at', 'desc')->get()]);
    }

    public function loadOrder($paginate = false)
    {
        if ($paginate) {
            $order = Order::whereIn('pagseguro_status', array(3, 4))
                ->whereMonth('created_at', '=', date('m'))
                ->whereYear('created_at', '=', date('Y'))
                ->get();
            view()->share(['order' => $order]);
        } else {
            $last_orders = Order::where('created_at', '=', Carbon::now()->startOfMonth())->orderBy('created_at', 'desc')->paginate(5);
            view()->share(['last_orders' => $last_orders]);
        }
    }

    public function loadEndTest()
    {
        view()->share(['end_tests' => UserTest::whereNotNull('end_test')->orderBy('created_at', 'desc')->get()]);
    }

    public function loadTestReports()
    {
        view()->share(['reports' => QuestionReport::whereOk(false)->orderBy('created_at', 'desc')->get()]);
    }

    public function index()
    {
        $this->loadOrder();
        $this->loadOrder(true);
        $this->loadAbandoned();
        $this->loadEndTest();
        $this->loadTestReports();
        return view('dashboard::index', [
            'last_orders' => Order::findWithSearch(request())->paginate(5)
        ]);
    }

}
