<?php namespace Modules\User\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserQuestionChoose extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["user_test_id", "user_id", "test_id", "question_id", "answer_id", "dissertative", "time"];

    public function answer()
    {
        return $this->belongsTo('Modules\Test\Entities\Answer', 'answer_id');
    }

}