<?php namespace Modules\User\Entities;

use App\BaseModel;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamUser;


class User extends BaseModel implements AuthenticatableContract, AuthorizableContract
{

    use Authenticatable, Authorizable;

    protected $fillable = [
        'partner', 'confirm', 'discipline_id', 'group_id',
        'name', 'email', 'status', 'password', 'description',
        'filename', 'remember_token', 'notifications',
        'created_at', 'updated_at', 'slug'
    ];

    public $excel = [
        'Nome', 'E-mail', 'Status', 'Data de registro', 'Data de alteração no regsitro'
    ];

    public $excelModel = [
        'name', 'email', 'status', 'created_at', 'updated_at'
    ];

    public function getNameEmailAttribute()
    {
        return $this->name . ' - ' . $this->email;
    }

    public function socialize()
    {
        return $this->hasOne('Modules\User\Entities\UserSocialize', 'user_id');
    }

    public function user_socializes()
    {
        return $this->hasOne('Modules\User\Entities\UserSocialize', 'user_id');
    }

    public function group()
    {
        return $this->belongsTo('Modules\Acl\Entities\Group', 'group_id');
    }

    public function user_informations()
    {
        return $this->hasOne('Modules\User\Entities\UserInformation', 'user_id');
    }

    public function setPasswordAttribute($value)
    {
        if (!empty($value))
            $this->attributes['password'] = bcrypt($value);
        else {
            unset($this->attributes['password']);
        }
    }

    public static function getPointsByModule($module_id, $user_id = false, $team_id = false)
    {
        $sections = Section::whereModuleId($module_id)->select('id')->get()->toArray();
        $sections_ids = [];
        foreach ($sections as $section) {
            array_push($sections_ids, $section['id']);
        }
        if (count($sections_ids) > 0) {
            $tests = Test::whereIn('section_id', $sections_ids)->select('id', 'simulated')->get()->toArray();
            $tests_ids = [];
            foreach ($tests as $test) {
                if ($test['simulated'] == 0) {
                    array_push($tests_ids, $test['id']);
                }
            }
            if (count($tests_ids) > 0) {
                $points = UserTestPath::whereUserId($user_id ? $user_id : Auth::user()->id)->whereIn('test_id', $tests_ids);
                if ($team_id) {
                    $points = $points->whereTeamId($team_id);
                }
                $points = $points->sum('points');
                if ($points) {
                    return $points;
                } else {
                    return 0;
                }
            }
        }
        return 0;
    }

    public static function getPointsByModuleAndTeam($team_id, $module_id, $user_id)
    {
        $qTeam = Team::whereModuleId($module_id)->select(['id', 'name', 'course_id', 'module_id'])->find($team_id);
        if ($qTeam) {
            $qTeamUser = TeamUser::whereTeamId($qTeam->id)->whereUserId($user_id)->first();
            if ($qTeamUser) {
                return static::getPointsByModule($qTeam->module_id, $user_id, $team_id);
            } else {
                return 'Usuário não encotrado na turma: ' . $qTeam->name;
            }
        } else {
            return 'Turma não encontrada.';
        }
    }

    public static function updateOrNot($request)
    {
        $socialize = UserSocialize::whereUserId(Auth::user()->id)->first();
        if (!$socialize) {
            $request->merge(['user_id' => Auth::user()->id, 'route_number' => $request->get('address_number')]);
            UserSocialize::create($request->only([
                'user_id',
                'route_number',
                'birth_date',
                'national_id',
                'phone',
                'postal_code',
                'district',
                'route',
                'state',
                'city',
                'country'
            ]));
        } else {
            $socialize->update($request->only([
                'route_number',
                'birth_date',
                'national_id',
                'phone',
                'postal_code',
                'district',
                'route',
                'state',
                'city',
                'country'
            ]));
        }
    }

    /**
     * @param bool $user_id
     */
    public static function setNotifications($user_id = false)
    {
        if (!$user_id) {
            $users = parent::all();
            foreach ($users as $user) {
                $user->notifications = $user->notifications + 1;
                $user->save();
            }
        } else {
            $user = parent::find($user_id);
            $user->notifications = $user->notifications + 1;
            $user->save();
        }
    }

    public static function getNotifications($user_id)
    {
        $user = parent::find($user_id);
        if ($user)
            return $user->notifications;
        return 0;
    }


    public static function updatePhoto($id, $filename)
    {
        $user = self::find($id);
        if ($user) {
            if (File::exists('assets/users/' . $user->filename)) {
                File::delete('assets/users/' . $user->filename);
            }
            return $user->update(['filename' => $filename]);
        } else {
            return false;
        }
    }

    public function customFastRegister($request, $public = '5')
    {
        $request->merge(['group_id' => $public, 'password' => bcrypt($request->get('password'))]);
        $exists = $this->whereEmail($request->get('email'))->first();
        if ($exists) {
            if (auth()->attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {
//                Auth::login($exists);
                return [
                    'status' => false,
                    'msg' => 'Oi ' . $request->get('name') . ', você já está registrado em nosso sistema.',
                    'redirectUrl' => route('front.home')
                ];
            } else {
                return [
                    'status' => false,
                    'msg' => 'Desculpe, mas, seu usuário já está cadastrado.'
                ];
            }
        } else {
            if ($store = $this->create($request->except(['_token', 'null', 'captcha', 'r_password']))) {
                Auth::login($store);
                return [
                    'status' => false,
                    'msg' => 'Obrigado ' . $request->get('name') . ', por se registrar. Você tem acesso aos nossos arquivos e cursos grátis, basta acessar a aba no topo do site.',
                    'redirectUrl' => route('front.home')
                ];
            } else {
                return [
                    'error' => true,
                    'msg' => 'Ops, um erro inesperado aconteceu. Por favor, atualize a página e preencha o formulário novamente.'
                ];
            }
        }
    }

    public function log()
    {
        return $this->hasMany('Modules\User\Entities\UserContent', 'user_id');
    }

}