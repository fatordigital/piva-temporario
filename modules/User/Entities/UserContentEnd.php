<?php namespace Modules\User\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserContentEnd extends BaseModel
{

    use SoftDeletes;

    protected $fillable = [
        'course_id', 'lesson_id', 'team_id',
        'user_id', 'content_id', 'time', 'status'
    ];

}