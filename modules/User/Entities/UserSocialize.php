<?php namespace Modules\User\Entities;

use App\BaseModel;

class UserSocialize extends BaseModel
{

    protected $primaryKey = 'user_id';

    protected $fillable = [
        'user_id', 'birth_date', 'national_id',
        'phone', 'postal_code', 'district', 'route',
        'route_number', 'state', 'city',
        'country'
    ];

    private static $not = ['user_id', 'id'];

    public static function getField($values)
    {
        if ($values == null) {
            return self::defaultParams();
        } else {
            $return = [];
            foreach ($values as $item => $value) {
                if (!in_array($item, self::$not)) {
                    if (empty($value)) {
                        $return[$item] = self::defaultParams($item);
                    }
                }
            }
            return $return;
        }
    }

    public function validateUserDataToFinish($user_id)
    {
        $return = true;
        $socialize = $this->where('user_id', '=', $user_id)->select($this->fillable)->first();
        if (!$socialize) {
            return false;
        } else {
            foreach ($socialize->toArray() as $field => $value) {
                if ($value == '') {
                    $return = false;
                }
            }
            return $return;
        }
    }

    private static function defaultParams($field = false)
    {
        $return = [
            'birth_date'   => [
                'label' => 'Data de nascimento <strong class="text-danger">(*)</strong>',
                'class' => 's-datepicker date-mask'
            ],
            'national_id'  => [
                'label' => 'CPF <strong class="text-danger">(*)</strong>',
                'class' => 'cpf-mask'
            ],
            'phone'        => [
                'label' => 'Telefone <strong class="text-danger">(*)</strong>',
                'class' => 'phone-mask'
            ],
            'postal_code'  => [
                'label' => 'CEP <strong class="text-danger">(*)</strong>',
                'class' => 'cep-mask'
            ],
            'district'     => [
                'label' => 'Bairro <strong class="text-danger">(*)</strong>'
            ],
            'route'        => [
                'label' => 'Logradouro <strong class="text-danger">(*)</strong>'
            ],
            'route_number' => [
                'label' => 'Número <strong class="text-danger">(*)</strong>'
            ],
            'state'        => [
                'label'  => 'Estado <strong class="text-danger">(*)</strong>',
                'select' => getStates()
            ],
            'city'         => [
                'label' => 'Cidade <strong class="text-danger">(*)</strong>'
            ]
        ];
        if ($field)
            return  $return[$field];
        return $return;
    }

}