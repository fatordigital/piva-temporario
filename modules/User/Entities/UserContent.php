<?php namespace Modules\User\Entities;

use App\BaseModel;

class UserContent extends BaseModel
{

    protected $fillable = ["user_id", "log", "content_id", "team_id", "course_id"];

    public $excel = [
        'Histórico', 'Data/Hora'
    ];

    public $excelModel = [
        'log', 'created_at'
    ];

    public function Content()
    {
        return $this->belongsTo('Modules\Lesson\Entities\Content', 'content_id');
    }

    public function Team()
    {
        return $this->belongsTo('Modules\Team\Entities\Team', 'team_id');
    }

    public function Course()
    {
        return $this->belongsTo('Modules\Course\Entities\Course', 'course_id');
    }

}