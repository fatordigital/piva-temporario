<?php namespace Modules\User\Entities;

use App\BaseModel;
use Modules\Test\Entities\Test;

class UserTest extends BaseModel
{

    protected $fillable = [
        "end_date", "dissertative", "corrects", "wrongs", "jump", "points", "user_id", "test_id", "points", "end_test"];

    protected $appends = [
        'end_seconds'
    ];

    public function Test()
    {
        return $this->belongsTo('Modules\Test\Entities\Test', 'test_id');
    }

    public function getEndSecondsAttribute()
    {
        return time_to_seconds($this->end_date);
    }

    private function verifyNotEmpty($test_id, $user_id)
    {
        $test = Test::find($test_id);
//        foreach ($test->questions as $question) {
//            $empty = UserQuestionChoose::whereUserId($user_id)
//                ->whereUserTestId(session('user_test_id'))
//                ->whereTestId($test_id)
//                ->whereQuestionId($question->id)
//                ->first();
//            if (!$empty)
//                return false;
//        }
        return true;
    }

    public function end($test_id, $user_id)
    {
        if ($this->verifyNotEmpty($test_id, $user_id)) {
            $jump = $error = $correct = $points = 0;
            $user_test = UserTest::find(session('user_test_id'));
            $test = Test::find($test_id);
            $chooses = UserQuestionChoose::with('answer')
                ->whereUserTestId(session('user_test_id'))
                ->whereTestId($test_id)->whereUserId($user_id)->get();
            if ($chooses->count()) {
                $dissertative = false;
                foreach ($chooses as $choose) {
                    if (!$choose->dissertative) {
                        if ($choose->answer->check == 1) {
                            $points++;
                            $correct++;
                        } else {
                            $error++;
                            if ($test->error_question == 1) {
                                $points = $points > 0 ? $points - 1 : 0;
                                $correct = $correct > 0 ? $correct - 1 : 0;
                            }
                            if ($test->jump_question == 1 && $choose->jump == 1) {
                                $points = $points > 0 ? $points - 1 : 0;
                                $jump++;
                            }
                        }
                    } else {
                        $dissertative = true;
                    }
                }

                if ($user_test)
                    $user_test->update(['dissertative' => $dissertative, 'corrects' => $correct, 'wrongs' => $error, 'jump' => $jump, 'points' => $points, 'end_test' => date('Y-m-d H:i:s')]);
                else {
                    UserTest::create(['dissertative' => $dissertative, 'test_id' => $test_id, 'user_id' => $user_id, 'corrects' => $correct, 'wrongs' => $error, 'jump' => $jump, 'points' => $points, 'end_test' => date('Y-m-d H:i:s')]);
                }

                session()->forget('simulate');
                return true;
            }
        } else {
            return false;
        }
    }

}