<?php

    Route::group([
        'prefix'     => 'fatorcms',
        'middleware' => [
            'auth',
            'acl',
            'cms'
        ],
        'namespace'  => 'Modules\User\Http\Controllers'
    ], function () {
        Route::resource('/user', 'UserController');
        Route::get('/professor', 'UserController@index')->name('fatorcms.user.professor');
        Route::get('/parceiros', 'UserController@index')->name('fatorcms.user.parceiros');
        Route::get('export/user/{id}/xls', ['as' => 'fatorcms.user.export.xls', 'uses' => 'UserController@exportXls']);
        Route::post('/user/data', [
            'as'   => 'fatorcms.user.data',
            'uses' => 'UserController@table'
        ]);
        Route::patch('/user/status/{id}', [
            'as'   => 'fatorcms.user.status',
            'uses' => 'UserController@status'
        ]);
        Route::post('/profile-photo', [
            'as'   => 'fatorcms.profile.save',
            'uses' => 'UserController@profilePhoto'
        ]);
        Route::get('destroy-user/{user_id}/{team_id}', ['as' => 'fatorcms.user.destroyUser', 'uses' => 'UserController@destroyUser']);
    });

    Route::group([
        'prefix'    => 'fatorcms',
        'namespace' => 'Modules\User\Http\Controllers'
    ], function () {
        Route::get('teachers.json', ['as' => 'fatorcms.teachers.json', 'uses' => 'UserController@teachers_json']);
        Route::get('teachers-team.json', ['as' => 'fatorcms.teachers-team.json', 'uses' => 'UserController@teachers_team_json']);
    });
