<?php namespace Modules\User\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Support\Facades\Input;

class UserRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $return = [
            'name'                        => 'required|max:255',
            'group_id'                    => 'required',
            'email'                       => 'required|email|max:255|unique:users,email,' . $this->getSegmentFromEnd() . ',id',
            'password'                    => 'required|confirmed|max:255',
            'filename'                    => 'mimes:png,jpg,jpeg',
            'user_socializes[national_id]' => 'unique:user_socializes,national_id'
        ];
        if ($this->method() == 'PATCH' && Input::get('password') == "") {
            unset($return['password']);
        }
        return $return;
    }

}
