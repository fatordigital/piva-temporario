<?php namespace Modules\User\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use App\Notification;
use App\NotificationUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Acl\Entities\Group;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamTeacher;
use Modules\Team\Entities\TeamUser;
use Modules\User\Entities\User;
use Modules\User\Entities\UserContent;
use Modules\User\Entities\UserSocialize;
use Modules\User\Http\Requests\UserRequest;

class UserController extends BaseModuleController
{

    private $teacher_id = 7;

    private function lists()
    {
        if (Auth::user()->isAdmin()) {
            $groups = Group::all()->lists('name', 'id')->toArray();
        } else {
            $groups = Group::whereRaw('( `parent_id` = ? OR `id` = 5 )', array(Auth::user()->group_id))->get()->lists('name', 'id')->toArray();
        }
        View::share(compact('groups'));
    }

    public function profilePhoto(Request $request)
    {
        if ($request->hasFile('file') && $request->has('id')) {
            $filename = $this->customFileName($request, 'assets/users/', 'file');
            if ($request->file('file')->move('assets/users/', $filename)) {
                if (User::updatePhoto($request->get('id'), $filename))
                    return response()->json(['msg' => 'success'], 200);
                else {
                    File::delete('assets/users/' . $filename);
                    return response()->json(['msg' => 'error'], 400);
                }
            }
        }
        return response()->json(['msg' => 'error'], 400);
    }

    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        if (Auth::user()->isAdmin()) {
            $list = User::orderBy('name');
        } else {
//            $list = User::whereIn('group_id', Auth::user()->getGroups())->orderBy('name');
            $list = User::where('group_id', '!=', 1)->orderBy('name');
        }
        if ($request->has('search')) {
            foreach ($request->get("search") as $key => $value) {
                if (!empty($value) && $key != 'type') {
                    $list->where($key, 'LIKE', '%' . $value . '%');
                } else if ($key == 'type' && !empty($value)) {
                    $list->where($value, '=', true);
                }
            }
        }

        if (request()->route()->getName() == 'fatorcms.user.professor') {
            $list->where('group_id', '=', 7);
        }

        if (request()->route()->getName() == 'fatorcms.user.parceiros') {
            $list->where('group_id', '=', 11);
        }

        if (request()->has("export")) {
            $query_export = $list->with(['socialize'])->get();
            if ($query_export) {
                $return = [];
                foreach ($query_export as $item) {
                    $return[] = [
                        'Nome' => $item->name,
                        'E-mail' => $item->email,
                        'CPF' => $item->socialize ? $item->socialize->national_id : 'Não informado',
                        'Data de nascimento' => $item->socialize ? $item->socialize->birth_date : 'Não informado',
                        'Telefone/Celular' => $item->socialize ? $item->socialize->phone : 'Não informado',
                        'Estado' => $item->socialize ? $item->socialize->state : 'Não informado',
                        'Cidade' => $item->socialize ? $item->socialize->city : 'Não informado',
                        'Endereço' => $item->socialize ? $item->socialize->route_number : 'Não informado'
                    ];
                }
                $this->export_excel($return, 'Usuários');
            }
        }

//        if (request()->has('export')) {
//            $this->exportSimpleExcel(new User(), $list, 'Usuários');
//        }

        $request->session()->flashInput($request->all());
        $list = $list->paginate(40);

        return view('user::index', compact('list'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->lists();
        return view('user::create');
    }

    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        $store = User::create($request->except(['_token', 'user_socializes']));
        if ($store) {
            UserSocialize::create($request->get('user_socializes') + ['user_id' => $store->id]);
            n('fatorcms.user.store', 'Novo Usuário', 'O usuário ' . $request->get('name') . ', foi criado recentemente.');
            return redirect()->route('fatorcms.user.index')->with('success', trans('messages.insert.success'));
        } else {
            return redirect()->route('fatorcms.user.create')->withInput($request->except(['_token']))->with('error', 'Desculpe, houve um erro inesperado ao salvar o usuário. Tente novamente.');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $edit = User::with(['user_socializes'])->find($id);
        if (!$edit)
            return redirect()->route('fatorcms.user.index')->with('error', 'Desculpe, o usuário não existe.');

        $log = UserContent::where('user_id', '=', $id)->orderBy('created_at', 'desc');
        if (request()->has('log')) {
            if (request()->has('log.begin_date')) {
                $log = $log->whereRaw('(DATE(created_at) >= DATE(?))', [dateToSql(request('log.begin_date'))]);
            }
            if (request()->has('log.end_date')) {
                $log = $log->whereRaw('(DATE(created_at) <= DATE(?))', [dateToSql(request('log.end_date'))]);
            }
            if (request()->has('log.team_id')) {
                $log = $log->where('team_id', '=', request('log.team_id'));
            }
            if (request()->has('log.course_id')) {
                $log = $log->where('course_id', '=', request('log.course_id'));
            }
            request()->session()->flashInput(request()->except(['_token']));
        }

        if (request()->has('full')) {
            if (request('full') == true) {
                $this->exportSimpleExcel(new UserContent(), UserContent::where('user_id', '=', $edit->id), 'Histórico '. $edit->name);
            } else {
                $this->exportSimpleExcel(new UserContent(), $log, 'Histórico '. $edit->name);
            }
            return redirect()->route('fatorcms.user.edit', request()->except(['_token', 'full']));
        }

        $this->lists();
        $edit->password = '';
        return view('user::edit', [
            'edit' => $edit,
            'log'  => $log->paginate(15)
        ]);
    }

    public function exportXls($id)
    {
        $edit = User::with(['user_socializes'])->find($id);
        if (!$edit)
            return redirect()->route('fatorcms.user.index')->with('error', 'Desculpe, o usuário não existe.');

        $log = UserContent::where('user_id', '=', $id)->orderBy('created_at', 'desc');
        if (request()->has('log')) {
            if (request()->has('log.begin_date')) {
                $log = $log->whereRaw('(DATE(created_at) >= DATE(?))', [dateToSql(request('log.begin_date'))]);
            }
            if (request()->has('log.end_date')) {
                $log = $log->whereRaw('(DATE(created_at) <= DATE(?))', [dateToSql(request('log.end_date'))]);
            }
            if (request()->has('log.team_id')) {
                $log = $log->where('team_id', '=', request('log.team_id'));
            }
            if (request()->has('log.course_id')) {
                $log = $log->where('course_id', '=', request('log.course_id'));
            }
            request()->session()->flashInput(request()->except(['_token']));
        }

        if (request()->has('full')) {
            if (request('full') == 'true') {
                $this->exportSimpleExcel(new UserContent(), UserContent::where('user_id', '=', $edit->id), 'Histórico '. $edit->name);
            } else {
                $this->exportSimpleExcel(new UserContent(), $log, 'Histórico '. $edit->name);
            }
        }
    }

    /**
     * @param UserRequest $request
     * @param             $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, $id)
    {
        $update = User::findOrFail($id);

        $update->update($request->except(['_token', 'user_socializes']));
        $update_user_informations = UserSocialize::where('user_id', '=', $id)->first();

        if ($update_user_informations) {
            $update_user_informations->update($request->get('user_socializes'));
        } else {
            UserSocialize::create($request->get('user_socializes') + ['user_id' => $id]);
        }

        return redirect()->route('fatorcms.user.index')->with('success', trans('messages.update.success', ['name' => $update->name]));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * Ativa ou Inativa o Usuário
     */
    public function status($id)
    {
        $status = User::findOrFail($id);
        $to = ($status->status) ? 0 : 1;
        $status->status = $to;
        $status->save();
        return redirect()->route('fatorcms.user.index')->with('success', trans('messages.status.success', [
            'status' => ($to) ? trans('dictionary.status_text.active') : trans('dictionary.status_text.inactive')
        ]));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        if (Auth::user()->id == $id) {
            return redirect()->route('fatorcms.user.index')->with('error', trans('messages.errors.logon'));
        } else {
            User::destroy($id);
            return redirect()->route('fatorcms.user.index')->with('success', trans('messages.delete.success'));
        }
    }

    public function readNotification()
    {
        if (Auth::user()->isAdmin()) {
            $return = Notification::with(['myAlerts' => function ($q) {
                return $q->whereUserId(Auth::user()->id)->whereRead(false)->get();
            }])->orderBy('created_at', 'DESC')->groupBy('route')->get();
        } else {
            $return = Notification::with(['myAlerts' => function ($q) {
                return $q->whereUserId(Auth::user()->id)->whereRead(false)->get();
            }])->whereIn('route', Auth::user()->getActions())->groupBy('route')->orderBy('created_at', 'DESC')->get();
        }
        return response()->json(['status' => 'success', 'data' => $return->toArray(), 'length' => $return->count()], 200);
    }

    public function markNotification(Request $request)
    {
        $exists = NotificationUser::whereNotificationId($request->get('id'))->whereUserId(Auth::user()->id);
        if (!$exists)
            NotificationUser::create(['notification_id' => $request->get('id'), 'user_id' => Auth::user()->id, 'read' => true]);
        $notification = Notification::firstOrFail($request->get('id'));
        return response()->json(['status' => 'success', 'data' => $notification->toArray()], 200);
    }

    public function teachers_json()
    {
        return response()->json(['data' => User::whereGroupId($this->teacher_id)->get()->toArray(), 'error' => false]);
    }

    public function teachers_team_json(Request $request)
    {
        if ($request->has('id')) {
            return response()->json(['error' => false, 'data' => TeamTeacher::with(['user'])->whereTeamId($request->get('id'))->get()->toArray()], 200);
        } else {
            return response()->json(['data' => [], 'msg' => 'Nenhum registro encontrado', 'status' => true], 200);
        }
    }

    public function destroyUser($user_id, $team_id)
    {
        $team_user = TeamUser::whereUserId($user_id)->whereTeamId($team_id)->first();
        if (!$team_user)
            return redirect()->route('fatorcms.team.show', $team_user->team->slug)->with('error', 'Não foi possível remover o usuário da turma. Tente novamente.');
        else {
            $team_user->delete();
            return redirect()->route('fatorcms.team.show', $team_user->team->slug)->with('success', 'Usuário '.$team_user->user->name.' desvinculado da turma.');
        }
    }

}