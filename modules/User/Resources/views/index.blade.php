@extends('layouts.cms')

@section('title')
<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-9">
      <h2>Usuários</h2>
      <ol class="breadcrumb">
          <li>
              <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
          </li>
          <li class="active">
              <strong>Usuários</strong>
          </li>
      </ol>
  </div>
</div>

<div class="row page-heading white-bg">
  <div class="col-lg-9">
      <br />
      <a href="{!! URL::route('fatorcms.user.create') !!}" class="btn btn-success">
          {!! trans('dictionary.create') !!} Usuário
      </a>
      <a href="{!! route('fatorcms.user.index', 'export=true') !!}" class="btn btn-primary">
        Eportar usuários
      </a>
  </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
      <div class="ibox-content table-responsive">
        {!! Form::open(['route' => 'fatorcms.user.index', 'method' => 'GET']) !!}
        <div class="row">
          <div class="col-lg-3">
            <div class="form-group">
              <label for="name">
                Nome
              </label>
              {!! Form::text('search[name]', old('search[name]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
          </div>
          @if (request()->route()->getName() != 'fatorcms.user.professor' && request()->route()->getName() != 'fatorcms.user.parceiros')
          <div class="col-lg-3">
            <div class="form-group">
              <label for="name">
                Grupo
              </label>
              {!! Form::select('search[group_id]', ['' => ''] + \Modules\Acl\Entities\Group::where('id', '!=', 1)->lists('name','id')->toArray(), old('search[group_id]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
          </div>
          @endif
          <div class="col-lg-6">
            <div class="form-group">
              <label for="name">
                E-mail
              </label>
              <div class="input-group">
                {!! Form::text('search[email]', old('search[email]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-primary">Procurar</button>
                </span>
                <span class="input-group-btn">
                  <a class="btn btn-warning" href="{!! route('fatorcms.user.index') !!}">Limpar</a>
                </span>
              </div>
            </div>
          </div>
        </div>
        {!! Form::close() !!}
        @if ($list->count())
          <table class="table table-bordered">
            <thead>
            <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Grupo</th>
                <th><i class="fa fa-phone"></i> Telefone</th>
                <th width="6%">Opções</th>
            </tr>
            </thead>
            <tbody>
              @foreach($list as $item)
                <tr>
                  <td>{!! $item->name !!}</td>
                  <td>{!! $item->email !!}</td>
                  <td>{!! $item->group ? $item->group->name : '' !!}</td>
                  <td>{!! $item->socialize ? $item->socialize->phone : '' !!}</td>
                  <td>
                    <a class="btn btn-primary no-rounded btn-xs pull-left"
                       href="{!! URL::route('fatorcms.user.edit', $item->id) !!}">
                        <i class="fa fa-pencil"></i>
                    </a>
                    <a href="mailto:{!! $item->email !!}" title="{!! $item->email !!}" class="btn no-rounded pull-left btn-xs btn-success" data-loading="false">
                      <i class="fa fa-envelope-o"></i>
                    </a>
                    {!! Form::open(['route' => ['fatorcms.user.destroy', $item->id], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deleta o usuário '. $item->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                    <button class="btn no-rounded btn-danger btn-xs">
                      <i class="fa fa-trash-o"></i>
                    </button>
                    {!! Form::close() !!}
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
          @include('layouts.elements.paginator', ['paginator' => $list])
        @endif
      </div>
    </div>
</div>
@endsection