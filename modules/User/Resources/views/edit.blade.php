@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Editando Usuário - {!! $edit->name !!}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li>
                <a href="{!! route('fatorcms.user.index') !!}">Usuários</a>
            </li>
            <li class="active">
                <strong>Editando Usuário - {!! $edit->name !!}</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <p>
        Todos os campos com <strong class="text-danger">(*)</strong> são obrigatórios.
      </p>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! route('fatorcms.user.index') !!}"
           class="btn btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
        {!! Form::open(['route' => ['fatorcms.user.destroy', $edit->id], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deleta o usuário '. $edit->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left margin-right']) !!}
        <button class="btn btn-danger">
            <i class="fa fa-trash"></i> DELETAR
        </button>
        {!! Form::close() !!}
        <a href="{!! route('fatorcms.user.export.xls', $edit->id) !!}?full=true" class="btn btn-success">
          Imprimir todo histórico deste usuário
        </a>
        @if (request()->has("log"))
        <a href="{!! route('fatorcms.user.export.xls', $edit->id) !!}?{!! request()->getQueryString() !!}&full=false" class="btn btn-primary">
          Imprimir histórico com base nos filtros
        </a>
        @endif
    </div>
  </div>
@endsection

@section('content')
  <div class="ibox float-e-margins">
    <div class="tabs-container">
       <ul class="nav nav-tabs">
         <li>
           <a data-toggle="tab" href="#tab-1">
             Dados do usuário
           </a>
         </li>
         <li class="active">
          <a data-toggle="tab" href="#tab-2">
            Histórico de acessos
          </a>
         </li>
       </ul>
    </div>
    <div class="ibox-content">
    @include('user::edit_form')
    </div>
  </div>
@endsection