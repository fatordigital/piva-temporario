<div class="col-lg-12">
  <h3><i class="fa fa-user"></i> Dados pessoais</h3>
  <hr />
</div>

<div class="col-lg-6">
    <div id="teacher" style="display: none;" class="form-group {!! ($errors->first('discipline_id')) ? 'has-error' : '' !!}">
        <label for="group_id">
            Disciplina <strong class="text-success">(Opcional)</strong>
        </label>
        {!! Form::select('discipline_id', ['' => 'Selecione a disciplina do professor'] + \Modules\Lesson\Entities\Discipline::lists('name', 'id')->toArray(), null, ['class' => 'form-control', 'id' => 'select']) !!}
        {!! $errors->first('discipline_id', '<span class="help-block">:message</span>') !!}
    </div>
    <div class="form-group {!! ($errors->first('group_id')) ? 'has-error' : '' !!}">
        <label for="group_id">
          {!! trans('dictionary.group') !!} <strong class="text-danger">(*)</strong>
        </label>
        {!! Form::select('group_id', ['' => trans('dictionary.select', ['name' => trans('dictionary.group')])] + $groups, null, ['class' => 'form-control', 'id' => 'select']) !!}
        {!! $errors->first('group_id', '<span class="help-block">:message</span>') !!}
    </div>
    <div class="form-group {!! ($errors->first('name')) ? 'has-error' : '' !!}">
        <label for="name">
          {!! trans('dictionary.name') !!} <strong class="text-danger">(*)</strong>
        </label>
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('dictionary.name'). ' (*)', 'required' => 'required']) !!}
        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
    </div>
    <div class="form-group {!! ($errors->first('email')) ? 'has-error' : '' !!}">
        <label for="email">
          E-mail <strong class="text-danger">(*)</strong>
        </label>
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail (*)', 'required' => 'required']) !!}
        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="col-lg-6">
    <div class="form-group {!! ($errors->first('password')) ? 'has-error' : '' !!}">
        <label for="attribute">
            {!! trans('dictionary.password') !!} <strong class="text-danger">(*)</strong>
        </label>
      {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('dictionary.password') . ' (*)']) !!}
      {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
    </div>

    <div class="form-group {!! ($errors->first('password_confirmation')) ? 'has-error' : '' !!}">
        <label for="attribute">
            {!! trans('dictionary.password_confirmation') !!} <strong class="text-danger">(*)</strong>
        </label>
      {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('dictionary.password_confirmation') . ' (*)']) !!}
      {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
    </div>

    <div class="form-group{!! $errors->first('user_socializes.national_id') ? ' has-error' : '' !!}">
      <label for="user_socializes.national_id">
        CPF <strong class="text-success">(Opcional)</strong>
      </label>
      {!! Form::text('user_socializes.national_id', null, ['placeholder' => 'xxx.xxx.xxx-xx', 'class' => 'form-control cpf-mask', 'maxlength' => 30]) !!}
      {!! $errors->first('user_socializes.national_id', '<span class="help-block text-danger">:message</span>') !!}
    </div>
</div>

<div class="col-lg-12">
  <hr />
  <h3><i class="fa fa-user"></i> Endereço</h3>
  <hr />
</div>

<div class="col-lg-3">
  <div class="form-group{!! $errors->first('user_socializes[postal_code]') ? ' has-error' : '' !!}">
    <label for="user_socializes[postal_code]">
      CEP <strong class="text-success">(Opcional)</strong>
    </label>
    {!! Form::text('user_socializes[postal_code]', null, ['class' => 'form-control cep-mask', 'placeholder' => 'Digite aqui o CEP do usuário']) !!}
    {!! $errors->first('user_socializes[postal_code]', '<span class="help-block text-danger">:message</span>') !!}
  </div>
</div>

<div class="col-lg-3">
  <div class="form-group{!! $errors->first('user_socializes[state]') ? ' has-error' : '' !!}">
    <label for="user_socializes[state]">
      Estado <strong class="text-success">(Opcional)</strong>
    </label>
    {!! Form::select('user_socializes[state]', getStates(), null, ['class' => 'form-control select']) !!}
    {!! $errors->first('user_socializes[state]', '<span class="help-block text-danger">:message</span>') !!}
  </div>
</div>

<div class="col-lg-3">
  <div class="form-group{!! $errors->first('user_socializes[district]') ? ' has-error' : '' !!}">
    <label for="user_socializes[district]">
      Bairro <strong class="text-success">(Opcional)</strong>
    </label>
    {!! Form::text('user_socializes[district]', null, ['class' => 'form-control', 'placeholder' => 'Bairro']) !!}
    {!! $errors->first('user_socializes[district]', '<span class="help-block text-danger">:message</span>') !!}
  </div>
</div>

<div class="col-lg-3">
  <div class="form-group{!! $errors->first('user_socializes[city]') ? ' has-error' : '' !!}">
    <label for="user_socializes[city]">
      Cidade <strong class="text-success">(Opcional)</strong>
    </label>
    {!! Form::text('user_socializes[city]', null, ['class' => 'form-control', 'placeholder' => 'Cidade']) !!}
    {!! $errors->first('user_socializes[city]', '<span class="help-block text-danger">:message</span>') !!}
  </div>
</div>

<div class="clearfix"></div>

<div class="col-lg-3">
  <div class="form-group{!! $errors->first('user_socializes[route]') ? ' has-error' : '' !!}">
    <label for="user_socializes[route]">
      Logradouro <strong class="text-success">(Opcional)</strong>
    </label>
    {!! Form::text('user_socializes[route]', null, ['class' => 'form-control', 'placeholder' => 'Logradouro']) !!}
    {!! $errors->first('user_socializes[route]', '<span class="help-block text-danger">:message</span>') !!}
  </div>
</div>

<div class="col-lg-3">
  <div class="form-group{!! $errors->first('user_socializes[number]') ? ' has-error' : '' !!}">
    <label for="user_socializes[number]">
      Número <strong class="text-success">(Opcional)</strong>
    </label>
    {!! Form::text('user_socializes[number]', null, ['class' => 'form-control', 'placeholder' => 'Exemplo: 222, Apto 12 / 33']) !!}
    {!! $errors->first('user_socializes[number]', '<span class="help-block text-danger">:message</span>') !!}
  </div>
</div>

<div class="col-lg-12 menu">
  <hr />
  <button type="submit" class="btn btn-success">
    <i class="fa fa-save"></i> Salvar Dados
  </button>
</div>

<div class="clearfix"></div>


@section('cjs')
    {!! HTML::script('/js/cms/scripts/user/index.js') !!}
@endsection
