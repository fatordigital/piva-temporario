<div class="tabs-container">
  <div class="tab-content">

    <div id="tab-1" class="tab-pane">
      {!! Form::model($edit,['route' => ['fatorcms.user.update', $edit->id], 'method' => 'PATCH', 'files' => true]) !!}
      {!! Form::hidden('id', $edit->id) !!}
      <div class="row">

        <div class="col-lg-12">
          <h3><i class="fa fa-user"></i> Dados pessoais</h3>
          <hr />
        </div>

        <div class="col-lg-6">
          <div class="form-group">
            <div style="cursor: pointer;">
                <label title="Adicionar/editar foto"
                       for="inputImage">
                    <input type="file"
                           accept="image/*"
                           name="file"
                           id="inputImage"
                           class="hide">
                  {!! HTML::image('/img/1468049205_25.Camera-Front.png', 'Adicionar/editar foto') !!}
                </label>
            </div>
            <div class="profile-image">
                @if (!$edit->filename)
                {!! HTML::image('/img/user_male4-64.png', '', ['class' => 'm-b-md', 'id' => 'foto']) !!}
              @else
                {!! HTML::image('/assets/users/'.$edit->filename, '', ['class' => 'm-b-md', 'id' => 'foto']) !!}
              @endif
            </div>
          </div>
        </div>

        <div class="clearfix"></div>

        <hr />

        <div class="col-lg-6">
            {{--<div class="form-group">--}}
                {{--<label for="status" class="control-label">Parceiro</label>--}}
                {{--<br/>--}}
                {{--<div class="radio radio-success radio-inline">--}}
                    {{--{!! Form::radio('partner', 1, true) !!}--}}
                  {{--<label for="radio4">--}}
                        {{--{!! trans('dictionary.status_text.active') !!}--}}
                    {{--</label>--}}
                {{--</div>--}}
                {{--<div class="radio radio-danger radio-inline">--}}
                    {{--{!! Form::radio('partner', 0, false) !!}--}}
                  {{--<label for="radio4">--}}
                        {{--{!! trans('dictionary.status_text.inactive') !!}--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group {!! ($errors->first('group_id')) ? 'has-error' : '' !!}">
                <label for="group_id">
                  {!! trans('dictionary.group') !!} <strong class="text-danger">(*)</strong>
                </label>
              {!! Form::select('group_id', ['' => trans('dictionary.select', ['name' => trans('dictionary.group')])] + $groups, null, ['class' => 'form-control', 'id' => 'select']) !!}
              {!! $errors->first('group_id', '<span class="help-block">:message</span>') !!}
            </div>
            <div class="form-group {!! ($errors->first('name')) ? 'has-error' : '' !!}">
                <label for="name">
                  {!! trans('dictionary.name') !!} <strong class="text-danger">(*)</strong>
                </label>
              {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('dictionary.name'). ' (*)', 'required' => 'required']) !!}
              {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
            </div>
            <div class="form-group {!! ($errors->first('email')) ? 'has-error' : '' !!}">
                <label for="email">
                  E-mail <strong class="text-danger">(*)</strong>
                </label>
              {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail (*)', 'required' => 'required']) !!}
              {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group {!! ($errors->first('password')) ? 'has-error' : '' !!}">
                <label for="attribute">
                    {!! trans('dictionary.password') !!} <strong class="text-success">(Opcional)</strong>
                </label>
              {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('dictionary.password')]) !!}
              {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group {!! ($errors->first('password_confirmation')) ? 'has-error' : '' !!}">
                <label for="attribute">
                    {!! trans('dictionary.password_confirmation') !!} <strong class="text-success">(Opcional)</strong>
                </label>
              {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => trans('dictionary.password_confirmation')]) !!}
              {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group{!! $errors->first('user_socializes.national_id') ? ' has-error' : '' !!}">
              <label for="user_socializes.national_id">
                CPF <strong class="text-success">(Opcional)</strong>
              </label>
              {!! Form::text('user_socializes.national_id', isset($edit->user_socializes) ? $edit->user_socializes->national_id : null, ['placeholder' => 'xxx.xxx.xxx-xx', 'class' => 'form-control cpf-mask', 'maxlength' => 30]) !!}
              {!! $errors->first('user_socializes.national_id', '<span class="help-block text-danger">:message</span>') !!}
            </div>
        </div>

        <div class="col-lg-12">
          <hr />
          <h3><i class="fa fa-user"></i> Endereço</h3>
          <hr />
        </div>

        <div class="col-lg-3">
          <div class="form-group{!! $errors->first('user_socializes[postal_code]') ? ' has-error' : '' !!}">
            <label for="user_socializes[postal_code]">
              CEP <strong class="text-success">(Opcional)</strong>
            </label>
            {!! Form::text('user_socializes[postal_code]', null, ['class' => 'form-control cep-mask', 'placeholder' => 'Digite aqui o CEP do usuário']) !!}
            {!! $errors->first('user_socializes[postal_code]', '<span class="help-block text-danger">:message</span>') !!}
          </div>
        </div>

        <div class="col-lg-3">
          <div class="form-group{!! $errors->first('user_socializes[state]') ? ' has-error' : '' !!}">
            <label for="user_socializes[state]">
              Estado <strong class="text-success">(Opcional)</strong>
            </label>
            {!! Form::select('user_socializes[state]', getStates(), null, ['class' => 'form-control selectize']) !!}
            {!! $errors->first('user_socializes[state]', '<span class="help-block text-danger">:message</span>') !!}
          </div>
        </div>

        <div class="col-lg-3">
          <div class="form-group{!! $errors->first('user_socializes[district]') ? ' has-error' : '' !!}">
            <label for="user_socializes[district]">
              Bairro <strong class="text-success">(Opcional)</strong>
            </label>
            {!! Form::text('user_socializes[district]', null, ['class' => 'form-control', 'placeholder' => 'Bairro']) !!}
            {!! $errors->first('user_socializes[district]', '<span class="help-block text-danger">:message</span>') !!}
          </div>
        </div>

        <div class="col-lg-3">
          <div class="form-group{!! $errors->first('user_socializes[city]') ? ' has-error' : '' !!}">
            <label for="user_socializes[city]">
              Cidade <strong class="text-success">(Opcional)</strong>
            </label>
            {!! Form::text('user_socializes[city]', null, ['class' => 'form-control', 'placeholder' => 'Cidade']) !!}
            {!! $errors->first('user_socializes[city]', '<span class="help-block text-danger">:message</span>') !!}
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-lg-3">
          <div class="form-group{!! $errors->first('user_socializes[route]') ? ' has-error' : '' !!}">
            <label for="user_socializes[route]">
              Logradouro <strong class="text-success">(Opcional)</strong>
            </label>
            {!! Form::text('user_socializes[route]', null, ['class' => 'form-control', 'placeholder' => 'Logradouro']) !!}
            {!! $errors->first('user_socializes[route]', '<span class="help-block text-danger">:message</span>') !!}
          </div>
        </div>

        <div class="col-lg-3">
          <div class="form-group{!! $errors->first('user_socializes[number]') ? ' has-error' : '' !!}">
            <label for="user_socializes[number]">
              Número <strong class="text-success">(Opcional)</strong>
            </label>
            {!! Form::text('user_socializes[number]', null, ['class' => 'form-control', 'placeholder' => 'Exemplo: 222, Apto 12 / 33']) !!}
            {!! $errors->first('user_socializes[number]', '<span class="help-block text-danger">:message</span>') !!}
          </div>
        </div>
      </div>

          <hr />

        @if (isset($edit) && $edit->group_id == 7)
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="description">
                            Descrição <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::textarea('description', null, ['class' => 'form-control summernote']) !!}
                    </div>
                </div>
            </div>
        @endif

          <hr />

          <div class="row">
              <div class="col-lg-12">
                  <div class="form-group">
                      <button type="submit" class="btn btn-success">
                          <i class="fa fa-save"></i> Salvar dados
                      </button>
                  </div>
              </div>
          </div>
      {!! Form::close() !!}
    </div>



    <div id="tab-2" class="tab-pane active">
      <div class="row table-responsive">

        <div class="col-lg-12">
          {!! Form::open(['route' => ['fatorcms.user.edit', $edit->id], 'method' => 'GET']) !!}
          <div class="row">
            <div class="col-lg-3">
              <label for="team_id">
                Turma
              </label>
              <div class="form-group">
                {!! Form::select('log[team_id]', ['' => 'Selecione a Turma'] + \Modules\Team\Entities\Team::whereIn('id', \Modules\User\Entities\UserContent::where('user_id', '=', $edit->id)->lists('team_id')->toArray())->lists('name', 'id')->toArray(), null,
                ['class' => 'form-control']) !!}
              </div>
            </div>
            <div class="col-lg-3">
              <label for="course_id">
                Turma
              </label>
              <div class="form-group">
                {!! Form::select('log[course_id]', ['' => 'Selecione o Curso'] + \Modules\Course\Entities\Course::whereIn('id', \Modules\User\Entities\UserContent::where('user_id', '=', $edit->id)->lists('course_id')->toArray())->lists('name', 'id')->toArray(), null,
                ['class' => 'form-control']) !!}
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="name">
                  Data de início
                </label>
                {!! Form::text('log[begin_date]', old('log[begin_date]'), ['class' => 'form-control s-datepicker', 'autocomplete' => 'off']) !!}
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="name">
                  Data final
                </label>
                <div class="input-group">
                  {!! Form::text('log[end_date]', old('log[end_date]'), ['class' => 'form-control s-datepicker', 'autocomplete' => 'off']) !!}
                  <span class="input-group-btn">
                    <button type="submit" class="btn no-rounded btn-primary">
                      <i class="fa fa-search"></i> Procurar
                    </button>
                  </span>
                  <span class="input-group-btn">
                    <a class="btn no-rounded btn-warning" href="{!! route('fatorcms.user.edit', $edit->id) !!}">
                      <i class="fa fa-eraser"></i> Limpar
                    </a>
                  </span>
                </div>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
        </div>

        <div class="col-lg-12">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td>Turma</td>
                <td>Conteúdo</td>
                <td>Histórico</td>
                <td>Curso</td>
                <td>Data/Hora</td>
              </tr>
            </thead>
            @if ($log->count())
              @foreach($log as $item)
                <tr>
                  <td>{!! $item->Team ? $item->Team->name : '' !!}</td>
                  <td>{!! $item->Content ? $item->Content->name : '' !!}</td>
                  <td>{!! $item->log !!}</td>
                  <td>{!! $item->Course ? $item->Course->name : '' !!}</td>
                  <td>{!! $item->Created !!}</td>
                </tr>
              @endforeach
            @endif
          </table>
          {!! $log->appends(Input::all())->render() !!}
        </div>
      </div>
    </div>

  </div>
</div>

@section('cjs')
  {!! HTML::script('js/cms/scripts/user/index.js') !!}
@endsection