@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Novo Usuário</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li>
                <a href="{!! route('fatorcms.user.index') !!}">Usuários</a>
            </li>
            <li class="active">
                <strong>Novo Usuário</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <p>
        Todos os campos com <strong class="text-danger">(*)</strong> são obrigatórios.
      </p>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.user.index') !!}"
           class="btn btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="ibox float-e-margins">
    <div class="ibox-content">
      {!! Form::open(['route' => 'fatorcms.user.store', 'files' => true]) !!}
        @include('user::create_form')
      {!! Form::close() !!}
    </div>
  </div>
@endsection