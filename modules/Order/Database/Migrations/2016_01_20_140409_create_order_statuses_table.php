<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateOrderStatusesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('order_statuses', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('debit_stock')->nullable();
                $table->string('credit_stock')->nullable();
                $table->boolean('notification_email')->default(0);
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('order_status');
        }

    }
