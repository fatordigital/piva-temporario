<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateAbandonedOrderItemsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('abandoned_order_items', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('order_id');
                $table->string('code');
                $table->decimal('value', 10, 2);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('abandoned_order_items');
        }

    }
