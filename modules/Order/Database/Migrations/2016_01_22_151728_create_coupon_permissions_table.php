<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateCouponPermissionsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('coupon_permissions', function (Blueprint $table) {
                $table->integer('coupon_id')->unsigned();
                $table->integer('user_id')->nullable();
                $table->string('email', 60)->nullable();
                $table->boolean('used')->default(0);
                $table->foreign('coupon_id')->references('id')->on('coupons')->onDeleteCascade();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('coupon_permissions');
        }

    }
