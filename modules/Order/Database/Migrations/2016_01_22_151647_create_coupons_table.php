<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateCouponsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('coupons', function (Blueprint $table) {
                $table->increments('id');
                $table->string('coupon_key');
                $table->string('code', 15)->nullable();
                $table->decimal('percentage', 10, 2)->nullable();
                $table->decimal('value', 10, 2)->nullable();
                $table->integer('quantity')->default(1);
                $table->boolean('status')->default(1);
                $table->date('begin_date')->nullable();
                $table->date('end_date')->nullable();
                $table->time('time')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('coupons');
        }

    }
