<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateAbandonedOrdersTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('abandoned_orders', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->nullable();
                $table->string('ip', 30)->nullable();
                $table->string('name');
                $table->decimal('sub_total', 10, 2)->default(0.00);
                $table->decimal('total', 10, 2)->default(0.00);
                $table->string('coupon')->nullable();
                $table->string('coupon_value')->nullable();
                $table->decimal('discount', 10, 2)->nullable();
                $table->string('address')->nullable();
                $table->string('address_number')->nullable();
                $table->string('city')->nullable();
                $table->string('state')->nullable();
                $table->boolean('finished')->default(0);
                $table->string('file_log');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('abandoned_orders');
        }

    }
