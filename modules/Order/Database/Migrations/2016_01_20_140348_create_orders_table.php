<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateOrdersTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('orders', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->string('coupon')->nullable();
                $table->decimal('discount', 10, 2)->nullable();
                $table->decimal('sub_total', 10, 2);
                $table->decimal('total', 10, 2);
                $table->boolean('notification_email')->default(0);
                $table->string('filename');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('orders');
        }

    }
