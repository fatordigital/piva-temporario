<?php namespace Modules\Order\Entities;

use App\BaseModel;

class CouponPermission extends BaseModel
{

    protected $primaryKey = 'coupon_id';

    protected $fillable = [
        'coupon_id',
        'user_id',
        'email',
        'used'
    ];

}