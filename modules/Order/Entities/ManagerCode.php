<?php namespace Modules\Order\Entities;

use App\BaseModel;

class ManagerCode extends BaseModel
{

    protected $fillable = [
        'code',
        'table',
        'model'
    ];

    public static function getItemData($code)
    {
        $item = parent::whereCode($code)->get()->first();
        if ($item) {
            $product = new $item->model();
            $getITEM = $product->whereCode($code)->get()->first();
            if ($getITEM) {
                return $getITEM;
            }
        }
        return false;
    }

}