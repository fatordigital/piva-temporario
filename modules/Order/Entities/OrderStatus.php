<?php namespace Modules\Order\Entities;

use App\BaseModel;

class OrderStatus extends BaseModel
{

    protected $fillable = [
        'name',
        'debit_stock',
        'credit_stock',
        'notification_email',
        'status'
    ];

}