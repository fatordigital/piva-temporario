<?php namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\MessageBag;
use Modules\User\Entities\User;
use Modules\User\Entities\UserSocialize;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class PagSeguro extends Model
{

    protected $rules = ['card', 'card_birth_date', 'card_national_id', 'card_name', 'card_number', 'card_cvv', 'card_year', 'card_month', 'card_installment', 'hash'];
    protected $message = [
        'card' => 'Escolha o Cartão de Crédito',
        'card_birth_date' => 'Digite a data de nascimento do titular',
        'card_national_id' => 'Digite o cpf do titular',
        'card_name' => 'Digite o nome impresso no cartão',
        'card_year' => 'Selecione o ano de validade',
        'card_number' => 'Digite o número impresso no cartão',
        'card_month' => 'Selecione o mês da validade',
        'card_installment' => 'Selecione as parcelas',
        'card_cvv' => 'Digite o código de segurança impresso no cartão',
        'hash' => 'Falha na requisição com o servidor, por favor, atualize a página e tente novamente.'
    ];

    protected $table = false;
    protected $primaryKey = false;
    protected $timeStamps = false;

    private $email = '';
    private $password = '';

    private $urls = [
        'production' => [
            'https://pagseguro.uol.com.br',
            'https://ws.pagseguro.uol.com.br',
            'https://stc.pagseguro.uol.com.br'
        ],
        'sandbox' => [
            'https://sandbox.pagseguro.uol.com.br',
            'https://ws.sandbox.pagseguro.uol.com.br',
            'https://stc.sandbox.pagseguro.uol.com.br'
        ]
    ];

    public $requestData = [];
    public $id = '';

    public $errors;

    public function __construct()
    {
        parent::__construct();
        $this->errors = new MessageBag();
        $this->loadServerStatus();
        $this->setNotificationURL();
        $this->setPagSeguroEmail();
        $this->setPagSeguroPassword();
        $this->pagseguroUser();
    }

    private function save_user_action($valor)
    {
        if (session()->has('cart.abandoned_id')) {
            $log = new Logger('View Logs');
            $log->pushHandler(new StreamHandler(storage_path() . '/user_paths/order-' . session('cart.abandoned_id') . '.log', Logger::INFO));
            $log->addInfo($valor);
        }
    }

    public function pagseguroUser()
    {
        $this->requestData += [
            'email' => $this->email,
            'token' => $this->password
        ];
    }

    public function getTransactionData($transaction_id, $code = false)
    {

        if (!$code) {
            $url = $this->urls[1] . '/v2/transactions/?reference=' . $transaction_id . '&email=' . $this->email . '&token=' . $this->password;
        } else {
            $url = $this->urls[1] . '/v2/transactions/' . $transaction_id . '?email=' . $this->email . '&token=' . $this->password;
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => "GET"
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $result_pagseguro = simplexml_load_string($response);
        //print('<pre>'); var_dump($result_pagseguro); die;
        return $result_pagseguro;

    }

    public function getRules()
    {
        return $this->rules;
    }

    public function getMessages()
    {
        return $this->message;
    }

    public function loadServerStatus($status = 'sandbox')
    {
        $this->urls = $this->urls[env('PAGSEGURO_STATUS', $status)];
        return $this;
    }

    public function setPagSeguroEmail($email = 'fatordigital@fatordigital.com.br')
    {
        $this->email = env('PAGSEGURO_EMAIL', $email);
        return $this;
    }

    public function setPagSeguroPassword($password = 'FF8DE4E9D4614178BBA0777B92B17EC5')
    {
        $this->password = env('PAGSEGURO_PASSWORD', $password);
        return $this;
    }

    public function getLibraryPagSeguro()
    {
        view()->share('pagseguro_url', $this->urls[2] . '/pagseguro/api/v2/checkout/pagseguro.directpayment.js');
        return $this;
    }

    public function getSessionID()
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->urls[1] . "/v2/sessions",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "email=" . $this->email . "&token=" . $this->password
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $this->save_user_action('Erro ao pegar o ID de sessão do pagseguro, o usuário irá ser retornado para a listagem do carrinho' . var_export($err, true));
            return redirect()->route('carrinhos.finaliza')->with('error', 'Ops, houve um problema ao gerar sua forma de pagamento. Atualize a página ou entre contato conosco.');
        } else {
            $return = simplexml_load_string(utf8_encode($response));
            if ($return->id)
                view()->share('pagseguro_id', $return->id);
            else {
                return redirect()->route('carrinhos.finaliza')->with('error', 'Ops, houve um problema ao gerar sua forma de pagamento. Atualize a página ou entre contato conosco.');
            }
        }
    }

    public function validateFields()
    {
        $return = [];
        $user = User::with(['socialize' => function ($q) {
            $q->select(['user_id', 'birth_date', 'national_id', 'phone', 'postal_code', 'district', 'route', 'route_number', 'state', 'city']);
        }])->select(['id', 'name', 'email'])->find(Auth::user()->id)->toArray();
        if (empty($user['name'])) {
            array_push($return, ['name' => ['label' => 'Nome <strong class="text-danger">(*)</strong>']]);
        }
        if (empty($user['email'])) {
            array_push($return, ['email' => ['label' => 'E-mail <strong class="text-danger">(*)</strong>']]);
        }
        array_push($return, UserSocialize::getField($user['socialize']));
        return $return;
    }

    public function setNotificationURL()
    {
        $this->requestData += [
            'notificationURL' => url('payment/alert')
        ];
        return $this;
    }

    public function load($abandoned_id, $discount = '0.00', $email = '')
    {
        $abandoned = AbandonedOrder::find($abandoned_id);
        $this->requestData += [
            'paymentMode' => 'default',
            'receiverEmail' => $email,
            'currency' => 'BRL',
            'extraAmount' => '-' . number_format((float)$abandoned->total - $abandoned->total_discount, 2, '.', ''),
            'reference' => $this->setId($abandoned_id)
        ];
        return $this;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $id;
    }

    public function sender($pagseguroHash)
    {
        $this->requestData += [
            'senderName' => auth()->user()->name,
            'senderCPF' => onlyNumber(auth()->user()->socialize->national_id),
            'senderAreaCode' => getDDD(auth()->user()->socialize->phone),
            'senderPhone' => getPHONE(auth()->user()->socialize->phone),
            'senderEmail' => env('PAGSEGURO_STATUS') == 'sandbox' ? 'c28193752884547338238@sandbox.pagseguro.com.br' : auth()->user()->email,
            'senderHash' => $pagseguroHash
        ];
        return $this;
    }

    public function shipping($cost = '0.00', $type = 1)
    {
        $this->requestData += [
            'shippingAddressStreet' => auth()->user()->socialize->route,
            'shippingAddressNumber' => preg_replace('/[^\0-9]/', '', auth()->user()->socialize->route_number),
            'shippingAddressComplement' => '',
            'shippingAddressDistrict' => auth()->user()->socialize->district,
            'shippingAddressPostalCode' => preg_replace('/[^\0-9]/', '', auth()->user()->socialize->postal_code),
            'shippingAddressCity' => auth()->user()->socialize->city,
            'shippingAddressState' => auth()->user()->socialize->state,
            'shippingAddressCountry' => 'BRA',
            'shippingType' => $type,
            'shippingCost' => $cost
        ];
        return $this;
    }

    public function addressBilling()
    {
        $this->requestData += [
            'billingAddressStreet' => auth()->user()->socialize->route,
            'billingAddressNumber' => preg_replace('/[^\0-9]/', '', auth()->user()->socialize->route_number),
            'billingAddressComplement' => '',
            'billingAddressDistrict' => auth()->user()->socialize->district,
            'billingAddressPostalCode' => preg_replace('/[^\0-9]/', '', auth()->user()->socialize->postal_code),
            'billingAddressCity' => auth()->user()->socialize->city,
            'billingAddressState' => auth()->user()->socialize->state,
            'billingAddressCountry' => 'BRA'
        ];
        return $this;
    }

    /**
     * quantidade # valor quantidade # total # juros(boolean)
     * 12#78.51#942.15#false
     * @param $value
     * @return object
     */
    public function installments($value)
    {
        if (strstr($value, '#')) {
            $value = explode('#', $value);
            $this->requestData += [
                'installmentQuantity' => $value[0],
                'installmentValue' => simple_coin($value[1])
            ];
        }
        return $this;
    }

    public function credit($token, $data)
    {
        $this->requestData += [
            'paymentMethod' => 'creditCard',
            'creditCardToken' => $token,
            'creditCardHolderName' => $data['card_name'],
            'creditCardHolderCPF' => onlyNumber($data['card_national_id']),
            'creditCardHolderBirthDate' => $data['card_birth_date'],
            'creditCardHolderAreaCode' => getDDD(auth()->user()->socialize->phone),
            'creditCardHolderPhone' => getPHONE(auth()->user()->socialize->phone)
        ];
        return $this;
    }

    public function debit($bank)
    {
        $this->requestData += [
            'bankName' => $bank,
            'paymentMethod' => 'eft'
        ];
        return $this;
    }

    public function boleto()
    {
        $this->requestData += [
            'paymentMethod' => 'boleto'
        ];
        return $this;
    }

    public function items()
    {
        $items = [];
        $i = 1;
        $abandoned = AbandonedOrderItem::where('order_id', '=', $this->id)->get();
        foreach ($abandoned as $product) {
            $item = explode('#', $product->ref);
            $items['itemId' . $i] = $item['1'];
            $items['itemDescription' . $i] = $product->name;
            $items['itemAmount' . $i] = $product->sale > 0 ? $product->sale : $product->price;
            $items['itemQuantity' . $i] = 1;
            $i++;
        }
        $this->requestData += $items;
        return $this;
    }

    public function professorpiva(array $product)
    {
        $model = new $product[0]();
        $r = $model->find($product[1]);
        return $r;
    }

    public function get()
    {
        $ch = curl_init($this->urls[1] . '/v2/transactions');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded; charset=UTF-8',
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->requestData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
        if ($err) {
            $this->save_user_action(var_export($err, true));
            return false;
        } else {
            $this->save_user_action(var_export($response, true));
            $return = $this->saveRequest($response);
            $this->save_user_action(var_export($return, true));
            if ($return->count()) {
                return $return;
            } else {
                return $return;
            }
        }
    }

    private function saveRequest($ch)
    {
        $return = simplexml_load_string($ch);
        if ($return->error) {
            foreach ($return as $error) {
                $this->errors->add((string)$error->code, $this->errors_code((string)$error->code));
            }
            return $this->errors;
        } else {
            $this->order(json_decode(json_encode($return)));
            return $return;
        }
    }


    public function order($xml)
    {
        $abandoned = AbandonedOrder::find($this->id);
        $abandoned->update(['finished' => 1]);
        $order = Order::create([
            'user_id' => auth()->user()->id,
            'abandoned_order_id' => $abandoned->id,
            'coupon' => $abandoned->coupon,
            'discount' => $abandoned->total_discount,
            'sub_total' => $abandoned->sub_total,
            'total' => $abandoned->total,
            'net_amount' => $xml->netAmount,
            'fee_amount' => $xml->feeAmount,
            'pagseguro_code' => $xml->code,
            'pagseguro_method_type' => $xml->paymentMethod->type,
            'pagseguro_method_code' => $xml->paymentMethod->code,
            'pagseguro_status' => $xml->status,
            'pagseguro_json' => json_encode($xml),
            'payment_link' => isset($xml->paymentLink) ? $xml->paymentLink : '',
            'pagseguro_notification' => ''
        ]);

//        Mail::send('emails.order', [
//            'user' => User::find(auth()->user()->id),
//            'pedido_id' => $order->id,
//            'pedido_data' => dateToSql($order->create_at, 'Y-m-d H:i:s', 'd/m/Y H:i:s'),
//            'pagamento_forma' => 'Pagseguro - '. $order->pagseguro->name,
//            'cupom' => $abandoned->coupon_key ? $abandoned->coupon_key : 'Não utilizado',
//            'cupom_valor' => $abandoned->coupon_percentage > 0 ? $abandoned->coupon_percentage . '%' : 'R$ ' . $abandoned->coupon_value,
//            'valor_total' => $order->total,
//            'items' => $abandoned->items
//        ], function ($m) {
//            $m->bcc(env('EMAIL_COPY'), env('EMAIL_COPY_NAME'));
//            $m->bcc(env('DEVELOPER_EMAIL'), 'Developer');
//            $m->from('nao-responda@professorpiva.com.br', 'Professor Piva');
//            $m->to(auth()->user()->email, auth()->user()->name)->subject('Confirmação do pedido');
//        });

        foreach ($xml->items as $item) {
            if (count($item) > 1) {
                foreach ($item as $r) {
                    OrderItem::create(['name' => $r->description, 'order_id' => $order->id, 'ref' => $r->id, 'quantity' => $r->quantity, 'value' => $r->amount]);
                }
            } else {
                OrderItem::create(['name' => $item->description, 'order_id' => $order->id, 'ref' => $item->id, 'quantity' => $item->quantity, 'value' => $item->amount]);
            }
        }
        OrderHistoricalStatus::create(['order_id' => $order->id, 'order_status_id' => 1]);
    }


//    public function order($xml)
//    {
//        $abandoned = AbandonedOrder::find($this->id);
//        $abandoned->update(['closed' => true]);
//
//        $order = Order::firstOrCreate([
//            'user_id' => auth()->user()->id,
//            'abandoned_order_id' => $abandoned->id,
//            'coupon' => $abandoned->coupon_id,
//            'discount' => $abandoned->coupon_discount,
//            'sub_total' => $abandoned->sub_total,
//            'total' => $abandoned->coupon_id > 0 ? $abandoned->total_with_coupon : $abandoned->total,
//            'net_amount' => (string)$xml->netAmount,
//            'fee_amount' => (string)$xml->feeAmount,
//            'pagseguro_code' => $xml->code,
//            'pagseguro_type' => $xml->paymentMethod->type,
//            'pagseguro_method_code' => $xml->paymentMethod->code,
//            'pagseguro_status' => $xml->status,
//            'notification_email' => 1,
//            'pagseguro_json' => json_encode($xml),
//            'payment_link' => isset($xml->paymentLink) ? $xml->paymentLink : '',
//            'pagseguro_notification' => ''
//        ]);
//
//        Mail::send('emails.order', [
//            'user' => User::find(auth()->user()->id),
//            'pedido_id' => $order->id,
//            'pedido_data' => dateToSql($order->create_at, 'Y-m-d H:i:s', 'd/m/Y H:i:s'),
//            'pagamento_forma' => 'Pagseguro - '. $order->pagseguro->name,
//            'cupom' => $abandoned->coupon_key ? $abandoned->coupon_key : 'Não utilizado',
//            'cupom_valor' => $abandoned->coupon_percentage > 0 ? $abandoned->coupon_percentage . '%' : 'R$ ' . $abandoned->coupon_value,
//            'valor_total' => $order->total,
//            'items' => $abandoned->items
//        ], function ($m) {
//            $m->bcc(env('EMAIL_COPY'), env('EMAIL_COPY_NAME'));
//            $m->bcc(env('DEVELOPER_EMAIL'), 'Developer');
//            $m->from(setting('Email.contact'), setting('Project'));
//            $m->to(auth()->user()->email, auth()->user()->name)->subject('Confirmação do pedido');
//        });
//
//        foreach ($abandoned->items as $item) {
//            OrderItem::firstOrCreate([
//                'order_id' => $order->id,
//                'ref' => $item->item_id,
//                'name' => $item->name,
//                'slug' => $item->slug,
//                'value' => $item->price,
//                'sale' => $item->sale
//            ]);
//        }
//        OrderHistoricalStatus::create(['order_id' => $order->id, 'order_status_id' => 1]);
//    }

    public function errors_code($key)
    {
        //10 ~ 20
        $errors = [
            'bad_request' => 'O pagamento não pode ser processado',
            '5003' => 'falha de comunicação com a instituição financeira',
            '10000' => 'bandeira inválida',
            '10001' => "número do cartão inválido",
            '10002' => "data no formato inválido",
            '10003' => "código de segurança inválido",
            '10004' => "cvv obrigatório",
            '10006' => "código de segurança inválido",
            '53004' => "quantidade de ítens inválido",
            '53005' => "moeda obrigatória",
            '53006' => "moeda inválida",
            '53007' => "tamanho do campo referência inválido",
            '53008' => "url de notificação inválida",
            '53009' => "tamanho da url de notificação inválida",
            '53010' => "email do comprador inválido.",
            '53011' => "email do comprador inválido",
            '53012' => "tamanho do campo email do comprador inválido",
            '53013' => "nome do comprador obrigatório",
            '53014' => "nome do comprador inválido",
            '53015' => "tamanho do campo nome do comprador inválido",
            '53017' => "cpf do comprador inválido",
            '53018' => "código de área do telefone inválido",
            '53019' => "tamanho do campo código de área do telefone inválido",
            '53020' => "telefone obrigatório",
            '53021' => "telefone inválido",
            '53022' => "CEP obrigatório",
            '53023' => "CEP inválido",
            '53024' => "rua do endereço obrigatório",
            '53025' => "rua do endereço inválido",
            '53026' => "número do endereço obrigatório",
            '53027' => "número do endereço inválido",
            '53028' => "complemento do endereço inválido",
            '53029' => "bairro do endereço obrigatório",
            '53030' => "bairro do endereço inválido",
            '53031' => "cidade do endereço obrigatória",
            '53032' => "cidade do endereço inválida",
            '53033' => "estado do endereço obrigatório",
            '53034' => "estado do endereço inválido",
            '53035' => "país do endereço obrigatório.",
            '53036' => "país do endereço inválido",
            '53037' => "token do cartão de crédito obrigatório",
            '53038' => "quantidade de parcelas obrigatória",
            '53039' => "quantidade de parcelas inválida",
            '53040' => "valor da parcela obrigatória",
            '53041' => "valor da parcela inválida",
            '53042' => "portador do cartão obrigatório",
            '53043' => "portador do cartão inválido",
            '53044' => "tamanho do campo portador do cartão inválido",
            '53045' => "CPF do portador do cartão obrigatório",
            '53046' => "CPF do portador do cartão inválido",
            '53047' => "data de nascimento do portador do cartão obrigatório",
            '53048' => "data de nascimento do portador do cartão inválido",
            '53049' => "código de área do portador do cartão obrigatório",
            '53050' => "código de área do portador do cartão inválido",
            '53051' => "telefone do portador do cartão obrigatório",
            '53052' => "telefone  do portador do cartão inválido",
            '53053' => "CEP do portador do cartão obrigatório",
            '53054' => "CEP do portador do cartão inválido",
            '53055' => "rua do portador do cartão obrigatório",
            '53056' => "rua do portador do cartão inválido",
            '53057' => "número do endereço do portador do cartão obrigatório",
            '53058' => "número do endereço do portador do cartão inválido",
            '53059' => "tamanho do campo complemento do endereço do portador do cartão inválido",
            '53060' => "bairro do portador do cartão obrigatório",
            '53061' => "tamanho do campo bairro do portador do cartão inválido",
            '53062' => "cidade do portador do cartão obrigatório",
            '53063' => "tamanho do campo cidade do portador do cartão inválido",
            '53064' => "estado do portador do cartão obrigatório",
            '53065' => "estado do portador do cartão inválido",
            '53066' => "país do portador do cartão obrigatório",
            '53067' => "tamanho do campo país do portador do cartão inválido",
            '53068' => "tamanho do email do vendedor inválido",
            '53069' => "email do vendedor inválido",
            '53070' => "código do ítem obrigatório",
            '53071' => "tamanho do código do ítem inválido",
            '53072' => "descrição do ítem obrigatório",
            '53073' => "tamanho do campo ítem inválido",
            '53074' => "quantidade de ítens obrigatória",
            '53075' => "quantidade de ítens fora do limite",
            '53076' => "quantidade de ítens inválido",
            '53077' => "montante do ítem obrigatório",
            '53078' => "montante do ítem inválido.",
            '53079' => "montante fora do limite",
            '53081' => "comprador é igual ao vendedor",
            '53084' => "vendedor inválido, verifique se é uma conta com status de vendedor",
            '53085' => "método de pagamento indisponível",
            '53086' => "montante total acima do limite do cartão",
            '53087' => "dados do cartão inválidos",
            '53091' => "hash do comprador inválido",
            '53092' => "bandeira do cartão não aceita",
            '53095' => "tipo de entrega inválido",
            '53096' => "custo de entrega inválido",
            '53097' => "custo da entrega fora do limite",
            '53098' => "valor total é negatívo",
            '53099' => "montante extra inválido.",
            '53101' => "modo de pagamento inválido, valores válidos são default e gateway",
            '53102' => "método de pagamento inválido, valores válidos são creditCard, boleto e eft",
            '53104' => "custo de entrega informado, endereço de entrega deve ser completo",
            '53105' => "informações do comprador informado, email também deve ser informado",
            '53106' => "portador do cartão incompleto",
            '53109' => "endereço do comprador informado, email do comprador também deve ser informado",
            '53110' => "banco eft obrigatório",
            '53111' => "banco eft não aceito",
            '53115' => "data de nascimento do comprador inválida",
            '53117' => "CPNJ do comprador inválido",
            '53122' => "domínio do email do comprador inválido. Você deve usar um email @sandbox.pagseguro.com.br",
            '53140' => "quantidade de parcelas fora do limite. O valor deve ser maior que zero",
            '53141' => "comprador bloqueado",
            '53142' => "token do cartão de crédito inválido"
        ];

        if ($key >= 53010 && $key <= 53036)
            return $errors[$key]. ' <a class="btn btn-info btn-xs" href="' . route('front.user.register', ['redirect' => base64_encode(route('cart.checkout'))]) . '">editar cadastro</a>';

        if ($key >= 53053 && $key <= 53067)
            return $errors[$key] .' <a class="btn btn-info btn-xs" href="' . route('front.user.register', ['redirect' => base64_encode(route('cart.checkout'))]) . '">editar cadastro</a>';

        return $errors[$key];
    }


    public function setDataNotification($code)
    {
        //$ch = curl_init('https://ws.pagseguro.uol.com.br/v2/transactions/notifications/' . $code);
        $ch = curl_init($this->urls[1] . '/v2/transactions/notifications/' . $code . '?email=' . $this->email . '&token=' . $this->password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        $this->setNotification($response);
        curl_close($ch);
    }

    private function setNotification($xml_string)
    {
        $object = simplexml_load_string($xml_string);
        $this->pagReturn = json_encode($object);
        $this->pagReturn = json_decode($this->pagReturn, true);
    }

    public function getNotificationReturn($field = null)
    {
        if (is_null($field))
            return $this->pagReturn;
        return $this->pagReturn[$field];
    }

}