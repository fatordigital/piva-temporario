<?php namespace Modules\Order\Entities;

use App\BaseModel;

class OrderHistoricalStatus extends BaseModel
{

    protected $fillable = [
        'order_id',
        'order_status_id'
    ];

    public function order_statuses()
    {
        return $this->belongsTo('Modules\Order\Entities\OrderStatus', 'order_status_id');
    }

}