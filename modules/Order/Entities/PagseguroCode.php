<?php namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;

class PagseguroCode extends Model
{

    protected $fillable = [
        'code',
        'name',
        'image'
    ];

}