<?php namespace Modules\Order\Entities;

use App\BaseModel;

class Coupon extends BaseModel
{

    protected $fillable = [
        'coupon_key', 'code', 'user_id', 'email',
        'percentage', 'value', 'quantity',
        'status', 'begin_date', 'end_date', 'time'
    ];

    public function sum_for_me($coupon, $total, $rest = false)
    {
        if ($coupon->percentage > 0) {
            if (!$rest)
                $total = $total - round(($total * $coupon->percentage / 100));
            else {
                $total = number_format(round($total * $coupon->percentage / 100), 2, '.', '');
            }
            if ($total > 0)
                return $total;
            else {
                return '0.00';
            }
        }
        if ($rest) {
            return number_format($coupon->value, 2, '.', '');
        }
        $total = $total - $coupon->value;
        if ($total > 0)
            return $total;
        else {
            return '0.00';
        }
    }

    public static function customCreate($request)
    {
        if ($request->has('value'))
            $request->merge(['value' => coinToBco($request->get('value'))]);
        if ($request->has('percentage'))
            $request->merge(['percentage' => coinToBco($request->get('percentage'))]);
        $coupons = static::create($request->except(['_token', 'coupon_permissions']));
        if ($request->get('coupon_permissions')['user_id'] != "" || $request->get('coupon_permissions')['email'] != "") {
            CouponPermission::firstOrCreate([
                'coupon_id' => $coupons->id,
                'user_id'   => $request->get('coupon_permissions')['user_id'],
                'email'     => $request->get('coupon_permissions')['email']
            ]);
        }
        if ($coupons)
            return true;
        return false;
    }

    public static function customUpdate($request, $id)
    {
        $coupons = parent::find($id);
        $coupons->update($request->except(['_token', 'coupon_permissions']));
        if ($request->get('coupon_permissions')['user_id'] != "" || $request->get('coupon_permissions')['email'] != "") {
            $cp = CouponPermission::find($id);
            $cp->update([
                'user_id'   => $request->get('coupon_permissions')['user_id'],
                'email'     => $request->get('coupon_permissions')['email']
            ]);
        }
    }

    public function coupon_uses()
    {
        return $this->hasMany('Modules\Order\Entities\CouponUse', 'coupon_key', 'coupon_key');
    }


    public function coupon_permissions()
    {
        return $this->hasOne('Modules\Order\Entities\CouponPermission', 'coupon_id');
    }

    public function manager_codes()
    {
        return $this->hasMany('Modules\Order\Entities\Coupon', 'coupon_id', '');
    }

    public function validateCoupon($coupon)
    {
        //Caso não tenha passado nenhum cupom
        if (!$coupon)
            return false;

        $exists = $this->with('coupon_permissions')->whereCouponKey($coupon)->first();
        //Caso não exista
        if (!$exists)
            return false;


        //Pertence a um usuário
        if ($exists->coupon_permissions) {
            if (!auth()->guest()) {
                if ($exists->coupon_permissions->user_id != '' && $exists->coupon_permissions->user_id == auth()->user()->id && $exists->coupon_permissions->used != 0)
                    return false;
                if ($exists->coupon_permissions->email != '' && $exists->coupon_permissions->email == auth()->user()->email && $exists->coupon_permissions->used != 0)
                    return false;
            } else {
                return false;
            }
        }

        $abandoned = AbandonedOrder::where('coupon', '=', $exists->coupon_key)->get();
        if ($abandoned->count() > $exists->quantity)
            return false;
        $order = Order::where('coupon', '=', $exists->coupon_key)->get();
        if ($order->count() > $exists->quantity)
            return false;

        if ((session('cart.data.total') - $exists->value) <= 0) {
            return 'O valor do carrinho é menor que o valor do cupom.';
        }

        if (!empty($exists->percentage) && $exists->percentage > 0) {
            session([
                'cart.data.total' => session('cart.data.total') - round((session('cart.data.total') * $exists->percentage / 100)),
                'cart.data.discount' => round((session('cart.data.total') * $exists->percentage / 100))
            ]);
        } else {
            session([
                'cart.data.total' => session('cart.data.total') - $exists->value,
                'cart.data.discount' => $exists->value
            ]);
        }

        session(['cart.coupon' => $exists->coupon_key]);

        return $exists;
    }
}