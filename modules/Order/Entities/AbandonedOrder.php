<?php namespace Modules\Order\Entities;

use App\BaseModel;
use Illuminate\Support\Facades\Auth;

class AbandonedOrder extends BaseModel
{

    protected $fillable = [
        'user_id', 'national_id', 'total_discount',
        'phone', 'installment', 'installment_amount', 'coupon_percentage',
        'installment_interest', 'transaction_json', 'ip',
        'name', 'sub_total', 'total', 'coupon', 'district', 'coupon_value', 'discount',
        'postal_code', 'country', 'route', 'route_number', 'city', 'state', 'finished',
        'file_log', 'birth_date', 'payment_wait'
    ];

    protected $appends = ['total_with_discount', 'coupon_type'];

    public function setSubTotalAttribute($value)
    {
        $value = $value ? (strstr($value, ',') ? coinToBco($value) : $value) : 0.00;
        $this->attributes['sub_total'] = $value;
    }

    public function setTotalAttribute($value)
    {
        $value = $value ? (strstr($value, ',') ? coinToBco($value) : $value) : 0.00;
        $this->attributes['total'] = $value;
    }

    public function getTotalWithDiscountAttribute()
    {
        return $this->total_discount && $this->total_discount > 0 ? bcoToCoin($this->total_discount) : bcoToCoin($this->total);
    }

    public function getCouponTypeAttribute()
    {
        if ($this->coupon != '') {
            return $this->coupon_percentage ? $this->coupon_percentage . '%' : 'R$ ' . $this->coupon_value;
        }
        return 'R$ 0,00';
    }

    public function setDiscountAttribute($value)
    {
        if (!empty($value) && strstr($value, ','))
            $this->attributes['discount'] = coinToBco($value);
        else if (empty($value)) {
            $this->attributes['discount'] = '0.00';
        }
    }

    public function AbandonedOrderItem()
    {
        return $this->hasMany('Modules\Order\Entities\AbandonedOrderItem', 'order_id');
    }

    public function items()
    {
        return $this->hasMany('Modules\Order\Entities\AbandonedOrderItem', 'order_id');
    }

    public static function createByIp($ip)
    {
        $store = parent::create(['ip' => $ip]);
        return $store->id;
    }

    public static function createByUser($ip)
    {
        $store = parent::create([
            'user_id' => Auth::user()->id,
            'ip' => $ip
        ]);
        return $store->id;
    }

    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'user_id');
    }

}