<?php namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{

    protected $fillable = [
        'order_id',
        'name',
        'quantity',
        'value',
        'ref'
    ];

    public function Item()
    {
        return $this->belongsTo('Modules\Team\Entities\Team', 'ref');
    }

    public function Order()
    {
        return $this->belongsTo('Modules\Order\Entities\Order', 'order_id');
    }

}