<?php namespace Modules\Order\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class AbandonedOrderItem extends BaseModel
{

    use SoftDeletes;

    protected $fillable = [
        'order_id', 'ref', 'quantity', 'value', 'value_discount', 'name', 'slug'
    ];

    protected $appends = ['price'];

    public function getPriceAttribute()
    {
        return $this->value_discount != '' && $this->value_discount > 0 ? $this->value_discount : $this->value;
    }

    public function order()
    {
        return $this->belongsTo('Modules\Order\Entities\AbandonedOrder', 'order_id');
    }

    public function courses()
    {
        $explode = explode('#', $this->ref);
        $this->ref = $explode[1];
        return $this->belongsTo('Modules\Team\Entities\Team', 'ref');
    }


}