<?php namespace Modules\Order\Entities;

use App\BaseModel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Team\Entities\Team;
use Modules\User\Entities\User;

/**
 *
 * @apiVersion    0.0.1
 * @api           {class} Order
 * @apiGroup      OrderModel
 *
 * @apiPermission Developers/Public
 *
 */
class Order extends BaseModel
{

    protected $fillable = [
        'user_id',
        'abandoned_order_id',
        'coupon',
        'discount',
        'fee_amount',
        'net_amount',
        'pagseguro_code',
        'pagseguro_type',
        'pagseguro_method_type',
        'pagseguro_method_code',
        'pagseguro_json',
        'pagseguro_status',
        'sub_total',
        'total',
        'notification_email',
        'filename',
        'payment_link',
        'payment_wait',
        'payment_reference_old',
        'created_at'
    ];

    protected $appends = ['UserEmail'];

    public $excel = [
        '#Identificação', 'E-mail', 'Cupom', 'Desconto', 'Sub Total', 'Total', 'Data de registro', 'Data de alteração no registro'
    ];

    public $excelModel = [
        'id', 'UserEmail', 'coupon', 'discount', 'sub_total', 'total', 'created_at', 'updated_at'
    ];

    public function getUserEmailAttribute()
    {
        $user = User::find($this->user_id);
        if ($user) {
            return $user->email;
        } else {
            return 'Sem e-mail';
        }
    }

    public function pagseguro()
    {
        return $this->hasOne('Modules\Order\Entities\PagseguroCode', 'code', 'pagseguro_method_code');
    }

    public function orderStatus()
    {
        return $this->belongsTo('Modules\Order\Entities\OrderStatus', 'pagseguro_status');
    }

    public function orderItems()
    {
        return $this->hasMany('Modules\Order\Entities\OrderItem', 'order_id');
    }

    public function abandoned()
    {
        return $this->belongsTo('Modules\Order\Entities\AbandonedOrder', 'abandoned_order_id');
    }

    public function historical()
    {
        return $this->hasMany('Modules\Order\Entities\OrderHistoricalStatus', 'order_id')->orderBy('created_at', 'desc');
    }

    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'user_id');
    }


    /**
     *
     * @apiVersion    0.0.1
     * @api           {function} findWithSearch
     * @apiGroup      OrderModel
     *
     * @apiParam {Object} request Parâmetros de pesquisa ou vazio
     * @apiSuccess {Object} list Dados da tabela 'orders'
     *
     * @param $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function findWithSearch($request)
    {
        $get = Order::with(['user']);
        if (!static::verifyIfNotEmpty($request))
            return Order::orderBy('created_at', 'desc');
        if ($request->get('user_id'))
            $get->where('user_id', '=', $request->get('user_id'));
        if ($request->get('created_at'))
            $get->whereRaw('DATE(`created_at`) >= DATE(?)', [Carbon::createFromFormat('d-m-Y', $request->get('created_at'))->toDateString()]);
        else {
            $get->whereMonth('created_at', '=', date('m'))->whereYear('created_at', '=', date('Y'))->orderBy('created_at', 'desc');
        }
        if ($request->get('updated_at'))
            $get->where('updated_at', '<=', Carbon::createFromFormat('d-m-Y', $request->get('updated_at'))->toDateString());
        if ($request->get('order_id'))
            $get->where('id', '=', $request->get('order_id'));
        if ($request->get('order_status'))
            $get->where('pagseguro_status', '=', $request->get('order_status'));
        if ($request->get('product'))
            $get->whereIn('id', self::searchByProduct($request->get('product')));
        return $get;
    }

    private static function searchByProduct($product_id)
    {
        $teams = Team::where('course_id', '=', $product_id)->lists('id', 'id')->toArray();
        if ($teams) {
            $order_items = OrderItem::whereIn('ref', $teams)->lists('order_id', 'order_id')->toArray();
            if ($order_items) {
                return Order::whereIn('id', $order_items)->lists('id', 'id')->toArray();
            }
        }
        return null;
    }

    /**
     * @param $request
     * @return bool
     */
    private static function verifyIfNotEmpty($request)
    {
        if ($request->get('created_at') || $request->get('updated_at') || $request->get('order_id') || $request->get('order_status') || $request->get('user_id') || $request->get('product'))
            return true;
        return false;
    }

}