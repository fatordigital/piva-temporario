<?php namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponUse extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'coupon_key', 'coupon_percentage',
        'coupon_value', 'coupon_user_id', 'coupon_user_email',
        'coupon_quantity', 'coupon_begin_date', 'coupon_end_date',
        'coupon_time'
    ];

}