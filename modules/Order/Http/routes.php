<?php

    Route::group([
        'prefix'     => 'fatorcms',
        'middleware' => [
            'auth',
            'acl',
            'cms'
        ],
        'namespace'  => 'Modules\Order\Http\Controllers'
    ], function () {
        Route::get('order', [
            'as'   => 'fatorcms.order.index',
            'uses' => 'OrderController@index'
        ]);
        Route::get('order/{id}/show', [
            'as'   => 'fatorcms.order.show',
            'uses' => 'OrderController@show'
        ]);
        Route::get('order/search', [
            'uses' => 'OrderController@index',
            'as'   => 'fatorcms.order'
        ]);
        Route::resource('coupon', 'CouponController');
        Route::patch('/coupon/status/{id}', [
            'as'   => 'fatorcms.coupon.status',
            'uses' => 'CouponController@status'
        ]);

    });

    Route::get('/fatorcms/order/make/manual', [
        'middleware' => 'cms',
        'as'         => 'order.make.manual',
        'uses'       => 'Modules\Order\Http\Controllers\OrderController@manualOrder'
    ]);

    Route::post('/fatorcms/order/store/manual', [
        'middleware' => 'cms',
        'as'         => 'order.store.manual',
        'uses'       => 'Modules\Order\Http\Controllers\OrderController@postCreateManualOrder'
    ]);

    Route::post('/fatorcms/order/update/status/{id}', [
        'middleware' => 'cms',
        'as'         => 'order.status.update',
        'uses'       => 'Modules\Order\Http\Controllers\OrderController@updateStatus'
    ]);