<?php namespace Modules\Order\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Illuminate\Support\Facades\View;
use Modules\Order\Entities\Coupon;
use Modules\Order\Entities\CouponPermission;
use Modules\Order\Entities\Order;
use Modules\Order\Http\Requests\CouponRequest;
use Modules\User\Entities\TemporaryContact;
use Modules\User\Entities\User;

class CouponController extends BaseModuleController
{


    private function lists($order)
    {
        $users = User::where('group_id', '!=', '1')->get()->lists('name', 'id')->toArray();
        View::share(compact('codes', 'users'));
    }

    public function index()
    {
        $list = Coupon::orderBy('id', 'desc')->paginate(15);
        return view('order::coupon.index', compact('list'));
    }

    public function create()
    {
        $this->lists(new Order);
        return view('order::coupon.create');
    }

    public function store(CouponRequest $request)
    {
        if (Coupon::customCreate($request)) {
            return redirect()->route('fatorcms.coupon.index')->with('success', 'Cupom criado com sucesso.');
        } else {
            return redirect()->route('fatorcms.coupon.create')->with('error', 'Erro ao criar o cupom, tente novamente')->withInput($request->except(['_token']));
        }
    }

    public function edit($id)
    {
        $this->lists(new Order);
        $edit = Coupon::with('coupon_permissions')->where('id', '=', $id)->get()->first();
        return view('order::coupon.edit', compact('edit'));
    }

    public function update(CouponRequest $request, $id)
    {
        Coupon::customUpdate($request, $id);
        return redirect()->route('fatorcms.coupon.index')->with('success', trans('messages.update.success'));
    }

    public function status($id)
    {
        $status = Coupon::findOrFail($id);
        $to = ($status->status) ? 0 : 1;
        $status->status = $to;
        $status->save();
        return redirect()->route('fatorcms.coupon.index')->with('success', trans('messages.status.success', [
            'status' => ($to) ? trans('dictionary.status_text.active') : trans('dictionary.status_text.inactive')
        ]));
    }

    public function destroy($id)
    {
        $order = Order::where('coupon', '=', $id)->get();
        if ($order->count()) {
            foreach ($order as $showid) {
                $show[] = $showid->id;
            }
            return redirect()->route('fatorcms.coupon.index')->with('error', trans('messages.delete.coupon.order', ['id' => implode(',', $show)]));
        }
        CouponPermission::destroy($id);
        Coupon::destroy($id);
        return redirect()->route('fatorcms.coupon.index')->with('success', trans('messages.delete.success'));
    }

}