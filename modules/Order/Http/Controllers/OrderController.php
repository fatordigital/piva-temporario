<?php namespace Modules\Order\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Modules\Order\Entities\AbandonedOrder;
use Modules\Order\Entities\AbandonedOrderItem;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderHistoricalStatus;
use Modules\Order\Entities\OrderItem;
use Modules\Order\Entities\OrderStatus;
use Modules\Order\Http\Requests\OrderSearchRequest;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamUser;
use Modules\User\Entities\User;

/**
 *
 * @apiVersion     0.0.1
 * @api            {class} OrderController
 * @apiGroup       Order
 * @apiDescription Controlador do módulo de pedidos do usuário. <br />
 *                 Ao inserir o módulo no projeto verificar os seguintes itens no projeto principal <br />
 *                 1 uma classe <code>ModuleController</code> <br />
 *                 2 uma classe <code>BaseController</code> <br />
 *                 Tabelas envolvidas <br />
 *                 <code>orders</code>,<code>order_statuses</code>,<code>order_items</code>,<code>order_historical_statuses</code>
 *
 *
 * Class OrderController
 * @package        Modules\Order\Http\Controllers
 */
class OrderController extends BaseModuleController
{

    private function lists()
    {
        $users = User::where('group_id', '!=', '1')->get()->lists('name_email', 'id')->toArray();
        $orderStatus = OrderStatus::all()->lists('name', 'id')->toArray();
        View::share(compact('orderStatus', 'users'));
    }

    /**
     *
     * @apiVersion    0.0.1
     * @api           {get} /order/:search OrderController:index
     * @apiGroup      Order
     *
     * @apiParam {Object} request Parâmetros de pesquisa ou vazio
     * @apiSuccess {Object} list Dados da tabela 'orders'
     * @apiPermission Developers/Public
     *
     * @param OrderSearchRequest $request
     * @return View
     */
    public function index(OrderSearchRequest $request)
    {
        $this->lists();
        $list = Order::findWithSearch($request);

        if (request()->has('export')) {
            $data = [];
            $items = $list->with('user')->get();
            $sub_total = $total = $taxa = $total_taxa = 0;
            foreach ($items as $item) {
                $pagseguro = json_decode($item->pagseguro_notification);
                $data[] = [
                    '#ID Pedido' => $item->id,
                    'Nome' => $item->user ? $item->user->name : '-',
                    'Pedido Status' => $item->orderStatus ? $item->orderStatus->name : '',
                    'E-mail' => $item->user ? $item->user->email : '-',
                    'Subtotal' => 'R$ ' . bcoToCoin($item->sub_total),
                    'Total' => 'R$ ' . bcoToCoin($item->total),
                    'Total Taxa' => 'R$ '.bcoToCoin($item->fee_amount),
                    'Total com taxa Pagseguro' => 'R$ '.bcoToCoin($item->net_amount),
                    'Data & Hora' => dateToSql($item->created_at, 'Y-m-d H:i:s', 'd/m/Y H:i:s')
                ];
                $sub_total += $item->sub_total;
                if ($item->pagseguro_status == 3 || $item->pagseguro_status == 4)
                    $total += $item->total;
                if ($item->pagseguro_status == 3 || $item->pagseguro_status == 4) {
                    $taxa += $item->fee_amount;
                    $total_taxa += $item->net_amount;
                }
            }
            $this->export_excel($data, 'Pedidos', [
                '#ID Pedido',
                'Nome',
                'Pedido Status',
                'E-mail',
                'Subtotal',
                'Total',
                'Total Taxa',
                'Total com taxa Pagseguro',
                'Data & Hora'
            ], [
                '',
                '',
                '',
                '',
                'Subtotal',
                'Total Pago',
                'Total Taxa',
                'Total Pago com taxa Pagseguro',
                ''
            ], [[
                '',
                '',
                '',
                '',
                'R$ '. bcoToCoin($sub_total),
                'R$ '. bcoToCoin($total),
                'R$ '. bcoToCoin($taxa),
                'R$ '. bcoToCoin($total_taxa),
                ''
            ]]);
        }

        $list = $list->paginate(50);

        return view('order::order.index', compact('list'));
    }

    /**
     *
     * @apiVersion    0.0.1
     * @api           {get} /order/:order_id OrderController:show
     * @apiGroup      Order
     * @apiSuccess {Object} show Dados referente ao id do pedido
     * @apiError      OrderNotFound <code>id</code> da tabela orders.
     * @apiPermission Developers/Public
     *
     * @param $id
     * @return View
     */
    public function show($id)
    {
        $statuses = OrderStatus::all()->lists('name', 'id')->toArray();
        $show = Order::with(['user', 'abandoned', 'historical', 'orderItems'])->find($id);
        return view('order::order.show', compact('show', 'statuses'));
    }

    public function updateStatus(Request $request, $id)
    {
        $order = Order::find($id);
        if ($order) {

            $order->update(['pagseguro_status' => $request->get('pagseguro_status')]);
            OrderHistoricalStatus::create([
                'order_id' => $id,
                'order_status_id' => $request->get('pagseguro_status')
            ]);
            $orderItem = OrderItem::whereOrderId($id)->get();
            if ($request->get('pagseguro_status') == 3 || $request->get('pagseguro_status') == 4) {
                foreach ($orderItem as $item) {
                    $team = Team::whereId($item->ref)->orderBy('id', 'desc')->first();
                    if ($team) {
                        $end_days = $team->end_days ? date('Y-m-d', strtotime('+' . $team->end_days . ' days')) : null;
                        TeamUser::firstOrCreate([
                            'course_id' => $team->course_id,
                            'team_id' => $team->id,
                            'user_id' => $order->user_id,
                            'end_date' => $end_days
                        ]);
                    } else {
                        return redirect()->back()->with('error', 'Ops, houve um erro inesperado. Tente novamente.');
                    }
                }
            } else {
                foreach ($orderItem as $item) {
                    $userinteam = TeamUser::whereUserId($order->user_id)->whereTeamId($item->ref)->first();
                    if ($userinteam) {
                        $userinteam->delete();
                    }
                }
            }

        }
        return redirect()->route('fatorcms.order.show', $id)->with('success', 'Pedido Atualizado');
    }

    /**
     * @return View
     */
    public function manualOrder()
    {
        $this->lists();
        return view('order::order.make_order');
    }

    public function postCreateManualOrder(Request $request)
    {
        $user = User::find($request->get('user_id'));
        $module = Team::find($request->get('team_id'));

        $abandoned = AbandonedOrder::create([
            'user_id' => $user->id,
            'total' => $module->value_discount > 0 ? $module->value_discount : $module->value,
            'sub_total' => $module->value,
            'transaction_json' => 'MANUALMENTE',
            'finished' => true
        ]);

        $abandoned_item = AbandonedOrderItem::create([
            'order_id' => $abandoned->id,
            'code' => 'Modules\Team\Entities\Team#' . $module->id,
            'value' => $abandoned->total
        ]);

        $order = Order::create([
            'user_id' => $abandoned->user_id,
            'abandoned_order_id' => $abandoned->id,
            'pagseguro_method_code' => $request->get('pagseguro_method_code'),
            'total' => $abandoned->total,
            'sub_total' => $abandoned->total,
            'payment_link' => 'http://google.com.br'
        ]);

        $order_item = OrderItem::create([
            'name' => $module->name,
            'order_id' => $order->id,
            'ref' => $module->id,
            'value' => $module->value,
            'quantity' => 1
        ]);


        $historical = OrderHistoricalStatus::create([
            'order_id' => $order->id,
            'order_status_id' => 1
        ]);

        return redirect()->route('fatorcms.order.show', $order->id)->with('success', 'O pedido foi gerado com sucesso.');
    }

}