<?php namespace Modules\Order\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderSearchRequest extends FormRequest
{

    /**
     *
     * @apiVersion     0.0.1
     * @api            {none} authorize OrderSearchRequest:authorize
     * @apiGroup       Order
     * @apiSuccess {Boolean} true
     * @apiError {Boolean} false
     * @apiDescription Caso exista um erro no request de filtros é retornado para a página que fez a requisição com o
     *                 erros
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *
     * @apiVersion 0.0.1
     * @api        {none} rules OrderSearchRequest:rules
     *
     * @apiGroup   Order
     * @apiParam {string} created_at Caso seja passado algum valor é obrigatório o Formato y-m-d
     * @apiParam {string} updated_at Caso seja passado algum valor é obrigatório o Formato y-m-d
     * @apiParam {integer} order_id Caso seja passado algum valor é obrigatório ser inteiro
     * @apiParam {integer} order_status Caso seja passado algum valor é obrigatório ser inteiro
     * @apiParam {integer} user_id Caso seja passado algum valor é obrigatório ser inteiro
     *
     * @return array
     */
    public function rules()
    {
        return [
            'created_at'   => 'date_format:d-m-Y',
            'updated_at'   => 'date_format:d-m-Y',
            'order_id'     => 'integer',
            'order_status' => 'integer',
            'user_id'      => 'integer'
        ];
    }

    /**
     *
     * @apiVersion     0.0.1
     * @api            {none} messages OrderSearchRequest:messages
     * @apiGroup       Order
     * @apiDescription Caso seja necessário editar as mensagens de erro da validação dos filtros, ajustar diretamente
     *                 na tradução
     *
     * @return array
     */
    public function messages()
    {
        return [
            'order_id.integer'       => trans('order::db.validation.order_id'),
            'order_status.integer'   => trans('order::db.validation.order_status'),
            'user_id.integer'        => trans('order::db.validation.user_id'),
            'created_at.date_format' => trans('order::db.validation.created_at'),
            'updated_at.date_format' => trans('order::db.validation.updated_at')
        ];
    }

}
