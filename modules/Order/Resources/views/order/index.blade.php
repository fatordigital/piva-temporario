@extends('layouts.cms')

@section('content')


    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a href="{!! URL::route('fatorcms.coupon.create') !!}"
               class="btn btn-success pull-right">
                {!! trans('dictionary.coupon') !!}
            </a>
            <h2>{!! trans('dictionary.order_v') !!}</h2>
        </div>
        <div class="ibox-content">
            {!! Form::open(['url' => 'fatorcms/order/search', 'method' => 'GET']) !!}
            <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group {!! ($errors->first('order_id')) ? 'has-error' : ''  !!}">
                            <label class="control-label"
                                   for="order_id">
                                {!! trans('dictionary.order_id') !!}
                            </label>
                            {!! Form::text('order_id', Input::get('order_id'), ['id' => 'order_id', 'data-column-id' => '0', 'name' => 'order_id', 'class' => 'form-control', 'placeholder' => trans('dictionary.order_id')]) !!}
                            {!! $errors->first('order_id', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group {!! ($errors->first('order_status')) ? 'has-error' : ''  !!}">
                            <label class="control-label"
                                   for="status">
                                {!! trans('dictionary.order_status.title') !!}
                            </label>
                            {!! Form::select('order_status', ['' => trans('dictionary.select', ['name' => trans('dictionary.order_status.title')])] + $orderStatus, Input::get('order_status'), [ 'class' => 'form-control']) !!}
                            {!! $errors->first('order_status', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group {!! ($errors->first('user_id')) ? 'has-error' : ''  !!}">
                            <label class="control-label"
                                   for="customer">
                                {!! trans('dictionary.user') !!}
                            </label>
                            {!! Form::select('user_id', ['' => trans('dictionary.select', ['name' => trans('dictionary.user')])] + $users, Input::get('user_id'), [ 'data-column-id' => '1', 'class' => 'form-control select']) !!}
                            {!! $errors->first('user_id', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group {!! ($errors->first('created_at')) ? 'has-error' : ''  !!}">
                            <label class="control-label"
                                   for="date_added">
                                {!! trans('dictionary.created_at') !!}
                            </label>
                            <div class="input-group date">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                {!! Form::text('created_at', Input::get('created_at'), ['class' => 'form-control date-mask-us', 'maxlength' => '10', 'placeholder' => 'Todos os dias']) !!}
                            </div>
                            {!! $errors->first('created_at', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group {!! ($errors->first('updated_at')) ? 'has-error' : ''  !!}">
                            <label class="control-label"
                                   for="date_modified">
                                {!! trans('dictionary.updated_at') !!}
                            </label>
                            <div class="input-group date">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                {!! Form::text('updated_at', Input::get('updated_at'), ['class' => 'form-control date-mask-us', 'placeholder' => 'Todos os dias', 'maxlength' => '10']) !!}
                            </div>
                            {!! $errors->first('updated_at', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group {!! ($errors->first('product')) ? 'has-error' : ''  !!}">
                            <label class="control-label"
                                   for="product">
                                Curso
                            </label>
                            {!! Form::select('product', ['' => 'Selecione o curso que procura'] + \Modules\Course\Entities\Course::lists('name', 'id')->toArray(), Input::get('product'), ['class' => 'form-control']) !!}
                            {!! $errors->first('product', '<span class="help-block">:message</span>') !!}
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <br />
                            <button type="submit" class="btn btn-info" title="{!! trans('dictionary.search') !!}">
                                <i class="fa fa-search"></i> Procurar
                            </button>
                            <a href="{!! URL::route('fatorcms.order.index') !!}" title="{!! trans('dictionary.clear') !!}" class="btn btn-success">
                                <i class="fa fa-eraser"></i> Limpar
                            </a>
                            <a href="{!! URL::route('order.make.manual') !!}" title="Criar pedido manualmente" class="btn btn-default">
                                <i class="fa fa-plus"></i> Gerar pedido
                            </a>
                            <a href="{!! route('fatorcms.order.index', 'export=true&' . request()->getQueryString()) !!}" class="btn btn-primary">
                                Exportar
                            </a>
                        </div>
                    </div>

                </div>
            {!! Form::close() !!}

            <div class="table-responsive">
                <table id="commerceTable"
                       class="table table-bordered">
                    <thead>
                    <tr>
                        <th data-ref="id">{!! trans('dictionary.order_id') !!}</th>
                        <th data-ref="user_id">{!! trans('dictionary.user') !!}</th>
                        <th data-ref="coupon">Status</th>
                        <th data-ref="discount">{!! trans('dictionary.discount') !!}</th>
                        <th data-ref="sub_total">{!! trans('dictionary.sub_total') !!}</th>
                        <th data-ref="total">{!! trans('dictionary.total') !!}</th>
                        <th data-ref="notification_email">{!! trans('dictionary.notified') !!}</th>
                        <th data-ref="created_at">{!! trans('dictionary.created_at') !!}</th>
                        <th data-ref="updated_at">{!! trans('dictionary.updated_at') !!}</th>
                        <th width="11%">{!! trans('dictionary.options') !!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($list->count())
                        @foreach($list as $row)
                            <tr>
                                <td>{!! $row->id !!}</td>
                                <td>{!! (!is_null($row->user)) ? $row->user->name . ' - ' . $row->user->email  : 'USUÁRIO NÃO EXISTENTE' !!}</td>
                                <td>{!! (!is_null($row->orderStatus)) ? $row->orderStatus->name : 'DADOS INDISPONÍVEIS' !!}</td>
                                <td>{!! $row->discount !!}</td>
                                <td>{!! $row->sub_total !!}</td>
                                <td>{!! $row->total !!}</td>
                                <td>{!! $row->notification_email !!}</td>
                                <td>{!! \Carbon\Carbon::parse($row->created_at)->format('d/m/Y H:i:s') !!}</td>
                                <td>{!! \Carbon\Carbon::parse($row->updated_at)->format('d/m/Y H:i:s') !!}</td>
                                <td>
                                    <a href="{!! URL::route('fatorcms.order.show', $row->id) !!}" class="btn btn-success" title="{!! trans('dictionary.view') !!}">
                                        <i class="fa fa-align-justify"></i>
                                    </a>
                                </td>
                            </tr>
                            @if ($row->OrderItems)
                                <tr class="bg-danger">
                                    <td colspan="5">Curso</td>
                                    <td colspan="4">Valor</td>
                                    <td>Quantidade</td>
                                </tr>
                                @foreach($row->OrderItems as $item)
                                    <tr class="bg-primary">
                                        <td colspan="5">{!! $item->name !!}</td>
                                        <td colspan="4">{!! $item->value !!}</td>
                                        <td>{!! $item->quantity !!}</td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @include('layouts.elements.paginator', ['paginator' => $list])
            </div>

        </div>
    </div>
@endsection
