@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Novo Pedido</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li>
                <a href="{!! route('fatorcms.order.index') !!}">Pedidos</a>
            </li>
            <li class="active">
                <strong>Novo Pedido</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <p>
        Todos os campos com <strong class="text-danger">(*)</strong> são obrigatórios.
      </p>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.team.index') !!}"
           class="btn btn-warning">
            Voltar
        </a>
    </div>
  </div>
@endsection

@section('content')

  <div class="row">
    <div class="col-lg-12">
        <div class="ibox-title">
            <h4 class="panel-title">
                {!! trans('dictionary.create') !!} Pedido Manualmente
            </h4>
            <p>{!! trans('messages.form.alert.fields_required') !!}</p>
        </div>
    </div>
    {!! Form::open(['route' => 'order.store.manual']) !!}
    <div class="col-lg-12">
          <div class="ibox">
              <div class="ibox-content  float-e-margins">
                  <div class="form-group {!! ($errors->first('user_id')) ? 'has-error' : ''  !!}">
                      <label class="control-label"
                             for="customer">
                          Usuário <strong class="text-danger">(*)</strong>
                      </label>
                    {!! Form::select('user_id', ['' => trans('dictionary.select', ['name' => 'Usuário'])] + $users, Input::get('user_id'), [
                    'class' => 'form-control select', 'required']) !!}
                    {!! $errors->first('user_id', '<span class="help-block">:message</span>') !!}
                  </div>

                  <div class="form-group">
                      <label for="module_id">
                          Turma
                      </label>
                    {!! Form::select('team_id', ['' => 'Selecione a Turma'] + \Modules\Team\Entities\Team::all()->lists('name', 'id')->toArray(), Input::get('team_id'), ['class' => 'form-control select']) !!}
                    {!! $errors->first('team_id', '<span class="help-block">:message</span>') !!}
                  </div>

                  <div class="row">
                      <div class="col-lg-6">
                          <div class="form-group">
                              <label for="">
                                  Valor
                              </label>
                              {!! Form::text('value', null, ['class' => 'form-control money', 'placeholder' => '0,00']) !!}
                              {!! $errors->first('value', '<span class="help-block">:message</span>') !!}
                          </div>
                      </div>
                      <div class="col-lg-6">
                          <div class="form-group">
                              <label for="">
                                  Valor de desconto
                              </label>
                              {!! Form::text('discount', null, ['class' => 'form-control money', 'placeholder' => '0,00']) !!}
                              {!! $errors->first('discount', '<span class="help-block">:message</span>') !!}
                          </div>
                      </div>
                  </div>

                  <div class="form-group">
                      <label for="module_id">
                          Forma de pagamento <strong class="text-danger">(*)</strong>
                      </label>
                    {!! Form::select('pagseguro_method_code', ['' => 'Selecione a forma de pagamento'] + \Modules\Order\Entities\PagseguroCode::all()->lists('name', 'code')->toArray(), Input::get('team_id'), ['class' => 'form-control select', 'required']) !!}
                    {!! $errors->first('pagseguro_method_code', '<span class="help-block">:message</span>') !!}
                  </div>

                  <div class="form-group">
                      <button type="submit"
                              class="btn btn-success pull-right">
                          Criar Pedido
                      </button>
                      <div class="clearfix"></div>
                  </div>

              </div>
          </div>
      </div>
  </div>
  {!! Form::close() !!}
@endsection