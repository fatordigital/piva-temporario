@extends('layouts.cms')

@section('content')
  <div class="title">
        <h3>Pedido #{!! $show->id !!}</h3>
    </div>
  <div class="row">
        <div class="col-lg-12">
            <div class="ibox ibox-content">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Nome: </label>
                      {!! !is_null($show->user) ? $show->user->name : 'USUÁRIO NÃO EXISTENTE' !!}
                    </div>
                    <div class="form-group">
                        <label>Cupom: </label>
                      {!! ($show->coupon != "") ? $show->coupon : 'Nenhum' !!}
                    </div>
                    <div class="form-group">
                        <label>Desconto: </label>
                      {!! $show->discount !!}
                    </div>
                    <div class="form-group">
                        <label>Sub Total: </label>
                        R$ {!! bcoToCoin($show->sub_total) !!}
                    </div>
                    <div class="form-group">
                        <label>Total: </label>
                        R$ {!! bcoToCoin($show->total) !!}
                    </div>
                </div>

                <div class="col-lg-6">

                    <div class="form-group">
                        <label>FORMA DE PAGAMENTO: {!! $show->pagseguro ? $show->pagseguro->name : 'Não definido' !!}
                        @if ($show->pagseguro)
                          <img src="{!! $show->pagseguro->image !!}"
                               alt="{!! $show->pagseguro->name !!}"/></label>
                        @endif
                      @if ($show->payment_link != "")
                        <a href="{!! $show->payment_link !!}"
                           target="_blank"
                           class="btn btn-xs btn-success">MANUALMENTE</a>
                      @endif
                    </div>

                    <div class="form-group">
                      <label>Data e hora da transação: </label>
                      {!! $show->Created !!}
                    </div>

                    <div class="form-group">
                        <label>Status do Pedido: </label>
                        <strong class="text-success">{!! isset($show->orderStatus) && !is_null($show->orderStatus) ? $show->orderStatus->name : 'MANUALMENTE' !!}</strong>
                    </div>

                  {!! Form::open(['route' => ['order.status.update', $show->id]]) !!}
                  <div class="form-group">
                            <label>Alterar Status: </label>
                    {!! Form::select('pagseguro_status', $statuses, $show->pagseguro_status, ['class' => 'form-control']) !!}
                    <br>
                            <button type="submit"
                                    class="pull-right btn btn-success">Atualizar Pedido</button>
                        </div>
                  {!! Form::close() !!}
                </div>

                <div class="clearfix"></div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="ibox ibox-content">
                <h3>Itens/Produtos</h3>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td>Item/Produto</td>
                            <td>Link</td>
                            <td>Valor</td>
                            <td>Valor promoção</td>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($show->orderItems->count())
                          @foreach($show->orderItems as $item)
                            @if ($item->Item)
                              <tr>
                                  <td>{!! $item->Item->name !!}</td>
                                  <td><a target="_blank" href="{!! route('cursos.show',  $item->Item->slug) !!}">{!! route('cursos.show',  $item->Item->slug) !!}</a></td>
                                  <td>R$ {!! bcoToCoin($item->Item->value) !!}</td>
                                  <td>R$ {!! bcoToCoin($item->Item->value_discount) !!}</td>
                              </tr>
                            @endif
                          @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="ibox ibox-content">
                <h3>Histórico deste pedido</h3>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td>Status</td>
                            <td>Data do Pedido</td>
                            <td>Data de Alteração</td>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($show->historical->count())
                          @foreach($show->historical as $historical)
                            <tr>
                                    <td>{!! !is_null($historical->order_statuses) ? $historical->order_statuses->name : '' !!}</td>
                                    <td>{!! $historical->Created !!}</td>
                                    <td>{!! $historical->Updated !!}</td>
                                </tr>
                          @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection