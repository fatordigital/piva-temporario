<div class="ibox float-e-margins">
  <div class="ibox-content">

    <div class="row">
      <div class="col-lg-6">
          <div class="form-group">
              <label for="status" class="control-label">
              {!! trans('dictionary.status') !!}
              </label>
              <br/>

              <div class="radio radio-success radio-inline">
                  {!! Form::radio('status', 1, true) !!}
                <label for="radio4">
                      {!! trans('dictionary.status_text.active') !!}
                  </label>
              </div>
              <div class="radio radio-danger radio-inline">
                  {!! Form::radio('status', 0, false) !!}
                <label for="radio4">
                      {!! trans('dictionary.status_text.inactive') !!}
                  </label>
              </div>
          </div>
          <div class="form-group">
              <label for="coupon_key">{!! trans('dictionary.coupon_key') !!} (*)</label>
              <div class="input-group {!! ($errors->first('coupon_key')) ? 'has-error' : '' !!}">
                {!! Form::text('coupon_key', null, ['class' => 'form-control', 'placeholder' => trans('dictionary.coupon_key')]) !!}
                {!! $errors->first('coupon_key', '<span class="help-block">:message</span>') !!}
                <span class="input-group-btn">
                    <button type="button" class="btn btn-primary" id="generator">
                        {!! trans('dictionary.generate_code') !!}
                    </button>
                  </span>
              </div>
          </div>
          <div class="form-group {!! ($errors->first('percentage')) ? 'has-error' : '' !!}">
              <p class="font-bold">
                  {!! trans('dictionary.percentage') !!} ({!! trans('dictionary.optional') !!})
              </p>
              {!! Form::text('percentage', null, ['class' => 'form-control', 'id' => 'percentage', 'placeholder' => 'Exemplo: 10.25%']) !!}
          </div>

          <div class="form-group {!! ($errors->first('value')) ? 'has-error' : '' !!}">
              <label for="value">
                  {!! trans('dictionary.value') !!} ({!! trans('dictionary.optional') !!})
              </label>
            {!! Form::text('value', null, ['class' => 'form-control money value', 'placeholder' => 'Exemplo: 200,00']) !!}
            {!! $errors->first('value', '<span class="help-block">:message</span>')  !!}
          </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group {!! ($errors->first('quantity')) ? 'has-error' : '' !!}">
            <label for="quantity">
                {!! trans('dictionary.quantity') !!} (*)
            </label>
          {!! Form::text('quantity', isset($edit) ? null : 1, ['class' => 'form-control', 'placeholder' => trans('dictionary.quantity')]) !!}
          {!! $errors->first('quantity', '<span class="help-block">:message</span>')  !!}
        </div>

        <div class="form-group">
            <label class="font-noraml">
                {!! trans('dictionary.range_date') !!} ({!! trans('dictionary.optional') !!})
            </label>
            <div class="input-daterange input-group" id="range_date">
                {!! Form::text('begin_date', null, ['class' => 'input-sm form-control s-datepicker', 'placeholder' => date('d/m/Y')]) !!}
              <span class="input-group-addon">{!! trans('dictionary.to') !!}</span>
              {!! Form::text('end_date', null, ['class' => 'input-sm form-control s-datepicker', 'placeholder' => date('d/m/Y')]) !!}
            </div>
        </div>

        <div class="form-group">
            <label for="time">
                {!! trans('dictionary.time') !!} ({!! trans('dictionary.optional') !!})
            </label>
            <div class="input-group clockpicker"
                 data-autoclose="true">
                {!! Form::text('time', null, ['class' => 'form-control', 'placeholder' => trans('dictionary.time')]) !!}
              <span class="input-group-addon">
                    <span class="fa fa-clock-o"></span>
                </span>
            </div>
        </div>

        <div class="form-group {!! ($errors->first('coupon_permissions[user_id]')) ? 'has-error' : '' !!}">
            <label for="coupon_permissions[user_id]">
                Usuário ({!! trans('dictionary.optional') !!})
            </label>
          {!! Form::select('coupon_permissions[user_id]', ['' => 'Selecione o usuário'] + $users, null, ['class' => 'form-control select']) !!}
          {!! $errors->first('coupon_permissions.user_id', '<span class="help-block">:message</span>') !!}
        </div>

        <div class="form-group {!! ($errors->first('coupon_permissions[email]')) ? 'has-error' : '' !!}">
            <label for="coupon_permissions[email]">
                E-mail ({!! trans('dictionary.optional') !!})
            </label>
          {!! Form::text('coupon_permissions[email]', null, ['class' => 'form-control', 'placeholder' => 'E-mail', 'autocomplete' => 'off']) !!}
          {!! $errors->first('coupon_permissions.email', '<span class="help-block">:message</span>')  !!}
        </div>
      </div>

      <div class="col-lg-12">
        <hr />
        <div class="form-group">
          <button class="btn btn-success">Salvar</button>
        </div>
      </div>

    </div>

  </div>
</div>



@section('cjs')
  {!! HTML::script('js/cms/coupon/coupon.js') !!}
@stop