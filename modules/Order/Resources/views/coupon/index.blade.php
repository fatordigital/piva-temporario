@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Cupons</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Cupons</strong>
            </li>
        </ol>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.coupon.create') !!}"
           class="btn btn-success">
            {!! trans('dictionary.create') !!} Cupom
        </a>
    </div>
  </div>
@endsection


@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-content">

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th data-ref="coupon_key">{!! trans('dictionary.coupon_key') !!}</th>
                        <th data-ref="percentage">{!! trans('dictionary.percentage') !!}</th>
                        <th data-ref="value">{!! trans('dictionary.value') !!}</th>
                        <th data-ref="quantity">{!! trans('dictionary.quantity') !!}</th>
                        <th data-ref="begin_date">{!! trans('dictionary.begin_date') !!}</th>
                        <th data-ref="end_date">{!! trans('dictionary.end_date') !!}</th>
                        <th data-ref="time">{!! trans('dictionary.time') !!}</th>
                        <th data-ref="status">{!! trans('dictionary.status') !!}</th>
                        <th width="11%">{!! trans('dictionary.options') !!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($list->count())
                        @foreach($list as $row)
                            <tr>
                                <td>{!! $row->coupon_key !!}</td>
                                <td>{!! $row->percentage !!}</td>
                                <td>{!! $row->value !!}</td>
                                <td>{!! $row->quantity !!}</td>
                                <td>{!! $row->Begin !!}</td>
                                <td>{!! $row->End !!}</td>
                                <td>{!! $row->time !!}</td>
                                <td>{!! $row->StatusText !!}</td>
                                <td>
                                    <a class="btn btn-primary btn-xs pull-left margin-right"
                                       href="{!! URL::route('fatorcms.coupon.edit', $row->id) !!}">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    {!! Form::open(['route' => ['fatorcms.coupon.status', $row->id], 'method' => 'PATCH', 'class' => 'form-horizontal pull-left']) !!}
                                    <button class="btn {!! ($row->status) ? 'btn-success' : 'btn-danger' !!} btn-xs margin-right"
                                            title="{!! $row->StatusText !!}">
                                        <i class="glyphicon {!! ($row->status) ? 'glyphicon-ok' : 'glyphicon-off' !!}"></i>
                                    </button>
                                    {!! Form::close() !!}

                                    {!! Form::open(['route' => ['fatorcms.coupon.destroy', $row->id], 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                                    <button class="btn btn-danger btn-xs">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @else
                      <tr>
                        <td colspan="9">Nenhum cupom registrado no momento.</td>
                      </tr>
                    @endif
                    </tbody>
                </table>
                @include('layouts.elements.paginator', ['paginator' => $list])
            </div>

        </div>
    </div>
@endsection