<?php namespace Modules\Certified\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use App\User;
use Modules\Certified\Entities\Certified;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamUser;

class CertifiedController extends BaseModuleController
{

    public function index()
    {
        return view('certified::index', [
            'list' => Certified::orderBy('created_at', 'desc')->paginate(15)
        ]);
    }


    public function create()
    {
        return view('certified::create');
    }

    public function store()
    {
        if (request()->has('team_id') || request()->has('user_id')) {
            if (Certified::create(request()->except(['_token']))) {
                return redirect()->route('fatorcms.certified.index')->with('success', 'O Certificado foi gerado com sucesso e está disponível na área do aluno.');
            } else {
                return redirect()->route('fatorcms.certified.create')->with('error', 'Falha ao criar o certificado, verifique se todos os campos foram preenchidos corretamente.')->withInput(request()->except(['_token']));
            }
        }
        return redirect()->route('fatorcms.certified.create')->with('error', 'Falha ao criar o certificado, verifique se todos os campos foram preenchidos corretamente.')->withInput(request()->except(['_token']));
    }

    public function show($id, Certified $model)
    {
        $show = $model->find($id);
        if (!$show)
            return redirect()->route('fatorcms.certified.index')->with('error', 'Desculpe, o registro não foi encontrado.');

        $team = $show->team_id ? Team::find($show->team_id)->toArray() : Team::first()->toArray();
        $certified = Certified::changeCommands($show->certified, auth()->user()->toArray(), $team);

        return view('certified::show', compact('show', 'certified'));
    }

    public function edit($id, Certified $model)
    {
        $edit = $model->find($id);
        if (!$edit)
            return redirect()->route('fatorcms.certified.index')->with('error', 'Desculpe, o registro não foi encontrado.');
        return view('certified::edit', compact('edit'));
    }

    public function update($id, Certified $model)
    {
        if (request()->has('team_id') || request()->has('user_id')) {

            $edit = $model->find($id);
            if (!$edit)
                return redirect()->route('fatorcms.certified.index')->with('error', 'Desculpe, o registro não foi encontrado.');

            if ($edit->update(request()->except(['_token']))) {
                return redirect()->route('fatorcms.certified.index')->with('success', 'O Certificado foi atualizado com sucesso.');
            } else {
                return redirect()->route('fatorcms.certified.create')->with('error', 'Falha ao criar o certificado, verifique se todos os campos foram preenchidos corretamente.')->withInput(request()->except(['_token']));
            }
        }
        return redirect()->route('fatorcms.certified.create')->with('error', 'Falha ao criar o certificado, verifique se todos os campos foram preenchidos corretamente.')->withInput(request()->except(['_token']));
    }

    public function destroy($id, Certified $model)
    {
        $destroy = $model->find($id);
        if (!$destroy)
            return redirect()->route('fatorcms.certified.index')->with('error', 'Desculpe, o registro não foi encontrado.');
        if ($destroy->delete()) {
            return redirect()->route('fatorcms.certified.index')->with('success', 'O Certificado foi removido com sucesso.');
        }
        return redirect()->route('fatorcms.certified.index')->with('error', 'Desculpe, houve uma falha ao deletar o registro, tente novamente.');
    }

}