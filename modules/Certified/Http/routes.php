<?php

    Route::group([
        'prefix'     => 'fatorcms',
        'middleware' => [
            'acl',
            'auth',
            'cms'
        ],
        'namespace'  => 'Modules\Certified\Http\Controllers'
    ], function () {
        Route::resource('certified', 'CertifiedController');
    });