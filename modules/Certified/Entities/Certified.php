<?php namespace Modules\Certified\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Certified extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["team_id", "user_id", "certified"];

    public static function changeCommands($certified, $user, $team)
    {
        $certified = str_replace(
            array(
                '{teams.name}',
                '{users.name}',
                '{users.email}',
                '{user_socializes.national_id}',
                '{user_socializes.postal_code}',
                '{teams.begin_date}',
                '{teams.end_date}'
            ),
            array(
                isset($team['name']) ? $team['name'] : '',
                isset($user['name']) ? $user['name'] : '',
                isset($user['email']) ? $user['email'] : '',
                isset($user['national_id']) ? $user['national_id'] : '',
                isset($team['postal_code']) ? $team['postal_code'] : '',
                isset($team['begin_date']) ? $team['begin_date'] : '',
                isset($team['end_date']) ? $team['end_date'] : '',
            ), $certified);
        return $certified;
    }

    public function team()
    {
        return $this->belongsTo('Modules\Team\Entities\Team', 'team_id');
    }


    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'user_id');
    }
}