@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-9">
      <h2>Certificados</h2>
      <ol class="breadcrumb">
          <li>
              <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
          </li>
          <li class="active">
              <strong>Certificados</strong>
          </li>
      </ol>
  </div>
</div>

  <div class="row page-heading white-bg">
  <div class="col-lg-9">
      <br />
      <a href="{!! URL::route('fatorcms.certified.create') !!}" class="btn btn-success">
          {!! trans('dictionary.create') !!} Certificado
      </a>
  </div>
</div>
@endsection

@section('content')

  <div class="col-lg-12">
    <div class="ibox-content table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <td>Turma</td>
            <td>Usuário</td>
            <td>Data/Hora</td>
            <td width="8%">Opções</td>
          </tr>
        </thead>
        <tbody>
          @if (isset($list))
            @foreach($list as $item)
              <tr>
                <td>{!! $item->team ? $item->team->name : '' !!}</td>
                <td>{!! $item->user ? $item->user->name : '' !!}</td>
                <td>{!! $item->Created !!}</td>
                <td>
                  <a href="{!! route('fatorcms.certified.edit', $item->id) !!}"
                     class="btn btn-xs no-rounded btn-primary pull-left">
                    <i class="fa fa-pencil"></i>
                  </a>
                  <a href="{!! route('fatorcms.certified.show', $item->id) !!}"
                     data-title="title"
                     title="Visualizar como ficou o certificado"
                     class="btn btn-xs btn-success no-rounded pull-left">
                    <i class="fa fa-eye"></i>
                  </a>
                  {!! Form::open(['route' => ['fatorcms.certified.destroy', $item->id], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deletar este certificado?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                  <button data-toggle="tooltip" title="Excluir Certificado" class="btn btn-danger no-rounded btn-xs pull-left">
                    <i class="fa fa-trash-o"></i>
                  </button>
                  {!! Form::close() !!}
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="4">Nenhum registro encontrado</td>
            </tr>
          @endif
        </tbody>
      </table>
    </div>
  </div>

@stop