<div class="modal inmodal fade" id="commands" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title">Ajuda do sistema</h4>
                  <p class="text-success">Abaixo é possível ver comandos e exemplos de uso.</p>
              </div>

              <div class="modal-body">

                <ins>Observação:. O comando é um atalho, caso seja enviado para diversos usuários não se preocupe, os comandos irão ser substituído de acordo com cada usuário.</ins>

                <h3>Comandos</h3>

                <ul class="list-group">
                  <li class="list-group-item">{users.name} - Nome do usuário</li>
                  <li class="list-group-item">{users.email} - E-mail do usuário</li>
                  <li class="list-group-item">{user_socializes.national_id} - CPF do usuário</li>
                  <li class="list-group-item">{user_socializes.phone} - Telefone do usuário</li>
                  <li class="list-group-item">{user_socializes.postal_code} - CEP do usuário</li>
                  <li class="list-group-item">{teams.begin_date} - Data de início da turma</li>
                  <li class="list-group-item">{teams.end_date} - Data final da turma</li>
                </ul>

                <h3>Exemplos</h3>
                <p class="text-success">Os exemplos são baseados na mensagem do envio.</p>
                <code>
                  Olá {usuario.nome}, <br />
                  Aviso que semana que vem irá ser efetuado o simulado para que possamos ver como você se saiu no curso.
                </code>
                <div class="clearfix"></div> <br /><br />
                <code>
                  Bom dia {users.name},<br />
                  Apenas para te lembrar que a sua turma encerra {teams.begin_date}.<br /> Esperamos que você tenha gostado do nosso curso e que seu conhecimento tenha se aprimorado com ele. <br />

                  A sua turma teve início no dia: {teams.begin_date} e finaliza {teams.end_date} e é conhecida como {teams.name} <br />
                  Agradecemos por tudo professorpiva
                </code>

              </div>

              <div class="modal-footer">
                  <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
              </div>
          </div>
      </div>
  </div>