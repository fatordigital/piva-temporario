@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Visualizando Certificado</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Visualizando Certificado</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <p>
        <strong class="text-danger">Atenção: Isto é apenas um exemplo de como ficou o certificado criado</strong>
      </p>
    </div>
  </div>

  <div class="row page-heading white-bg">
  <div class="col-lg-9">
      <br />
      <a href="{!! route('fatorcms.certified.index') !!}" class="btn btn-warning">
          Voltar
      </a>
      <a href="{!! URL::route('fatorcms.certified.create') !!}" class="btn btn-success">
          {!! trans('dictionary.create') !!} Certificado
      </a>
      <a href="{!! URL::route('fatorcms.certified.create') !!}" class="btn btn-primary">
          <i class="fa fa-pencil"></i> Editar Certificado
      </a>
  </div>
</div>
@endsection

@section('content')

  <div class="row">
    <div class="col-lg-12">
      <div class="ibox-content">
        {!! $certified !!}
      </div>
    </div>
  </div>

@endsection