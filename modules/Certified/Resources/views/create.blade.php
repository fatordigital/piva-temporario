@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Novo Certificado</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li>
                <a href="{!! route('fatorcms.certified.index') !!}">Certificados</a>
            </li>
            <li class="active">
                <strong>Novo Certificado</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <p>
        Todos os campos com <strong class="text-danger">(*)</strong> são obrigatórios.
      </p>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! route('fatorcms.certified.index') !!}" class="btn btn-warning">
            Voltar
        </a>
        <a href="#" class="btn btn-primary" data-target="#commands" data-toggle="modal">
           <i class="fa fa-cogs"></i> Comandos
        </a>
    </div>
  </div>
@endsection

@section('content')

  <div class="row">
    {!! Form::open(['route' => 'fatorcms.certified.store']) !!}
    @include('certified::form')
    {!! Form::close() !!}
  </div>


@endsection