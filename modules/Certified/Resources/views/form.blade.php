<div class="col-lg-12">
  <div class="ibox-content">

    <div class="row">

      <div class="col-lg-6">
        <div class="form-group{!! $errors->first('team_id') ? ' has-error' : '' !!}">
          <label for="team_id">
            <i class="fa fa-question-circle" data-title="title" title="Ao selecionar a turma, irá enviar para todos os usuários que compraram a mesma."></i> Turma <strong class="text-success">(Opcional)</strong>
          </label>
          {!! Form::select('team_id', ['' => 'Selecione a turma para enviar o certificado'] + \Modules\Team\Entities\Team::lists('name', 'id')->toArray(), null, ['class' => 'form-control select']) !!}
          {!! $errors->first('team_id', '<span class="help-block text-danger">:message</span>') !!}
        </div>
      </div>

      <div class="col-lg-6">
        <div class="form-group{!! $errors->first('user_id') ? ' has-error' : '' !!}">
          <label for="user_id">
           Usuário <strong class="text-success">(Opcional)</strong>
          </label>
          {!! Form::select('user_id', ['' => 'Selecione o usuário que irá receber o certificado'] + \Modules\User\Entities\User::lists('name', 'id')->toArray(), null, ['class' => 'form-control select']) !!}
          {!! $errors->first('user_id', '<span class="help-block text-danger">:message</span>') !!}
        </div>
      </div>

      <div class="col-lg-12">
        <div class="form-group{!! $errors->first('certified') ? ' has-error' : '' !!}">
          <label for="certified">
            <i class="fa fa-question-circle"></i> Certificado <strong class="text-danger">(*)</strong>
          </label>
          {!! Form::textarea('certified', '
          <table cellspacing="0" cellpadding="0" border="0" width="100%" style="font-family: \'Arial\', \'Tahoma\', sans-serif">
              <tr>
                  <td valign="top" align="center">
                      <table cellspacing="0" cellpadding="0" border="0" width="100%" style="background-color: #E6E7E2;width: 1150px;">
                          <tr>
                              <td valign="top" align="center" style="padding: 0 110px 50px;">
                                  <table cellspacing="0" cellpadding="0" border="0" width="900" style="background-color: #E6E7E2;">
                                      <tr>
                                          <td colspan="2" style="background:#34A1B6; height:50px"></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2" style="background-color:#5CB3C6; height: 19px;border-top: 2px solid #BDF7FF;"></td>
                                      </tr>
                                      <tr>
                                          <td align="center" colspan="2">
                                              <h1 style="color: #046A9B;font-size: 70px;font-family: sans-serif;font-weight: 800;">CERTIFICADO</h1>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td colspan="2" align="center">
                                              <p style="color: #131313; font-size: 28px;text-align:center;width:800px"><strong>Professor Piva</strong>,confere a <strong>{users.name}</strong>, o presente certificado, referente à sua participação no curso EAD de {teams.name} ministrado pela professora Mirian Ferrazzi no período de 12/01/2016 a 23/03/2016 com duração de 240 horas.</p>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td colspan="2" align="center">
                                              <p style="color: #0E6FA4"><strong>Porto Alegre, '.date('d').' de '.date('M').' de '.date('Y').'</strong></p>
                                          </td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>
                      </table>
                  </td>
              </tr>
          </table>
          ', ['class' => 'summernote']) !!}
          {!! $errors->first('certified', '<span class="help-block text-danger">:message</span>') !!}
        </div>
      </div>

      <div class="col-lg-12">
        <div class="form-group">
          <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
      </div>

    </div>

  </div>
</div>

@include('certified::modal')