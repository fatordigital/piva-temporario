<?php

    Route::group([
        'prefix'     => 'fatorcms',
        'middleware' => [
            'acl',
            'auth',
            'cms'
        ],
        'namespace'  => 'Modules\News\Http\Controllers'
    ], function () {
        Route::resource('news', 'NewsController');
    });