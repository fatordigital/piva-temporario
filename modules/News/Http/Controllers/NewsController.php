<?php namespace Modules\News\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Illuminate\Support\Facades\File;
use Modules\News\Entities\News;
use Modules\News\Http\Requests\NewsRequest;

class NewsController extends BaseModuleController
{

    public function index(News $model)
    {
        $list = $model;
        if (request()->has('search')) {
            foreach(request()->get("search") as $key => $value) {
                if (!empty($value) && $key != 'type') {
                    $list->where($key, 'LIKE', '%' . $value . '%');
                } else if ($key == 'type' && !empty($value)) {
                    $list->where($value, '=', true);
                }
            }
        }
        if (request()->has('date'))
            $list->orderBy('created_at', request('date'));
        $list = $list->paginate(15);
        return view('news::news.index', compact('list'));
    }

    public function create()
    {
        return view('news::news.create');
    }

    public function store(NewsRequest $request, News $model)
    {
        if ($request->hasFile('filename')) {
            $filename = $this->customFileName($request, 'assets/news/', 'filename');
            if ($request->file('filename')->move('assets/news/', $filename)) {
                $request->merge(['image' => $filename]);
            }
        }
        if ($store = $model->customCreate($request)) {
            n(route('fatorcms.news.edit', $store->id), 'Nova notícia', 'A Notícia ' . $store->name . ', foi criada por ' . userHistorical());
            return redirect()->route('fatorcms.news.index')->with('success', 'A notícia foi criada com sucesso.');
        }
        return redirect()->back()->with('error', 'Ops, não foi possível criar a notícia, tente novamente.');
    }

    public function edit($slug, News $model)
    {
        $edit = $model->whereSlug($slug)->first();
        $edit->begin_date = $edit->Begin;
        $edit->end_date = $edit->End;
        if (!$edit)
            return redirect()->route('fatorcms.news.index')->with('error', 'Ops! Registro não econtrado.');
        return view('news::news.edit', compact('edit'));
    }

    public function update($slug, NewsRequest $request, News $model)
    {
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.news.index')->with('error', 'Ops! Registro não econtrado.');
        else {
            if ($request->hasFile('filename')) {
                $filename = $this->customFileName($request, 'assets/news/', 'filename');
                if ($request->file('filename')->move('assets/news/', $filename)) {
                    $request->merge(['image' => $filename]);
                }
            }
            if ($model->customUpdate($edit, $request)) {
                n(route('fatorcms.news.edit', $slug), 'Notícia atualizada', 'A Notícia ' . $edit->name . ', foi atualizada por ' . userHistorical());
                return redirect()->route('fatorcms.news.index')->with('success', 'O Registro foi atualizado com sucesso.');
            } else {
                return redirect()->route('fatorcms.news.edit', $slug)->with('error', 'Ops! Não foi possível atualizar o registro, tente novamente.');
            }
        }
    }

    public function destroy($slug, News $model)
    {
        $destroy = $model->whereSlug($slug)->first();
        if (!$destroy)
            return redirect()->route('fatorcms.news.index')->with('error', 'Ops! Registro não econtrado.');
        else {
            if (fex('assets/news/', $destroy->image))
                File::delete('assets/news/' . $destroy->image);
            if ($destroy->delete()) {
                n(route('fatorcms.news.index'), 'Notícia deletada', 'A Notícia ' . $destroy->name . ', foi deletado por ' . userHistorical());
                return redirect()->route('fatorcms.news.index')->with('success', 'O Registro foi atualizado com sucesso.');
            } else {
                return redirect()->route('fatorcms.news.index')->with('error', 'Ops! Não foi possível deletar o registro, tente novamente.');
            }
        }
    }

}