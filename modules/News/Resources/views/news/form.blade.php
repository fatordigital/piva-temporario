<div class="row">
  <div class="col-lg-12">
    <div class="form-group">
      <label for="filename">
        <i class="fa fa-question-circle" data-toggle="tooltip" title="Adicione uma imagem de 1280 x 400"></i> Imagem de capa <strong class="text-success">(Opcional)</strong>
      </label>
      <div class="clearfix"></div>
      <label title="Adicionar/editar foto" for="image">
        {!! Form::file('filename', ['class' => 'hide', 'id' => 'image', 'accept' => 'image/jpeg']) !!}
        {!! HTML::image('/img/1468049205_25.Camera-Front.png', 'Adicionar/editar foto') !!}
      </label>
      <div style="overflow: hidden; {!! isset($edit) ? 'height: 400px;' : '' !!}" id="recipe-image">
        @if (!isset($edit))
          <img id="foto" class="img-responsive" style="width: 100%" />
        @else
          @if ($edit->image && fex('/assets/news/', $edit->image))
            {!! HTML::image('assets/news/'.$edit->image, $edit->name, ['id' => 'foto', 'class' => 'img-responsive', 'style' => 'width: 100%;']) !!}
          @endif
        @endif
      </div>
    </div>
  </div>
</div>


<div class="row">

  <div class="col-lg-6">

    <div class="form-group">
        <label for="status" class="control-label">Destacado na página inicial</label>
        <br/>
        <div class="radio radio-success radio-inline">
            {!! Form::radio('principal', 1, true) !!}
          <label for="radio4">
                {!! trans('dictionary.status_text.active') !!}
            </label>
        </div>
        <div class="radio radio-danger radio-inline">
            {!! Form::radio('principal', 0, false) !!}
          <label for="radio4">
                {!! trans('dictionary.status_text.inactive') !!}
            </label>
        </div>
    </div>

    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
      <label for="name">
        Título <strong class="text-danger">(*)</strong>
      </label>
      {!! Form::text('name', null, ['required', 'class' => 'form-control', 'maxlength' => 120]) !!}
      {!! $errors->first('name', '<span class="help-block text-danger">:message</span>') !!}
    </div>

    <div class="form-group {!! ($errors->first('new_category_id]')) ? 'has-error' : '' !!}">
      <label for="new_category_id">
          Categoria <strong class="text-danger">(*)</strong>
      </label>
      <div class="input-group my-group">
          <span class="input-group-btn">
            <button type="button" class="btn btn-default" data-title="title" title="Para criar uma categoria, você precisa deixar o selecionador no estado (Selecione uma categoria existente) e digitar no campo vazio">
              <i class="fa fa-question-circle"></i>
            </button>
          </span>
          {!! Form::text('new_category', null, ['class' => 'form-control', 'placeholder' => 'Digite aqui uma nova categoria']) !!}
          {!! Form::select('new_category_id', ['' => 'Selecione uma categoria existente'] + \Modules\News\Entities\NewCategory::orderBy('name')->lists('name', 'id')->toArray(), null, ['class' => 'form-control']) !!}
      </div>
    </div>

  </div>

  <div class="col-lg-6">

    <div class="form-group{!! $errors->first('redirect') ? ' has-error' : '' !!}">
      <label for="redirect">
        <i class="fa fa-question-circle" data-title="title" title="Tu pode por uma URl para redirecionar o usuário ao um link específico, exemplo:. http://google.com.br/search?q=professorpiva"></i>
        Redirecionar <strong class="text-success">(Opcional)</strong>
      </label>
      {!! Form::text('redirect', null, ['class' => 'form-control', 'maxlength' => 255]) !!}
      {!! $errors->first('redirect', '<span class="help-block text-danger">:message</span>') !!}
    </div>

    <div class="form-group" id="datepicker">
        <label class="font-noraml">
          Data de início e fim da notícia <strong class="text-success">(Opcional)</strong>
        </label>
        <div class="input-daterange input-group" id="datepicker">
          {!! Form::text('begin_date', null, ['autocomplete' => 'off', 'class' => 'form-control', 'style' => 'width:100% !important', 'placeholder' => date('d/m/Y')]) !!}
          <span class="input-group-addon">{!! trans('dictionary.to') !!}</span>
          {!! Form::text('end_date', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => date('d/m/Y')]) !!}
        </div>
    </div>
  </div>

</div>

<div class="form-group">
  <label for="description">
    <i class="fa fa-question-circle" data-toggle="tooltip" title="Tu pode escrever a notícia como quiser nessa área, podendo adicionar imagens e vídeos."></i> Texto da notícia <strong class="text-danger">(*)</strong>
  </label>
  {!! Form::textarea('text', null, ['class' => 'form-control summernote']) !!}
</div>

<div class="form-group">
  <button type="submit" class="btn btn-primary">
    Salvar Registro
  </button>
</div>

@section('cjs')
  <script type="text/javascript">
    $(document).ready(function () {
      $('#image').change(function(e) {
        openFile(e, 'foto');
        $("#recipe-image").height('400');
        var formData = new FormData();
        formData.append('file', $(this)[0].files[0]);
        formData.append('_token', $('meta[name="_token"]').data('content'));
        formData.append('id', $('input[name="id"]').val());
      });
    });
  </script>
@endsection