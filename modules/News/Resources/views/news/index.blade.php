@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Notícias</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Notícias</strong>
            </li>
        </ol>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.news.create') !!}"
           class="btn btn-success">
            {!! trans('dictionary.create') !!} Notícia
        </a>
    </div>
  </div>
@endsection

@section('content')
  @if ($list->count())

    {!! Form::open(['route' => 'fatorcms.news.index', 'method' => 'GET']) !!}
    <div class="row">
      <div class="col-lg-12">
        <div class="col-lg-3">
          <div class="form-group">
            <label for="name">
              Nome/Título
            </label>
            {!! Form::text('search[name]', old('search[name]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
          </div>
        </div>
        <div class="col-lg-3">
          <div class="form-group">
            <label for="name">
              Order por data
            </label>
            {!! Form::select('date', ['' => '', 'asc' => 'Ordem crescente', 'desc' => 'Ordem decrescente'], old('date'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group">
            <label for="name">
              Categoria
            </label>
            <div class="input-group">
              {!! Form::select('search[new_category_id]', ['' => ''] + \Modules\News\Entities\NewCategory::lists('name', 'id')->toArray() , old('search[new_category_id]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">Procurar</button>
              </span>
              <span class="input-group-btn">
                <a class="btn btn-warning" href="{!! route('fatorcms.team.index') !!}">Limpar</a>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
    {!! Form::close() !!}

    <hr />

    <?php $rand = ['success', 'danger', 'warning', 'primary', 'info']; ?>
    <div class="col-lg-12">
        @foreach(\Modules\News\Entities\NewCategory::all() as $category)
          @if ($category->news->count())
            <label data-category-id="{!! $category->id !!}" data-toggle="tooltip" title="Apenas {!! $category->name !!}" class="cursor-pointer label label-{!! $rand[rand(0, 3)] !!}">{!! $category->name !!}</label>
          @endif
        @endforeach
        <label class="label label-default cursor-pointer" data-category-id="*">Todos</label>
    </div>

    <hr />

    @foreach($list as $row)
      <div class="col-lg-3 course{!! !$row->new_category_id ? ' text-danger' : '' !!}" data-category-childrens="{!! $row->new_category_id !!}">
          <label class="label label-primary" data-toggle="tooltip" title="Quantidade de visitas">{!! $row->views !!}</label>
          <div class="contact-box center-version active">
              <a class="link-img-course" style="max-height: 165px; overflow: hidden;" href="{!! route('fatorcms.news.edit', $row->slug) !!}">
                @if(!$row->image || !fex('/assets/news/', $row->image))
                  <img src="{!! url('/images/no-image.jpg') !!}"
                       alt="{!! $row->name !!}" style="width: 100%">
                @else
                  <img src="{!! url('/assets/news/'.$row->image) !!}"
                       alt="{!! $row->name !!}" class="img-responsive">
                @endif
              </a>
              <a href="{!! route('fatorcms.news.edit', $row->id) !!}">
                  <h3 class="m-b-xs"><strong>{!! $row->name !!}</strong></h3>
                  <div class="font-bold{!! !$row->new_category_id ? ' text-danger' : '' !!}">{!! $row->category ? $row->category->name : 'A Notícia não está em uma categoria!' !!}</div>
                  <div class="m-t-md">
                    {!! str_limit(strip_tags($row->text), 50) !!}
                  </div>
              </a>
              <div class="contact-box-footer">
                  <div class="m-t-xs btn-group">
                    <a href="{!! route('fatorcms.news.edit', $row->slug) !!}"
                       class="btn btn-xs btn-primary">
                      <i class="fa fa-pencil"></i>
                    </a>
                    {!! Form::open(['route' => ['fatorcms.news.destroy', $row->slug], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deleta a notícia '. $row->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                    <button data-toggle="tooltip" title="Excluir curso" class="btn no-rounded btn-danger btn-xs">
                        <i class="fa fa-trash-o"></i>
                    </button>
                    {!! Form::close() !!}
                  </div>
              </div>
          </div>
      </div>
    @endforeach
  @endif

  <div class="col-lg-12">
    @include('layouts.elements.paginator', ['paginator' => $list])
  </div>
@endsection

@section('cjs')
  <script type="text/javascript">
    $(document).ready(function(){
      $('[data-category-id]').click(function(e){
        e.preventDefault();
        var $id = $(this).data('categoryId');
        if ($id != '*') {
          $.each($('[data-category-childrens]'), function (key, element) {
            if ($(this).data('categoryChildrens') == $id) {
              $(this).fadeIn();
            } else {
              $(this).fadeOut();
            }
          });
        } else {
          $('[data-category-childrens]').fadeIn('slow');
        }
      });
    });
  </script>
@endsection