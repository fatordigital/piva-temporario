<?php namespace Modules\News\Entities;

use App\BaseModel;

class NewCategory extends BaseModel
{

    protected $fillable = ["parent_id", "name", "slug"];

    public function news()
    {
        return $this->hasMany('Modules\News\Entities\News', 'new_category_id');
    }

}