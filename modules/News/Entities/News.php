<?php namespace Modules\News\Entities;

use App\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ["principal", "views", "image", "text", "user_create_id", "new_category_id", "name", "slug", "redirect", "begin_date", "end_date"];

    protected $table = 'news';

    public function category()
    {
        return $this->belongsTo('Modules\News\Entities\NewCategory', 'new_category_id');
    }

    private function createCategory($name)
    {
        $store = NewCategory::create(['name' => $name, 'slug' => $this->notRepeatModelString(str_slug($name, '-'), new NewCategory())]);
        return $store->id;
    }


    public function customCreate($request)
    {
        if ($request->has('new_category') && $request->get('new_category')) {
            $request->merge(['new_category_id' => $this->createCategory($request->get('new_category'))]);
        }
        $request->merge(['slug' => $this->notRepeatModelString(str_slug($request->get('name'), '-'), new News())]);
        return $this->create($request->except(['_token', 'new_category', 'filename']));
    }

    public function customUpdate($edit, $request)
    {
        if ($request->has('new_category') && $request->get('new_category')) {
            $request->merge(['new_category_id' => $this->createCategory($request->get('new_category'))]);
        }
        return $edit->update($request->except(['_token', 'new_category', 'filename']));
    }

}