<?php namespace Modules\Lesson\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Modules\Lesson\Entities\Material;
use Modules\Lesson\Http\Requests\MaterialRequest;

class MaterialController extends BaseModuleController
{

    public function index(Material $model)
    {
        $list = $model->orderBy('name')->paginate(15);
        return view('lesson::materials.index', compact('list'));
    }

    public function create()
    {
        return view('lesson::materials.create');
    }

    public function store(MaterialRequest $request, Material $model)
    {
        $request->merge(['user_create_id' => \Auth::user()->id, 'slug' => $model->notRepeatModelString(str_slug($request->get('name', 'slug'), '-'), $model)]);
        if ($model->customCreate($request)) {
            n(route('fatorcms.material.edit', $request->get('slug')), 'Novo Material', 'O Material ' . $request->get('name') . ', foi criado recente por ' . userHistorical());
            return redirect()->route('fatorcms.material.index')->with('success', 'O Material foi criado com succeso, agora é possível vincular ele ao uma aula.');
        } else
            return redirect()->back()->with('error', 'Ops, não foi possível criar o material, tente novamente.');
    }

    public function edit(Material $model, $slug)
    {
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.material.index')->with('error', 'Ops, registro não foi encontrado.');
        return view('lesson::materials.edit', compact('edit'));
    }

    public function update(MaterialRequest $request, $slug, Material $model)
    {
        $request->merge(['user_create_id' => \Auth::user()->id, 'slug' => $model->notRepeatModelString(str_slug($request->get('name', 'slug'), '-'), $model)]);
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.material.index')->with('error', 'Ops, registro não foi encontrado.');
        if ($edit->customUpdate($edit, $request)) {
            n(route('fatorcms.material.edit', $request->get('slug')), 'Material atualizado', 'O Material ' . $edit->name . ', acaba de ser atualizado por ' . userHistorical());
            return redirect()->route('fatorcms.material.index')->with('success', 'A Matéria ' . $edit->name . ' foi atualizada com sucesso.');
        } else
            return redirect()->back()->with('error', 'Ops, não foi possível atualizar a matéria, verifique todos os campos e tente novamente.')->withInput($request->except(['_token', 'user_create_id', 'slug']));
    }

    public function destroy($slug, Material $model)
    {
        $destroy = $model->whereSlug($slug)->first();
        if (!$destroy)
            return redirect()->route('fatorcms.material.index')->with('error', 'Ops, registro não foi encontrado.');
        if ($destroy->delete()) {
            n(route('fatorcms.material.destroy', $slug), 'Material deletado', 'O Material ' . $destroy->name . ', acabou de ser removido pelo usuário ' . userHistorical());
            return redirect()->route('fatorcms.material.index')->with('success', 'A Matéria ' . $destroy->name . ' foi removida com sucesso.');
        } else
            return redirect()->route('fatorcms.material.index')->with('error', 'Ops, não foi possível deletar o registro, tente novamente.');
    }

}