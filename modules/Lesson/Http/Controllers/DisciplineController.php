<?php namespace Modules\Lesson\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Illuminate\Http\Request;
use Modules\Lesson\Entities\Discipline;
use Modules\Lesson\Http\Requests\DisciplineRequest;

class DisciplineController extends BaseModuleController
{

    public function index(Request $request, Discipline $model)
    {
        $list = $model->orderBy('id', 'desc');
        if ($request->has('search')) {
            foreach($request->get("search") as $key => $value) {
                if (!empty($value) && $key != 'type') {
                    $list->where($key, 'LIKE', '%' . $value . '%');
                } else if ($key == 'type' && !empty($value)) {
                    $list->where($value, '=', true);
                }
            }
        } else {
            $list->where('parent_id', '=', 0);
        }
        $request->session()->flashInput($request->all());
        $list = $list->paginate(15);
        return view('lesson::materials.index', [
            'list' => $list
        ]);
    }

    public function create()
    {
        return view('lesson::materials.create');
    }

    public function store(DisciplineRequest $request, Discipline $model)
    {
        $request->merge(['description' => strip_tags($request->get('description')), 'slug' => $model->notRepeatModelString(str_slug($request->get('name'), '-'), $model)]);
        $return = $model->create($request->except(['_token']));
        if ($return)
            return redirect()->route('fatorcms.discipline.index')->with('success', 'Disciplina criada com sucesso.');
        else
            return redirect()->back()->with('error', 'Ops, não foi possível criar a disciplina.');
    }

    public function edit($slug, Discipline $model)
    {
        $edit = $model->whereSlug($slug)->first();

        if (!$edit)
            return redirect()->route('fatorcms.discipline.index')->with('error', 'Ops, não foi possível encontrar a disciplina.');

        return view('lesson::materials.edit', [
            'edit' => $edit
        ]);
    }

    public function update($slug, Discipline $model)
    {
        $edit = $model->whereSlug($slug)->first();

        if (!$edit)
            return redirect()->route('fatorcms.discipline.index')->with('error', 'Ops, não foi possível encontrar a disciplina.');

        request()->merge(['description' => strip_tags(request()->get('description'))]);
        $return = $edit->update(request()->except(['_token']));

        if ($return)
            return redirect()->route('fatorcms.discipline.index')->with('success', 'Disciplina atualizada com sucesso.');
        else
            return redirect()->back()->with('error', 'Ops, não foi possível atualizar a disciplina.');
    }

    public function destroy($slug, Discipline $model)
    {
        $destroy = $model->whereSlug($slug)->first();
        if ($destroy) {
            $destroy->delete();
            return redirect()->route('fatorcms.discipline.index')->with('success', 'Disciplina deletada com sucesso.');
        }
        return redirect()->back()->with('error', 'Ops, não foi possível deletar a disciplina.');
    }

}