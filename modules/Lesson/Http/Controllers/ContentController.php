<?php namespace Modules\Lesson\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Modules\Lesson\Entities\Content;
use Modules\Lesson\Entities\Lesson;
use Modules\Lesson\Entities\LessonContent;
use Modules\Lesson\Http\Requests\ContentRequest;
use Modules\Test\Entities\Test;

class ContentController extends BaseModuleController
{


    public function index(Request $request, Content $model)
    {
        $list = $model->orderBy('id', 'desc');
        if ($request->has('search')) {
            foreach($request->get("search") as $key => $value) {
                if (!empty($value) && $key != 'type') {
                    $list->where($key, 'LIKE', '%' . $value . '%');
                } else if ($key == 'type' && !empty($value)) {
                    $list->where($value, '=', true);
                }
            }
        }
        $request->session()->flashInput($request->all());
        $list = $list->paginate(15);
        return view('lesson::resource.index', compact('list'));
    }

    public function create()
    {
        return view('lesson::resource.create');
    }

    public function store(ContentRequest $request, Content $model)
    {
        if ($request->hasFile('filename')) {
            $filename = $this->customFileName($request, 'assets/contents/', 'filename');
            if ($request->file('filename')->move('assets/contents/', $filename)) {
                $request->merge(['content' => $filename]);
            }
        }
        $request->merge(['discipline_id' => $model->discipline($request->get('discipline_id'))]);
        $request->merge(['slug' => $model->notRepeatModelString(str_slug($request->get('name', 'slug'), '-'), $model)]);

        if ($request->get("type") == 'video') {
            $request->merge([
                'video_link' => $request->get('content')
            ]);
        }
        if ($model->customCreate($request)) {
            n(route('fatorcms.content.edit', $request->get('slug')), 'Novo Conteúdo', 'O Conteúdo ' . $request->get('name') . ', foi criado recente por ' . userHistorical());
            return redirect()->route('fatorcms.content.create')->with('success', 'Conteúdo criado com sucesso.');
        } else
            return redirect()->back()->with('error', 'Ops, não foi possível criar o conteúdo.');
    }

    public function edit($slug, Content $model)
    {
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.content.index')->with('error', 'Ops, registro não foi encontrado.');
        $edit->type = $edit->Type;
        if ($edit->type == 'video') {
//            $json = json_decode($edit->content);
//            $edit->content = 'https://www.youtube.com/watch?v=' . $json->id;
            $edit->content = $edit->video_link;
        }
        return view('lesson::resource.edit', compact('edit'));
    }

    public function update($slug, ContentRequest $request, Content $model)
    {
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.content.index')->with('error', 'Ops, registro não foi encontrado.');

        if ($request->hasFile('file')) {
            $filename = $this->customFileName($request, 'assets/contents/', 'file');
            if ($request->file('file')->move('assets/contents/', $filename)) {
                $request->merge(['content' => $filename]);
            }
        }

        if ($request->get("type") == 'video') {
            $request->merge([
                'video_link' => $request->get('content')
            ]);
        }

        $request->merge(['discipline_id' => $model->discipline($request->get('discipline_id'))]);
        if ($model->customUpdate($edit, $request)) {
            n(route('fatorcms.content.edit', $request->get('slug')), 'Conteúdo Atualizado', 'O Conteúdo ' . $request->get('name') . ', foi criado recente por ' . userHistorical());
            return redirect()->route('fatorcms.content.index')->with('success', 'O Conteúdo foi criado com succeso, agora é possível vincular ele a uma matéria.');
        } else
            return redirect()->back()->with('error', 'Ops, não foi possível criar o material, tente novamente.');
    }

    public function destroy($slug, Content $model)
    {
        $destroy = $model->whereSlug($slug)->first();
        if (!$destroy)
            return redirect()->route('fatorcms.material.index')->with('error', 'Ops, registro não foi encontrado.');
        if ($destroy->delete()) {
            n(route('fatorcms.content.destroy', $slug), 'Conteúdo deletado', 'O Conteúdo ' . $destroy->name . ', acabou de ser removido pelo usuário ' . userHistorical());
            return redirect()->route('fatorcms.content.index')->with('success', 'O Conteúdo ' . $destroy->name . ' foi removida com sucesso.');
        } else
            return redirect()->route('fatorcms.content.index')->with('error', 'Ops, não foi possível deletar o registro, tente novamente.');
    }

    public function getContents(Request $request, Content $model, Test $model_test)
    {
        $test = $model_test->orderBy('name');
        $contents = $model->with('disciplines')->orderBy('discipline_id', 'desc');

        if (request()->has('not_tests'))
            $test = $test->whereNotIn('id', request('not_tests'));

        if (request()->has('not_contents'))
            $contents = $contents->whereNotIn('id', request('not_contents'));

        if (request()->has('discipline'))
            $contents = $contents->where('discipline_id', '=', request('discipline'));

        return response()->json(['error' => false, 'data' => ['tests' => $test->get()->toArray(), 'contents' => $contents->get()->toArray()]]);
    }

    public function getEditContent(Request $request)
    {
        return response()->json(['error' => false, 'data' => LessonContent::has('contents')->with(['contents', 'tests'])->whereLessonId($request->get('id'))->get()->toArray()], 200);
    }

    public function saveContent(Content $model)
    {
        if (request()->hasFile('filename')) {
            $filename = $this->customFileName(request(), 'assets/contents/', 'filename');
            if (request()->file('filename')->move('assets/contents/', $filename)) {
                request()->merge(['content' => $filename]);
            }
        }
        request()->merge(['discipline_id' => $model->discipline(request()->get('discipline_id'))]);
        request()->merge(['slug' => $model->notRepeatModelString(str_slug(request()->get('name', 'slug'), '-'), $model)]);
        if ($return = $model->customCreate(request())) {
            n(route('fatorcms.content.edit', request()->get('slug')), 'Novo Conteúdo', 'O Conteúdo ' . request()->get('name') . ', foi criado recente por ' . userHistorical());
            return response()->json(['error' => false, 'msg' => 'O Conteúdo foi inserido com sucesso.', 'data' => $model->find($return->id)]);
        } else
            return response()->json(['error' => true, 'msg' => 'Falha ao criar conteúdo, tente novamente']);
    }
}