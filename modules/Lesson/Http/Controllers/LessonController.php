<?php namespace Modules\Lesson\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Illuminate\Support\Facades\Auth;
use Modules\Lesson\Entities\Discipline;
use Modules\Lesson\Entities\Lesson;
use Modules\Lesson\Entities\LessonContent;
use Modules\Lesson\Entities\LessonMaterial;
use Modules\Lesson\Entities\MaterialSubject;
use Modules\Lesson\Http\Requests\LessonRequest;
use Modules\Team\Entities\TeamLesson;

class LessonController extends BaseModuleController
{
    public function index(Lesson $model)
    {
        $list = $model->orderBy('id', 'desc');
        if (request()->has('search')) {
            foreach(request()->get("search") as $key => $value) {
                if (!empty($value) && $key != 'type') {
                    $list->where($key, 'LIKE', '%' . $value . '%');
                } else if ($key == 'type' && !empty($value)) {
                    $list->where($value, '=', true);
                }
            }
        }
        if (request()->has('export')) {
            $this->exportSimpleExcel(new Lesson(), $list, 'Aulas');
        }
        request()->session()->flashInput(request()->all());
        $list = $list->paginate(15);
        return view('lesson::lesson.index', compact('list'));
    }

    public function create()
    {
        return view('lesson::lesson.create');
    }

    public function store(LessonRequest $request, Lesson $model, Discipline $discipline_model, MaterialSubject $material_subject_model)
    {
        $request->merge(['user_create_id' => Auth::user()->id, 'slug' => $model->notRepeatModelString(str_slug($request->get('name', 'slug'), '-'), $model), 'discipline_id' => $discipline_model->storeDiscipline($request->get('discipline_id'))]);
        $request->merge(['resume', substr(strip_tags($request->get('resume')), 0, 255)]);
        if ($save = $model->create($request->except(['_token']))) {
            $getMaterialIDS = [];
            if ($request->has('material_id'))
                $getMaterialIDS = $material_subject_model->storeLesson($request->get('material_id'));
            if (count($getMaterialIDS)) {
                foreach ($getMaterialIDS as $row) {
                    LessonMaterial::create(['lesson_id' => $save->id, 'material_id' => $row]);
                }
            }
            if ($request->has('content')) {
                LessonContent::whereNotIn('content_id', array_keys($request->get('content')))->whereLessonId($save->id)->delete();
                foreach ($request->get('content') as $item) {
                    if ($item['type'] == 'contents')
                        LessonContent::create(['lesson_id' => $save->id, 'order' => $item['order'], 'content_id' => $item['id'], 'begin_date' => dateToSql($item['begin_date']), 'end_date' => dateToSql($item['end_date'])]);
                    else {
                        LessonContent::create(['lesson_id' => $save->id, 'order' => $item['order'], 'test_id' => $item['id'], 'begin_date' => dateToSql($item['begin_date']), 'end_date' => dateToSql($item['end_date'])]);
                    }
                }
            }
            n(route('fatorcms.lesson.edit', $request->get('slug')), 'Nova Aula', 'A Aula ' . $request->get('name') . ', foi criada por ' . userHistorical());
            return redirect()->route('fatorcms.lesson.index')->with('success', 'A Aula foi criada com sucesso.');
        } else
            return redirect()->back()->with('error', 'Ops, não foi possível criar a aula, verifique todos os campos e tente novamente.')->withInput($request->except(['_token', 'user_create_id', 'slug']));
    }

    public function edit($slug, Lesson $model)
    {
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.lesson.index')->with('error', 'Ops, registro não foi encontrado.');
        return view('lesson::lesson.edit', compact('edit'));
    }

    public function update(LessonRequest $request, $slug, Lesson $model, Discipline $discipline_model)
    {
        $request->merge(['user_create_id' => Auth::user()->id, 'discipline_id' => $discipline_model->storeDiscipline($request->get('discipline_id'))]);
        $request->merge(['resume', substr(strip_tags($request->get('resume')), 0, 255)]);
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.lesson.index')->with('error', 'Ops, registro não foi encontrado.');

        if ($model->customUpdate($edit, $request)) {
            n(route('fatorcms.lesson.edit', $request->get('slug')), 'Aula atualizada', 'A Aula ' . $request->get('name') . ', foi criada por ' . userHistorical());
            return redirect()->route('fatorcms.lesson.index', $slug)->with('success', 'A Aula ' . $edit->name . ' foi atualizada com sucesso.');
        } else
            return redirect()->back()->with('error', 'Ops, não foi possível atualizar a aula, verifique todos os campos e tente novamente.')->withInput($request->except(['_token', 'user_create_id', 'slug']));
    }

    public function destroy($slug, Lesson $model)
    {
        $destroy = $model->whereSlug($slug)->first();
        if (!$destroy)
            return redirect()->route('fatorcms.lesson.index')->with('error', 'Ops, registro não foi encontrado.');
        if ($destroy->delete()) {
            $team_lessons = TeamLesson::whereLessonId($destroy->id)->delete();
            n(route('fatorcms.lesson.destroy', $slug), 'Nova Aula', 'A Aula ' . $destroy->name . ', foi criada por ' . userHistorical());
            return redirect()->route('fatorcms.lesson.index')->with('success', 'A Aula ' . $destroy->name . ' foi deletada com sucesso.');
        } else
            return redirect()->route('fatorcms.lesson.index')->with('error', 'Ops, não foi possível deletar o registro, tente novamente.');
    }


}