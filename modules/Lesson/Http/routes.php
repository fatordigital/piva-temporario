<?php

    Route::group([
        'prefix'     => 'fatorcms',
        'middleware' => [
            'auth',
            'acl',
            'cms'
        ],
        'namespace'  => 'Modules\Lesson\Http\Controllers'
    ], function () {
        Route::resource('lesson', 'LessonController');
        Route::resource('discipline', 'DisciplineController');
        Route::resource('material', 'MaterialController');
        Route::resource('content', 'ContentController');
        Route::post('save-content.json', ['as' => 'save-content.json', 'uses' => 'ContentController@saveContent']);
    });

    Route::post('fatorcms/content.json', ['as' => 'getContent.json', 'uses' => 'Modules\Lesson\Http\Controllers\ContentController@getContents']);
    Route::post('fatorcms/content-edit.json', ['as' => 'getContentEdit.json', 'uses' => 'Modules\Lesson\Http\Controllers\ContentController@getEditContent']);
