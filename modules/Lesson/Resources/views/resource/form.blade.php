<div class="row">
  <div class="col-lg-6">
    <div class="form-group">
        <label for="status" class="control-label">Conteúdo Grátis?</label>
        <br/>
        <div class="radio radio-success radio-inline">
            {!! Form::radio('free', 1, false) !!}
          <label for="radio4">
            Sim
          </label>
        </div>
        <div class="radio radio-danger radio-inline">
            {!! Form::radio('free', 0, true) !!}
          <label for="radio4">
            Não
          </label>
        </div>
    </div>

    <div class="form-group">
      <p>
        <label for="status">
          Tipo de conteúdo <strong class="text-danger">(*)</strong>
        </label>
      </p>
      <div class="radio radio-success radio-inline">
        {!! Form::radio('type', 'video', false) !!}
        <label for="on">
            Vídeo
        </label>
      </div>
      <div class="radio radio-danger radio-inline">
        {!! Form::radio('type', 'text', true) !!}
        <label for="off">
            Texto
        </label>
      </div>
      <div class="radio radio-primary radio-inline">
        {!! Form::radio('type', 'file', false) !!}
        <label for="off">
            Arquivo
        </label>
      </div>
    </div>
  </div>

  <div class="col-lg-6">

    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
      <label for="name">
        Nome <strong class="text-danger">(*)</strong>
      </label>
      {!! Form::text('name', null, ['class' => 'form-control', 'maxlength' => 120]) !!}
      {!! $errors->first('name', '<span class="help-block text-danger">:message</span>') !!}
    </div>

    <div class="form-group {!! ($errors->first('discipline_id')) ? 'has-error' : '' !!}">
      <label for="icon">
          <i class="fa fa-question-circle" data-toggle="tooltip" title="Caso não exista a disciplina, digite o nome da disciplina e após salvar a aula será criado automaticamente um novo registro."></i> Selecione/Digite a Disciplina <strong class="text-danger">(*)</strong>
      </label>
      {!! Form::select('discipline_id', ['' => 'Selecione um disciplina'] + \Modules\Lesson\Entities\Discipline::lists('name', 'id')->toArray(), isset($edit) ? $edit->discipline_id : null, ['style' => 'display: none;', 'class' => 'text-autocomplete']) !!}
      {!! $errors->first('discipline_id', '<span class="help-block">:message</span>') !!}
    </div>

  </div>

  <div class="col-lg-12">

    <div class="form-group{!! $errors->first('content') ? ' has-error' : '' !!}" data-option="video"{!! old('type') == 'video' || isset($edit) && $edit->type == 'video' ? '' : ' style="display: none;"' !!}>
      <label for="content">
        Conteúdo Vídeo <strong class="text-danger">(*)</strong>
      </label>
        {!! Form::text('content', null, ['class' => 'form-control', 'maxlength' => 255] + (old('type') == 'video' || isset($edit) && $edit->type == 'video' ? [] : ['disabled' => 'disabled'])) !!}
        {!! $errors->first('content', '<span class="help-block text-danger">:message</span>') !!}
    </div>

    <div class="form-group{!! $errors->first('filename') ? ' has-error' : '' !!}" data-option="file" style="display: none;"{!! old('type') == 'filename' || isset($edit) && $edit->type == 'filename' ? '' : ' style="display: none;"' !!}>
      <label for="content">
        Conteúdo Arquivo <strong class="text-danger">(*)</strong>
      </label>
      {!! Form::file('filename', ['class' => 'form-control'] + (old('type') == 'filename' || isset($edit) && $edit->type == 'file' ? [] : ['disabled' => 'disabled'])) !!}
      {!! $errors->first('filename', '<span class="help-block text-danger">:message</span>') !!}
    </div>

    <div class="form-group" data-option="text"{!! old('type') == 'text' || !old() && !isset($edit) || isset($edit) && $edit->type == 'text' ? '' : ' style="display: none;"' !!}>
      <label for="description">
        <i class="fa fa-question-circle" data-toggle="tooltip" title="Você pode inserir imagens, vídeos e arquivos ao montar o conteúdo."></i> Conteúdo Texto <strong class="text-danger">(*)</strong>
      </label>
      {!! Form::textarea('content', null, ['class' => 'form-control summernote'] + (old('type') == 'text' || !old() && !isset($edit) || isset($edit) && $edit->type == 'text' ? [] : ['disabled' => 'disabled'])) !!}
    </div>

    <div class="form-group" data-option="video"{!! old('type') == 'video' || isset($edit) && $edit->type == 'video' ? '' : ' style="display: none;"' !!}>
      <label for="description">
        <i class="fa fa-question-circle" data-toggle="tooltip" title="Tu pode escrever a notícia como quiser nessa área, podendo adicionar imagens e vídeos."></i> Descrição do vídeo <strong class="text-success">(Opcional)</strong>
      </label>
      {!! Form::textarea('description', null, ['class' => 'form-control summernote']) !!}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="form-group">
      <button type="submit" class="btn btn-primary">
        Salvar registros
      </button>
    </div>
  </div>
</div>

@section('cjs')
  {!! HTML::script('js/cms/scripts/content/content.js') !!}
@endsection