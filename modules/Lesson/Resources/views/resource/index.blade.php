@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Conteúdo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Conteúdo</strong>
            </li>
        </ol>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.content.create') !!}"
           class="btn btn-success">
            {!! trans('dictionary.create') !!} Conteúdo
        </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
      <div class="col-lg-12">
        <div class="ibox-content">
          {!! Form::open(['route' => 'fatorcms.content.index', 'method' => 'GET']) !!}
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="name">
                  Título
                </label>
                {!! Form::text('search[name]', old('search[name]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="name">
                  Tipo
                </label>
                {!! Form::select('search[type]', ['' => '', 'text' => 'Texto', 'video' => 'Vídeo', 'file' => 'Arquivo'], old('search[type]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="name">
                  Disciplina
                </label>
                <div class="input-group">
                  {!! Form::select('search[discipline_id]', ['' => ''] + \Modules\Lesson\Entities\Discipline::lists('name', 'id')->toArray() , old('search[discipline_id]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary">Procurar</button>
                  </span>
                  <span class="input-group-btn">
                    <a class="btn btn-warning" href="{!! route('fatorcms.content.index') !!}">Limpar</a>
                  </span>
                </div>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
          @if ($list->count())
          <table class="table table-bordered">
              <thead>
              <tr>
                  <th>Título</th>
                  <th>Disciplina</th>
                  <th>Tipo</th>
                  <th>Resumo do conteúdo</th>
                  <th>Opções</th>
              </tr>
              </thead>
              <tbody>
              @foreach($list as $row)
                <tr>
                    <td>{!! $row->name !!}</td>
                    <td>{!! $row->disciplines ? $row->disciplines->name : '' !!}</td>
                    <td>{!! $row->typeContent() !!}</td>
                    @if ($row->video && strstr($row->content, 'youtube.com'))
                      <?php $json = json_decode($row->content) ?>
                      <td><a target="_blank" href="http://www.youtube.com/watch?v={!! $json->id !!}">http://www.youtube.com/watch?v={!! $json->id !!}</a></td>
                    @elseif ($row->file)
                      <td>
                        <a href="{!! url('assets/contents/'. $row->content) !!}">{!! url('assets/contents/'. $row->content) !!}</a>
                      </td>
                    @elseif($row->video)
                      <td>{!! $row->video_link !!}</td>
                    @else
                      <td>{!! str_limit(strip_tags($row->content)) !!}</td>
                    @endif
                    <td>
                      <a href="{!! route('fatorcms.content.edit', $row->slug) !!}" class="btn btn-xs btn-primary pull-left margin-right">
                        <i class="fa fa-pencil"></i>
                      </a>
                      {!! Form::open(['route' => ['fatorcms.content.destroy', $row->slug], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deletar o conteúdo '. $row->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                      <button data-toggle="tooltip" title="Excluir Conteúdo" class="btn btn-danger btn-xs pull-left">
                        <i class="fa fa-trash-o"></i>
                      </button>
                      {!! Form::close() !!}
                    </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          @include('layouts.elements.paginator', ['paginator' => $list])
          </div>
      </div>
    @endif
  </div>
@endsection