@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Aulas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Aulas</strong>
            </li>
        </ol>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.lesson.create') !!}"
           class="btn btn-success">
            {!! trans('dictionary.create') !!} Aula
        </a>
        <a href="{!! route('fatorcms.lesson.index', 'export=true') !!}"
         class="btn btn-primary">
            Expotar
        </a>
    </div>
  </div>
@endsection


@section('content')

  <div id="generator" data-settings='{!! json_encode(array('delete' => route('fatorcms.discipline.destroy', '*id*'), 'store' => route('fatorcms.discipline.store'), 'update' => route('fatorcms.discipline.update', '*id*'))) !!}'
          data-return='{!! \Modules\Lesson\Entities\Discipline::orderBy('id', 'desc')->get()->toJson() !!}'></div>

  <div class="row">
    @if ($list->count())
      <div class="col-lg-12">
        <div class="ibox-content">
          {!! Form::open(['route' => 'fatorcms.lesson.index', 'method' => 'GET']) !!}
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="name">
                  Disciplina
                </label>
                {!! Form::select('search[discipline_id]', ['' => ''] + \Modules\Lesson\Entities\Discipline::lists('name', 'id')->toArray(), old('search[discipline_id]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
              </div>
            </div>
            <div class="col-lg-9">
              <div class="form-group">
                <label for="name">
                  Nome/Título
                </label>
                <div class="input-group">
                  {!! Form::text('search[name]', old('search[name]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary">Procurar</button>
                  </span>
                  <span class="input-group-btn">
                    <a class="btn btn-warning" href="{!! route('fatorcms.lesson.index') !!}">Limpar</a>
                  </span>
                </div>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
          <table class="table table-bordered">
              <thead>
              <tr>
                  <th>Título</th>
                  <th>Disciplina</th>
                  <th>Resumo</th>
                  <th>Média Pontuação</th>
                  <th>Opções</th>
              </tr>
              </thead>
              <tbody>
              @foreach($list as $row)
                <tr>
                    <td>{!! $row->name !!}</td>
                    <td>{!! $row->discipline ? $row->discipline->name : 'Não selecionada' !!}</td>
                    <td>{!! str_limit($row->resume, 50) !!}</td>
                    <td>
                        @if ($row->ratings->count() > 0)
                            {!! $row->ratings->sum('point') / $row->ratings->count() !!}
                        @else
                            Sem votação
                        @endif
                    </td>
                    <td>
                      <a href="{!! route('fatorcms.lesson.edit', $row->slug) !!}" class="btn btn-xs btn-primary pull-left margin-right">
                        <i class="fa fa-pencil"></i>
                      </a>
                      {!! Form::open(['route' => ['fatorcms.lesson.destroy', $row->slug], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deleta a aula '. $row->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                      <button data-toggle="tooltip" title="Excluir Aula" class="btn btn-danger btn-xs pull-left">
                          <i class="fa fa-trash-o"></i>
                      </button>
                      {!! Form::close() !!}
                    </td>
                </tr>
              @endforeach
              </tbody>
            </table>
            @include('layouts.elements.paginator', ['paginator' => $list])
          </div>
      </div>
    @endif
  </div>
@endsection