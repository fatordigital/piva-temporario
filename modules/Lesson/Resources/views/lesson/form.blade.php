<div class="tabs-container">
    <div class="tab-content">
        <div id="tab-1" class="tab-pane active">
            <div class="row">

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="status" class="control-label">Status</label>
                        <br/>
                        <div class="radio radio-success radio-inline">
                            {!! Form::radio('status', 1, true) !!}
                            <label for="radio4">
                                {!! trans('dictionary.status_text.active') !!}
                            </label>
                        </div>
                        <div class="radio radio-danger radio-inline">
                            {!! Form::radio('status', 0, false) !!}
                            <label for="radio4">
                                {!! trans('dictionary.status_text.inactive') !!}
                            </label>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <div class="form-group {!! ($errors->first('name')) ? 'has-error' : '' !!}">
                        <label for="name">
                            {!! trans('menu::db.name') !!}/Título <strong class="text-danger">(*)</strong>
                        </label>
                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'autocomplete' => 'off']) !!}
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group {!! ($errors->first('discipline_id')) ? 'has-error' : '' !!}">
                        <label for="icon">
                            <i class="fa fa-question-circle" data-toggle="tooltip"
                               title="Caso não exista a disciplina, digite o nome da disciplina e após salvar a aula será criado automaticamente um novo registro."></i>
                            Selecione/Digite a Disciplina <strong class="text-danger">(*)</strong>
                        </label>
                        {!! Form::select('discipline_id', ['' => 'Selecione um disciplina'] + \Modules\Lesson\Entities\Discipline::lists('name', 'id')->toArray(), isset($edit) ? $edit->discipline_id : null, ['style' => 'display: none;', 'class' => 'text-autocomplete']) !!}
                        {!! $errors->first('discipline_id', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group {!! $errors->first('resume') ? 'has-error' : '' !!}"  style="display: none">
                        <label for="resume">
                            Resumo <strong class="text-success">({!! trans('dictionary.optional') !!})</strong> - máximo
                            255 caracteres
                        </label>
                        {!! Form::textarea('resume', null, ['class' => 'form-control textarea-no-resize']) !!}
                        {!! $errors->first('resume', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-6">

                    <div class="form-group {!! ($errors->first('name')) ? 'has-error' : '' !!}">
                        <label for="name">
                            <i class="fa fa-question-circle" data-toggle="tooltip"
                               title="Selecione/Digite que complementam essa aula"></i> Selecione/Digite as matérias
                            <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::select('material_id[]', \Modules\Lesson\Entities\MaterialSubject::lists('name', 'id')->toArray(), isset($edit) ? $edit->materials->lists('material_id')->toArray() : null, ['style' => 'display: none;', 'multiple' => 'multiple', 'class' => 'tag-autocomplete']) !!}
                        {!! $errors->first('material_id[]', '<span class="help-block">:message</span>') !!}
                    </div>

                    <div class="form-group {!! ($errors->first('user_id')) ? 'has-error' : '' !!}">
                        <label for="user_id">
                            <i class="fa fa-question-circle" data-toggle="tooltip"
                               title="Selecione o aluno que essa aula está disponível"></i> Selecione o usuário que vai receber está aula
                            <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::select('user_id', ['' => 'Selecione o usuário'] + \Modules\User\Entities\User::all()->lists('name_email', 'id')->toArray(), null , ['style' => 'display: none;', 'class' => 'text-autocomplete']) !!}
                        {!! $errors->first('user_id', '<span class="help-block">:message</span>') !!}
                    </div>

                </div>
            </div>

            <div class="form-group" style="display: none">
                <label for="description">
                    <i class="fa fa-question-circle" data-toggle="tooltip"
                       title="Crie um texto com imagens, arquivos e etc..."></i> Descrição completa da aula <strong
                            class="text-success">(Opcional)</strong>
                </label>
                {!! Form::textarea('text', null, ['class' => 'form-control summernote']) !!}
            </div>
        </div>

        <style>
            input {
                min-width: 100px;
            }
        </style>

        <div id="tab-2" class="tab-pane">
            <div id="content" data-edit="{!! isset($edit) ? $edit->id : false !!}"></div>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <a href="{!! route('fatorcms.lesson.index') !!}"
               class="btn btn-warning">
                Voltar
            </a>
            <button type="submit" class="btn btn-primary">
                Salvar registros
            </button>
        </div>
    </div>
</div>

@section('cjs')
    {!! HTML::script(elixir('js/cms/components/content/content.min.js')) !!}
@endsection

