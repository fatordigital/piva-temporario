@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Editar Aula</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li>
                <a href="{!! route('fatorcms.lesson.index') !!}">Aulas</a>
            </li>
            <li class="active">
                <strong>Editar Aula</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <p>
        Todos os campos com <strong class="text-danger">(*)</strong> são obrigatórios.
      </p>
    </div>
  </div>
  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.lesson.index') !!}"
           class="btn btn-warning margin-right pull-left">
            Voltar
        </a>
        {!! Form::open(['route' => ['fatorcms.lesson.destroy', $edit->slug], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deleta a aula '. $edit->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
        <button data-toggle="tooltip" title="Excluir Aula" class="btn margin-right btn-danger">
              Excluir Aula
        </button>
        {!! Form::close() !!}
    </div>
  </div>
@endsection

@section('content')
  <div class="ibox float-e-margins">
    <div class="tabs-container">
       <ul class="nav nav-tabs">
         <li class="active"><a data-toggle="tab" href="#tab-1">Dados da Aula</a></li>
         <li class="">
          <a data-toggle="tab" href="#tab-2">
            Conteúdos/Simulados
          </a>
         </li>
       </ul>
    </div>
    <div class="ibox-content">
      {!! Form::model($edit, ['route' => ['fatorcms.lesson.update', $edit->slug], 'method' => 'PATCH', 'files' => true]) !!}
      @include('lesson::lesson.form')
      {!! Form::close() !!}
    </div>
  </div>
@endsection