@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Editar Disciplina</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li>
                <a href="{!! route('fatorcms.discipline.index') !!}">Disciplinas</a>
            </li>
            <li class="active">
                <strong>Editar Disciplina</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <p>
        Todos os campos com <strong class="text-danger">(*)</strong> são obrigatórios.
      </p>
    </div>
  </div>
  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.discipline.index') !!}"
           class="btn btn-warning margin-right pull-left">
            Voltar
        </a>
        {!! Form::open(['route' => ['fatorcms.discipline.destroy', $edit->slug], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deletar a disciplina '. $edit->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
        <button data-toggle="tooltip" title="Excluir Aula" class="btn margin-right btn-danger">
          Excluir Disciplina
        </button>
        {!! Form::close() !!}
    </div>
  </div>
@endsection

@section('content')
  <div class="ibox float-e-margins">
    <div class="ibox-content">
      {!! Form::model($edit, ['route' => ['fatorcms.discipline.update', $edit->slug], 'method' => 'PATCH', 'files' => true]) !!}
      @include('lesson::materials.form')
      {!! Form::close() !!}
    </div>
  </div>
@endsection