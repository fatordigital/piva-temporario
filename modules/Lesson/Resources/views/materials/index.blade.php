@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Disciplinas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Disciplinas</strong>
            </li>
        </ol>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! route('fatorcms.discipline.create') !!}"
           class="btn btn-success">
            {!! trans('dictionary.create') !!} Disciplina
        </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    @if ($list->count())
      <div class="col-lg-12">
        <div class="ibox-content">
          {!! Form::open(['route' => 'fatorcms.discipline.index', 'method' => 'GET']) !!}
          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                <label for="name">
                  Disciplina
                </label>
                <div class="input-group">
                  {!! Form::text('search[name]', old('search[discipline_id]'), ['class' => 'form-control']) !!}
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary">Procurar</button>
                  </span>
                  <span class="input-group-btn">
                    <a class="btn btn-warning" href="{!! route('fatorcms.discipline.index') !!}">Limpar</a>
                  </span>
                </div>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
          <table class="table table-bordered">
              <thead>
              <tr>
                  <th>Título</th>
                  <th width="8%">Opções</th>
              </tr>
              </thead>
              <tbody>
              @foreach($list as $row)
                <tr>
                    <td>{!! $row->name !!}</td>
                    <td>
                      <a href="{!! route('fatorcms.discipline.edit', $row->slug) !!}" class="btn btn-xs btn-primary pull-left margin-right">
                        <i class="fa fa-pencil"></i>
                      </a>
                      {!! Form::open(['route' => ['fatorcms.discipline.destroy', $row->slug], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deletar a matéria '. $row->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                      <button data-toggle="tooltip" title="Excluir Matéria" class="btn btn-danger btn-xs pull-left">
                        <i class="fa fa-trash-o"></i>
                      </button>
                      {!! Form::close() !!}
                    </td>
                </tr>
                @if ($row->SubDisciplines && $row->SubDisciplines->count())
                    @foreach($row->SubDisciplines as $disciplines)
                        <tr class="bg-success">
                            <td>- {!! $disciplines->name !!}</td>
                            <td>
                                <a href="{!! route('fatorcms.discipline.edit', $disciplines->slug) !!}" class="btn btn-xs btn-primary pull-left margin-right">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                {!! Form::open(['route' => ['fatorcms.discipline.destroy', $disciplines->slug], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deletar a matéria '. $row->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                                <button data-toggle="tooltip" title="Excluir Matéria" class="btn btn-danger btn-xs pull-left">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach
                @endif
              @endforeach
              </tbody>
            </table>
          @include('layouts.elements.paginator', ['paginator' => $list])
          </div>
      </div>
    @endif
  </div>
@endsection