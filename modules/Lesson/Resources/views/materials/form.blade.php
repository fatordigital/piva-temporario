<div class="row">
    <div class="col-lg-6">
        <div class="form-group {!! ($errors->first('name')) ? 'has-error' : '' !!}">
            <label for="name">
                {!! trans('menu::db.name') !!}/Título <strong class="text-danger">(*)</strong>
            </label>
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group {!! ($errors->first('parent_id')) ? 'has-error' : '' !!}">
            <label for="name">
                Disciplina
            </label>
            {!! Form::select('parent_id', ['' => 'Selecione uma disciplina'] + \Modules\Lesson\Entities\Discipline::lists('name', 'id')->toArray(), null, ['class' => 'form-control']) !!}
            {!! $errors->first('parent_id', '<span class="help-block">:message</span>') !!}
        </div>
    </div>
    <div class="col-lg-12">
        <div class="form-group">
            <label for="">
                Descrição
            </label>
            {!! Form::textarea('description', null, ['class' => 'summernote']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <a href="{!! route('fatorcms.discipline.index') !!}"
               class="btn btn-warning">
                Voltar
            </a>
            <button type="submit" class="btn btn-primary">
                Salvar registros
            </button>
        </div>
    </div>
</div>

@section('cjs')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#new-subject').click(function (e) {
                e.preventDefault();
                var $recipe = $('.select2-container:eq(0)');
                if ($recipe.is(':visible')) {
                    $recipe.hide();
                    $recipe.after('<input type="text" name="new_subject" class="form-control" placeholder="Digite aqui o novo assunto..." />');
                    $(this).html('<i class="fa fa-arrow-circle-left" data-toggle="tooltip" title="Voltar com os registros já existentes."></i>');
                } else {
                    $recipe.show();
                    $('input[name="new_subject"]').remove();
                    $(this).html('<i class="fa fa-plus-circle text-success"></i>');
                }
            });
        });
    </script>
@endsection