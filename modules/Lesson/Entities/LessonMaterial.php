<?php namespace Modules\Lesson\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonMaterial extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["lesson_id", "material_id"];

}