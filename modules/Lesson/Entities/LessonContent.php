<?php namespace Modules\Lesson\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonContent extends BaseModel
{

    protected $fillable = ["user_create_id", "test_id", "content_id", "material_id", "lesson_id", "order", "begin_date", "end_date"];

    protected $appends = [
        'Begin',
        'End',
        'ContentType',
        'DiffDate'
    ];

    public function getDiffDateAttribute()
    {
        $d1 = date_create($this->begin_date);
        $d2 = date_create(date('Y-m-d'));
        $diff = date_diff($d1, $d2);
        return (int)$diff->format("%R%a");
    }

    public function getContentTypeAttribute()
    {
        return $this->test_id ? 'tests' : 'contents';
    }

    public function contents()
    {
        return $this->belongsTo('Modules\Lesson\Entities\Content', 'content_id');
    }

    public function tests()
    {
        return $this->belongsTo('Modules\Test\Entities\Test', 'test_id');
    }

}