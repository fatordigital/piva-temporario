<?php namespace Modules\Lesson\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaterialSubject extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["user_create_id", "name", "slug"];

    public function storeLesson(array $array)
    {
        $return = [];
        foreach($array as $item) {
            if (!is_int(filter_var($item, FILTER_VALIDATE_INT))) {
                $query = $this->whereSlug(str_slug($item, '-'))->first();
                if ($query)
                    $return[] = $query->id;
                else {
                    $store = $this->create(['user_create_id' => auth()->user()->id, 'name' => $item, 'slug' => str_slug($this->notRepeatModelString(str_slug($item, '-'), $this))]);
                    $return[] = $store->id;
                }
            } else {
                $return[] = $item;
            }
        }
        return $return;
    }

}