<?php namespace Modules\Lesson\Entities;

use App\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["user_id", "user_create_id", "discipline_id", "name", "slug", "resume", "text"];

    protected $appends = ['DisciplineName', 'ContentTypeCount', 'Rating'];

    public $excel = [
        'Nome', 'Disciplina', 'Resumo', 'Data de registro', 'Data de alteração no registro'
    ];

    public $excelModel = [
        'name', 'DisciplineName', 'resume', 'created_at', 'updated_at'
    ];

    public function getRatingAttribute()
    {
        if (!auth()->guest()) {
            $ratings = ContentRating::whereUserId(auth()->user()->id)->whereLessonId($this->id)->first();
            if ($ratings)
                return $ratings->point;
        }
        return 0;
    }
    
    public function getContentTypeCountAttribute()
    {
        $key = 'lesson-content-' . $this->id;

        if (!cache()->has($key)) {

            $cache = LessonContent::has('contents')->with(['contents' => function ($q) {
                return $q->select(['id', 'discipline_id', 'video', 'text', 'file']);
            }])->whereLessonId($this->id)->get();

            if (!$cache) {
                return collect([
                    'videos' => 0,
                    'files' => 0,
                    'texts' => 0,
                    'tests' => 0
                ]);
            }

            cache()->put($key, $cache, Carbon::now()->addDay());
        } else {
            $cache = cache($key);
        }

        $v = $f = $t = $tt = 0;
        foreach ($cache as $r) {
            if ($r->contents && $r->content_id) {
                if ($r->contents->file)
                    $f++;
                if ($r->contents->text)
                    $t++;
                if ($r->contents->video)
                    $v++;
            } else {
                $tt++;
            }
        }
        return collect([
            'videos' => $v,
            'files' => $f,
            'texts' => $t,
            'tests' => $tt
        ]);
    }

    public function getDisciplineNameAttribute()
    {
        $discipline = Discipline::find($this->discipline_id);
        if ($discipline) {
            return $discipline->name;
        } else {
            return 'Disciplina não definida';
        }
    }

    public function discipline()
    {
        return $this->belongsTo('Modules\Lesson\Entities\Discipline', 'discipline_id');
    }

    public function materials()
    {
        return $this->hasMany('Modules\Lesson\Entities\LessonMaterial', 'lesson_id');
    }

    public function contents()
    {
        return $this->hasMany('Modules\Lesson\Entities\LessonContent', 'lesson_id');
    }

    public function customUpdate($edit, $request)
    {
        $material_subject_model = new MaterialSubject();
        $relations = $edit->materials->lists('material_id')->toArray();
        if ($request->has('material_id')) {
            $getMaterialIDS = [];
            if ($request->has('material_id'))
                $getMaterialIDS = $material_subject_model->storeLesson($request->get('material_id'));
            $this->updateRelations($edit, new LessonMaterial(), $relations, $getMaterialIDS, 'material_id', 'lesson_id');
        } else if (count($relations)) {
            LessonMaterial::whereLessonId($edit->id)->delete();
        }
        if ($request->has('content')) {
            LessonContent::whereIn('test_id', array_keys($request->get('content')))->orWhereIn('content_id', array_keys($request->get('content')))->whereLessonId($edit->id)->delete();
            foreach ($request->get('content') as $item) {
                if ($item['type'] == 'contents')
                    $relation = LessonContent::whereContentId($item['id'])->whereLessonId($edit->id)->first();
                else {
                    $relation = LessonContent::whereTestId($item['id'])->whereLessonId($edit->id)->first();
                }
                if ($relation) {
                    $relation->update(['lesson_id' => $edit->id, 'order' => $item['order'], 'begin_date' => dateToSql($item['begin_date']), 'end_date' => dateToSql($item['end_date'])]);
                } else {
                    if ($item['type'] == 'contents')
                        LessonContent::create(['lesson_id' => $edit->id, 'order' => $item['order'], 'content_id' => $item['id'], 'begin_date' => dateToSql($item['begin_date']), 'end_date' => dateToSql($item['end_date'])]);
                    else {
                        LessonContent::create(['lesson_id' => $edit->id, 'order' => $item['order'], 'test_id' => $item['id'], 'begin_date' => dateToSql($item['begin_date']), 'end_date' => dateToSql($item['end_date'])]);
                    }
                }
            }
        }


        if ($edit->update($request->except(['_token', 'LessonMaterial'])))
            return true;
        return false;
    }

    public function ratings()
    {
        return $this->hasMany('Modules\Lesson\Entities\ContentRating', 'lesson_id');
    }


}