<?php namespace Modules\Lesson\Entities;

use Alaouy\Youtube\Facades\Youtube;
use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["discipline_id", "description", "video_link", "free", "user_create_id", "slug", "name", "content", "video", "text", "file"];

    protected $appends = [
        'Begin',
        'End',
        'Rating'
    ];

    public function getRatingAttribute()
    {
        if (!auth()->guest()) {
            $ratings = ContentRating::whereUserId(auth()->user()->id)->whereContentId($this->id)->first();
            if ($ratings)
                return $ratings->point;
        }
        return 0;
    }

    public function getTypeAttribute()
    {
        if ($this->video == 1)
            return 'video';
        else if ($this->text == 1)
            return 'text';
        else if ($this->file == 1)
            return 'file';
        else
            return false;
    }

    private function content($type)
    {
        if ($type == 'video')
            return 'video';
        else if ($type == 'text')
            return 'text';
        else if ($type == 'file')
            return 'file';
    }

    public function typeContent()
    {
        if ($this->video)
            return 'Vídeo';
        else if ($this->text)
            return 'Texto';
        else if ($this->file)
            return 'Arquivo';
        else
            return 'Não definido';
    }

    public function customCreate($request)
    {
        $type = $this->content($request->get('type'));
        $request->merge(['user_create_id' => \Auth::user()->id, $type => true]);
        if ($type == 'video' && strstr($request->get('content'), 'youtube.com')) {
            $request->merge(['content' => json_encode(Youtube::getVideoInfo(Youtube::parseVidFromURL($request->get('content'))))]);
        } else if ($type == 'video' && strstr($request->get('content'), 'vimeo.com')) {
            $request->merge(['content' => apiV($request->get('content'))]);
        }
        return $this->create($request->except(['_token', 'type', 'filename']));
    }

    public function customUpdate($edit, $request)
    {
        $type = $this->content($request->get('type'));
        $request->merge(['user_create_id' => \Auth::user()->id, 'slug' => $this->notRepeatModelString(str_slug($request->get('name'), '-'), $this), $type => true]);
        if ($type == 'video' && strstr($request->get('content'), 'youtube.com')) {
            $request->merge(['content' => json_encode(Youtube::getVideoInfo(Youtube::parseVidFromURL($request->get('content'))))]);
        }else if ($type == 'video' && strstr($request->get('content'), 'vimeo.com')) {
            $request->merge(['content' => apiV($request->get('content'))]);
        }
        return $edit->update($request->except(['_token', 'type', 'file']));
    }

    public function rating()
    {
        return $this->hasOne('Modules\Lesson\Entities\ContentRating', 'content_id');
    }

    //total de pontos / qtd
    public function ratings()
    {
        return $this->hasMany('Modules\Lesson\Entities\ContentRating', 'content_id');
    }

    public function disciplines()
    {
        return $this->belongsTo('Modules\Lesson\Entities\Discipline', 'discipline_id');
    }

    public function discipline($name)
    {
        if (!filter_var($name, FILTER_VALIDATE_INT)) {
            $exists = Discipline::where('slug', '=', str_slug($name, '-'))->first();
            if ($exists)
                return $exists->id;
            else {
                $store = Discipline::create(['slug' => str_slug($name, '-'), 'name' => $name]);
                return $store->id;
            }
        } else {
            return $name;
        }
    }

}