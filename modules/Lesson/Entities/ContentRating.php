<?php namespace Modules\Lesson\Entities;

use App\BaseModel;

class ContentRating extends BaseModel
{

    protected $fillable = ["user_id", "lesson_id", "content_id", "point"];

}