<?php namespace Modules\Lesson\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["user_create_id", "material_subject_id", "name", "slug"];

    public function subject()
    {
        return $this->belongsTo('Modules\Lesson\Entities\MaterialSubject', 'material_subject_id');
    }

    private function newSubject($name)
    {
        $return = MaterialSubject::firstOrCreate([
            'user_create_id' => \Auth::user()->id,
            'name'           => $name,
            'slug'           => $this->notRepeatModelString(str_slug($name, '-'), new MaterialSubject())
        ]);
        return $return->id;
    }

    public function customCreate($request)
    {
        if ($request->has('new_subject') && $request->get('new_subject') != '') {
            $request->merge(['material_subject_id' => $this->newSubject($request->get('new_subject'))]);
        }
        $request->merge(['user_create_id' => \Auth::user()->id]);
        $model = $this->create($request->except(['_token', 'MaterialContent']));
        if ($request->has('MaterialContent')) {
            foreach ($request->get('MaterialContent')['content_id'] as $row) {
                MaterialContent::create(['material_id' => $model->id, 'content_id' => $row]);
            }
        }
        return $model;
    }

    public function customUpdate($edit, $request)
    {
        if ($request->has('new_subject') && $request->get('new_subject') != '') {
            $request->merge(['material_subject_id' => $this->newSubject($request->get('new_subject'))]);
        }
        $relations = $edit->contents->lists('id')->toArray();
        if ($request->has('MaterialContent')) {
            $this->updateRelations($edit, new MaterialContent(), $relations, $request->get('MaterialContent')['content_id']);
        } else if (count($relations)) {
            if (!MaterialContent::whereMaterialId($edit->id)->delete())
                return false;
        }
        if ($edit->update($request->except(['_token', 'MaterialContent'])))
            return true;
        return false;
    }

    public function contents()
    {
        return $this->hasMany('Modules\Lesson\Entities\MaterialContent', 'material_id');
    }

}