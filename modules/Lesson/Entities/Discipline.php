<?php namespace Modules\Lesson\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discipline extends BaseModel
{

    use SoftDeletes;

    protected $fillable = [
        "parent_id",
        "slug",
        "name",
        "description"
    ];

    public function getDisciplineQuestionsAttribute()
    {
        return $this->name . " Fácil (" . $this->Question()->whereDifficulty(1)->count() . ") Média (".$this->Question()->whereDifficulty(2)->count().") Difícil (".$this->Question()->whereDifficulty(3)->count().")";
    }

    public function storeDiscipline($name)
    {
        if (!is_int(filter_var($name, FILTER_VALIDATE_INT))) {
            $query = $this->whereSlug(str_slug($name, '-'))->first();
            if ($query)
                return $query->id;
            else {
                $store = $this->create(['name' => $name, 'slug' => $this->notRepeatModelString(str_slug($name, '-'), $this)]);
                return $store->id;
            }
        } else {
            return $name;
        }
    }

    public function Lessons()
    {
        return $this->hasMany('Modules\Lesson\Entities\Lesson', 'discipline_id');
    }

    public function Question()
    {
        return $this->hasMany('Modules\Test\Entities\Question', 'discipline_id');
    }

    public function SubDisciplines()
    {
        return $this->hasMany('Modules\Lesson\Entities\Discipline', 'parent_id');
    }

}