<?php

    Route::group([
        'prefix' => 'fatorcms',
        'middleware' => [
            'auth',
            'acl',
            'cms'
        ],
        'namespace'  => 'Modules\Acl\Http\Controllers'
    ], function () {
        Route::resource('/acl', 'AclController', [
            'only' => [
                'index',
                'store'
            ]
        ]);
        Route::patch('/acl/status/{id}', [
            'as'   => 'fatorcms.acl.status',
            'uses' => 'AclController@status'
        ]);

        Route::resource('/group', 'GroupController');
        Route::patch('/group/status/{id}', [
            'as'   => 'fatorcms.group.status',
            'uses' => 'GroupController@status'
        ]);
    });