<?php namespace Modules\Acl\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AclMiddleware
{

    /**
     * @var array
     * Rotas que não serão afetadas na leitura do Acl
     */
    private $doNothing = [
        'logout',
        'settings'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return ($this->checkRole($request->route()) === false) ? redirect()->route('fatorcms.login') : $next($request);
    }

    /**
     * @param $route
     * @return bool
     */
    private function checkRole($route)
    {
        if (!is_null($route) && Auth::user()) {
            $currentRoute = $route->getAction();
            $actionGroups = Auth::user()->getActions();
            if (isset($currentRoute['as'])) {
                $routeName = $currentRoute['as'];
                if (!is_null($routeName)) {
                    if (!in_array($routeName, $this->doNothing) && !in_array($routeName, $actionGroups) && !in_array('*', $actionGroups)) {
                        return false;
                    }
                }
            }
        }
    }

}
