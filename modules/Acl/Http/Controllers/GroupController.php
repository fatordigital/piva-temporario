<?php namespace Modules\Acl\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Modules\Acl\Entities\Action;
use Modules\Acl\Entities\ActionGroup;
use Modules\Acl\Entities\Group;
use Modules\Acl\Http\Requests\GroupRequest;

class GroupController extends BaseModuleController
{
    private function getArrayActions($id)
    {
        foreach (ActionGroup::where('group_id', '=', $id)->get(['action_id'])->toArray() as $value) {
            $actionsGroup[] = $value['action_id'];
        }
        View::share(compact('actionsGroup'));
    }

    /**
     * @return void
     */
    private function getActions()
    {
        if (Auth::user()->isAdmin()) {
            $actions = Action::with('parents')->whereParentId(0)->get();
        } else {
            $actions = Action::with(['parents' => function ($q) {
                $q->whereIn('id', Auth::user()->getActions('id'));
            }])->whereIn('id', Auth::user()->getActions('id'))->whereParentId(0)->get();
        }
        View::share(compact('actions'));
    }

    /**
     * @return View
     */
    public function index()
    {
        if (Auth::user() && !Auth::user()->isAdmin()) {
            $list = Group::with('groups')->whereParentId(Auth::user()->id)->orderBy('id', 'desc')->paginate(15);
        } else {
            $list = Group::orderBy('id', 'desc')->paginate(15);
        }
        return view('acl::group.index', compact('list'));
    }

    /**
     * @return View
     */
    public function create()
    {
        $this->getActions();
        return view('acl::group.create');
    }

    /**
     * @param GroupRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * O parent_id será relacionado ao grupo do usuário logado
     */
    public function store(GroupRequest $request)
    {
        //Adicioanr group_id do usuário
        $request->merge(['parent_id' => Auth::user()->group_id]);
        Group::customCreate($request);
        return redirect()->route('fatorcms.group.index')->with('success', trans('messages.insert.success'));
    }

    /**
     * @param $id
     * @return View
     */
    public function edit($id)
    {
        $this->getActions();
        $this->getArrayActions($id);
        $edit = Group::findOrFail($id);
        return view('acl::group.edit', compact('edit'));
    }

    /**
     * @param GroupRequest $request
     * @param              $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(GroupRequest $request, $id)
    {
        //Adicioanr group_id do usuário
        $request->merge(['parent_id' => Auth::user()->group_id]);
        Group::customUpdate($id, $request);
        return redirect()->route('fatorcms.group.index')->with('success', trans('messages.update.success'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * Ativa ou Inativa o menu
     */
    public function status($id)
    {
        $status = Group::findOrFail($id);
        $to = ($status->status) ? 0 : 1;
        $status->status = $to;
        $status->save();
        return redirect()->route('fatorcms.group.index')->with('success', trans('messages.status.success', [
            'status' => ($to) ? trans('dictionary.status_text.active') : trans('dictionary.status_text.inactive')
        ]));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        // Verifica se existe usuários relacionado ao grupo para deletar
        $users = User::whereGroupId($id)->first();
        if ($users) {
            $alert = [
                'type' => 'warning',
                'msg'  => trans('acl::db.messages.delete.warning')
            ];
        } else {
            $alert = [
                'type' => 'success',
                'msg'  => trans('acl::db.messages.delete.success')
            ];
        }
        Group::destroy($id);
        return redirect()->route('fatorcms.group.index')->with($alert['type'], $alert['msg']);
    }

}