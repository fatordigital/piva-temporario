<?php namespace Modules\Acl\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Modules\Acl\Entities\Action;

class AclController extends BaseModuleController
{

    public function index()
    {
        $list = Action::with(['action_groups'])->orderBy('id', 'desc')->paginate(20);
        return view('acl::acl.index', compact('list'));
    }

    public function store()
    {
        Action::createRoutes();
        return redirect()->route('fatorcms.acl.index')->with('success', trans('acl::db.messages.success'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * Ativa ou Inativa o menu
     */
    public function status($id)
    {
        $status = Action::findOrFail($id);
        $to = ($status->status) ? 0 : 1;
        $status->status = $to;
        $status->save();
        return redirect()->route('acl.index')->with('success', trans('messages.status.success', [
            'status' => ($to) ? trans('dictionary.status_text.active') : trans('dictionary.status_text.inactive')
        ]));
    }

    public function destroy($id)
    {

    }

}