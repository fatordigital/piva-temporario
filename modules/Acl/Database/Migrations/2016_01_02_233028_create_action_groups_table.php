<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateActionGroupsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('action_groups', function (Blueprint $table) {
                $table->integer('group_id')->unsigned();
                $table->integer('action_id')->unsigned();
                $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
                $table->foreign('action_id')->references('id')->on('actions')->onDelete('cascade');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('action_groups');
        }

    }
