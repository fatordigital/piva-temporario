<?php namespace Modules\Acl\Database\Seeders;

use Illuminate\Database\Seeder;

class AclDatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('Modules\Acl\Database\Seeders\AclTableSeeder');
        $this->call('Modules\Acl\Database\Seeders\GroupTableSeeder');
    }

}