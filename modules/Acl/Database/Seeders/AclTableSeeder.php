<?php namespace Modules\Acl\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Acl\Entities\Action;

class AclTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Action::firstOrCreate([
            'parent_id'   => 0,
            'http_method' => '*',
            'controller'  => '*',
            'name'        => 'Sistema Geral',
            'route'       => '*',
            'action'      => '*',
            'status'      => true
        ]);
        Action::createRoutes();
    }

}