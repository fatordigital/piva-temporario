<?php namespace Modules\Acl\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Acl\Entities\ActionGroup;
use Modules\Acl\Entities\Group;

class GroupTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group = Group::firstOrCreate([
            'name'      => 'Developers',
            'parent_id' => 0,
            'status'    => 1
        ]);
        $group->action_groups()->save(new ActionGroup(['action_id' => 1]));
    }

}