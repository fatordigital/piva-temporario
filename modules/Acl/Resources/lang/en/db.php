<?php
    $return = [
        'action'   => [
            'create' => 'Find available actions',
            'update' => 'Update actions'
        ],
        'messages' => [
            'success' => 'Actions ready to use',
            'delete' => [
                'success' => 'Group has been removed with success',
                'warning' => 'Some users were connected to this group'
            ]
        ]
    ];

    return $return;