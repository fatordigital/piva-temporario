<?php
    $return = [
        'action'   => [
            'create' => 'Procurar ações disponíveis',
            'update' => 'Atualizar ações'
        ],
        'messages' => [
            'success' => 'Ações prontas para uso',
            'delete'  => [
                'success' => 'Grupo foi removido com sucesso',
                'warning' => 'Alguns usuários estavam relacionados a este grupo'
            ]
        ]
    ];

    return $return;