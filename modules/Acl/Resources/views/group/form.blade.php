<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="form-group">
                <label for="status" class="control-label">{!! trans('dictionary.status') !!}</label>
                <br/>

                <div class="radio radio-success radio-inline">
                    {!! Form::radio('status', 1, true) !!}
                    <label for="radio4">
                        {!! trans('dictionary.status_text.active') !!}
                    </label>
                </div>
                <div class="radio radio-danger radio-inline">
                    {!! Form::radio('status', 0, false) !!}
                    <label for="radio4">
                        {!! trans('dictionary.status_text.inactive') !!}
                    </label>
                </div>
            </div>
            <div class="form-group {!! ($errors->first('name')) ? 'has-error' : '' !!}">
                <label for="name">{!! trans('dictionary.name'). ' (*)' !!}</label>
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('dictionary.name'). ' (*)', 'required' => 'required']) !!}
                {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-body">
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <tr>
                        <th width="5%"></th>
                        <th>{!! trans('dictionary.name') !!}</th>
                        <th>{!! trans('dictionary.route') !!}</th>
                        <th width="5%">{!! trans('dictionary.status') !!}</th>
                        <th width="5%">{!! trans('dictionary.options') !!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($actions as $action)
                        <tr>
                            <td>
                                {!! Form::checkbox('action_id[]', $action->id, null,
                                    [
                                        'title' => 'Selecionar módulo inteiro',
                                        'data-select-all' => $action->id,
                                        (isset($actionsGroup) && in_array($action->id,$actionsGroup)) ? 'checked' : ''
                                    ]
                                ) !!}
                            </td>
                            <td>{!! $action->name !!}</td>
                            <td>{!! $action->route !!}</td>
                            <td>{!! ($action->status) ? 'Ativo' : 'Inativo' !!}</td>
                            <td>
                                <div class="col-md-3">
                                    @if($action->parents->count() > 1)
                                        <i style="cursor: pointer;" class="fa fa-angle-double-down"
                                           data-arrow="{!! $action->id !!}"
                                           data-extend-action="{!! $action->id !!}"
                                           title="extender"></i>
                                    @endif
                                </div>
                            </td>
                        </tr>
                        @if($action->parents)
                            @foreach($action->parents as $sAction)
                                <tr style="display: none; background-color: #add8e6;"
                                    data-action-id="{!! $sAction->parent_id !!}">
                                    <td>
                                        {!! Form::checkbox('action_id[]', $sAction->id, null, ['data-action-id' => $sAction->parent_id, (isset($actionsGroup) && in_array($sAction->id,$actionsGroup)) ? 'checked' : '']) !!}
                                    </td>
                                    <td>{!! $sAction->name !!}</td>
                                    <td>{!! $sAction->route !!}</td>
                                    <td>{!! ($sAction->status) ? 'Ativo' : 'Inativo' !!}</td>
                                </tr>
                            @endforeach
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="col-sm-12">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <button class="btn btn-success pull-right">{!! trans('menu::db.btn.submit') !!}</button>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

@section('cjs')
    {!! HTML::script('js/cms/acl/group.js') !!}
@endsection