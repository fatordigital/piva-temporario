@extends('layouts.cms')

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a href="{!! URL::route('fatorcms.group.create') !!}" class="btn btn-success pull-right">
                {!! trans('dictionary.create') !!} {!! trans('dictionary.group') !!}
            </a>

            <h2>{!! trans('dictionary.group') !!}</h2>
        </div>
        <div class="ibox-content">

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover datatable">
                    <thead>
                    <tr>
                        <th data-ref="name">{!! trans('dictionary.name') !!}</th>
                        <th data-ref="status">{!! trans('dictionary.status') !!}</th>
                        <th data-ref="created_at">{!! trans('dictionary.created_at') !!}</th>
                        <th width="11%">{!! trans('dictionary.options') !!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($list->count())
                        @foreach($list as $row)
                            <tr>
                                <td>{!! $row->name !!}</td>
                                <td>{!! $row->statusText !!}</td>
                                <td>{!! $row->created_at !!}</td>
                                <td>
                                    <a class="btn btn-primary btn-xs pull-left margin-right"
                                       href="{!! URL::route('fatorcms.group.edit', $row->id) !!}">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    {!! Form::open(['route' => ['fatorcms.group.status', $row->id], 'method' => 'PATCH', 'class' => 'form-horizontal pull-left']) !!}
                                    <button class="btn {!! ($row->status) ? 'btn-success' : 'btn-danger' !!} btn-xs margin-right"
                                            title="{!! $row->StatusText !!}">
                                        <i class="glyphicon {!! ($row->status) ? 'glyphicon-ok' : 'glyphicon-off' !!}"></i>
                                    </button>
                                    {!! Form::close() !!}

                                    {!! Form::open(['route' => ['fatorcms.group.destroy', $row->id], 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                                    <button class="btn btn-danger btn-xs">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">Nenhum registro encontrado.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                @include('layouts.elements.paginator', ['paginator' => $list])
            </div>

        </div>
    </div>
@endsection