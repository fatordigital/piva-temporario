@extends('layouts.cms')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-title">
                <h4 class="panel-title">{!! trans('dictionary.create') !!} {!! trans('dictionary.group') !!}</h4>
                <p>{!! trans('messages.form.alert.fields_required') !!}</p>
            </div>
        </div>
        {!! Form::open(['route' => 'fatorcms.group.store']) !!}
        @include('acl::group.form')
        {!! Form::close() !!}
    </div>
@endsection