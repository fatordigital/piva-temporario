@extends('layouts.cms')

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            {!! Form::open(['route' => 'fatorcms.acl.store']) !!}
            <button class="btn btn-success pull-right">
                {!! ($list->count()) ? trans('acl::db.action.update') : trans('acl::db.action.create') !!}
            </button>
            {!! Form::close() !!}

            <h2>{!! trans('dictionary.controller') !!}</h2>
        </div>
        <div class="ibox-content">

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover datatable">
                    <thead>
                    <tr>
                        <th width="20%" data-ref="http_method">HTTP METHOD</th>
                        <th data-ref="name">{!! trans('dictionary.name') !!}</th>
                        <th data-ref="route">{!! trans('dictionary.route') !!}</th>
                        <th data-ref="action">{!! trans('dictionary.action') !!}</th>
                        <th data-ref="status">{!! trans('dictionary.status') !!}</th>
                        {{--<th width="11%">{!! trans('dictionary.options') !!}</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @if ($list->count())
                        @foreach($list as $row)
                            <tr>
                                <td>{!! $row->http_method !!}</td>
                                <td>{!! $row->name !!}</td>
                                <td>{!! $row->route !!}</td>
                                <td>{!! $row->action !!}</td>
                                <td>{!! $row->StatusText !!}</td>
                                {{--<td>--}}
                                    {{--{!! Form::open(['route' => ['fatorcms.acl.destroy', $row->id], 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}--}}
                                    {{--<button class="btn btn-danger btn-xs">--}}
                                        {{--<i class="fa fa-trash-o"></i>--}}
                                    {{--</button>--}}
                                    {{--{!! Form::close() !!}--}}
                                {{--</td>--}}
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">Nenhum registro encontrado.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                @include('layouts.elements.paginator', ['paginator' => $list])
            </div>

        </div>
    </div>
@endsection