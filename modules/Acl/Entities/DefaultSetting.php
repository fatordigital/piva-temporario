<?php namespace Modules\Acl\Entities;

use App\BaseModel;

class DefaultSetting extends BaseModel
{

    protected $fillable = [
        'key',
        'value',
        'status'
    ];

    public static function getSetting($key)
    {
        $setting = parent::whereKey($key)->whereStatus(true)->first();
        if ($setting) {
            return $setting->value;
        } else {
            return $key;
        }
    }

}