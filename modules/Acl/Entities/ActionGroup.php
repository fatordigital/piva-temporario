<?php namespace Modules\Acl\Entities;

use App\BaseModel;

class ActionGroup extends BaseModel
{
    protected $primaryKey = 'group_id';

    protected $fillable = [
        'action_id',
        'group_id'
    ];

    public function users()
    {
        return $this->belongsTo('App\User', 'group_id');
    }

    public function actions()
    {
        return $this->belongsTo('Modules\Acl\Entities\Action', 'action_id');
    }

    public function groups()
    {
        return $this->hasMany('Modules\Acl\Entities\Group', 'group_id');
    }
}