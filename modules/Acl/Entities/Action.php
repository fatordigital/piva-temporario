<?php namespace Modules\Acl\Entities;

use App\BaseModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class Action extends BaseModel
{

    /**
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'http_method',
        'controller',
        'name',
        'route',
        'action',
        'status'
    ];

    /**
     * @return void
     */
    public static function createRoutes()
    {
        $routeCollection = Route::getRoutes();
        foreach ($routeCollection as $route) {
            $array = [];
            preg_match('/([A-Za-z]+)@([a-z]+)/', $route->getActionName(), $a);
            if (isset($a[0])) {
                $array['name'] = $a[0];
                $parent = Action::whereController($a[1])->get()->first();
                Action::firstOrCreate([
                    'parent_id'   => ($parent) ? $parent->id : 0,
                    'http_method' => $route->getMethods()[0],
                    'controller'  => $a[1],
                    'name'        => $array['name'],
                    'route'       => $route->getName(),
                    'action'      => $route->getActionName(),
                    'status'      => true
                ]);
            }
        }
    }

    /**
     * @param $route
     * @return bool
     */
    public static function checkRoute($route)
    {
        if (in_array($route, Auth::user()->getActions())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function action_groups()
    {
        return $this->hasMany('Modules\Acl\Entities\ActionGroup', 'action_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parents()
    {
        return $this->hasMany('Modules\Acl\Entities\Action', 'parent_id');
    }

}