<?php namespace Modules\Acl\Entities;

use App\BaseModel;

class Group extends BaseModel
{
    protected $fillable = [
        'parent_id',
        'name',
        'status'
    ];

    protected $appends = [
        'ParentGroup'
    ];

    public function getParentGroupAttribute()
    {
        if ($this->parent_id == 0) {
            $return = '---';
        } else {
            $return = Group::find($this->parent_id);
            $return = $return->name;
        }
        return $return;
    }

    private static function queryActionGrups($group, $request)
    {
        if ($request->get('action_id')) {
            $action_groups = [];
            foreach ($request->get('action_id') as $action_id) {
                $action_groups[] = new ActionGroup([
                    'action_id' => $action_id
                ]);
            }
            $group->action_groups()->saveMany($action_groups);
        }
    }

    /**
     * @param $request
     * Relaciona o grupo com as ações selecionadas
     */
    public static function customCreate($request)
    {
        $group = parent::firstOrCreate($request->except([
            '_token',
            'action_id'
        ]));
        ActionGroup::destroy($group->id);
        self::queryActionGrups($group, $request);
    }

    /**
     * @param $id
     * @param $request
     */
    public static function customUpdate($id, $request)
    {
        $group = Group::findOrFail($id);
        $group->update($request->except(['_token']));
        ActionGroup::destroy($id);
        self::queryActionGrups($group, $request);
    }

    public function groups()
    {
        return $this->belongsTo('Modules\Acl\Entities\Group', 'parent_id');
    }

    public function action_groups()
    {
        return $this->hasMany('Modules\Acl\Entities\ActionGroup', 'group_id');
    }

    public function actions()
    {
        return $this->belongsToMany('Modules\Acl\Entities\Action', 'action_groups', 'group_id', 'action_id');
    }
}