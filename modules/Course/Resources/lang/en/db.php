<?php
    $return = [
        'add_content'        => 'Add content',
        'info_modal_content' => 'The parameters with (*) are required.',
        'type_content'       => 'Section',
        'required'           => 'Required modules',
        'module_description' => 'Module description',
        'add_module'         => 'Add module',
        'early_description'  => 'Early Description',
        'course_description' => 'Course Description',
        'date'               => 'Course Date'
    ];

    return $return;