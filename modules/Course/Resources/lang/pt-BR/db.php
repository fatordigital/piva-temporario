<?php
    $return = [
        'add_content'        => 'Adicionar conteúdo',
        'info_modal_content' => 'Os parâmetros com (*) são obrigatórios',
        'type_content'       => 'Seção',
        'required'           => 'Módulos obrigatórios',
        'add_module'         => 'Adicionar módulo',
        'early_description'  => 'Pré inscrição',
        'course_description' => 'Desrição do Curso',
        'module_description' => 'Descrição do módulo',
        'date'               => 'Data do Curso'
    ];

    return $return;