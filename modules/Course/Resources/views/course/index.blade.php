@extends('layouts.cms')

@section('title')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Cursos</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Cursos</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="row page-heading white-bg">
        <div class="col-lg-9">
            <br/>
            <a href="{!! URL::route('fatorcms.course.create') !!}" class="btn btn-success">
                {!! trans('dictionary.create') !!} Curso
            </a>
            <a href="{!! route('fatorcms.course.index', 'export=true') !!}" class="btn btn-primary">
                Exportar
            </a>
        </div>
    </div>
@endsection

@section('content')

    @if ($list->count())

        {!! Form::open(['route' => 'fatorcms.course.index', 'method' => 'GET']) !!}
        <div class="col-lg-12">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="name">
                                Nome/Título
                            </label>
                            <div class="input-group">
                                {!! Form::text('search[name]', old('search[name]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                                <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Procurar</button>
                          </span>
                          <span class="input-group-btn">
                            <a class="btn btn-warning" href="{!! route('fatorcms.course.index') !!}">Limpar</a>
                          </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}

        <div class="col-lg-12">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <td>#</td>
                                <td>Nome</td>
                                <td>Link/Url</td>
                                <td>Visitas</td>
                                <td width="5%">Status</td>
                                <td width="10%">Ações</td>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($list->count())
                                @foreach($list as $row)
                                    <tr>
                                        <td>{!! $row->id !!}</td>
                                        <td>{!! $row->name !!}</td>
                                        <td></td>
                                        <td>{!! $row->access !!}</td>
                                        <td>
                                            {!! Form::open(['route' => ['fatorcms.course.status', $row->id], 'method' => 'PATCH', 'class' => 'pull-left']) !!}
                                            @if ($row->status == 0)
                                                <button type="submit" class="btn btn-xs no-rounded btn-primary" data-toggle="tooltip" title="Ativar este curso"><i class="fa fa-arrow-up"></i> Ativar </button>
                                            @else
                                                <button type="submit" class="btn btn-xs no-rounded btn-danger" data-toggle="tooltip" title="Desativar este curso"><i class="fa fa-arrow-down"></i> Desativar </button>
                                            @endif
                                            {!! Form::close() !!}
                                        </td>
                                        <td>
                                            {!! Form::open(['route' => ['fatorcms.course.destroy', $row->id], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deleta o curso '. $row->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                                            <a class="btn btn-xs btn-rounded no-rounded btn-primary" href="{!! route('fatorcms.course.edit', $row->id) !!}" data-toggle="tooltip" title="Editar Curso"><i class="fa fa-pencil"></i></a>
                                            <a class="btn btn-xs btn-rounded no-rounded btn-success" href="{!! route('fatorcms.course.show', $row->alias) !!}" data-toggle="tooltip" title="Turmas"><i class="fa fa-table"></i></a>
                                            <button data-toggle="tooltip" title="Excluir curso" class="btn no-rounded btn-danger btn-xs">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="5">Nenhum registro no momento. <a href="">clique aqui</a> e adicione um curso.</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            @include('layouts.elements.paginator', ['paginator' => $list])
        </div>
    @endif
@endsection

@section('cjs')
    {!! HTML::script('js/cms/scripts/course/index.js') !!}
@endsection