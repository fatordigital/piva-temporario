@extends('layouts.cms')
@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>{!! $show->name !!} - Turmas relacionadas</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li class="active">
                <strong>{!! $show->name !!} - Turmas relacionadas</strong>
            </li>
        </ol>
    </div>
  </div>
@endsection
@section('content')
  <div class="row">
      <div class="col-lg-12">
        <div class="ibox-title">
          <a href="{!! route('fatorcms.course.index') !!}"
             class="btn btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
          </a>
          <a href="{!! route('fatorcms.team.create', 'course='. $show->id) !!}"
             class="btn btn-success">
            Adicionar Turma
          </a>
        </div>
        @if ($show->teams->count())
        <div class="ibox-content table-responsive">
          <table class="table table-bordered">
            <thead>
            <tr>
              <td>Nome/Título</td>
              <td>Total de estudantes</td>
              <td>Valor</td>
              <td>Promoção</td>
            </tr>
            </thead>
            <tbody>
            @foreach($show->teams as $row)
              <tr>
                <td>{!! $row->name !!}</td>
                <td>{!! $row->max_student !!}</td>
                <td>{!! bcoToCoin($row->value) !!}</td>
                <td>{!! bcoToCoin($row->value_discount) !!}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        @endif
      </div>
    @if ($show->teams->count())
      <div class="col-lg-12">
        <h3>Nenhuma turma vinculada a este curso.</h3>
      </div>
    @endif
  </div>
@endsection