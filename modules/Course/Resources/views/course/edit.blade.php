@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Editando Curso - {!! $edit->name !!}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li>
                <a href="{!! route('fatorcms.course.index') !!}">Cursos</a>
            </li>
            <li class="active">
                <strong>Editando Curso - {!! $edit->name !!}</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <p>
        Todos os campos com <strong class="text-danger">(*)</strong> são obrigatórios.
      </p>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.course.index') !!}"
           class="btn btn-warning">
            Voltar
        </a>
      {!! Form::open(['route' => ['fatorcms.course.destroy', $edit->id], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deleta o curso '. $edit->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left margin-right']) !!}
      <button class="btn btn-danger">
        DELETAR
      </button>
      {!! Form::close() !!}
    </div>
  </div>
@endsection

@section('content')
  <div class="ibox float-e-margins">
    <div class="ibox-content">
      {!! Form::model($edit, ['route' => ['fatorcms.course.update', $edit->id], 'method' => 'PATCH', 'files' => true]) !!}
        @include('course::course.form')
      {!! Form::close() !!}
    </div>
  </div>
@endsection