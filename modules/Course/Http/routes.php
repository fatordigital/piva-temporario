<?php

    Route::group([
        'prefix'     => 'fatorcms',
        'middleware' => [
            'acl',
            'auth',
            'cms'
        ],
        'namespace'  => 'Modules\Course\Http\Controllers'
    ], function () {
        Route::resource('course', 'CourseController');
        Route::patch('/course/status/{id}', [
            'as'   => 'fatorcms.course.status',
            'uses' => 'CourseController@status'
        ]);
        Route::delete('course-unlink/{slug}', ['as' => 'fatorcms.course.unlink', 'uses' => 'CourseController@unlink']);
    });

    Route::group([
        'middleware' => [
            'auth',
            'cms'
        ]
    ], function () {
        Route::post('fatorcms/redactor/save/image', [
            'as'   => 'redactor.save.image',
            'uses' => 'App\Http\Controllers\BaseModuleController@ajaxRedactorImage'
        ]);

        Route::post('fatorcms/redactor/save/file', [
            'as'   => 'redactor.save.file',
            'uses' => 'App\Http\Controllers\BaseModuleController@ajaxRedactorFile'
        ]);
    });


