<?php namespace Modules\Course\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Modules\Course\Entities\Course;
use Modules\Course\Entities\CourseCategory;
use Modules\Course\Entities\CourseRelation;
use Modules\Course\Http\Requests\CourseRequest;
use Modules\Team\Entities\Team;

class CourseController extends BaseModuleController
{

    private $randomNoImages = [
        'no-image-knowledge-1.jpg',
        'no-image-knowledge-2.jpg'
    ];

    public function __construct()
    {
        View::share('randomNoImages', $this->randomNoImages);
    }

    public function index()
    {
        $list = Course::orderBy('id', 'desc');
        if (request()->has('search')) {
            foreach (request()->get("search") as $key => $value) {
                if (!empty($value) && $key != 'type') {
                    $list->where($key, 'LIKE', '%' . $value . '%');
                } else if ($key == 'type' && !empty($value)) {
                    $list->where($value, '=', true);
                }
            }
        }
        if (request()->has('export')) {
            $this->exportSimpleExcel(new Course(), $list, 'Cursos');
        }
        $list = $list->paginate(30);
        return view('course::course.index', compact('list'));
    }

    public function show($slug, Course $model)
    {
        $show = $model->with(['teams' => function($q) {
            $q->paginate(15);
        }])->whereAlias($slug)->first();
        if (!$show)
            return redirect()->route('fatorcms.course.index')->with('error', 'Ops, o registro não foi encontrado.');
        return view('course::course.show', compact('show'));
    }

    public function unlink($slug, Request $request, Team $team, Course $course)
    {
        if (!$request->has('_team') || $request->get('_team') == "")
            return redirect()->route('fatorcms.course.show', $slug)->with('error', 'Ops, nenhum registro encontrado.');
        $cQuery = $course->whereAlias($slug)->first();
        $tQuery = $team->whereSlug($request->get('_team'))->whereCourseId($cQuery->id)->first();
        if (!$tQuery || !$cQuery)
            return redirect()->back()->with('error', 'Ops, nenhum registro encontrado.');

        if ($tQuery->update(['course_id' => 0]))
            return redirect()->route('fatorcms.course.show', $slug)->with('success', 'A turma foi desvinculada do curso. *AVISO* Os alunos não terão acesso a nenhuma aula sem o vínculo de um curso');
        else
            return redirect()->route('fatorcms.course.show', $slug)->with('error', 'Ops, não foi possível desvincular o registro, tente novamente.');
    }

    public function create()
    {
        $categories = CourseCategory::orderBy('name')->lists('name', 'id')->toArray();
        $courses = Course::orderBy('id', 'desc')->lists('name', 'id')->toArray();
        return view('course::course.create', compact('categories', 'courses'));
    }

    public function store(Course $model, CourseRequest $request)
    {
        $model->customCreate($request, $this);
        return redirect()->route('fatorcms.course.index')->with('success', trans('messages.insert.success'));
    }

    public function edit($id)
    {
        $edit = Course::findOrFail($id);
        $edit->begin_date = $edit->begin_date && $edit->begin_date != '0000-00-00' ? Carbon::createFromFormat('Y-m-d', $edit->begin_date)->format('d/m/Y') : null;
        $edit->end_date = $edit->end_date && $edit->end_date != '0000-00-00' ? Carbon::createFromFormat('Y-m-d', $edit->end_date)->format('d/m/Y') : '';

        $categories = CourseCategory::orderBy('name')->lists('name', 'id')->toArray();
        $courses = Course::where('id', '!=', $edit->id)->orderBy('id', 'desc')->lists('name', 'id')->toArray();
        return view('course::course.edit', compact('edit', 'categories', 'courses'));
    }

    public function update(CourseRequest $request, Course $model, $id)
    {
        if ($model->customUpdate($id, $request, $this)) {
            n('fatorcms.course.update', 'Atualização de curso', 'Curso ' . $request->get('name') . ', foi atualizado.');
            return redirect()->route('fatorcms.course.index')->with('success', 'Curso foi atualizado com sucesso.');
        }

        return redirect()->route('fatorcms.course.edit', $id)->with('error', 'Ops, não foi possível atualizar o curso, tente novamente.');
    }

    public function status($id)
    {
        $status = Course::findOrFail($id);
        $to = ($status->status) ? 0 : 1;
        $status->status = $to;
        $status->save();
        return redirect()->route('fatorcms.course.index')->with('success', trans('messages.status.success', [
            'status' => ($to) ? trans('dictionary.status_text.active') : trans('dictionary.status_text.inactive')
        ]));
    }

    public function destroy($id)
    {
        $destroy = Course::find($id);
        if (!$destroy || (isset($destroy) && !$destroy->delete()))
            return redirect()->route('fatorcms.course.index')->with('error', 'Ops, não foi possível deletar este registro. Tente novamente.');
        else
            return redirect()->route('fatorcms.course.index')->with('success', 'O curso foi deletado com sucesso.');
    }

}