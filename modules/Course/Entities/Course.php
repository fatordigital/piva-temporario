<?php namespace Modules\Course\Entities;

use App\BaseModel;
use App\Events\FilesEvent;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Request;

class Course extends BaseModel
{

    use SoftDeletes;

    protected $fillable = [
        'course_category_id',
        'alias',
        'contest',
        'name',
        'resume',
        'value',
        'value_discount',
        'filename',
        'discount',
        'early_description',
        'description',
        'status',
        'filename_box',
        'access',
        'begin_date',
        'end_date'
    ];

    public $excel = [
        'Nome', 'Resumo', 'Status', 'Data de registro', 'Data de alteração no registro'
    ];

    public $excelModel = [
        'name', 'resume', 'status', 'created_at', 'updated_at'
    ];


    public function category()
    {
        return $this->belongsTo('Modules\Course\Entities\CourseCategory', 'course_category_id');
    }

    public function relations()
    {
        return $this->hasMany('Modules\Course\Entities\CourseRelation', 'course_id');
    }

    public function teams()
    {
        return $this->hasMany('Modules\Team\Entities\Team', 'course_id');
    }

    public function showTeams()
    {
        return $this->hasMany('Modules\Team\Entities\Team', 'course_id')->orderBy('created_at', 'desc');
    }

    public function customCreate($request, $controller)
    {

        $request->merge(['alias' => str_slug($request->get('name'), '-')]);
        $request->merge(['value' => coinToBco($request->get('value'))]);
        $request->merge(['value_discount' => coinToBco($request->get('value_discount'))]);
        if ($request->has('begin_date')) {
            $request->merge(['begin_date' => Carbon::createFromFormat('d/m/Y', $request->get('begin_date'))->format('Y-m-d')]);
            $request->merge(['end_date' => Carbon::createFromFormat('d/m/Y', $request->get('end_date'))->format('Y-m-d')]);
        }

        if ($request->hasFile('file')) {
            event(new FilesEvent($request->file('file'), array('1900x564'), 'assets/courses/', function ($filename) use ($request) {
                $request->merge(['filename' => $filename]);
            }));
//            $filename = $controller->customFileName($request, 'assets/courses/', 'file');
//            if ($request->file('file')->move(public_path('assets/courses/'), $filename)) {
//                $request->merge(['filename' => $filename]);
//            }
        }

        if ($request->hasFile('file_box')) {
            event(new FilesEvent($request->file('file_box'), array(), 'assets/courses/', function ($filename) use ($request) {
                $request->merge(['filename_box' => $filename]);
            }, 'file_box'));
        }

        $status = $this->create($request->except(['relations']));

        if ($request->has('relations')) {
            foreach ($request->get('relations')['course_id'] as $relation) {
                echo $relation;
                CourseRelation::firstOrCreate(['relation_id' => $relation, 'course_id' => $status->id]);
            }
        }

        return $status;
    }

    public function customUpdate($id, $request, $controller)
    {
        $edit = $this->find($id);
        if (!$edit)
            return false;

        $request->merge(['alias' => str_slug($request->get('name'), '-')]);
        $request->merge(['value' => coinToBco($request->get('value'))]);
        $request->merge(['value_discount' => coinToBco($request->get('value_discount'))]);
        if ($request->has('begin_date')) {
            $request->merge(['begin_date' => Carbon::createFromFormat('d/m/Y', $request->get('begin_date'))->format('Y-m-d')]);
            $request->merge(['end_date' => Carbon::createFromFormat('d/m/Y', $request->get('end_date'))->format('Y-m-d')]);
        }


        if ($request->hasFile('file')) {
            event(new FilesEvent($request->file('file'), array('1900x564'), 'assets/courses/', function ($filename) use ($request) {
                $request->merge(['filename' => $filename]);
            }));
        }


        if ($request->hasFile('file_box')) {
            event(new FilesEvent($request->file('file_box'), array(), 'assets/courses/', function ($filename) use ($request) {
                $request->merge(['filename_box' => $filename]);
            }, 'file_box'));
        }

        if ($request->has('relations')) {
            foreach ($request->get('relations')['course_id'] as $relation) {
                CourseRelation::firstOrCreate(['relation_id' => $relation, 'course_id' => $id]);
            }
        } else {
            foreach (CourseRelation::where('course_id', '=', $id)->get(['id'])->toArray() as $register) {
                CourseRelation::destroy($register['id']);
            }
        }

        return $edit->update($request->except(['relations']));
    }

}