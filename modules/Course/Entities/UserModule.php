<?php namespace Modules\Course\Entities;
   
use App\BaseModel;

class UserModule extends BaseModel {

    protected $fillable = [
        'user_id',
        'module_id',
        'status'
    ];

    public function modules()
    {
        return $this->belongsTo('Modules\Course\Entities\Module', 'module_id');
    }

    public function users()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'user_id');
    }

    public static function getCountTeachers($product, $delegateGroup = 4)
    {
        $query = parent::with(['users' => function($q) use ($delegateGroup) {
            $q->whereGroupId($delegateGroup)->count();
        }])->whereModuleId($product)->get();

        if (!$query)
            return 0;

        $count = 0;
        foreach ($query as $q) {
            if ($q->users) {
                if ($q->users->group_id == $delegateGroup) {
                    $count++;
                }
            }
        }
        return $count;
    }

}