<?php namespace Modules\Course\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseRelation extends BaseModel
{

    use SoftDeletes;

    protected $fillable = [
        'course_id',
        'relation_id'
    ];

}