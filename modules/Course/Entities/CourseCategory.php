<?php namespace Modules\Course\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseCategory extends BaseModel
{

    use SoftDeletes;

    protected $fillable = [
        'name',
        'slug'
    ];

}