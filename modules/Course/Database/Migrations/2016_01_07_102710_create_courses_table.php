<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateCoursesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('courses', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_category_id')->default(0);
                $table->string('alias');
                $table->string('code', 15);
                $table->string('name', 150);
                $table->decimal('value', 10, 2)->nullable();
                $table->boolean('early_description')->default(0);
                $table->text('description')->nullable();
                $table->boolean('status')->default(1);
                $table->integer('access')->default(0);
                $table->date('begin_date')->nullable();
                $table->date('end_date')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('courses');
        }

    }
