<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateContentsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('contents', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('section_id');
                $table->string('name', 100);
                $table->string('code', 15);
                $table->text('text')->nullable();
                $table->string('link')->nullable();
                $table->string('video')->nullable();
                $table->boolean('video_live')->default(0);
                $table->string('file')->nullable();
                $table->integer('type_question_id')->nullable();
                $table->string('icon')->nullable();
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('contents');
        }

    }
