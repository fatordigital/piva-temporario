<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSectionsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('sections', function (Blueprint $table) {
                $table->increments('id');
                $table->string('filename')->nullable();
                $table->integer('module_id')->nullable();
                $table->string('name', 100);
                $table->string('code', 15);
                $table->string('resume')->nullable();
                $table->text('description')->nullable();
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('sections');
        }

    }
