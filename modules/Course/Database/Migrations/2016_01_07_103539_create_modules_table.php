<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateModulesTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('modules', function (Blueprint $table) {
                $table->increments('id');
                $table->string('alias');
                $table->string('code', 15);
                $table->string('name', 150);
                $table->decimal('value', 10, 2);
                $table->string('resume')->nullable();
                $table->integer('installment', 2)->default(0);
                $table->string('media', 10)->nullable();
                $table->integer('required')->nullable();
                $table->text('description')->nullable();
                $table->boolean('status')->default(1);
                $table->integer('access')->default(0);
                $table->date('begin_date');
                $table->date('end_date');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('modules');
        }

    }
