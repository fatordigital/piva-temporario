<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="description">
                            <i class="fa fa-question-circle" data-toggle="tooltip" title="Evite a usar imagem, algumas caixas de e-mail entendem como spam."></i> Mensagem <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::textarea('message', null, ['class' => 'form-control summernote']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <button class="btn btn-success">
                            <i class="fa fa-save"></i> Salvar notificação
                        </button>
                        <a href="{!! route('fatorcms.alert.index') !!}" class="btn btn-warning">
                            <i class="fa fa-arrow-left"></i> Voltar
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>