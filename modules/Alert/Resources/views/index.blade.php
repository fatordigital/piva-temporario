@extends('layouts.cms')

@section('title')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Notificações</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
                </li>
                <li class="active">
                    <strong>Notificações</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="row page-heading white-bg">
        <div class="col-lg-9">
            <br />
            <a href="#" data-target="#myModal5" class="btn btn-default" data-title="title" data-toggle="modal" title="Avisos, notificações, alertas...">
                <i class="fa fa-paper-plane"></i> Notificações
            </a>
        </div>
    </div>
@endsection

@section('content')

    @include('team::modal')

    <div class="ibox float-e-margins">

        <div class="ibox-content">

            <div class="table-responsive">
                <table class="table table-bordered datatable">
                    <thead>
                    <tr>
                        <td>Usuário</td>
                        <td>Turma</td>
                        <td>Mensagem</td>
                        <td>Até dia</td>
                        <th width="11%">{!! trans('dictionary.options') !!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (isset($list) && $list->count())
                        @foreach($list as $item)
                            <tr>
                                <td>{!! $item->User ? $item->User->name : '' !!}</td>
                                <td>{!! $item->Team ? $item->Team->name : '' !!}</td>
                                <td>{!! substr(strip_tags($item->message), 0, 60) !!}</td>
                                <td>{!! dateToSql($item->end_date, 'Y-m-d', 'd/m/Y') !!}</td>
                                <td>
                                    <a class="btn btn-primary btn-xs pull-left margin-right"
                                       href="{!! URL::route('fatorcms.alert.edit', $item->id) !!}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    {!! Form::open(['route' => ['fatorcms.alert.destroy', $item->id], 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                                    <button class="btn btn-danger btn-xs">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">Nenhum notificação efetuada no momento. <a href="#" data-target="#myModal5" class="text text-success" data-title="title" data-toggle="modal" title="Avisos, notificações, alertas...">Clique aqui para enviar uma notificação</a></td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                @include('layouts.elements.paginator', ['paginator' => $list])
            </div>

        </div>
    </div>

@endsection

@section('cjs')
    {!! HTML::script('js/cms/scripts/team/index.js') !!}
@endsection