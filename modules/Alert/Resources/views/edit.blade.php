@extends('layouts.cms')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-title">
                <h4 class="panel-title">{!! trans('dictionary.edit') !!} Notificação</h4>
                <p>{!! trans('messages.form.alert.fields_required') !!}</p>
            </div>
        </div>
        {!! Form::model($edit, ['route' => ['fatorcms.alert.update', $edit->id], 'method' => 'PATCH']) !!}
        @include('alert::form')
        {!! Form::close() !!}
    </div>
@endsection