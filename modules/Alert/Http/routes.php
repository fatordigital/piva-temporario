<?php

Route::group([
    'prefix' => 'fatorcms',
    'middleware' => [
        'auth',
        'cms'
    ],
    'namespace' => 'Modules\Alert\Http\Controllers'
], function () {
    Route::resource('alert', 'AlertController');
});
