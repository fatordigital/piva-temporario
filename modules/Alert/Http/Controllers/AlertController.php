<?php namespace Modules\Alert\Http\Controllers;

use App\User;
use Modules\Course\Entities\Course;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamAlert;
use Pingpong\Modules\Routing\Controller;

class AlertController extends Controller
{

    public function lists()
    {
        $courses = Course::orderBy('id', 'desc')->lists('name', 'id')->toArray();
        $teams = Team::orderBy('name')->lists('name', 'id')->toArray();
        if (auth()->user()->isAdmin())
            $users = User::orderBy('name')->get()->lists('NameEmail', 'id')->toArray();
        else
            $users = User::where('group_id', '!=', 1)->orderBy('name')->get()->lists('NameEmail', 'id')->toArray();

        view()->share(compact('courses', 'modules', 'users', 'teams'));
    }

    public function index()
    {
        $this->lists();
        return view('alert::index', [
            'list' => TeamAlert::orderBy('created_at', 'desc')->paginate(30)
        ]);
    }

    public function edit($id, TeamAlert $model)
    {
        $edit = $model->findOrFail($id);
        return view('alert::edit', [
            'edit' => $edit
        ]);
    }

    public function update($id, TeamAlert $model)
    {
        if (request('_method') == 'PATCH') {
            $edit = $model->find($id);
            if ($edit) {
                if ($edit->update(request()->all())) {
                    return redirect()->route('fatorcms.alert.index')->with('success', 'Notificação atualizada com sucesso.');
                }
            }
        }
        return redirect()->route('fatorcms.alert.index')->with('error', 'Desculpe, não foi possível atualizar a notificação.');
    }

    public function destroy($id, TeamAlert $model)
    {
        if (request('_method') == 'DELETE') {
            $data = $model->find($id);
            if ($data) {
                if ($data->delete()) {
                    return redirect()->route('fatorcms.alert.index')->with('success', 'Notificação deletada com sucesso.');
                } else {
                    return redirect()->route('fatorcms.alert.index')->with('error', 'Erro ao deleta a notificação, tente novamente.');
                }
            }
        }
        return redirect()->route('fatorcms.alert.index')->with('error', 'Desculpe, não foi possível deletar a notificação.');
    }

}