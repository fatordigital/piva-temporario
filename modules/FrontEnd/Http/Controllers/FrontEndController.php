<?php namespace Modules\FrontEnd\Http\Controllers;

use App\Http\Controllers\FrontDefaultController;

use Illuminate\Support\Facades\Config;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Modules\News\Entities\News;
use Modules\Order\Entities\Coupon;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderHistoricalStatus;
use Modules\Order\Entities\OrderItem;
use Modules\Order\Entities\PagSeguro;
use Modules\Order\Entities\PagseguroCode;
use Modules\Popup\Entities\Popup;
use Modules\Team\Entities\Team;
use Modules\User\Entities\User;
use Modules\User\Entities\UserSocialize;

class FrontEndController extends FrontDefaultController
{

    protected $rules = ['name', 'email', 'subject', 'message'];

    protected $messages = [
        'name' => 'Preenchimento obrigatório',
        'email' => 'Preenchimento obrigatório',
        'subject' => 'Preenchimento obrigatório',
        'message' => 'Preenchimento obrigatório',
    ];

    private function modulocoaching($ref)
    {
        if (in_array($ref, array(25, 26, 27, 32, 33))) {
            return '1';
        } else if (in_array($ref, array(28, 29, 34, 35))) {
            return '2';
        } else if (in_array($ref, array(30, 31, 36, 37))) {
            return '3';
        }
    }

    public function insight_txt($pedido_id = null)
    {
        $implode = '';
        $query = OrderItem::with(['order' => function ($q) {
            $q->with(['user' => function ($q) {
                $q->with('socialize');
            }]);
        }])->whereIn('ref', array(25, 26, 27, 32, 33, 28, 29, 34, 35, 30, 31, 36, 37))->get()->toArray();
        if ($query) {
            foreach ($query as $item) {
                $user = $item['order']['user'];
                $socialize = $item['order']['user']['socialize'];
                $nome = explode(' ', $user['name']);
                $txt = [
                    'nome' => str_limit($nome[0], 60, ''),
                ];
                if (isset($nome[1])) {
                    if (isset($nome[0]))
                        unset($nome[0]);
                    $sobrenome = implode(' ', $nome);
                } else {
                    $sobrenome = '-';
                }
                $txt += [
                    'sobrenome' => str_limit($sobrenome, 32, ''),
                    'cpf' => $socialize['national_id'],
                    'rg' => '',
                    'email' => $user['email'],
                    'telefone' => $socialize['phone'],
                    'celular' => $socialize['phone'],
                    'datanascimento' => $socialize['birth_date'],
                    'sexo' => $socialize['gender'],
                    'estado_civil' => '',
                    'endereco' => $socialize['route'],
                    'numero' => str_limit($socialize['route_number'], 10, ''),
                    'bairro' => $socialize['district'],
                    'cidade' => $socialize['city'],
                    'cep' => $socialize['postal_code'],
                    'uf' => $socialize['state'],
                    'modulocoaching' => $this->modulocoaching($item['ref'])
                ];
                $implode .= implode(';', $txt);
            }
        }


        File::put(public_path('exportacao/insight/' . date('d-m-Y') . '.txt'), $implode);
        header('Content-type: text/plain');
        header("Content-Disposition: attachment; filename=insight-" . date('d-m-Y') . ".txt");

        echo $implode;
        exit;
    }


    public function index(News $newsModel)
    {
        $popup = Popup::whereNotNull('image')
            ->whereRaw('( 
            (DATE(`release_date`) <= ? || `release_date` IS NULL) 
            &&
            (DATE(`closed_date`) <= ? || `closed_date` IS NULL)
            )', [Carbon::now(), Carbon::now()])->orderBy('created_at', 'desc')->first();

        view()->share(compact('popup'));


//        if (!$principal)
//            $news = $model->orderBy('created_at', 'desc')->limit(3)->get(['new_category_id', 'name', 'slug', 'text', 'image']);
//        else {
//            $news = $model->where('principal', '=', true)->orderBy('created_at', 'desc')->limit(3)->get(['new_category_id', 'name', 'slug', 'text', 'image']);
//            if ($news->count() == 0) {
//                $news = $model->orderBy('created_at', 'desc')->limit(3)->get(['new_category_id', 'name', 'slug', 'text', 'image']);
//            }
//        }
//        View::share(compact('news'));

        $news = $newsModel->where('principal', '=', true)
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get(['new_category_id', 'name', 'slug', 'text', 'image']);
        
        $this->loadCourses(new Team(), [], true);
        $this->loadBanners(new Team());


        return view('frontend::home.index', compact('news'));
    }

    public function contact()
    {
        view()->share('titleHead', 'Contato');
        return view('frontend::contact.contact');
    }

    public function contactStore()
    {
        if (!$this->customEncryptValidate(request())) {
            return response()->json(CryptoJSEncrypt(request()->old() + ['msg' => 'Verifique se todos os campos foram preenchidos corretamente.', 'error' => true]), 200);
        } else {
            $mail = request()->except(['message']);
            $mail['mensagem'] = request('message');
            Mail::send('emails.contact', $mail, function ($m) use ($mail) {
//                $m->from('contato@professorpiva.com.br', 'Professor Piva');
//                $m->replyTo(request('email'), request('name'));
                $m->bcc('andrew.rodrigues@fatordigital.com.br', 'Andrew');
                $m->to('contato@professorpiva.com.br')->subject('Contato');
            });
            return response()->json(CryptoJSEncrypt(['msg' => 'Obrigado, seu contato foi enviado com sucesso.']), 200);
        }
    }

    public function couponValidate(Coupon $model)
    {
        if (session()->has('cart.coupon')) {
            return response()->json(CryptoJSEncrypt(['msg' => 'Você já adicionou um cupom no carrinho.', 'error' => true]), 200);
        }
        $this->rules = ['coupon'];
        $this->messages = ['coupon' => 'Digite um código válido para cupom.'];
        if (!$this->customEncryptValidate(request())) {
            return response()->json(CryptoJSEncrypt(request()->old() + ['msg' => 'Verifique se todos os campos foram preenchidos corretamente.', 'error' => true]), 200);
        } else {
            $coupon = $model->validateCoupon(request()->get('coupon'));
            if (is_object($coupon)) {
                return response()->json(CryptoJSEncrypt(request()->old() + ['msg' => 'O cupom foi adicionado no seu carrinho de compras', 'data' => [
                        'total' => bcoToCoin(session('cart.data.total')),
                        'discount' => bcoToCoin(session('cart.data.discount'))
                    ], 'error' => false]), 200);
            } else {
                return response()->json(CryptoJSEncrypt(request()->old() + ['msg' => $coupon === false ? 'Cupom não é válido.' : $coupon, 'error' => true]), 200);
            }
        }
    }
}