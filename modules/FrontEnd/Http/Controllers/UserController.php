<?php namespace Modules\FrontEnd\Http\Controllers;

use App\Http\Controllers\FrontDefaultController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Certified\Entities\Certified;
use Modules\FrontEnd\Http\Requests\RegisterRequest;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\PagSeguro;
use Modules\Team\Entities\TeamAlert;
use Modules\Team\Entities\TeamUser;
use Modules\User\Entities\User;
use Modules\User\Entities\UserSocialize;

class UserController extends FrontDefaultController
{

    private $pdf;
    protected $rules = ['name', 'email', 'password'];

    protected $messages = [
        'name' => 'Preenchimento obrigatório',
        'email' => 'Preenchimento obrigatório',
        'password' => ''
    ];


    public function fastRegister(Request $request, User $model)
    {
        if (!$this->customEncryptValidate($request)) {
            return response()->json(CryptoJSEncrypt($request->old() + ['msg' => 'Verifique se todos os campos foram preenchidos corretamente.', 'error' => true]), 200);
        } else {
            return response()->json(CryptoJSEncrypt($model->customFastRegister($request)), 200);
        }
    }

    public function login()
    {
        return view('frontend::auth.login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('front.home');
    }

    public function verify()
    {
        if ($this->captchaValidate(request())) {
            $this->verifyEmptyPassAndChange(request('email'));
            if (Auth::attempt(request()->only(['email', 'password']))) {
                return $this->requestRoute('front.home');
            }
            return $this->requestRoute('front.login', true)->with('error', 'Verifique se todos os campos foram digitados corretamente.');
        } else {
            return redirect()->route('front.login')->with('error', 'Não foi possível efetuar seu login, tente novamente.');
        }
    }

    private function verifyEmptyPassAndChange($email)
    {
        $user = User::where('email', '=', $email)->first();
        if ($user && $user->password == '') {
            if (request()->has('pass'))
                $user->update(['password' => bcrypt(request('pass'))]);
            else {
                $user->update(['password' => bcrypt(request('password'))]);
            }
        }
    }

    private function explodeRedirect()
    {
        $referer = parse_url(request()->header('referer'));
        if (isset($referer['query']) && !empty($referer['query'])) {
            $explode = substr($referer['query'], 9, strlen($referer['query']));
            if (!$explode || !base64_decode($explode, true))
                return false;
            return base64_decode($explode);
        }
        return request()->server('HTTP_REFERER');
    }

    public function verifyAjax()
    {
        $this->rules = ['email', 'pass'];
        $this->messages = [
            'email' => 'Preenchimento obrigatório',
            'pass' => 'Preenchimento Obrigatório'
        ];
        if (!$this->customEncryptValidate(request())) {
            return response()->json(CryptoJSEncrypt(request()->old() + ['msg' => 'Verifique se todos os campos foram preenchidos corretamente.', 'error' => true]), 200);
        } else {
            $this->verifyEmptyPassAndChange(request('email'));
            if (Auth::attempt(['email' => request()->get('email'), 'password' => request()->get('pass')])) {
                session()->save();
                return response()->json(CryptoJSEncrypt(['msg' => 'Seu login foi efetuado com sucesso.', 'studentAreaUrl' => route('front.user.home'), 'redirectUrl' => route('front.user.home'), 'append' => $this->effectHtml(), 'error' => false]), 200);
            }
            return response()->json(CryptoJSEncrypt(request()->old() + ['msg' => 'Verifique se todos os campos foram preenchidos corretamente.', 'redirectUrl' => $this->explodeRedirect(), 'error' => true]), 200);
        }
    }

    private function effectHtml()
    {
        return '<div class="logado" style="display: block;"><p class="pull-left">' . str_limit(auth()->user()->name, 25) . '</p><a href="' . route('front.logout') . '"}" class="logout pull-right">SAIR</a></div>';
    }

    public function checkMe(PagSeguro $pagSeguro)
    {
        $validate = $pagSeguro->validateFields();
        if (empty(end($validate)))
            return response()->json(CryptoJSEncrypt(['error' => false, 'msg' => 'Usuário está com todos os dados preenchidos.']), 200);
        else {
            return response()->json(CryptoJSEncrypt(['error' => true, 'msg' => 'Dados obrigatórios precisam ser preenchidos.', 'data' => end($validate)]), 200);
        }
    }

    public function saveRegister(PagSeguro $pagSeguro)
    {
        request()->merge(CryptoJSDecrypt(request()->get('request')));
        if (!Auth::guest() && request()->has('field') && request()->has('value')) {

            if ($this->changeField(request()->get('field')) == false)
                return response()->json(CryptoJSEncrypt(['error' => true, 'msg' => 'Verifique se o campo foi digitado corretamente.']), 200);

            $user = User::find(Auth::user()->id);
            $socialize = UserSocialize::whereUserId(Auth::user()->id)->first();
            if (!$socialize) {
                UserSocialize::create(['user_id' => Auth::user()->id]);
                $socialize = UserSocialize::whereUserId(Auth::user()->id)->first();
            }
            if (request()->get('field') == 'name' || request()->get('field') == 'email') {
                $status = $user->update([request()->get('field') => request()->has('value')]);
            } else {
                $status = $socialize->update([request()->get('field') => request()->get('value')]);
            }
            if ($status) {
                $validate = $pagSeguro->validateFields();
                if (empty(end($validate)))
                    return response()->json(CryptoJSEncrypt(['error' => false, 'msg' => 'Registro salvo com sucesso']), 200);
                else {
                    return response()->json(CryptoJSEncrypt(['error' => false, 'msg' => 'Dados obrigatórios precisam ser preenchidos.', 'data' => end($validate)]), 200);
                }
            } else {
                return response()->json(CryptoJSEncrypt(['error' => true, 'msg' => 'Falha ao salvar este registro, tente novamente.']), 200);
            }
        }
    }

    public function changeField($field, $value = 'value')
    {
        if ($field) {
            if ($field == 'birth_date')
                request()->merge([$value => dateToSql(request()->get($value))]);
            if ($field == 'national_id') {
                if (!cpfValidate(request()->get($value))) {
                    return false;
                }
            }
            if ($field == 'phone') {
                $phone = preg_replace('/[^0-9]/', '', request()->get($value));
                if (!in_array(strlen($phone), [10, 11])) {
                    return false;
                }
            }
            if ($field == 'postal_code') {
                if (!cepValidate(request()->get($value)))
                    return false;
            }
        }
        return true;
    }

    public function home(TeamAlert $model)
    {
        $this->loadUserCourses(auth()->user()->id);
        $this->loadEndTests(auth()->user()->id);

        $teams = TeamUser::whereUserId(auth()->user()->id)->lists('team_id')->toArray();

        $team_alert = $model->whereUserId(auth()->user()->id)->whereRaw('(DATE(end_date) > DATE(?) OR end_date IS NULL)', [date('Y-m-d')])
            ->orWhereIn('team_id', $teams)->whereRaw('(DATE(end_date) >= DATE(?) OR end_date IS NULL)', [date('Y-m-d')])->orderBy('created_at', 'DESC')->get();

        foreach ($team_alert as $index => $team) {
            $decode = json_decode($team->read, true);
            if (is_array($decode) && in_array(auth()->user()->id, $decode)) {
                unset($team_alert[$index]);
            }
        }


        foreach ($team_alert as $team) {
            $decode = json_decode($team->read, true);
            if (is_array($decode) && !in_array(auth()->user()->id, $decode))
                array_push($decode, auth()->user()->id);
            else {
                $decode = [auth()->user()->id];
            }
            $team->update(['read' => json_encode($decode)]);
        }

        return view('frontend::user.home', [
            'alerts' => $team_alert
        ]);
    }

    public function notifications(TeamAlert $model)
    {
        $teams = TeamUser::whereUserId(auth()->user()->id)->lists('team_id')->toArray();
        $team_alert = $model->whereUserId(auth()->user()->id)->whereRaw('(DATE(end_date) > DATE(?) OR end_date IS NULL)', [date('Y-m-d')])
            ->orWhereIn('team_id', $teams)->whereRaw('(DATE(end_date) >= DATE(?) OR end_date IS NULL)', [date('Y-m-d')])->orderBy('created_at', 'DESC')->get();

        return view('frontend::user.notification', [
            'alerts' => $team_alert
        ]);
    }

    public function register()
    {
        $user = User::with('socialize')->find(auth()->user()->id);
        return view('frontend::user.register', compact('user'));
    }

    public function guestRegister()
    {
        return view('frontend::user.guest-register');
    }

    public function storeGuestRegister(RegisterRequest $request)
    {
        $request->merge(['password' => bcrypt($request->get('password'))]);
        $request->merge(['group_id' => 5]);
        $store = User::create($request->except(['_token', 'submit', 'captcha', 'r_password', 'user_socializes']));
        if ($store) {
            UserSocialize::create(['user_id' => $store->id] + $request->get('user_socializes'));
            Auth::login(User::find($store->id));
            return $this->requestRoute('front.home')->with('success', 'Obrigado por se cadastrar, você pode acessar alguns cursos e simulados grátis.');
        } else {
            return redirect()->route('front.user.register.guest')->with('error', 'Ops, não foi possível salvar seus registros, tente novamente.');
        }
    }

    public function updateRegister()
    {

        $this->validate(request(), ['name' => 'regex:/([a-zA-Zà-ú]+) ([a-zA-Zà-ú]+)/', 'socialize.route_number' => 'alpha_num'], [
            'name.regex' => 'Digite o nome completo',
            'socialize.route_number.alpha_num' => 'O número do endereço é apenas número.'
        ]);

        if (request()->has('password')) {
            if (request('password') != request('r_password'))
                return redirect()->route('front.user.register')->withInput(request()->all());
        }

        $user = User::find(auth()->user()->id);
        $user->update(request()->except('socialize'));

        $socialize = UserSocialize::whereUserId(auth()->user()->id)->first();
        if (!$socialize) {
            UserSocialize::create(['user_id' => auth()->user()->id]);
            $socialize = UserSocialize::whereUserId(auth()->user()->id)->first();
        }
        if ($socialize->update(request()->get('socialize')))
            return $this->requestRoute('front.user.register')->with('success', 'Seus dados foram atualizados, obrigado.');
        else {
            return redirect()->route('front.user.register')->with('error', 'Falha ao salvar os dados, tente salvar novamente. Caso o erro persista, tente mais tarde.')->withInput(request()->all());
        }
    }

    public function teachers()
    {
        foreach (User::whereNull('slug')->get() as $item) {
            if ($item->name != '') {
                $item->update(['slug' => str_slug($item->name), '-']);
            }
        }


        $users = User::whereGroupId(7)->get();
        return view('frontend::user.teachers', compact('users'));
    }

    public function tests()
    {
        $this->loadFreeTests(auth()->user()->id);
        $this->loadEndTests(auth()->user()->id);
        return view('frontend::user.simulates');
    }

    public function certified()
    {
        $certifieds = Certified::whereUserId(auth()->user()->id)->orWhereIn('team_id', session('user_team_id'))->get();
        return view('frontend::user.certified', compact('certifieds'));
    }

    public function downloadCertified($slug)
    {
        if (base64_decode($slug, true)) {
            $slug = CryptoJSDecrypt(base64_decode($slug));
            if ($slug) {
                $certified = Certified::where('id', '=', $slug)->whereUserId(auth()->user()->id)->first();
                if ($certified) {
                    $certified = Certified::changeCommands($certified->certified, auth()->user()->toArray(), $certified->team ? $certified->team->toArray() : []);
                    $html = view('frontend::user.pdf', compact('certified'))->render();
                    \PDF::load($html, 'A2')->filename('certificado' . date('Ymdus') . '.pdf')->download();
                    exit;
                } else {
                    $certified = Certified::where('id', '=', $slug)->whereIn('team_id', session('user_team_id'))->first();
                    if ($certified) {
                        $certified = Certified::changeCommands($certified->certified, auth()->user()->toArray(), $certified->team->toArray());
                        $html = view('frontend::user.pdf', compact('certified'))->render();
                        \PDF::load($html, 'A2')->filename('certificado' . date('Ymdus') . '.pdf')->download();
                        exit;
                    }
                }

            }
        }
        return redirect()->route('front.user.certified')->with('error', 'Desculpe, certificado não encontrado.');
    }

    public function orders()
    {
        if (auth()->guest())
            return redirect('/');
        $user_orders = Order::where('user_id', '=', auth()->user()->id)->orderBy('created_at', 'desc')->get();
        return view('frontend::user.orders', compact('user_orders'));
    }

}