<?php namespace Modules\FrontEnd\Http\Controllers;

use App\Http\Controllers\FrontDefaultController;
use Carbon\Carbon;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamUser;

class EstudosController extends FrontDefaultController
{

    # Carrega os dados da turma e as aulas
    public function index($slug, Team $team, TeamUser $teamUser)
    {
        cache()->flush();

        # Carrega os dados da turma e as aulas, e transforma em cache.
        $show = $team->loadTeam($slug);
        # Se não existir a turma, o usuário volta pra página anterior com aviso de problema.
        if (!$show) {
            return redirect()->back()->with('error', 'Ops! não foi possível encontrar o curso para estudo, tente novamente, caso o erro persista, entre em contato conosco.');
        }

        $access = $teamUser->where('user_id', '=', auth()->user()->id)
            ->whereRaw(' ( DATE(`end_date`) >= ? || ( `end_date` IS NULL || `end_date` = "0000-00-00" ) ) ', [Carbon::today()])
            ->lists('team_id')
            ->toArray();

        # Verifica se o usuário tem acesso para estudar
        if (!in_array($show->id, $access)) {
            return redirect()->back();
        }


        l('Começou a estudar [' . $show->name . '][' . $show->course->name . ']', null, $show->id, $show->course->id);

        return view('frontend::estudar.index', compact('show'));
    }

}