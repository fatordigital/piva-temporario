<?php namespace Modules\FrontEnd\Http\Controllers;

use App\Http\Controllers\FrontDefaultController;
use Illuminate\Http\Request;
use Modules\News\Entities\News;

class NewsController extends FrontDefaultController
{

    public function index(Request $request, News $model)
    {
        $list = $model->orderBy('created_at', 'desc');
        if ($request->has('q')) {
            $request->merge(['q' => str_slug($request->get('q'), ' ')]);
            $list->where('name', 'LIKE', '%' . $request->get('q') . '%')->orWhere('text', 'LIKE', '%' . $request->get('q') . '%');
            $request->session()->flashInput($request->all());
        }
        $list = $list->paginate(100);
        view()->share('titleHead', 'Notícias');
        return view('frontend::news.index', compact('list'));
    }

    public function show($slug, News $model)
    {
        $show = $model->whereSlug($slug)->first();
        $list = $model->where('slug', '!=', $slug)->orderBy('created_at', 'desc')->limit(3)->get();
        if (!$show)
            return redirect()->route('novidades.index')->with('error', 'Desculpe, registro não foi encontrado.');
        view()->share(['titleHead' => $show->name, 'section' => 'Novidades', 'publish' => $show->created_at]);
        return view('frontend::news.detail', compact('show', 'list'));
    }

}