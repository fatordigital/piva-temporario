<?php namespace Modules\FrontEnd\Http\Controllers;

use App\Http\Controllers\FrontDefaultController;
use Illuminate\Http\Request;
use Modules\FrontEnd\Entities\Newsletter;

class NewsletterController extends FrontDefaultController
{

    protected $rules = ['name', 'email'];

    protected $messages = [
        'name'    => 'Preenchimento obrigatório',
        'email'   => 'Preenchimento obrigatório'
    ];

    public function newsletter(Request $request, Newsletter $model)
    {
		return redirect()->route('front.home');
        if (!$this->customEncryptValidate($request)) {
            return response()->json(CryptoJSEncrypt($request->old() + ['msg' => 'Verifique se todos os campos foram preenchidos corretamente.', 'error' => true]), 200);
        } else {
            return response()->json(CryptoJSEncrypt($model->customCreate($request)), 200);
        }
    }

}