<?php namespace Modules\FrontEnd\Http\Controllers;

use App\Http\Controllers\FrontDefaultController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Modules\Course\Entities\Course;
use Modules\Course\Entities\CourseCategory;
use Modules\Lesson\Entities\Content;
use Modules\Lesson\Entities\Lesson;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamUser;
use Modules\Test\Entities\Test;
use Modules\User\Entities\User;

class CourseController extends FrontDefaultController
{

    private function loadCategory($slug)
    {
        if ($slug) {
            $category = CourseCategory::whereSlug($slug)->first();
            if (!$category)
                return false;
            else {
                return $category;
            }
        }
    }

    public function index(Request $request, Course $model)
    {
        cache()->flush();
        $category = $this->loadCategory($request->get('q'));
        if (!$category)
            return redirect()->route('front.home')->with('error', 'Nenhum curso encontrado com essa categoria :(');
        if ($category) {
            $ids = [];
            $teams = Team::whereStatus(true)->get(['id', 'max_student']);
            foreach ($teams as $team) {
                if ($team->max_student > TeamUser::whereTeamId($team->id)->count())
                    $ids[] = $team->id;
            }
            $courses = Team::whereHas('course', function ($q) {
                $q->where('status', '=', true);
            })->whereIn('id', $ids)->where('course_category_id', '=', $category->id)->orderBy('created_at', 'desc');
            if (request()->has('search')) {
                $courses = $courses->where('name', 'like', '%' . request('search') . '%');
                request()->session()->flashInput(request()->only(['search']));
            }

            view()->share('titleHead', $category->name);

            $courses = $courses->get();
        } else {
            $courses = [];
        }

        return view('frontend::course.index', compact('courses', 'category'));
    }


    public function show($slug, Team $model)
    {
        $show = $model->whereSlug($slug)->whereStatus(true)->orderBy('created_at', 'desc')->first();
        if (!$show)
            return redirect()->back()->with('error', 'Ops, curso não foi encontrado.');

        view()->share(['titleHead' => $show->name, 'section' => 'Cursos', 'image' => url('/assets/team/' . $show->image), 'publish' => $show->created_at]);

        session(['current_team' => $show->id, 'ccourse_id' => $show->course ? $show->course->id : null]);
        session()->save();

        $interest_courses = $show->course ? Team::whereIn('course_id', $show->course->relations->pluck('relation_id', 'relation_id')->toArray())->get() : collect([]);

        return response()->view('frontend::course.detail', compact('show', 'interest_courses'));
    }

    private function loadCourseUser()
    {
        if (session()->has('current_team')) {
            if (in_array(session('current_team'), session('user_team_id'))) {
                $disciplines = TeamUser::getDisciplines(session('current_team'));
                $lessons = TeamUser::getLessonsByDisciplines($disciplines ? array_column($disciplines->toArray(), 'id') : []);

                if ($lessons)
                    view()->share(['disciplines' => $disciplines, 'data_lessons' => $lessons]);
            }
        }
    }

    public function loadLessons($course, $discipline, $slug, $slug_content = null)
    {
        $show = Team::whereSlug($course)->whereStatus(true)->orderBy('created_at', 'desc')->first();
        if (!$show || !in_array($show->id, session()->get('user_team_id')))
            return redirect()->route('front.home')->with('error', 'Ops, um erro inesperado aconteceu.');
        $lesson = Lesson::whereSlug($slug)->first();
        if (!$lesson)
            return redirect()->route('front.home')->with('error', 'Ops, um erro inesperado aconteceu.');
        $course = $show->course ? $show->course->name : '';
        $contents = TeamUser::getLessonContent($lesson->id, null);

        if ($contents && $contents->contents && $contents->contents->count() > 0) {
            if ($slug_content)
                $principal_content = Content::whereSlug($slug_content)->first();
            else {
                $i = 0;
                do {
                    if ($contents->contents[$i]->contents)
                        $principal_content = $contents->contents[$i]->contents;
                    else {
                        $principal_content = $contents->contents[$i]->tests;
                    }
                    $i++;
                } while (!$principal_content);
            }
        } else {
            $principal_content = false;
        }
        if (!$principal_content) {
            return redirect()->back()->withError('Conteúdo não disponível no momento, aguarde.');
        }

        if ($principal_content) {
            if ($principal_content->video) {
                $principal_content->video_status = videoView(auth()->user()->id, $show, $principal_content);
            } else {
                l('Visualizando o conteúdo ' . $principal_content->name, $principal_content->id, $show->id, $show->course_id, $principal_content);
            }
        } else {
            if ($slug_content)
                $principal_content = Test::whereSlug($slug_content)->first();
            else {
                $principal_content = $contents->contents[0]->tests;
            }
        }

        return view('frontend::course.content', ['principal_content' => $principal_content, 'lesson' => $lesson, 'contents' => $contents, 'course' => $course, 'show' => $show]);
    }


    public function contentRead()
    {
        if (request()->ajax() && request()->has('request')) {
            request()->merge(CryptoJSDecrypt(base64_decode(request('request'))));
            $content = Content::with(['rating' => function ($q) {
                $q->where('user_id', '=', auth()->user()->id);
            }])->find(request('id'));
            if ($content) {
                l('Está visualizando o conteúdo [' . $content->name . '].', $content->id, session('current_team'), session('ccourse_id'));
                if ($content->video) {
                    return response()->json(CryptoJSEncrypt(['error' => false, 'msg' => '', 'data' => $content->toArray(), 'video' => strstr($content->content, 'vimeo') ? apiV($content->content, true) : apiYC(json_decode($content->content, true), true)['player']['embedHtml']]), 200);
                } else {
                    return response()->json(CryptoJSEncrypt(['error' => false, 'msg' => '', 'data' => $content->toArray()]), 200);
                }
            } else {
                return response()->json(CryptoJSEncrypt(['error' => true, 'msg' => 'Nenhum registro encontrado.']), 200);
            }
        }
    }

    public function timeView()
    {
        if (request()->ajax() && request()->has('request')) {
            request()->merge(CryptoJSDecrypt(request('request')));
            $content = Content::find(request('id'));
            l('Visualizou o conteúdo [' . $content->name . ']  por ' . request('time') . ' segundos.', request('id'), session('current_team'), session('ccourse_id'));
        }
    }

    public function fazer_curso_gratis(Team $team, TeamUser $teamUser, $slug)
    {
        $queryTeam = $team->whereSlug($slug)
            ->first();

        if (!$queryTeam)
            return redirect()->back()->with('error', 'Curso não encontrado, tente novamente.');

        $queryTeamUser = $teamUser->where('user_id', '=', auth()->user()->id)
            ->where('team_id', '=', $queryTeam->id)
            ->first();

        if ($queryTeamUser)
            return redirect()->route('front.user.home')->with('info', 'O curso já está na ÁREA DO ALUNO.');

        if (($queryTeam->value == '' || $queryTeam->value == 0) && ($queryTeam->value_discount == '' || $queryTeam->value_discount == 0)) {
            $createTeamUser = $teamUser->create([
                'course_id' => $queryTeam->course_id,
                'team_id' => $queryTeam->id,
                'user_id' => auth()->user()->id,
                'end_date' => null
            ]);
            if (!$createTeamUser)
                return redirect()->back()->with('error', 'Não foi possível fazer o registro no curso grátis, tente novametne, caso o erro persista, tente novamente mais tarde.');
            return redirect()->back()->with('success', 'O curso ' . $queryTeam->name . ' está disponível na área do aluno.');
        }

        return redirect()->back()->with('error', 'Ops, não foi possível continuar com a ação. Caso esse erro persista, entre em contato conosco.');
    }

}