<?php namespace Modules\FrontEnd\Http\Controllers;

use App\Http\Controllers\FrontDefaultController;
use Illuminate\Http\Request;
use Modules\Lesson\Entities\Content;
use Modules\Test\Entities\Test;

class ContentController extends FrontDefaultController
{

    public function index()
    {
        return view('frontend::content.free');
    }


    public function view($slug, Content $model, Test $test)
    {
        if ($slug) {
            switch($slug) {
                case 'simulados':
                    $content = $test->whereFree(true)->orderBy('created_at', 'desc')->get();
                    break;
                case 'arquivos':
                    $content = $model->whereFree(true)->whereFile(true)->orderBy('created_at', 'desc')->get();
                    break;
                case 'videos':
                    $content = $model->whereFree(true)->whereVideo(true)->orderBy('created_at', 'desc')->get();
                    break;
                case 'text':
                    $content = $model->whereFree(true)->whereText(true)->orderBy('created_at', 'desc')->get();
                    break;
            }
            return view('frontend::content.detail', compact('content', 'slug'));
        }
        return redirect()->route('front.home');
    }

}