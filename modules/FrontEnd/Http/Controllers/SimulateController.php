<?php namespace Modules\FrontEnd\Http\Controllers;

use App\Http\Controllers\FrontDefaultController;
use Modules\Lesson\Entities\LessonContent;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamLesson;
use Modules\Test\Entities\QuestionReport;
use Modules\Test\Entities\Test;
use Modules\Test\Entities\TestQuestion;
use Modules\User\Entities\UserQuestionChoose;
use Modules\User\Entities\UserTest;

class SimulateController extends FrontDefaultController
{

    public function addSimulado($slug, Test $model)
    {
        $test = $model->whereSlug($slug)->first();
        if (!$test)
            return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
        session(['add_user_test' => $slug]);
        return redirect()->route('access.simulate', $slug);
    }

    public function index(Test $model)
    {
        if (session()->has('add_user_test')) {
            session()->forget('add_user_test');
            return redirect()->route('front.user.tests');
        }
        if (session()->has('simulate')) {
            $simulate = $model->with('questions')->find(session('simulate'));
            if (!$simulate || !($simulate && $simulate->questions && $simulate->questions->count()))
                return redirect()->route('front.home')->with('error', 'O Simulado não existe.');
            return view('frontend::simulate.index', compact('simulate'));
        }
        return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
    }

    public function correcao($slug)
    {
        if (!$slug)
            return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
        $test = Test::whereSlug($slug)->first();
        if (!$test)
            return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
        if (!$test->free) {
            $teams = Team::whereIn('id', session('user_team_id'))->lists('id');
            if (!$teams || !session('user_team_id'))
                return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
            $lessons = TeamLesson::whereIn('team_id', $teams)->lists('lesson_id');
            if (!$lessons)
                return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
            $exists = LessonContent::whereIn('lesson_id', $lessons)->where('test_id', '=', $test->id)->first();
            if (!$exists)
                return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
        }


        $user_test = UserTest::whereUserId(auth()->user()->id)
            ->whereTestId($test->id)
            ->whereNotNull('end_test')
            ->orderBy('created_at', 'desc')
            ->first();

//        if (is_null($user_test)) {
//            $user_test = UserTest::create(['end_date' => sum_hours(time_to_seconds($test->duration)), 'user_id' => auth()->user()->id, 'test_id' => $test->id, 'end_test' => null]);
//        }
//        if ($user_test && time_to_seconds($user_test->end_date, date('Y-m-d H:i:s')) < 0) {
//            $user_test->update(['end_test' => date('Y-m-d H:i:s')]);
//        }

        session(['simulate' => $test->id, 'user_test_id' => $user_test->id, 'correcao' => true]);
        session()->save();

        return redirect()->route('front.simulate.index');
    }

    public function validateYourAccessTest($slug)
    {
        session()->forget('correcao');
        if (!$slug)
            return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
        $test = Test::whereSlug($slug)->first();
        if (!$test)
            return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
        if (!$test->free) {
            $teams = Team::whereIn('id', session('user_team_id'))->lists('id');
            if (!$teams || !session('user_team_id'))
                return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
            $lessons = TeamLesson::whereIn('team_id', $teams)->lists('lesson_id');
            if (!$lessons)
                return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
            $exists = LessonContent::whereIn('lesson_id', $lessons)->where('test_id', '=', $test->id)->first();
            if (!$exists)
                return redirect()->route('front.home')->with('error', 'Simulado não encontrado, tente novamente ou entre em contato conosco.');
        }

        
        $user_test = UserTest::whereUserId(auth()->user()->id)
            ->whereTestId($test->id)
            ->whereNull('end_test')->first();


        if (is_null($user_test)) {
            $user_test = UserTest::create(['end_date' => sum_hours(time_to_seconds($test->duration)), 'user_id' => auth()->user()->id, 'test_id' => $test->id, 'end_test' => null]);
        }
        if ($user_test && time_to_seconds($user_test->end_date, date('Y-m-d H:i:s')) < 0) {
            $user_test->update(['end_test' => date('Y-m-d H:i:s')]);
        }

        session(['simulate' => $test->id, 'user_test_id' => $user_test->id]);
        session()->save();

        return redirect()->route('front.simulate.index');
    }

    public function dataSimulate(Test $model)
    {
        $simulate = $model->find(session('simulate'));

        if (!$simulate)
            return redirect()->route('front.home')->with('error', 'Desculpa, simulado não encontrado.');

        if (!session()->has('correcao')) {
            $user = UserTest::whereUserId(auth()->user()->id)
                ->whereTestId($simulate->id)
                ->whereNull('end_test')
                ->first();
        } else {
            $user = UserTest::whereUserId(auth()->user()->id)
                ->whereTestId($simulate->id)
                ->whereNotNull('end_test')
                ->orderBy('created_at', 'desc')
                ->first();
        }

        if (!$user)
            return redirect()->route('access.simulate', $simulate->slug);

        return response()->json(CryptoJSEncrypt([
            'error' => false,
            'user' =>  $user->toArray(),
            'simulate' => $simulate->toArray(),
            'questions' => $simulate->questions()->with(['question' => function ($q) {
                $q->with(['answers', 'user_choose' => function ($q) {
                    $q->where('user_id', '=', auth()->user()->id)->where('user_test_id', '=', session('user_test_id'));
                }]);
            }])->paginate(1)->toArray()]), 200);
    }

    public function simulateSaveQuestion()
    {
        if (request()->has('request') && !session()->has('correcao')) {
            request()->merge(CryptoJSDecrypt(request('request')));
            request()->merge(['user_id' => auth()->user()->id]);
            $exists = UserQuestionChoose::whereUserId(auth()->user()->id)
                ->whereUserTestId(session('user_test_id'))
                ->whereQuestionId(request('question_id'))
                ->whereTestId(request('test_id'))->first();
            if (!$exists) {
                request()->merge(['user_test_id' => session('user_test_id')]);
                if (UserQuestionChoose::create(request()->except(['_token', 'request']))) {
                    return response()->json(CryptoJSEncrypt(['error' => false, 'msg' => 'Questão salva com sucesso.']), 200);
                } else {
                    return response()->json(CryptoJSEncrypt(['error' => true, 'msg' => 'Falha ao salvar a questão, atualize a página e tente novamente.']), 200);
                }
            } else {
                if ($exists->update(request()->except(['_token', 'request']))) {
                    return response()->json(CryptoJSEncrypt(['error' => false, 'msg' => 'Questão salva com sucesso.']), 200);
                } else {
                    return response()->json(CryptoJSEncrypt(['error' => true, 'msg' => 'Falha ao salvar a questão, atualize a página e tente novamente.']), 200);
                }
            }
        }
    }

    public function endSimulate(UserTest $model)
    {
        if (request()->has('request')) {
            request()->merge(CryptoJSDecrypt(request('request')));
            if ($model->end(request('test_id'), auth()->user()->id)) {
                $test = Test::find(request('test_id'));
                return response()->json(CryptoJSEncrypt(['error' => false, 'redirectUrl' => route('simulate.points', base64_encode($test->slug)), 'msg' => 'Obrigado por fazer o simulado.']));
            } else {
                return response()->json(CryptoJSEncrypt(['error' => true, 'msg' => 'Por favor, preencha todos as questões antes de finalizar o simulado.']));
            }
        }
    }

    public function points($slug, Test $model)
    {
        $test = $model->whereSlug(base64_decode($slug))->first();
        if ($test) {
            $user_test_id = session('user_test_id');
            session()->forget('user_test_id');
            $data = UserTest::find($user_test_id);
            $count = TestQuestion::whereTestId($test->id)->count();
            if ($data) {
                $team = Team::whereCourseId($test->course_id)->whereStatus(true)->orderBy('created_at', 'desc')->first();
                $media = $model->media($count, $data->corrects);
                return view('frontend::simulate.end', compact('data', 'count', 'test', 'team', 'media', 'slug'));
            } else {
                return redirect()->route('front.home')->with('error', 'Ops, registro não encontrado.');
            }
        } else {
            return redirect()->route('front.home')->with('error', 'Ops, registro não encontrado.');
        }
    }

    public function reportQuestion()
    {
        if (request()->has('request') && !session()->has('correcao')) {
            request()->merge(CryptoJSDecrypt(request('request')));
            if (!request()->has('question_id') || !request()->has('report'))
                return response()->json(CryptoJSEncrypt(['error' => true, 'msg' => 'Não foi possível reportar o erro da questão, por favor, tente novamente.']), 200);
            $exist = QuestionReport::whereUserId(auth()->user()->id)->whereQuestionId(request('question_id'))->first();
            if ($exist)
                return response()->json(CryptoJSEncrypt(['error' => false, 'msg' => 'Agradecemos sua ajuda, já está em nossos registros a informação do erro.']), 200);
            else {
                request()->merge(['user_id' => auth()->user()->id]);
                QuestionReport::create(request()->except(['_token', 'request']));
                return response()->json(CryptoJSEncrypt(['error' => false, 'msg' => 'Agradecemos sua ajuda, será ajustado o erro o mais breve possível.']), 200);
            }
        }
    }

}