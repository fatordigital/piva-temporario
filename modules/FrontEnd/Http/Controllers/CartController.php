<?php namespace Modules\FrontEnd\Http\Controllers;

use App\Http\Controllers\FrontDefaultController;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Laravel\Socialite\One\User;
use Modules\FrontEnd\Entities\Cart;
use Modules\Order\Entities\AbandonedOrder;
use Modules\Order\Entities\AbandonedOrderItem;
use Modules\Order\Entities\Coupon;
use Modules\Order\Entities\Order;
use Modules\Order\Entities\OrderHistoricalStatus;
use Modules\Order\Entities\OrderItem;
use Modules\Order\Entities\PagSeguro;
use Modules\Order\Entities\PagseguroCode;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamUser;
use Modules\User\Entities\UserSocialize;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class CartController extends FrontDefaultController
{

    private function validate_abandoned()
    {
        if (session('cart.abandoned_id')) {
            $AbandonedOrder = AbandonedOrder::whereFinished(1)->find(session('cart.abandoned_id'));
            if ($AbandonedOrder) {
                session()->forget('cart');
                session()->save();
            }
        }
    }

    private function save_user_action($valor)
    {
        if (session()->has('cart.abandoned_id')) {
            $log = new Logger('View Logs');
            $log->pushHandler(new StreamHandler(storage_path() . '/user_paths/order-' . session('cart.abandoned_id') . '.log', Logger::INFO));
            $log->addInfo($valor);
        }
    }

    public function index()
    {
        $list = AbandonedOrder::with('items')->whereFinished(0)
            ->find(session('cart.abandoned_id'));

        $this->save_user_action('Usuário entrou na listagem do carrinho ' . (!auth()->guest() ? 'Logado' : 'Deslogado'));

        return view('frontend::cart.index', compact('list'));
    }

    public function store(Request $request, Cart $cart, Team $team)
    {
        $find = $team->find(session('current_team'));
        if (!$find)
            return redirect()->back()->with('error', 'O Curso não foi encontrado, tente novamente.');

        $AbandonedOrder = AbandonedOrder::whereFinished(0)->find(session('cart.abandoned_id'));
        if (!$AbandonedOrder)
            $cart->manage_cart($find, 'Module\Team\Entities\Team#' . $find->id);
        else {
            $errors = $cart->addCart($find, 'Module\Team\Entities\Team#' . $find->id);
        }

        return redirect()->route('carrinhos.index');
    }

    public function endCart(PagSeguro $pag, AbandonedOrder $abandonedOrder, UserSocialize $userSocialize, Cart $cart)
    {
        if (session()->has('cart.abandoned_id')) {
            $finalizado = $abandonedOrder->whereFinished(1)->find(session('cart.abandoned_id'));
            if ($finalizado)
                return redirect()->route('confirm.pay')->with('success', 'Obrigado, estamos aguardando o processo da sua compra, você pode acompanhar os pedidos na área do aluno.');
        }

        $this->save_user_action('Usuário entrou no pagamento ' . (!auth()->guest() ? 'Logado' : 'Deslogado'));

        if (auth()->guest()) {
            $this->save_user_action('Usuário foi direcionado para a página de login.');
            return redirect()->route('front.login', ['redirect' => base64_encode(route('carrinhos.finaliza'))])->with('error', 'Faça seu login ou cadastre-se');
        }

        $dados = \Modules\User\Entities\User::with('socialize')->find(auth()->user()->id);

        if (!$userSocialize->validateUserDataToFinish(auth()->user()->id)) {
            $this->save_user_action('Não tinha todos os dados no cadastro e será redirecionado para página de edição do cadastro. ' . PHP_EOL . ' os dados do usuário são ' . var_export($dados->toArray(), true));
            return redirect()->route('front.user.register', ['redirect' => base64_encode(route('carrinhos.finaliza'))])->with('error', 'Antes de finalizar é necessário preencher os dados que faltam.');
        }

        $this->save_user_action('Usuário está com todos os dados preenchidos ' . json_encode($dados->toArray(), JSON_PRETTY_PRINT));

        $abandonedOrderData = $abandonedOrder->find(session('cart.abandoned_id'));
        if (!$abandonedOrderData)
            return redirect()->route('carrinhos.index')->with('info', 'Seu carrinho está vazio.');

        $cart->add_user_abandoned();

        $years = [];
        for ($i = date('Y'); $i < date('Y') + 9; $i++) {
            $years[$i] = $i;
        }

        try {

            $pag->getLibraryPagSeguro();
            $pag->getSessionID();

        } catch (\Exception $e) {
            return redirect()->route('carrinhos.index')->with('error', 'Não foi possível continuar, aguarde uns instentes, estamos solucionando o problema.');
        }

        return view('frontend::cart.payment', compact('years'));
    }

    public function removeItem($item, Cart $cart, AbandonedOrderItem $abandonedOrderItem)
    {
        if (base64_decode($item, true)) {
            $item = CryptoJSDecrypt(base64_decode($item));
            $delete = $abandonedOrderItem->find($item);
            if ($delete) {
                $cart->delete_item_cart($delete);
                if ($delete->delete()) {
                    return redirect()->back()->with('success', 'O Curso foi removido do carrinho.');
                }
            }
        }
        return redirect()->route('carrinhos.index')->with('info', 'Não foi possível remover o item do carrinho, por favor, tente novamente.');
    }

    public function use_coupon(Request $request, Coupon $couponModel, AbandonedOrder $abandonedOrder)
    {
        if ($request->get('captcha') != '')
            return redirect()->back()->with('error', 'Ops, não foi possível continuar, tente novamente mais tarde.');

        $this->validate($request, ['coupon' => 'required'], ['coupon.required' => 'Por favor, preencha o campo com o seu cupom.']);

        $coupon = $couponModel->with('coupon_uses')->where('coupon_key', '=', $request->get('coupon'))
            ->whereRaw('((user_id = ? || email = ?) || (user_id IS NULL && email IS NULL))', [!auth()->guest() ? auth()->user()->id : null, !auth()->guest() ? auth()->user()->email : null])
            ->whereRaw('(
            (
                (DATE(begin_date) <= DATE(?) || (begin_date IS NULL || begin_date = ?)) && (DATE(end_date) >= DATE(?) || (end_date IS NULL || end_date = ?))
            ) || (
                (begin_date IS NULL || begin_date = ?) && (end_date IS NULL || end_date = ?))
            )', [
                Carbon::now()->format('Y-m-d'),
                '0000-00-00',
                Carbon::now()->format('Y-m-d'),
                '0000-00-00',
                '0000-00-00',
                '0000-00-00'
            ])
            ->first();

        if (!$coupon)
            return redirect()->back()->with('error', 'Cupom não encontrado.');

        if ($coupon->coupon_uses->count() >= $coupon->quantity)
            return redirect()->back()->with('error', 'Cupom não encontrado.');

        $abandoned = $abandonedOrder->find(session('cart.abandoned_id'));
        if (!$abandoned)
            return redirect()->back()->with('error', 'Seu carrinho está vazio.');

        if ($abandoned->coupon_key != '')
            return redirect()->back()->with('info', 'Você já adicionou o cupom.');

        if ($coupon->value > $abandoned->total) {

            return redirect()->back()
                ->with('info', 'Encontramos o seu cupom, porém, <strong>Excede o valor de desconto do total do seu carrinho</strong>.');

        } else {

            $update = $abandoned->update([
                'coupon_id' => $coupon->id,
                'coupon' => $coupon->coupon_key,
                'coupon_percentage' => $coupon->percentage,
                'coupon_value' => $coupon->value,
                'total_discount' => $couponModel->sum_for_me($coupon, $abandoned->total)
            ]);

            if ($update) {
                return redirect()->route('carrinhos.index')->with('success', 'O Cupom ' . $coupon->coupon_key . ', foi adicionado na sua compra.');
            } else {
                return redirect()->back()->with('error', 'Não foi possível inserir o cupom no seu carrinho, entre em contato conosco para solução do problema.');
            }
        }
    }

    public function delete_coupon(AbandonedOrder $abandonedOrder)
    {
        if (!session()->has('cart.abandoned_id'))
            return redirect()->route('carrinhos.index')->with('error', 'Seu carrinho está vazio.');

        $abandoned = $abandonedOrder->find(session('cart.abandoned_id'));
        if (!$abandoned)
            return redirect()->route('carrinhos.index')->with('error', 'Seu carrinho está vazio.');

        $update = $abandoned->update([
            'coupon' => null,
            'coupon_percentage' => 0.00,
            'coupon_value' => 0.00,
            'total_discount' => $abandoned->total
        ]);

        if (!$update) {
            return redirect()->route('carrinhos.index')->with('error', 'Desculpe, houve um erro interno em nosso sistema, tente novamente, caso o erro persista, tente novamente mais tarde.');
        } else {
            return redirect()->back()->with('success', 'Seu cupom foi removido do carrinho.');
        }

    }

    public function confirmPay()
    {
        if (session()->has('cart')) {
            $order = Order::with(['pagseguro', 'orderStatus', 'orderItems'])->whereAbandonedOrderId(session('cart.abandoned_id'))->first();
            if ($order) {
                session()->forget('cart');
                return view('frontend::cart.confirm', compact('order'));
            } else {
                return redirect()->route('front.my.orders')->with('success', 'O pedido já foi finalizado, para ver o seu histórico de pedidos, acesse sempre a seção de MEUS PEDIDOS.');
            }
        } else {
            return redirect()->route('front.my.orders')->with('success', 'O pedido já foi finalizado, para ver o seu histórico de pedidos, acesse sempre a seção de MEUS PEDIDOS.');
        }
    }


    public function checkout_store(Request $request, PagSeguro $pagSeguro)
    {
        if ($request->has('method')) {
            switch ($request->get('method')) {
                case 'CREDIT_CARD':
                    $pag = $pagSeguro->load(session('cart.abandoned_id'))
                        ->sender($request->get('pagseguro_hash'))
                        ->shipping()
                        ->addressBilling()
                        ->installments($request->get('plots'))
                        ->credit($request->get('card_token'), $request->all())
                        ->items()
                        ->get();
                    if ($pag->count())
                        return redirect()->route('carrinhos.finaliza')->withInput($request->except(['card_number', 'card_month', 'card_year', 'plots', 'card_security']))
                            ->with('error', 'Não foi possível continuar, tente novamente.')
                            ->withErrors($pag);
                    else {
                        return redirect()->route('confirm.pay')->with('success', 'Obrigado, etamos confirmando seu pagamento.');
                    }
                    break;
                case 'BOLETO':
                    $pag = $pagSeguro->load(session('cart.abandoned_id'));

                    $status = $pag->sender($request->get('pagseguro_hash'))
                        ->shipping()
                        ->addressBilling()
                        ->boleto()
                        ->items()
                        ->get();

                    if (!$status)
                        return redirect()->route('carrinhos.finaliza')->withInput($request->all())->withErrors($pag->errors)->with('error', 'Não foi possível continuar, tente novamente.');
                    else {
                        return redirect()->route('confirm.pay')->with('success', 'Obrigado, etamos confirmando seu pagamento.');
                    }
                    break;
                case 'ONLINE_DEBIT':
                    if (!$request->has('method_option'))
                        return redirect()->back()->with('error', 'Por favor, selecione o banco para pagamento.');

                    $pag = $pagSeguro->load(session('cart.abandoned_id'))
                        ->sender($request->get('pagseguro_hash'))
                        ->shipping()
                        ->addressBilling()
                        ->debit($request->get('method_option'))
                        ->items()
                        ->get();

                    if (!$pag)
                        return redirect()->route('carrinhos.finaliza')->with('error', 'Não foi possível continuar, tente novamente.');
                    else {
                        return redirect()->route('confirm.pay')->with('success', 'Obrigado, etamos confirmando seu pagamento.');
                    }
                    break;
            }
            return redirect()->route('confirm.pay')->with('success', 'Obrigado, etamos confirmando seu pagamento.');
        }
    }

    public function alert(Request $request)
    {
        $log = new Logger('View Logs');
        $log->pushHandler(new StreamHandler(storage_path() . '/pagseguro_return/' . $request->get('notificationCode') . '.log', Logger::INFO));
        $PagSeguro = new PagSeguro();
        $PagSeguro->setDataNotification($request->get('notificationCode'));
        //$PagSeguro->setDataNotification('739D77BE42AD42AD5AADD4223F841F8379CD');
//        $log->addInfo('Dados notificação', $PagSeguro->getNotificationReturn());
        $order = Order::wherePagseguroCode($PagSeguro->getNotificationReturn('code'))->first();
        if ($order) {
            $update = Order::find($order->id);
            $update->update([
                'pagseguro_status' => $PagSeguro->getNotificationReturn('status'),
                'pagseguro_notification' => json_encode($PagSeguro->getNotificationReturn())
            ]);
            $this->changeStatusPagSeguro($update);
        }
        $log->addInfo('Dados notificação', $PagSeguro->getNotificationReturn());
    }

    private function changeStatusPagSeguro($order)
    {
        if ($order) {
            $order->update(['pagseguro_status' => $order->pagseguro_status]);
            OrderHistoricalStatus::create([
                'order_id' => $order->id,
                'order_status_id' => $order->pagseguro_status
            ]);
            $orderItem = OrderItem::whereOrderId($order->id)->get();
            if ($order->pagseguro_status == 3 || $order->pagseguro_status == 4) {
                foreach ($orderItem as $item) {
                    $team = Team::whereId($item->ref)->orderBy('id', 'desc')->first();
                    if ($team) {
                        $end_days = $team->end_days ? date('Y-m-d', strtotime('+' . $team->end_days . ' days')) : null;
                        TeamUser::firstOrCreate([
                            'course_id' => $team->course_id,
                            'team_id' => $team->id,
                            'user_id' => $order->user_id,
                            'end_date' => $end_days
                        ]);
                    } else {
//                        return redirect()->back()->with('error', 'Ops, houve um erro inesperado. Tente novamente.');
                    }
                }
            } else {
                foreach ($orderItem as $item) {
                    $userinteam = TeamUser::whereUserId($order->user_id)->whereTeamId($item->ref)->first();
                    if ($userinteam) {
                        $userinteam->delete();
                    }
                }
            }
        }
    }

}