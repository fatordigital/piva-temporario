<?php namespace Modules\FrontEnd\Http\Controllers;

use App\Http\Controllers\FrontDefaultController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Modules\FrontEnd\Entities\Interest;

class ContactController extends FrontDefaultController
{
    private $messages_validate = [
        'name.required' => 'Preenchimento obrigatório',
        'email.required' => 'Preenchimento obrigatório',
        'email.email' => 'Digite um e-mail válido',
        'subject.required' => 'Preenchimento obrigatório',
        'message.required' => 'Preenchimento obrigatório',
        'phone.required' => 'Preenchimento obrigatório'
    ];

    private $validate = [
        'phone' => 'required',
        'name' => 'required',
        'email' => 'required|email',
        'subject' => 'required',
        'message' => 'required'
    ];

    public function learnWithUs(Request $request, Interest $model)
    {
        $this->validate($request, $this->validate, $this->messages_validate);
        $return = $model->customCreate($request);
        if ($return['status'] == false) {
            return redirect()->back()->with('success', $request['msg']);
        } else {
            return redirect()->back()->with('error', $request['msg']);
        }
    }
}