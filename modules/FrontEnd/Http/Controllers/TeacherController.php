<?php namespace Modules\Frontend\Http\Controllers;

use Modules\User\Entities\User;
use Pingpong\Modules\Routing\Controller;

class TeacherController extends Controller
{

    public function show($slug)
    {

        foreach (User::whereNull('slug')->get() as $item) {
            if ($item->name != '') {
                $item->update(['slug' => str_slug($item->name), '-']);
            }
        }

        $teacher = User::whereSlug($slug)
            ->first();
        if (!$teacher)
            return redirect()->back()->with('error', 'Professor não encontrado.');
        return view('frontend::teacher.show', compact('teacher'));
    }

}