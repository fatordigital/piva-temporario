<?php namespace Modules\Frontend\Http\Controllers\Api;

use App\Http\Controllers\FrontDefaultController;
use Modules\Lesson\Entities\Content;
use Modules\Team\Entities\TeamUser;

class LessonController extends FrontDefaultController
{

    public function contents()
    {
        if (session()->has('current_team')) {
            if (in_array(session('current_team'), session('user_team_id'))) {
//                $lessons = TeamUser::getLessonIds(session('current_team'), auth()->user()->id, false, request()->has('discipline_id') ? request('discipline_id') : null);
                $disciplines = TeamUser::getDisciplines(session('current_team'));
                $lessons = TeamUser::getLessonsByDisciplines(request()->has('discipline_id') ? [request('discipline_id')] : array_column($disciplines, 'id'));
                if ($lessons)
                    return response()->json(CryptoJSEncrypt(['error' => false, 'message' => '', 'disciplines' => $disciplines, 'data' => $lessons->toArray()]), 200);
            }
        }
        return response()->json(CryptoJSEncrypt(['error' => true, 'message' => 'Conteúdo não encontrado.']), 200);
    }

    public function getContents()
    {
        if (request()->has('request')) {
            $request = CryptoJSDecrypt(request('request'));
            if (isset($request['lesson_id'])) {
                $contents = TeamUser::getLessonContent($request['lesson_id'], isset($request['without']) ? $request['without'] : '');
                if ($contents) {
                    return response()->json(CryptoJSEncrypt(['error' => false, 'message' => '', 'data' => $contents->toArray()]), 200);
                }
            }
        }
        return response()->json(CryptoJSEncrypt(['error' => true, 'message' => 'Conteúdo não encontrado.']), 200);
    }

    public function updateContentStatus()
    {
//        $content_id = request('content_id');
//        $user_content_end = new \Modules\User\Entities\UserContentEnd();
//        $query_exists = $user_content_end->where('user_id', '=', auth()->user()->id)->whereContentId($content_id)->first();
//        if ($query_exists) {
//            $user_content_end->update(['status' => 1]);
//        } else {
//            $user_content_end->create(['content_id' => $content_id, 'lesson_id' => request('lesson_id'), 'team_id' => request('team_id'), 'user_id' => auth()->user()->id, 'status' => 1]);
//        }
//        return response()->json(['status' => true]);
    }

}