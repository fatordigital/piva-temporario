<?php namespace Modules\FrontEnd\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|regex:/([a-zA-Zà-ú]+) ([a-zA-Zà-ú]+)/',
            'email' => 'email|required|unique:users',
            'user_socializes.national_id' => 'required|unique:user_socializes',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'user_socializes.birth_date' => 'required',
            'user_socializes.postal_code' => 'required',
            'user_socializes.route' => 'required',
            'user_socializes.route_number' => 'required|alpha_num',
            'user_socializes.district' => 'required',
            'user_socializes.state' => 'required',
            'user_socializes.city' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.regex' => 'Digite o nome completo',

            'email.unique' => 'O e-mail já está cadastro',

            'password.required' => 'O campo senha é obrigatório',
            'password.confirmed' => 'Os campos de senha precisam ser iguais.',
            'password_confirmation.required' => 'Confirme a senha',


            'user_socializes.national_id.unique' => 'O CPF já está cadastrado.',
            'user_socializes.birth_date.required' => 'Campo obrigatório',
            'user_socializes.postal_code.required' => 'Campo obrigatório',
            'user_socializes.route.required' => 'Campo obrigatório',
            'user_socializes.route_number.required' => 'Campo obrigatório',
            'user_socializes.district.required' => 'Campo obrigatório',
            'user_socializes.state.required' => 'Campo obrigatório',
            'user_socializes.city.required' => 'Campo obrigatório',
            'user_socializes.route_number.alpha_num' => 'Campo endereço número não permite letras'
        ];
    }

}
