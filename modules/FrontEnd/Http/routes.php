<?php


#solid design
Route::get('estudar/{slug}', ['uses' => '\Modules\FrontEnd\Http\Controllers\EstudosController@index', 'middleware' => ['auth-front', 'team_check'], 'as' => 'estudar.index']);
#solid design

Route::group([
    'middleware' => ['course-ids', 'order'],
    'namespace' => 'Modules\FrontEnd\Http\Controllers'
], function () {

    Route::get('/professor/{slug}', ['as' => 'teacher.detail', 'uses' => 'TeacherController@show']);

//    Route::get('/', ['as' => 'front.home', 'uses' => 'FrontEndController@index']);

    Route::get('/', function(){
        return redirect()->route('front.user.home');
    })->name('front.home');

    Route::get('servico/autenticacao', ['middleware' => 'guest-front', 'as' => 'front.login', 'uses' => 'UserController@login']);
    Route::post('servico/verify', ['as' => 'front.verify', 'uses' => 'UserController@verify']);
    Route::post('servico/verify-user', ['as' => 'front.verify.ajax', 'uses' => 'UserController@verifyAjax']);
    Route::get('novo-registro', ['as' => 'front.user.register.guest', 'uses' => 'UserController@guestRegister']);
    Route::post('novo-registro', ['as' => 'front.user.store.register', 'uses' => 'UserController@storeGuestRegister']);
    Route::resource('novidades', 'NewsController');


    Route::resource('cursos', 'CourseController');


    Route::resource('conteudos', 'ContentController');
    Route::post('learn-with-us', ['as' => 'front.contact.learn', 'uses' => 'ContactController@learnWithUs']);
    Route::post('form-newsletter', ['as' => 'front.form.newsletter', 'uses' => 'NewsletterController@newsletter']);
    Route::post('fast-register', ['as' => 'front.fast-register', 'uses' => 'UserController@fastRegister']);
    Route::get('carrinhos', ['as' => 'carrinhos.index', 'uses' => 'CartController@index']);
    Route::post('carrinhos/create', ['as' => 'carrinhos.store', 'uses' => 'CartController@store']);
    Route::get('professores', ['as' => 'front.teachers', 'uses' => 'UserController@teachers']);
    Route::get('contato', ['as' => 'front.contact', 'uses' => 'FrontEndController@contact']);
    Route::post('contact', ['as' => 'front.contact.store', 'uses' => 'FrontEndController@contactStore']);
    Route::post('coupon', ['as' => 'front.coupon.validate', 'uses' => 'FrontEndController@couponValidate']);
    Route::get('remover-item/{item}', ['as' => 'front.remove.cart.item', 'uses' => 'CartController@removeItem']);

    Route::post('add-coupom', ['as' => 'cart.use_coupon', 'uses' => 'CartController@use_coupon']);
    Route::post('delete-coupom', ['as' => 'cart.delete_coupon', 'uses' => 'CartController@delete_coupon']);
});

Route::get('{slug}/guest/simulado', ['as' => 'add.simulate', 'uses' => 'Modules\FrontEnd\Http\Controllers\SimulateController@addSimulado']);

Route::group([
    'middleware' => ['auth-front', 'course-ids', 'order'],
    'namespace' => 'Modules\FrontEnd\Http\Controllers'
], function () {
    Route::post('checkout', ['as' => 'carrinhos.data', 'uses' => 'CartController@getData']);
    Route::post('check-me', ['as' => 'user.check', 'uses' => 'UserController@checkMe']);
    Route::post('save-my-data', ['as' => 'user.save.data', 'uses' => 'UserController@saveRegister']);
    if (request()->cookie('routePay')) {
        Route::post(\Illuminate\Support\Facades\Crypt::decrypt(request()->cookie('routePay')), ['as' => 'route.pay', 'uses' => 'CartController@requestPay']);
    }
    Route::post('pay', ['as' => 'carrinhos.pay', 'uses' => 'CartController@pay']);
    Route::get('confirmacao', ['as' => 'confirm.pay', 'uses' => 'CartController@confirmPay']);
    Route::get('carrinhos/pagamento', ['as' => 'carrinhos.finaliza', 'uses' => 'CartController@endCart']);
    Route::get('servico/sair', ['as' => 'front.logout', 'uses' => 'UserController@logout']);
    Route::get('meu/perfil', ['as' => 'front.user.home', 'uses' => 'UserController@home']);
    Route::get('meus/pedidos', ['as' => 'front.my.orders', 'uses' => 'UserController@orders']);
    Route::get('minhas/notificacoes', ['as' => 'front.user.notification', 'uses' => 'UserController@notifications']);
    Route::get('eu', ['as' => 'front.user.register', 'uses' => 'UserController@register']);
    Route::patch('eu', ['as' => 'front.user.register.patch', 'uses' => 'UserController@updateRegister']);
    Route::get('meus/simulados', ['as' => 'front.user.tests', 'uses' => 'UserController@tests']);


    Route::get('carrinho/pagamento', ['as' => 'cart.checkout', 'uses' => 'CartController@endCart']);
    Route::post('carrinho/pagamento', ['as' => 'cart.checkout_store', 'uses' => 'CartController@checkout_store']);


    Route::get('simulado', ['as' => 'front.simulate.index', 'uses' => 'SimulateController@index']);
    Route::post('simulate-data.json', ['as' => 'simulate.json.data', 'uses' => 'SimulateController@dataSimulate']);
    Route::post('save-question.json', ['as' => 'simulate.save.question.json', 'uses' => 'SimulateController@simulateSaveQuestion']);
    Route::post('end-simulate.json', ['as' => 'simulate.end.json', 'uses' => 'SimulateController@endSimulate']);
    Route::get('{slug}/pontuacao', ['as' => 'simulate.points', 'uses' => 'SimulateController@points']);
    Route::post('get-content.json', ['as' => 'course.lesson.content', 'uses' => 'CourseController@contentRead']);
    Route::post('time-view.json', ['as' => 'course.lesson.time.view', 'uses' => 'CourseController@timeView']);
    Route::post('set-content-rating.json', function () {
        if (request()->has('request')) {
            request()->merge(CryptoJSDecrypt(request('request')));
            cr(auth()->user()->id, request('lesson_id'), request('points'));
        }
    });
    Route::get('{slug}/gratis', ['as' => 'content.free', 'uses' => 'ContentController@view']);
    Route::get('{slug}/simulado', ['as' => 'access.simulate', 'uses' => 'SimulateController@validateYourAccessTest']);
    Route::get('{slug}/correcao', ['as' => 'access.correcao', 'uses' => 'SimulateController@correcao']);
    Route::post('report-question', ['as' => 'report.question.error', 'uses' => 'SimulateController@reportQuestion']);
    Route::get('meus/certificados', ['as' => 'front.user.certified', 'uses' => 'UserController@certified']);
    Route::get('meus/certificados/download/{slug}', ['as' => 'front.user.certified.download', 'uses' => 'UserController@downloadCertified']);

    Route::get('curso/{course}/{discipline}/{slug}/{content_slug?}', ['as' => 'front.lesson.view', 'uses' => 'CourseController@loadLessons']);

    Route::post('gratis/{slug}', ['as' => 'front.curso-gratis', 'uses' => 'CourseController@fazer_curso_gratis']);
});

Route::group([
    'prefix' => 'api',
//        'middleware' => ['auth-front', 'course-ids'],
    'namespace' => 'Modules\FrontEnd\Http\Controllers\Api'
], function () {
    Route::get('contents.json', ['uses' => 'LessonController@contents', 'as' => 'api.get.content']);
    Route::post('modal-contents.json', ['uses' => 'LessonController@getContents', 'as' => 'api.post.modal_contents']);
    Route::post('update-content-status', ['uses' => 'LessonController@updateContentStatus', 'as' => 'api.post.update-status-content']);
});
Route::post('/service/recoveryPassword', [
    'as' => 'forget.auth',
    'uses' => 'App\Http\Controllers\Auth\PasswordController@postEmail'
]);
Route::get('/service/recoveryRenew/{token}', [
    'as' => 'renew.auth',
    'uses' => 'App\Http\Controllers\Auth\PasswordController@getReset'
]);
Route::post('/service/recovery/data', [
    'as' => 'reset.auth',
    'uses' => 'App\Http\Controllers\Auth\PasswordController@postReset'
]);

Route::post('/payment/alert', [
    'as' => 'payment.alert',
    'uses' => 'Modules\FrontEnd\Http\Controllers\CartController@alert'
]);


Route::get('/gerar-txt', 'Modules\FrontEnd\Http\Controllers\FrontEndController@insight_txt');