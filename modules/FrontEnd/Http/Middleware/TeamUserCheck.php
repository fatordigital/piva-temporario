<?php namespace Modules\Frontend\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Modules\Team\Entities\TeamUser;

class TeamUserCheck
{

    protected $teamUser;

    protected $access;

    public function __construct()
    {
        $this->teamUser = new TeamUser();
    }

    public function handle($request, Closure $next)
    {
        if (!session()->has('user_access')) {
            $this->access = $this->teamUser->where('user_id', '=', auth()->user()->id)
                ->whereRaw(' ( DATE(`end_date`) >= ? || ( `end_date` IS NULL || `end_date` = "0000-00-00" ) ) ', [Carbon::today()])
                ->lists('team_id')
                ->toArray();
        }

        session(['user_access' => $this->access]);
        session()->save();

        return $next($request);
    }

}
