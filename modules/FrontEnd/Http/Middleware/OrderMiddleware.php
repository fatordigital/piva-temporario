<?php namespace Modules\Frontend\Http\Middleware;

use Closure;
use Modules\Order\Entities\AbandonedOrder;

class OrderMiddleware
{

    public function handle($request, Closure $next)
    {
        $this->load_abandoned();
        return $next($request);
    }

    public function load_abandoned()
    {
        $abandoned_order = AbandonedOrder::with('items')
            ->find(session('cart.abandoned_id'));

        view()->share(compact('abandoned_order'));
    }

}
