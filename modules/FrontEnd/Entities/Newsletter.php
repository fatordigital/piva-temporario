<?php namespace Modules\FrontEnd\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Newsletter extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["name", "email", "status"];

    public function customCreate($request)
    {
        $exists = $this->whereEmail($request->get('email'))->first();
        if ($exists)
            return [
                'status' => false,
                'msg'    => 'Oi '. $request->get('name').', seu e-mail já consta em nossos registros. Obrigado!'
            ];
        else {
            if ($this->create($request->except(['_token', 'null', 'captcha']))) {
                return [
                    'status' => false,
                    'msg'    => 'Obrigado '. $request->get('name').', por cadastrar seu e-mail, nós iremos manter você informado sobre nossos cursos e promoções.'
                ];
            } else {
                return [
                    'error' => true,
                    'msg'    => 'Ops, um erro inesperado aconteceu. Por favor, atualize a página e preencha o formulário novamente.'
                ];
            }
        }
    }

}