<?php namespace Modules\FrontEnd\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;
use Modules\Order\Entities\AbandonedOrder;
use Modules\Order\Entities\AbandonedOrderItem;
use Modules\Order\Entities\Coupon;
use Modules\User\Entities\UserSocialize;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Cart extends Model
{

    protected $table = false;

    private $index = 'id';
    private $qtd = 'quantity';
    private $value = 'value';
    private $discount = 'value_discount';

    private $msg;

    public function __construct()
    {
        parent::__construct();
        $this->msg = new MessageBag();
    }

    private function save_user_action($valor)
    {
        if (session()->has('cart.abandoned_id')) {
            $log = new Logger('View Logs');
            $log->pushHandler(new StreamHandler(storage_path() . '/user_paths/order-' . session('cart.abandoned_id') . '.log', Logger::INFO));
            $log->addInfo($valor);
        }
    }

    public function storeItemsAbandonedCart()
    {
        if (session()->has('cart.items')) {
            foreach (session()->get('cart.items') as $item) {
                $exist = AbandonedOrderItem::whereRef($item['ref'])
                    ->whereOrderId(session()->get('cart.abandoned_id'))
                    ->first();
                if (!$exist)
                    AbandonedOrderItem::create(['ref' => $item['ref'], 'quantity' => !isset($item['quantity']) ? 1 : $item['quantity'], 'order_id' => session()->get('cart.abandoned_id'), 'value' => !empty($item['value_discount']) && $item['value_discount'] > 0 ? $item['value_discount'] : $item['value']]);
                else {
                    $exist->update(['quantity' => isset($item['quantity']) ? $item['quantity'] : 1]);
                }
            }
        }
    }

    public function user()
    {
        if (!auth()->guest()) {
            $user_socialize = UserSocialize::whereUserId(auth()->user()->id)->first();
            return [
                'birth_date' => $user_socialize ? $user_socialize->birth_date : null,
                'phone' => $user_socialize ? $user_socialize->phone : null,
                'national_id' => $user_socialize ? $user_socialize->national_id : null,
                'country' => $user_socialize ? $user_socialize->country : null,
                'postal_code' => $user_socialize ? $user_socialize->postal_code : null,
                'district' => $user_socialize ? $user_socialize->district : null,
                'route' => $user_socialize ? $user_socialize->route : null,
                'route_number' => $user_socialize ? $user_socialize->route_number : null,
                'city' => $user_socialize ? $user_socialize->city : null,
                'state' => $user_socialize ? $user_socialize->state : null,
                'user_id' => auth()->user()->id,
                'name' => auth()->user()->name
            ];
        } else {
            return [
                'birth_date' => null,
                'phone' => null,
                'national_id' => null,
                'user_id' => null,
                'name' => null
            ];
        }
    }

    public function add_user_abandoned()
    {
        $AbandonedOrder = AbandonedOrder::find(session('cart.abandoned_id'));
        if ($AbandonedOrder) {
            $this->save_user_action('Carrinho abandonado recebeu um atualização para mesclar os dados do usuário' . json_encode($this->user(), JSON_PRETTY_PRINT));
            $AbandonedOrder->update($this->user());
        }
    }

    public function manage_cart($product, $ref)
    {
        #Caso não exista um sessão de carrinho abandonado
        if (!session()->has('cart.abandoned_id')) {
            $store = AbandonedOrder::create($this->user() + ['total_discount' => $product->price, 'total' => $product->price, 'sub_total' => $product->value]);
            if (!$store) {
                $this->msg->add('error.1', 'Ops, não foi possível adicionar ao carrinho, tente novamente, caso o erro persista, tente novamente mais tarde.');
            } else {
                session(['cart.abandoned_id' => $store->id]);
                session()->save();

                AbandonedOrderItem::create([
                    'ref' => $ref,
                    'order_id' => session('cart.abandoned_id'),
                    'value' => $product->value,
                    'value_discount' => $product->value_discount,
                    'name' => $product->name,
                    'slug' => $product->slug
                ]);

                $this->msg->add('success.1', 'Curso adicionado no carrinho.');
            }
        } else {
            $AbandonedOrder = AbandonedOrder::whereFinished(0)->find(session('cart.abandoned_id'));
            if (!$AbandonedOrder) {
                session()->forget('cart');
                session()->save();
                $this->manage_cart($product, $ref);
            } else {

                $coupon = new Coupon();

                $update = $AbandonedOrder->update($this->user() + [
                        'total_discount' => $AbandonedOrder->coupon ? $coupon->sum_for_me($coupon->whereCouponKey($AbandonedOrder->coupon)->first(), $AbandonedOrder->total + $product->price) : $AbandonedOrder->total + $product->price,
                        'total' => $AbandonedOrder->total + $product->price,
                        'sub_total' => $AbandonedOrder->sub_total + $product->value
                    ]);
                if (!$update) {
                    $this->msg->add('error.2', 'Ops, não foi possível adicionar ao carrinho, tente novamente, caso o erro persista, tente novamente mais tarde.');
                } else {

                    AbandonedOrderItem::create([
                        'ref' => $ref,
                        'order_id' => session('cart.abandoned_id'),
                        'value' => $product->value,
                        'value_discount' => $product->value_discount,
                        'name' => $product->name,
                        'slug' => $product->slug
                    ]);

                    $this->msg->add('success.2', 'Curso adicionado no carrinho.');
                }
            }
        }
    }

    public function addCart($product, $ref)
    {
        $AbandonedOrderItem = AbandonedOrderItem::whereRef($ref)->whereOrderId(session('cart.abandoned_id'))->first();
        if ($AbandonedOrderItem)
            return redirect()->route('carrinhos.index')->with('info', 'O Curso já está no carrinho.');

        $this->manage_cart($product, $ref);

        return $this->msg;
    }

    public function delete_item_cart($AbandonedOrderItem)
    {
        $AbandonedOrder = AbandonedOrder::with('items')->find(session('cart.abandoned_id'));
        if ($AbandonedOrder) {
            $coupon = new Coupon();

            $AbandonedOrder->update([
                'sub_total' => $AbandonedOrder->sub_total - $AbandonedOrderItem->value,
                'total' => $AbandonedOrder->total - $AbandonedOrderItem->price,
                'total_discount' => $AbandonedOrder->coupon ? $coupon->sum_for_me($coupon->whereCouponKey($AbandonedOrder->coupon)->first(), $AbandonedOrder->total - $AbandonedOrderItem->price) : $AbandonedOrder->total - $AbandonedOrderItem->price
            ]);
        }
    }


    public function checkCart()
    {
        session(['cart.data' => [
            'total' => $this->getTotal(),
            'sub_total' => $this->getSubTotal(),
            'discount' => $this->getDiscount()
        ]]);
        session()->save();
    }

    public function itemExists($id)
    {
        foreach (session()->get('cart.items') as $key => $item) {
            if (isset($item[$this->index]) && $item[$this->index] == $id) {
                return $key;
            }
        }
        return false;
    }

    public function getTotal()
    {
        $total = 0;
        foreach (session()->get('cart.items') as $item) {
            if (!isset($item[$this->discount]) || $item[$this->discount] == 0)
                $total += floatval($item[$this->value]) * (isset($item[$this->qtd]) ? $item[$this->qtd] : 1);
            else if (isset($item[$this->discount])) {
                $total += floatval($item[$this->discount]) * (isset($item[$this->qtd]) ? $item[$this->qtd] : 1);
            }
        }
        return $total - session('cart.data.discount');
    }

    public function getSubTotal()
    {
        $total = 0;
        foreach (session()->get('cart.items') as $item) {
            if (!isset($item[$this->discount]) || $item[$this->discount] == 0)
                $total += floatval($item[$this->value]) * (isset($item[$this->qtd]) ? $item[$this->qtd] : 1);
            else if (isset($item[$this->discount])) {
                $total += floatval($item[$this->discount]) * (isset($item[$this->qtd]) ? $item[$this->qtd] : 1);
            }
        }
        return $total;
    }

    public function getDiscount()
    {
        $discount = 0;
        if (session()->has('cart.data.discount')) {
            $discount = session('cart.data.discount');
        }
        return $discount;
    }


    public function makeCartSession($abandonedWithItems)
    {
        if ($abandonedWithItems) {
            foreach ($abandonedWithItems->items as $item) {
                $explode = explode('#', $item->ref);
                if (isset($explode[0]) && !empty($explode[0]) && isset($explode[1]) && !empty($explode[1])) {
                    $model = new $explode[0]();
                    $exist = $model->whereId($explode[1])->get(['id', 'max_student', 'name', 'value', 'value_discount'])->first()->toArray();
                    if ($exist) {
                        $this->addCart($exist + ['ref' => $item->ref, 'quantity' => $item->quantity]);
                    }
                }
            }
        }
    }

}