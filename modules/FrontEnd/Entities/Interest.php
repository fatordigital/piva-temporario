<?php namespace Modules\FrontEnd\Entities;

use App\BaseModel;

class Interest extends BaseModel
{

    protected $fillable = ["page", "name", "email", "phone", "subject", "message"];

    public function customCreate($request)
    {
        $exists = $this->whereEmail($request->get('email'))->first();
        if ($exists)
            return [
                'status' => false,
                'msg'    => 'Oi ' . $request->get('name') . ', seu e-mail já consta em nossos registros. Você recebera um contato em breve.'
            ];
        else {
            if ($this->create($request->except(['_token', 'null', 'captcha']))) {
                return [
                    'status' => false,
                    'msg'    => 'Obrigado ' . $request->get('name') . ', por registrar seu interesse em estudar conosco. Nós iremos retornar para o seu e-mail o mais breve possível.'
                ];
            } else {
                return [
                    'error' => true,
                    'msg'   => 'Ops, um erro inesperado aconteceu. Por favor, atualize a página e preencha o formulário novamente.'
                ];
            }
        }
    }

}