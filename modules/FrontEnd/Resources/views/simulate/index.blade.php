<!DOCTYPE html>
<html lan="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta id="_token"
              data-content="{!! csrf_token() !!}">
        <title>{!! s('Project.name') !!} - Simulado</title>
        {!! HTML::style('css/professorpiva/lib/bootstrap.min.css') !!}
        {!! HTML::style('css/plugins/sweetalert/sweetalert.css') !!}
        {!! HTML::style('css/professorpiva/docs.css') !!}
        {!! HTML::style('css/plugins/summernote/summernote.css') !!}
        {!! HTML::style('css/style.min.css') !!}
        {!! HTML::style('css/custom.me.css') !!}
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800'
              rel='stylesheet'
              type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'
              rel='stylesheet'
              type='text/css'>
        <link rel="stylesheet"
              href="https://cdn.shr.one/0.1.9/shr.css">
        <script type="text/javascript">var b = '{!! URL::to('/') !!}/';</script>
        <style>
            img {
                display: inline-block;
                height: auto;
                max-width: 100%;
            }
        </style>
    </head>
    <body>
    <input type="hidden" id="correcao" value="{!! !session()->has("correcao") ? 0 : 1 !!}" />
    <div id="simulate" style="height: 100%"></div>
    {!! HTML::script('js/cms/jquery-2.1.1.js') !!}
    {!! HTML::script('js/cms/bootstrap.min.js') !!}
    {!! HTML::script('css/plugins/summernote/summernote.min.js') !!}
    {!! HTML::script('js/plugins/sweetalert/sweetalert2.min.js') !!}
    {!! HTML::script('js/plugins/datapicker/bootstrap-datepicker.js') !!}
    {!! HTML::script('js/plugins/jquery-mask-plugin/jquery.mask.js') !!}
    {!! HTML::script('js/professorpiva/simulate/rainbow.min.js') !!}
    {!! HTML::script('js/professorpiva/simulate/jquery-asPieProgress.js') !!}
    {!! HTML::script('js/professorpiva/cronometro/cronometro.js') !!}
    {!! HTML::script('js/fd.min.js') !!}
    {!! HTML::script(elixir('js/components/simulate/simulate.min.js')) !!}
    {!! HTML::script('js/main.js') !!}
  </body>
</html>