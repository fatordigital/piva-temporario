<!DOCTYPE html>
<html lan="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible"
              content="IE=edge,chrome=1">
        <meta name="viewport"
              content="width=device-width, initial-scale=1">
        <meta id="_token"
              data-content="{!! csrf_token() !!}">
        <title>{!! s('Project.name') !!} - Simulado</title>
      {!! HTML::style('css/professorpiva/lib/bootstrap.min.css') !!}
      {!! HTML::style('css/plugins/sweetalert/sweetalert.css') !!}
      {!! HTML::style('css/professorpiva/docs.css') !!}
      {!! HTML::style('css/style.min.css') !!}
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800'
            rel='stylesheet'
            type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'
              rel='stylesheet'
              type='text/css'>
        <link rel="stylesheet"
              href="https://cdn.shr.one/0.1.9/shr.css">
        <script type="text/javascript">var b = '{!! URL::to('/') !!}/';</script>
    </head>
    <body style="overflow: hidden;">
    <main class="simulado gray">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1>{!! $test->name !!}</h1>
				</div>
			</div>
		</div>
		<div class="container white simulado-sucesso">
			<div class="col-xs-12">
                @if ($data->dissertative)
                    <h1 class="text-center">Aguarde a correção da sua prova</h1>
                @else
                    <h1 class="text-center">Você acertou <strong><span>{!! $data->corrects !!}</span></strong> questões de {!! $count !!}!</h1>
                @endif
				<br><br>
              {{--<a href="#" class="btn btn-primary center-block btn-sucesso">« REVISAR QUESTÕES</a>--}}
			</div>


              <div class="bg-form">
                  @if (!$data->dissertative)
                        @if (!$media && $team)
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                              <h2>Com base em seus resultados nós temos uma recomendação de curso para melhorar sua performance:</h2>
                          </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                              <div class="box-outro-curso">
                                  <img src="{!! url('images/simulado-outro-curos.jpg') !!}"
                                       class="img-responsive">
                                  <img src="{!! url('images/cursos/circle-1.jpg') !!}"
                                       class="img-circle"
                                       data-pin-nopin="true">
                                  <div class="description">
                                      <h5>{!! $team->course->name !!}</h5>
                                      <span>{!! $team->category->name !!}</span>
                                      <p>
                                          {!! str_limit($team->course->resume, 100) !!}
                                      </p>
                                      <hr>
                                      <a href="{!! route('cursos.show', $team->course->alias) !!}"
                                         class="btn btn-primary pull-left">SAIBA MAIS</a>
                                      <a href="#"
                                         class="link pull-right">R$ {!! $team->value_discount && $team->value_discount > 0 ? bcoToCoin($team->value_discount) : bcoToCoin($team->value) !!}</a>
                                  </div>
                              </div>
                          </div>
                        @endif
                    @endif
                  <div class="row">
                    <div class="col-lg-12">
                      <br />
                      <div class="form-group">
                          <a href="{!! route('front.user.tests') !!}"
                             class="btn btn-primary"> Voltar para página inicial</a>

                          <?php if ($test->return_response) { ?>
                          <a href="{!! route('access.correcao', base64_decode($slug)) !!}"
                             class="btn btn-primary"> <i class="fa fa-eye"></i> Ver correção</a>
                          <?php } ?>

                      </div>
                    </div>
                  </div>
              </div>
		</div>
	</main>
    {!! HTML::script('js/cms/jquery-2.1.1.js') !!}
    {!! HTML::script('js/cms/bootstrap.min.js') !!}
    {!! HTML::script('js/plugins/sweetalert/sweetalert2.min.js') !!}
    {!! HTML::script('js/plugins/datapicker/bootstrap-datepicker.js') !!}
    {!! HTML::script('js/plugins/jquery-mask-plugin/jquery.mask.js') !!}
    {!! HTML::script('js/professorpiva/simulate/rainbow.min.js') !!}
    {!! HTML::script('js/professorpiva/simulate/jquery-asPieProgress.js') !!}
    {!! HTML::script('js/professorpiva/cronometro/cronometro.js') !!}
    {!! HTML::script('js/fd.min.js') !!}
    {!! HTML::script(elixir('js/components/simulate/simulate.min.js')) !!}
    {!! HTML::script('js/main.js') !!}
  </body>
</html>