{!! Form::open(['route' => 'front.contact.learn', 'class' => 'form-detail', 'id' => 'form']) !!}
<div class="col-xs-12">
    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
        <h2>Quer estudar conosco?</h2>
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
    <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
        {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => 'Nome (*)']) !!}
        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
    </div>
    <div class="form-group{!! $errors->first('email') ? ' has-error' : '' !!}">
        {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'E-mail (*)']) !!}
        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
    </div>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
    <div class="form-group{!! $errors->first('phone') ? ' has-error' : '' !!}">
        {!! Form::text('phone', old('phone'), ['class' => 'form-control', 'placeholder' => 'Telefone Residencial/Celular']) !!}
        {!! $errors->first('phone', '<span class="help-block">:message</span>') !!}
    </div>
    <div class="form-group{!! $errors->first('subject') ? ' has-error' : '' !!}">
        {!! Form::text('subject', old('subject'), ['class' => 'form-control', 'placeholder' => 'Assunto (*)']) !!}
        {!! $errors->first('subject', '<span class="help-block">:message</span>') !!}
    </div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="form-group{!! $errors->first('message') ? ' has-error' : '' !!}">
        {!! Form::textarea('message', old('message'), ['class' => 'form-control', 'rows' => 4, 'placeholder' => 'Digite aqui a mensagem...']) !!}
        {!! $errors->first('message', '<span class="help-block">:message</span>') !!}
    </div>
    <button type="submit" class="btn btn-warning pull-right">Enviar</button>
    <h4>
        <small>(51)</small>
        -
    </h4>
</div>
{!! Form::close() !!}
