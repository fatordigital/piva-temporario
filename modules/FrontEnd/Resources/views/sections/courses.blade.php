@if (isset($courses) && $courses->count())
  @if (!isset($content))
  <section class="section-cursos">
      <div class="container">
          <div class="row">
              <div class="col-xs-12">
                  <h3>CONFIRA OS NOVOS CURSOS</h3>
              </div>
  @endif

              <?php
              $count = 1;
              ?>
              @foreach($courses as $i => $row)
                  <div class="col-md-4 col-xs-12">
                      <div class="thumbnail thumb-cursos">
                          @if ($row->image_show && file_exists(public_path('assets/team/' . $row->image_show)))
                              <a href="{!! route('cursos.show', $row->slug) !!}" title="Detalhe do curso {!! $row->course->name !!}" style="width: 100%;height: 274px;display: flex;justify-content: flex-start;align-items: center;overflow: hidden;">
                                {!! HTML::image('assets/team/' . $row->image_show, $row->name, ['style' => 'height: 100%; max-width: auto !important; width: auto;']) !!}
                              </a>
                          @elseif ($row->image_show && file_exists(public_path('assets/team/'. $row->image_show)))
                              <a href="{!! route('cursos.show', $row->slug) !!}" title="Detalhe do curso {!! $row->course->name !!}" style="width: 100%;height: 274px;display: flex;justify-content: flex-start;align-items: center;overflow: hidden;">
                                {!! HTML::image('assets/team/' . $row->image_show, $row->name, ['style' => 'height: 100%; max-width: auto !important; width: auto;']) !!}
                              </a>
                          @else
                              <img src="{!! url('/assets/images/piva-concursos.png') !!}" class="img-main" alt="Cursos">
                          @endif
                          <div class="caption">
                              @if ($row->icon && file_exists(public_path('assets/team/'. $row->icon)))
                                {!! HTML::image('assets/team/' . $row->icon, $row->name, ['class' => 'img-circle', 'width' => 84, 'height' => 84]) !!}
                              @else
                                <img src="{!! url('/images/cursos/circle-1.jpg') !!}" class="img-circle">
                              @endif
                              <span>
                                <img src="{!! url('/images/icons/icon-ead.png') !!}"> {!! $row->category->name !!}
                              </span>
                              <h5 style="min-height: 40px;">
                                  <a href="{!! route('cursos.show', $row->slug) !!}" title="Detalhe do curso {!! $row->course->name !!}" class="unstyled">
                                  {!! $row->name !!}
                                  </a>
                              </h5>
                              <p>
                                  <a href="{!! route('cursos.show', $row->slug) !!}" title="Detalhe do curso {!! $row->course->name !!}" class="unstyled">
                                  {!! !empty($row->course->resume) ? str_limit($row->course->resume, 100) : str_limit(strip_tags($row->course->description), 100) !!}
                                  </a>
                              </p>
                              <hr>
                              <p>
                                  @if (in_array($row->id, $user_team_id))
                                      <a href="{!! route('estudar.index', $row->slug) !!}" title="Detalhe do curso {!! $row->course->name !!}" class="btn btn-primary" role="button">
                                          Estudar
                                      </a>
                                  @else
                                      <a href="{!! route('cursos.show', $row->slug) !!}" title="Detalhe do curso {!! $row->course->name !!}" class="btn btn-primary" role="button">
                                          Saiba mais
                                      </a>
                                  @endif
                                  @if ($row->value > 0 || $row->value_discount > 0)
                                    <a href="#" class="link pull-right" ><small>R$</small> {!! $row->value_discount && $row->value_discount > 0 ? bcoToCoin($row->value_discount) : bcoToCoin($row->value) !!}</a>
                                  @else
                                    <a href="#" class="link pull-right" ><small></small>Grátis</a>
                                  @endif
                                  <div class="clearfix"></div>
                              </p>
                          </div>
                      </div>
                  </div>
                  {!! ($count % 3 == 0) ? '<div class="clearfix"></div>' : '' !!}
                  <?php
                  $count++;
                  ?>
              @endforeach
  @if (!isset($content))
              <div class="clearfix"></div>
          </div>
      </div>
  </section>
  @endif
@endif