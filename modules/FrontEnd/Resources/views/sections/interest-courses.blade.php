<div class="col-sm-6 col-md-3">
    <div class="thumbnail thumb-primary">
        <h2>
            VOCÊ <br> TAMBÉM <br> PODE SE <br> INTERESSAR
        </h2>
        <img src="{!! url('images/arrow.png') !!}" alt="" class="img-responsive">
    </div>
</div>

@include('frontend::sections.courses', isset($interest_courses) ? ['courses' => $interest_courses] : [])