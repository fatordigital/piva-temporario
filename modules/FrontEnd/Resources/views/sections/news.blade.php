@if (isset($news) && $news->count())
  <?php $i = 1; ?>
  @foreach($news as $new)
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail thumb-news">
          @if($new->image && fex('/assets/news/', $new->image))
            {!! HTML::image('assets/news/'.$new->image, $new->name) !!}
          @else
            <img src="{!! url('/images/no-image.jpg') !!}" alt="{!! $new->name !!}">
          @endif
          <div class="caption">
                <h5>{!! $new->name !!}</h5>
                <div class="news-height">
                  {!! str_limit(strip_tags($new->text), 255) !!}
                </div>
                <hr>
                <p>
                    <a href="{!! route('novidades.show', $new->slug) !!}" class="link">LEIA MAIS</a>
                </p>
            </div>
        </div>
    </div>
    @if ($i % 3 == 0)
      <div class="clearfix"></div>
    @endif
    <?php $i++; ?>
  @endforeach
@endif