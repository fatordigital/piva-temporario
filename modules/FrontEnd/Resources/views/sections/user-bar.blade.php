<section class="bar-yellow">
  <div class="container">
      <div class="row">
          <div class="col-lg-offset-2 col-md-offset-2 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="links-rapidos">
                  <ul class="nav nav-pills" style="width: 100%">
                      <li{!! isset($active) && $active == 'home' ? ' style="background-color: #ffffff;"' : '' !!}>
                          <a href="{!! route('front.user.home') !!}">Meus cursos</a>
                      </li>
                      <li{!! isset($active) && $active == 'simulates' ? ' style="background-color: #ffffff;"' : '' !!}>
                          <a href="{!! route('front.user.tests') !!}">Meus simulados</a>
                      </li>
                      <li{!! isset($active) && $active == 'orders' ? ' style="background-color: #ffffff;"' : '' !!}>
                          <a href="{!! route('front.my.orders') !!}">Meus pedidos</a>
                      </li>
                      <li{!! isset($active) && $active == 'notification' ? ' style="background-color: #ffffff;"' : '' !!}>
                          <a href="{!! route('front.user.notification') !!}">Notificações</a>
                      </li>
                      <li{!! isset($active) && $active == 'register' ? ' style="background-color: #ffffff;"' : '' !!}>
                          <a href="{!! route('front.user.register') !!}">Meus Dados</a>
                      </li>
                      <li{!! isset($active) && $active == 'certified' ? ' style="background-color: #ffffff;"' : '' !!}>
                          <a href="{!! route('front.user.certified') !!}">Meus Certificados</a>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
</section>