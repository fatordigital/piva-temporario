@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li>
                <a href="{!! route('front.home') !!}">Página inicial</a>
              </li>
              <li class="active">Autenticação</li>
          </ol>
          <div class="col-xs-12">
              <h1>Autenticação</h1>
          </div>
      </div>
  </div>
@endsection

@section('content')
  <section class="section-cursos pg-curso">
      <div class="container">
          <div class="row">
              <div class="col-xs-12">
                  <h3>2. CADASTRE-SE OU FAÇA SEU LOGIN</h3>
                  <hr><br><br>
              </div>
              <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                  <h5 class="text-center">Já sou cadastrado</h5>
                  {!! Form::open(['route' => request()->has('redirect') ? ['front.verify', 'redirect='. request()->get('redirect')] : 'front.verify', 'method' => 'POST', 'id' => 'form-login']) !!}
                      <div class="form-group">
                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail (*)']) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Senha (*)']) !!}
                      </div>
                      <button type="submit" class="btn btn-warning">Entrar</button>
                      <a href="#" data-toggle="modal" data-target="#forget-modal" class="link pull-right">Esqueci minha senha</a>
                  {!! Form::close() !!}
              </div>
              {{--<div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">--}}
                  {{--<div class="my-bar"></div>--}}
              {{--</div>--}}
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                  <h5 class="text-center">Quero me cadastrar</h5>
                  <br><br><br>
                  <a href="{!! route('front.user.register.guest') !!}{!! request()->has('redirect') ? '?redirect=' . request()->get('redirect') : ''  !!}" class="btn btn-warning btn-cad center-block">FAZER UM NOVO CADASTRO</a>
              </div>

          </div>
      </div>
  </section>
  @include('frontend::user.forget_password')
@endsection