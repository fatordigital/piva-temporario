@extends('layouts.front')

@section('top')
    <div class="banner-curso">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{!! route('front.home') !!}">Página inicial</a></li>
                <li>
                    <a href="{!! back()->getTargetUrl() != request()->fullUrl() ? back()->getTargetUrl() : route('front.home') !!}">
                        Professores
                    </a>
                </li>
                <li class="active">
                    {!! $teacher->name !!}
                </li>
            </ol>
            <div class="col-xs-12">
                <h1>{!! $teacher->name !!}</h1>
            </div>
        </div>

    </div>
@endsection

@section('content')

    <section class="section-cursos cursos-free">
        <div class="container">
            <div class="row">

                <div class="col-lg-8">
                    <div>
                        {!! $teacher->description !!}
                    </div>
                </div>

                <div class="col-lg-4">
                    @if ($teacher->filename)
                        <img src="{!! url('/assets/users/'. $teacher->filename) !!}" class="img-responsive"
                             alt="{!! $teacher->name !!}" title="{!! $teacher->title !!}"/>
                    @endif
                </div>

            </div>
        </div>
    </section>

@endsection