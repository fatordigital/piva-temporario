@extends('layouts.front')

@section('top')
    <div class="banner-curso">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{!! route('front.home') !!}">Página inicial</a></li>
                <li class="active">Contato</li>
            </ol>
            <div class="col-xs-12">
                <h1>Entre em contato</h1>
            </div>
        </div>

    </div>
@endsection

@section('content')
    <section class="section-cursos cursos-free">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    {!! Form::open(['route' => 'front.contact.store', 'id' => 'form-contato']) !!}
                    <div class="form-group">
                        <input type="text" placeholder="Nome" class="form-control" name="name" required/>
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="E-mail" class="form-control" name="email" required/>
                    </div>
                    <div class="form-group">
                        <input type="text" placeholder="Assunto" class="form-control" name="subject" required/>
                    </div>
                    <div class="form-group">
                        <textarea name="message" placeholder="Mensagem" rows="6" class="form-control"
                                  required></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-contato" id="send">Enviar</button>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <h5>SE PREFERIR, ENTRE EM CONTATO POR TELEFONE:</h5>
                    <p>
                        <strong>E-mail:</strong> teste@teste.com.br <br/><br/>
                        <strong>Horário de atendimento:</strong><br>
                        Segunda a Sexta: 9h-18h<br>
                    </p>
                </div>
                {{--<div class="col-lg-6 col-md-6 col-xs-12">--}}
                    {{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.2517821737624!2d-51.21388648441954!3d-30.02963368188758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951979ae11a7fc67%3A0xfde057c6cf9c00c4!2sAv.+Independ%C3%AAncia%2C+925+-+Independ%C3%AAncia%2C+Porto+Alegre+-+RS%2C+90035-072!5e0!3m2!1spt-BR!2sbr!4v1493147483786" width="383" height="250" frameborder="0" style="border:0"></iframe>--}}
                {{--</div>--}}
            </div>
        </div>
    </section>
@endsection

@section('js')
    {!! HTML::script('js/professorpiva/contact/contact.min.js') !!}
@endsection