@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li>
                <a href="{!! route('front.home') !!}">Página inicial</a>
              </li>
              <li>
                <a href="{!! route('front.user.home') !!}">Área do Aluno</a>
              </li>
              <li class="active">
                  Meus Certificados
              </li>
          </ol>
          <div class="col-xs-12">
              <h1>Certificados</h1>
          </div>
      </div>
  </div>
@endsection


@section('content')

  @include('frontend::sections.user-bar', ['active' => 'certified'])

  <section class="section-cursos pg-curso">
      <div class="container">
          <div class="row">
              <div class="col-xs-12">
                @if (isset($certifieds) && $certifieds->count())
                  @foreach($certifieds as $certified)
                    <div class="alert alert-success">
                      @if ($certified->team)
                        Curso : <strong>{!! $certified->team->name !!}</strong> <br />
                      @endif
                      Olá {!! auth()->user()->name !!}, foi gerado um certificado para você, por favor, <a target="_blank" href="{!! route('front.user.certified.download', base64_encode(json_encode(CryptoJSEncrypt($certified->id)))) !!}" class="text-primary">clique aqui</a> e imprima seu certificado.
                    </div>
                  @endforeach
                @else
                      <h5>Você não tem certificado no momento :( <a href="{!! route('front.home') !!}" class="text-success">Clique aqui e veja nossos cursos em destaque e tenha seu certificado.</a></h5>
                @endif
              </div>
          </div>
      </div>
  </section>

@endsection