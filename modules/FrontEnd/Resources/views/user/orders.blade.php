@extends('layouts.front')

@section('top')
    <div class="banner-curso">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="{!! route('front.home') !!}">Página inicial</a></li>
                <li class="active">Área do Aluno</li>
            </ol>
            <div class="col-xs-12">
                <h1>Área do Aluno</h1>
            </div>
        </div>
    </div>
@endsection

@section('content')
    @include('frontend::sections.user-bar', ['active' => 'orders'])

    @if (isset($alerts) && $alerts->count())
        <section class="section-cursos pg-curso">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?php $i = 0; ?>
                        @foreach($alerts as $alert)
                            <div class="alert {!! $i < 3 ? 'alert-professorpiva' : 'alert-info' !!}" role="alert">
                                <strong>[{!! $alert->Created !!}]: </strong>
                                {!! $alert->message !!}
                            </div>
                            <?php $i++; ?>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif

    <section class="section-cursos pg-curso">
        <div class="container">
            <div class="row table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <td width="8%">Pedido</td>
                            <td>Status</td>
                            <td>SubTotal</td>
                            <td>Total</td>
                            <td width="15%">Metodo</td>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($user_orders->count())
                            @foreach($user_orders as $row)
                                <tr>
                                    <td>{!! $row->id !!}</td>
                                    <td>{!! (!is_null($row->orderStatus)) ? $row->orderStatus->name : 'Aguardando' !!}</td>
                                    <td>R$ {!! bcoToCoin($row->sub_total > 0 && !empty($row->sub_total) ? $row->sub_total : 0.00) !!}</td>
                                    <td>R$ {!! bcoToCoin($row->total > 0 && !empty($row->total) ? $row->total : 0.00) !!}</td>
                                    <td>
                                        @if ($row->pagseguro_method_code < 200)
                                            Cartão de Crédito
                                        @elseif($row->pagseguro_method_code > 200 && $row->pagseguro_method_code < 300)
                                            @if ($row->pagseguro_status == 3 || $row->pagseguro_status == 4)
                                                Pago
                                            @else
                                                <a target="_blank" class="btn btn-xs btn-primary" href="{!! $row->payment_link !!}">
                                                    Imprimir Boleto
                                                </a>
                                            @endif
                                        @elseif($row->pagseguro_method_code > 300 && $row->pagseguro_method_code < 307)
                                            @if ($row->pagseguro_status == 3 || $row->pagseguro_status == 4)
                                                Pago
                                            @else
                                                <a target="_blank" class="btn btn-xs btn-primary" href="{!! $row->payment_link !!}">
                                                    Débito Online
                                                </a>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection