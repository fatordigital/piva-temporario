@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li>
                <a href="{!! route('front.home') !!}">Página inicial</a>
              </li>
              <li>
                <a href="{!! route('front.user.home') !!}">Área do Aluno</a>
              </li>
              <li class="active">
                  Notificações
              </li>
          </ol>
          <div class="col-xs-12">
              <h1>Notificações</h1>
          </div>
      </div>
  </div>
@endsection


@section('content')

  @include('frontend::sections.user-bar', ['active' => 'notification'])

  <section class="section-cursos pg-curso">
      <div class="container">
          <div class="row">
              <div class="col-xs-12">
                  @if (isset($alerts) && $alerts->count())
                    <?php $i = 0; ?>
                    @foreach($alerts as $alert)
                      <div class="alert {!! $i < 3 ? 'alert-professorpiva' : 'alert-info' !!}" role="alert">
                          <strong>[{!! $alert->Created !!}]: </strong>
                          {!! $alert->message !!}
                      </div>
                      <?php $i++; ?>
                    @endforeach
                  @else
                      <h5>Nenhuma notificação no momento.</h5>
                  @endif
              </div>
          </div>
      </div>
  </section>

@endsection