@extends('layouts.front')


@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li>
                <a href="{!! route('front.home') !!}">Página inicial</a>
              </li>
              <li class="active">Autenticação</li>
          </ol>
          <div class="col-xs-12">
              <h1>Autenticação</h1>
          </div>
      </div>
  </div>
@endsection


@section('content')

<section class="section-cursos pg-curso">
    <div class="container">
      <div class="row">
          <div class="col-md-4 col-md-offset-4">
              <div class="panel panel-default">
                  <div class="panel-body">
                      <div class="text-center">
                        <img src="/images/logo.png" class="img-responsive center-block">
                        <h3 class="text-center">Resetar a senha</h3>
                        <p>Digite uma senha com minímo 6 caracteres.</p>
                          <div class="panel-body">

                            {!! Form::open(['route' => 'reset.auth']) !!}
                            <fieldset>
                                <div class="form-group">
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                    <!--EMAIL ADDRESS-->
                                    {!! Form::text('email', null, [
                                        'data-toggle' => 'tooltip',
                                        'data-placemente' => 'top',
                                        'title' => 'Digite uma senha com minímo 6 caracteres',
                                        'class' => 'form-control',
                                        'required',
                                        'placeholder' => 'E-mail cadastrado (*)']) !!}
                                  </div>
                                  {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                                </div>

                                <div class="form-group">
                                  <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-pencil color-blue"></i></span>
                                    <!--EMAIL ADDRESS-->
                                    {!! Form::password('password', [
                                        'data-toggle' => 'tooltip',
                                        'data-placemente' => 'top',
                                        'title' => 'Digite uma senha com minímo 6 caracteres',
                                        'class' => 'form-control',
                                        'required',
                                        'placeholder' => 'Nova senha (*)']) !!}
                                    {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                                  </div>
                                </div>
                                  <div class="form-group">
                                      <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-check color-blue"></i></span>
                                        <!--EMAIL ADDRESS-->
                                        {!! Form::password('password_confirmation', [
                                            'data-toggle' => 'tooltip',
                                            'data-placemente' => 'top',
                                            'title' => 'Digite uma senha com minímo 6 caracteres',
                                            'class' => 'form-control',
                                            'required',
                                            'placeholder' => 'Nova senha (*)']) !!}
                                      </div>
                                    {!! $errors->first('password_confirmation', '<span class="help-block">:message</span>') !!}
                                </div>
                                <div class="form-group">
                                  <input class="btn btn-lg btn-primary btn-block" value="Salvar senha" type="submit">
                                </div>
                              </fieldset>
                            {!! Form::hidden('token', $token) !!}
                            {!! Form::close() !!}
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
@endsection