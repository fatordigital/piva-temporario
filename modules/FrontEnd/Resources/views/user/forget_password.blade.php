<div id="forget-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    {!! Form::open(['route' => 'forget.auth']) !!}
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="modal-title">Esqueci minha senha</h5>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label for="email">
            E-mail <strong class="text-danger">*</strong>
          </label>
          {!! Form::text('email', null, ['required' => 'required', 'class' => 'form-control', 'placeholder' => 'E-mail']) !!}
          {!! $errors->first('email', '<span class="help-block text-danger">:message</span>') !!}
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <a class="btn btn-info" href="{!! URL::route('front.user.register.guest') !!}">Registrar</a>
        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i> Enviar</button>
      </div>
    </div>
    {!! Form::close() !!}
  </div>
</div>