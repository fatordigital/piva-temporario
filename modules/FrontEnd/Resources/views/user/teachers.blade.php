@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li>
                <a href="{!! route('front.home') !!}">
                  Página inicial
                </a>
              </li>
              <li class="active">Professores</li>
          </ol>
          <div class="col-xs-12">
              <h1>Professores</h1>
          </div>
      </div>
  </div>
@endsection


@section('content')
  <section class="section-cursos cursos-free professores">
      <div class="container">
          @foreach($users as $user)
          <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6">
              <a href="{!! route('teacher.detail', $user->slug) !!}">
                  <figure>
                      @if (!fex('assets/users/', $user->filename))
                          <img src="{!! url('/images/no-photo.jpg') !!}" alt="Nome do Professor"/>
                      @else
                          <img src="{!! url('/assets/users/'.$user->filename) !!}" alt="{!! $user->name !!}"/>
                      @endif
                  </figure>
                  <div class="basic-info">
                      <h4>{!! $user->name !!}</h4>
                  </div>
              </a>
          </div>
          @endforeach
      </div>
  </section>
@endsection