@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li><a href="{!! route('front.home') !!}">Página inicial</a></li>
              <li class="active">Área do Aluno</li>
          </ol>
          <div class="col-xs-12">
              <h1>Área do Aluno</h1>
          </div>
      </div>
  </div>
@endsection

@section('content')
@include('frontend::sections.user-bar', ['active' => 'home'])

@if (isset($alerts) && $alerts->count())
<section class="section-cursos pg-curso">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php $i = 0; ?>
                @foreach($alerts as $alert)
                    <div class="alert {!! $i < 3 ? 'alert-professorpiva' : 'alert-info' !!}" role="alert">
                        <strong>[{!! $alert->Created !!}]: </strong>
                        {!! $alert->message !!}
                    </div>
                    <?php $i++; ?>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif

<section class="section-cursos pg-curso">
    <div class="container">
        <div class="row">
            @if ($user_courses->count())
                <div class="col-xs-12">
                    <h3>MEUS CURSOS</h3>
                </div>
              @foreach($user_courses as $i => $team_user)
                <?php $row_team = $team_user->team; ?>
                @if ($row_team)
                <div class="col-sm-4 col-md-3 col-xs-12">
                    <div class="thumbnail thumb-cursos meus-cursos">
                        @if ($row_team->image_show != '' && file_exists(public_path('assets/team/'. $row_team->image_show)))
                            {!! HTML::image('assets/team/'. $row_team->image_show, $row_team->name, ['width' => 261, 'height' => 200]) !!}
                        @else
                            <img src="{!! url('/assets/images/piva-concursos.png') !!}" alt="Cursos" width="321">
                        @endif
                        <div class="caption">
                            <img src="{!! url('/images/cursos/circle-1.jpg') !!}" class="img-circle">
                            <span>
                              <img src="{!! url('images/icons/icon-ead.png') !!}"> {!! $team_user->team->category->name !!}
                            </span>
                            <h5 style="min-height: 55px;">{!! $row_team->name !!}</h5>
                            <p>
                                {!! !empty($row_team->course->resume) ? str_limit($row_team->course->resume, 80) : str_limit(strip_tags($row_team->course->description), 80) !!}
                            </p>
                            <hr>
                            <p>
                                <a href="{!! route('cursos.show', $team_user->team->slug) !!}" class="btn btn-danger">{!! $team_user->UserContentEnds->count() !!} de {!! $team_user->team->lessons ? $team_user->team->lessons->count() : 0 !!} aulas concluídas</a>
                                <hr>
                                <a href="{!! route('cursos.show', $team_user->team->slug) !!}" class="btn btn-primary" role="button">VER DETALHE DO CURSO</a>
                                <hr>
                                <a href="{!! route('estudar.index', $team_user->team->slug) !!}" class="btn btn-primary" role="button">ESTUDAR</a>
                            </p>
                        </div>
                    </div>
                </div>
                @endif
                @if (($i + 1) % 4 == 0)
                    <div class="clearfix"></div>
                @endif
              @endforeach
            @else
              <div class="col-xs-9">
                <h5>Você não tem curso no momento :( <a href="{!! route('front.home') !!}" class="text-success">Clique aqui e veja nossos cursos em destaque.</a></h5>
              </div>
            @endif
            <div class="col-sm-6 col-md-3 col-xs-12">
                <div class="thumbnail thumb-cursos more-cursos">
                    <a href="{!! route('front.home') !!}">
                        <img style="height: 515px !important;" src="{!! url('/images/more-curso.jpg') !!}" class="img-main" alt="Professor Piva - Cursos Preparatórios para Concursos">
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
            @if ($user_tests && $user_tests->count())
              <div class="col-xs-12">
                  <h3>MEUS SIMULADOS</h3>
              </div>
              @foreach($user_tests as $i => $user_test)
                  @if ($user_test->Test)
                    <div class="col-sm-6 col-md-3 col-xs-12">
                        <div class="thumbnail thumb-simulado">
                            <img src="{!! url('images/icons/simulado2.png') !!}" class="img-main" alt="Simulados">
                            <div class="caption">
                                <h5 style="height: 55px;">{!! $user_test->Test ? $user_test->Test->name : '' !!}</h5>
                                <p style="height: 52px;">
                                    {!! $user_test->Test ? $user_test->Test->subject : '' !!}
                                </p>
                                <hr>
                                <p style="height: 50px;">
                                    <span>{!! $user_test->Test ? $user_test->Test->questions->count() : 0 !!} Questões</span>
                                </p>
                                <hr />
                                @if ($user_test->Test && empty($user_test->end_test))
                                  <a href="{!! route('access.simulate', $user_test->Test->slug) !!}" class="btn btn-primary">Faça o simulado</a>
                                @elseif ($user_test->Test)
                                  <a href="{!! route('access.simulate', $user_test->Test->slug) !!}" class="btn btn-primary">Refazer simulado</a>
                                @endif
                            </div>
                        </div>
                    </div>
                  @endif

                  {!! $i % 4 == 3 ? '<div class="clearfix"></div>' : '' !!}
              @endforeach
            @endif
        </div>
    </div>
</section>
@endsection