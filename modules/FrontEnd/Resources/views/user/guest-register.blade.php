@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li>
                <a href="{!! route('front.home') !!}">Página inicial</a>
              </li>
              <li class="active">
                Registro
              </li>
          </ol>
      </div>
  </div>
@endsection


@section('content')


  <section class="section-cursos cursos-free">
      <div class="container">
          @if (isset($errors) && $errors->count())
          <div class="row">
              <div class="col-lg-12">
                  <div class="alert alert-danger">
                      <ul>
                          @foreach($errors->all() as $error)
                            <li>
                                {!! $error !!}
                            </li>
                          @endforeach
                      </ul>
                  </div>
              </div>
          </div>
          @endif

        {!! Form::open(['route' => ['front.user.store.register', request()->has('redirect') ? 'redirect='.request('redirect') : null], 'id' => 'form-contato']) !!}
        <h3>Informações pessoais</h3>
          <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="form-group">
                      <label for="name">
                        Nome Completo <strong class="text-danger">(*)</strong>
                      </label>
                      <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                          {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                          {!! $errors->first('name', '<span>:message</span>') !!}
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <label for="password">
                            Senha <strong class="text-danger">(*)</strong>
                          </label>
                          <div class="form-group{!! $errors->first('password') ? ' has-error' : '' !!}">
                              {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Senha']) !!}
                          </div>
                      </div>
                      <div class="col-lg-6 col-md-3 col-sm-12 col-xs-12">
                          <label for="r_password">
                            Confirmar Senha <strong class="text-danger">(*)</strong>
                          </label>
                          <div class="form-group{!! $errors->first('password_confirmation') ? ' has-error' : '' !!}">
                              {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirmar senha']) !!}
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="email">
                          Seu E-mail
                        </label>
                        {!! Form::text('email', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('email', '<span>:message</span>') !!}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <label for="national_id">
                            CPF <strong class="text-danger">(*)</strong>
                          </label>
                          <div class="form-group{!! $errors->first('user_socializes.national_id') ? ' has-error' : '' !!}">
                              {!! Form::text('user_socializes[national_id]', null, ['class' => 'form-control cpf-mask', 'required' => 'required']) !!}
                              {!! $errors->first('user_socializes.national_id', '<span>:message</span>') !!}
                          </div>
                      </div>
                      <div class="col-lg-6 col-md-3 col-sm-12 col-xs-12">
                          <label for="birth_date">
                            Data de nascimento <strong class="text-danger">(*)</strong>
                          </label>
                          <div class="form-group{!! $errors->first('user_socializes.birth_date') ? ' has-error' : '' !!}">
                              {!! Form::text('user_socializes[birth_date]', null, ['class' => 'form-control s-datepicker date-mask', 'required' => 'required']) !!}
                          </div>
                      </div>
                      <div class="col-lg-6 col-md-3 col-sm-12 col-xs-12">
                          <label for="birth_date">
                              Telefone <strong class="text-danger">(*)</strong>
                          </label>
                          <div class="form-group{!! $errors->first('socialize.phone') ? ' has-error' : '' !!}">
                              {!! Form::text('socialize[phone]', null, ['class' => 'form-control phone-mask', 'required' => 'required']) !!}
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <h3>Endereço</h3>
          <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                  <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="postal_code">
                          CEP <strong class="text-danger">*</strong>
                        </label>
                        <div class="form-group">
                          {!! Form::text('user_socializes[postal_code]', null, ['class' => 'form-control cep-mask', 'placehodler' => 'CEP', 'required' => 'required']) !!}
                        </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12">
                          <label for="route">
                            Logradouro <strong class="text-danger">(*)</strong>
                          </label>
                          <div class="form-group">
                              {!! Form::text('user_socializes[route]', null, ['class' => 'form-control', 'required' => 'required']) !!}
                          </div>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                          <label for="route_number">
                            Número <strong class="text-danger">(*)</strong>
                          </label>
                          <div class="form-group">
                              {!! Form::text('user_socializes[route_number]', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Nº']) !!}
                          </div>
                      </div>
                  </div>
                  <div class="form-group">
                      <label for="district">
                        Bairro <strong class="text-danger">(*)</strong>
                      </label>
                      <div class="form-group">
                          {!! Form::text('user_socializes[district]', null, ['class' => 'form-control', 'required' => 'required']) !!}
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <label for="state">
                            Estado <strong class="text-danger">(*)</strong>
                          </label>
                          <div class="form-group">
                              {!! Form::select('user_socializes[state]', getStates(), null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Estado']) !!}
                          </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <label for="city">
                            Cidade <strong class="text-danger">(*)</strong>
                          </label>
                          <div class="form-group">
                              {!! Form::text('user_socializes[city]', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Cidade']) !!}
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <div class="row">
            <div class="col-lg-12">
              <div class="form-group">
                  <button type="submit" class="btn btn-contato" name="submit">Registrar</button>
              </div>
            </div>
          </div>
        {!! Form::close() !!}
      </div>
  </section>

@endsection