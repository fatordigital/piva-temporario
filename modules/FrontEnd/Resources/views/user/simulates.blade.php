@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li>
                <a href="{!! route('front.home') !!}">Página inicial</a>
              </li>
              <li>
                <a href="{!! route('front.user.home') !!}">Área do Aluno</a>
              </li>
              <li class="active">
                  Notificações
              </li>
          </ol>
          <div class="col-xs-12">
              <h1>Simulados</h1>
          </div>
      </div>
  </div>
@endsection


@section('content')

  @include('frontend::sections.user-bar', ['active' => 'simulates'])

  <section class="section-cursos pg-curso">
      <div class="container">
          <div class="row">
              <div class="col-xs-12">
                @if (!$user_tests->count())
                  <h5>Você não tem simulado no momento. <a href="{!! route('front.home') !!}" class="text-success">Clique aqui e veja nossos cursos em destaque.</a></h5>
                @else
                  <h3>MEUS SIMULADOS</h3>
                @endif
              </div>
              @if ($user_tests->count())
                @foreach($user_tests as $i => $user_test)
                    <?php $row = $user_test->Test; ?>
                     @if ($row)
                      <div class="col-sm-6 col-md-3">
                          <div class="thumbnail thumb-simulado-free" style="padding: 15px;">
                              <div style="height: 156px;">
                                  @if (!file_exists(public_path('/assets/simulado/'. $row->filename)) || $row->filename == '')
                                      <img src="{!! url('assets/images/simulado.jpg') !!}" class="img-responsive" alt="Cursos" />
                                  @else
                                      {!! HTML::image('assets/simulado/'. $row->filename, $row->name, ['width' => 232, 'height' => 232, 'id' => 'foto', 'class' => 'img-responsive']) !!}
                                  @endif
                              </div>
                              <div class="caption" style="min-height: 210px;">
                                  <h5>{!! $row->name !!}</h5>
                                  <p>
                                      {!! $row->subject !!}
                                  </p>
                                  <p>Qtd. Questões: <strong>{!! $row->questions->count() !!}</strong></p>
                                  <p>Tempo: <strong>{!! $row->duration !!}</strong></p>
                                  <p>Questão comentada: <strong>{!! $row->return_response ? 'Sim' : 'Não' !!}</strong></p>
                              </div>
                              @if (!auth()->guest())
                                  <a href="{!! route('access.simulate', $row->slug) !!}" class="btn btn-warning btn-simulado center-block">
                                      <h5><i class="glyphicon glyphicon-list-alt"></i> FAÇA ESSE SIMULADO</h5>
                                  </a>
                              @else
                                  <a href="{!! route('front.user.tests', $row->slug) !!}" class="btn btn-warning btn-simulado center-block">
                                      <h5><i class="glyphicon glyphicon-list-alt"></i> FAÇA ESSE SIMULADO</h5>
                                  </a>
                              @endif
                          </div>
                      </div>
                      @endif
                       <?php echo ($i % 4 == 3) ? '<div class="clearfix"></div>' : '' ?>
                @endforeach
              @endif
          </div>
      </div>
  </section>

@endsection