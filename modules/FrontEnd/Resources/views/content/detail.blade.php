@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li><a href="{!! url('front.home') !!}">Página inicial</a></li>
              @if ($slug != 'simulados')
                <li class="active">Conteúdo Grátis</li>
              @else
                <li class="active">Simulados Grátis</li>
              @endif
          </ol>
          <div class="col-xs-12">
            @if ($slug != 'simulados')
              <h1>Conteúdo Grátis</h1>
            @else
              <h1>Simulados Grátis</h1>
            @endif
          </div>
      </div>
  </div>
@endsection

@section('content')
  <section class="section-cursos cursos-free download-arquivos">
      <div class="container">
          <div class="row">
              @if ($slug != 'simulados')
                @foreach($content as $row)
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                      <a target="_blank" href="{!! url('assets/contents/'. $row->content) !!}" class="btn-download">
                        <span><i class="glyphicon glyphicon-download"></i> {!! $row->name !!}</span>
                        <span><img src="{!! url('/images/icons/cloud-computing.svg') !!}" alt="" /></span>
                      </a>
                  </div>
                @endforeach
              @else
                @foreach($content as $row)
                      <div class="col-sm-6 col-md-3">
                          <div class="thumbnail thumb-simulado-free" style="padding: 15px;">
                              <div style="height: 232px;">
                                  @if (!file_exists(public_path('/assets/simulado/'. $row->filename)) || $row->filename == '')
                                      <img src="{!! url('/images/cursos/curso-1.jpg') !!}" class="img-responsive" alt="Cursos" />
                                  @else
                                      {!! HTML::image('assets/simulado/'. $row->filename, $row->name, ['id' => 'foto', 'class' => 'img-responsive']) !!}
                                  @endif
                              </div>
                              <div class="caption">
                                  <h5 style="min-height: 40px;">{!! $row->name !!}</h5>
                                  <p>
                                      {!! $row->subject !!}
                                  </p>
                              </div>
                              @if (!auth()->guest())
                                  <a href="{!! route('access.simulate', $row->slug) !!}" class="btn btn-warning btn-simulado center-block">
                                      <h5><i class="glyphicon glyphicon-list-alt"></i> FAÇA ESSE SIMULADO</h5>
                                  </a>
                              @else
                                  <a href="{!! route('front.user.tests', $row->slug) !!}" class="btn btn-warning btn-simulado center-block">
                                      <h5><i class="glyphicon glyphicon-list-alt"></i> FAÇA ESSE SIMULADO</h5>
                                  </a>
                              @endif
                          </div>
                      </div>
                @endforeach
              @endif
          </div>
      </div>
  </section>
@endsection