@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li><a href="{!! route('front.home') !!}">Página inicial</a></li>
              <li class="active">Conteúdos Grátis</li>
          </ol>
          <div class="col-xs-12">
              <h1>Conteúdos Grátis</h1>
          </div>
      </div>
  </div>
@endsection

@section('content')
  <section class="section-cursos cursos-free">
      <div class="container">
          <div class="row">
              <div class="col-sm-6 col-md-4">
                  <div class="thumbnail thumb-cursos-free">
                      <img src="{!! url('images/conteudo-free/conteudo-free-1.png') !!}" alt="Cursos">
                      <div class="caption">
                          <h3>Cursos Grátis</h3>
                          <hr>
                          <p>
                              A fim de incentivá-lo em sua preparação, disponibilizamos alguns cursos gratuitamente com material didático em PDF e videoaulas.
                          </p>
                          <a href="{!! route('cursos.index', 'q=gratis') !!}" class="btn btn-primary">ACESSE ESSE CONTEÚDO</a>
                      </div>
                  </div>
              </div>
              <div class="col-sm-6 col-md-4">
                  <div class="thumbnail thumb-cursos-free">
                      <img src="{!! url('images/conteudo-free/conteudo-free-2.png') !!}" alt="Cursos">
                      <div class="caption">
                          <h3>Simulados Grátis</h3>
                          <hr>
                          <p>
                              A melhor forma de aprender é resolvendo questões. Pensando nisso, disponibilizamos simulados gratuitamente para aperfeiçoar seu conhecimento.
                          </p>
                          <a href="{!! route('front.user.tests') !!}" class="btn btn-primary">ACESSE ESSE CONTEÚDO</a>
                      </div>
                  </div>
              </div>
              <div class="col-sm-6 col-md-4">
                  <div class="thumbnail thumb-cursos-free">
                      <img src="{!! url('images/conteudo-free/conteudo-free-3.png') !!}" alt="Cursos">
                      <div class="caption">
                          <h3>Arquivos Grátis</h3>
                          <hr>
                          <p>
                              Pensando em você, aluno, oferecemos arquivos gratuitos para download, a fim de que possam conhecer melhor a nossa plataforma de ensino.
                          </p>
                          <a href="{!! route('content.free', 'arquivos') !!}" class="btn btn-primary">ACESSE ESSE CONTEÚDO</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
@endsection