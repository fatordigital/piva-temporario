@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li>
                <a href="{!! route('front.home') !!}">Página inicial</a>
              </li>
              <li class="active">Blog</li>
          </ol>
          {!! Form::open(['url' => route('novidades.index'), 'method' => 'get', 'class' => 'form-inline']) !!}
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('q', null, ['class' => 'form-control', 'placeholder' => 'Buscar']) !!}
                </div>
            </div>
            <button type="submit" class="btn btn-primary">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          {!! Form::close() !!}
          <div class="col-xs-12">
              <h1>Blog</h1>
          </div>
      </div>
  </div>
@endsection

@section('content')
  <section class="section-news">
      <div class="container">
          <div class="row">
              <div class="col-xs-12">
                  <h3>{!! $list->count() ? 'NOTÍCIAS' : 'NENHUMA NOTÍCIA NO MOMENTO' !!}</h3>
              </div>
              @if ($list->count())
                  @include('frontend::sections.news', ['news' => $list])
                  <div class="col-xs-12">
                      <nav class="read-more">
                        @include('frontend::elements.paginator', ['paginator' => $list])
                      </nav>
                  </div>
              @endif
          </div>
      </div>
  </section>
@endsection