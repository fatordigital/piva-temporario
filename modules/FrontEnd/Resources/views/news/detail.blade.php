@extends('layouts.front')

@section('top')
    <div class="banner-news">
        <img src="{!! url('/images/news/news-1.jpg') !!}" class="img-news-detail">
        <img src="{!! url('/images/bg-curso.png') !!}" class="filter">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('front.home') !!}">Página inicial</a>
                </li>
                <li>
                    <a href="{!! route('novidades.index') !!}">Novidades</a>
                </li>
                <li class="active">{!! str_limit($show->name, 50) !!}</li>
            </ol>
            <div class="col-xs-12">
                <h1>{!! $show->name !!}</h1>
            </div>
        </div>
    </div>
@endsection

@section('content')
  <section class="section-news">
      <div class="container full-new">
          <div class="row">
              <div class="col-xs-12 box-detail">
                  @if (fex('assets/news/', $show->image))
                      <img src="{!! url('/assets/news/'.$show->image) !!}" class="pull-left" />
                  @else
                      <img alt="image" class="pull-left" src="{!! url('images/no-image.jpg') !!}">
                  @endif
                  <p class="text-left">
                      
                  </p>
              </div>
              <div class="col-xs-12 box-detail-text">
                  {!! $show->text !!}
              </div>
              <div class="col-xs-12">
                  <div class="row">
                      @include('frontend::sections.learn-with-us')
                  </div>
              </div>
              <div class="col-xs-12">
                  <h3>ÚLTIMAS NOVIDADES</h3>
              </div>

              @foreach($list as $new)
              <div class="col-sm-6 col-md-4 col-xs-12">
                  <div class="thumbnail thumb-news">
                      @if($new->image && fex('/assets/news/', $new->image))
                          {!! HTML::image('assets/news/'.$new->image, $new->name, ['width' => 1900, 'height' => 564]) !!}
                      @else
                          <img src="{!! url('/images/no-image.jpg') !!}" alt="{!! $new->name !!}">
                      @endif
                      <div class="caption">
                          <h5>{!! $new->name !!}</h5>
                          <p>
                              {!! str_limit(strip_tags($new->text), 255) !!}
                          </p>
                          <hr>
                          <p>
                              <a href="{!! route('novidades.show', $new->slug) !!}" class="link">LEIA MAIS</a>
                          </p>
                      </div>
                  </div>
              </div>
              @endforeach

              <div class="col-xs-12">
                  <a href="{!! route('novidades.index') !!}" class="read-more">CONFIRA TODAS AS NOTÍCIAS</a>
              </div>
          </div>
      </div>
  </section>
@endsection

