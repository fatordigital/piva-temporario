@extends('layouts.front')

@section('top')

    <div class="banner-curso-detail" style="height: auto">

        @if ($show->image && file_exists(public_path('assets/team/'. $show->image)))
            {!! HTML::image('assets/team/'.$show->image, $show->name, ['class' => 'img-news-detail']) !!}
        @else
            <img src="{!! url('/images/news/news-1.jpg') !!}" class="img-news-detail">
        @endif

        <img src="{!! url('/images/bg-curso.png') !!}" class="filter">

        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('front.home') !!}">Página inicial</a>
                </li>
                @if ($show->category)
                    <li><a href="{!! route('cursos.index') !!}?q={!! $show->category->slug !!}">Curso - {!! $show->category->name !!}</a></li>
                @endif
                <li class="active">{!! str_limit($show->name, 50) !!}</li>
            </ol>
        </div>
    </div>

    @include('frontend::sections.user-bar', ['active' => 'home'])

    <section class="section-curso">
        <div class="container full-curso">
            <div class="row">
                <div class="col-xs-12">
                    <br><br><br />
                    <p>
                        {!! $show->course->resume !!}
                    </p>
                    <br><br>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        @foreach($show->TeamOrderDiscipline as $teamOrderDiscipline)
                            <?php $i = 1; ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
                                            {!! $teamOrderDiscipline->Discipline->name !!}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true">
                                    <div class="panel-body">
                                        <p>
                                            {!! $teamOrderDiscipline->description !!}
                                        </p>
                                        <ul class="lista-material">
                                            @foreach(\Modules\Team\Entities\TeamLesson::TeamDiscipline($show->id, $teamOrderDiscipline->discipline_id) as $lesson)
                                                <li>
                                                    @if ($lesson->lesson->content_type_count && ($lesson->lesson->ContentTypeCount->get('videos') || $lesson->lesson->ContentTypeCount->get('files') || $lesson->lesson->ContentTypeCount->get('texts')))
                                                        <a href="{!! route('front.lesson.view', [$show->slug, $teamOrderDiscipline->Discipline->slug, $lesson->lesson->slug]) !!}">
                                                    @else
                                                        <a href="javascript:void(0);">
                                                    @endif
                                                        <p>
                                                            {!! $i !!}. <span class="icon-file-text-o"></span> {!! $lesson->lesson->name !!}

                                                            @if ($lesson->Access)
                                                                <time>
                                                                    <span class="glyphicon glyphicon-play"></span> {!! $lesson->lesson->ContentTypeCount->get('videos') !!} Vídeos
                                                                </time>
                                                                <time>
                                                                    <span class="glyphicon glyphicon-folder-open"></span> {!! $lesson->lesson->ContentTypeCount->get('files') !!} Arquivos
                                                                </time>
                                                                <time>
                                                                    <span class="glyphicon glyphicon-font"></span> {!! $lesson->lesson->ContentTypeCount->get('texts') !!} Textos
                                                                </time>
                                                            @else
                                                                <time>
                                                                    <strong class="text-danger">
                                                                        Conteúdo disponível dia {!! dateToSql($lesson->begin_date, 'Y-m-d', 'd/m/Y') !!}
                                                                    </strong>
                                                                </time>
                                                            @endif
                                                        </p>
                                                    </a>
                                                </li>
                                                <?php $i++ ?>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection