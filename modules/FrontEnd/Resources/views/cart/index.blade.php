@extends('layouts.front')

@section('top')
    <div class="banner-curso">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('front.home') !!}">Página inicial</a>
                </li>
                <li class="active">
                    Carrinho de compras
                </li>
            </ol>
            <div class="col-xs-12">
                <h1>Carrinho de compras</h1>
            </div>
        </div>
    </div>
@endsection

@section('content')

    @if (isset($list) && $list->items()->count())
        <section class="section-cursos pg-curso">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3>1. CONFIRA O CURSO QUE VOCÊ ESTÁ COMPRANDO</h3>
                    </div>

                    @foreach($list->items as $item)
                        <div class="col-xs-12">
                            <div class="box-product">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div class="box-title">
                                        <h4>Curso</h4>
                                    </div>
                                    <div class="box-body">
                                        @if ($item->courses->image_show && file_exists(public_path('assets/team/' . $item->courses->image_show)))
                                            {!! HTML::image('assets/team/' . $item->courses->image_show, $item->courses->name, ['width' => 128]) !!}
                                        @else
                                            <img src="{!! url('/images/cursos/curso-1.jpg') !!}" class="img-main" width="128" alt="Cursos">
                                        @endif
                                        <div class="text-prod">
                                            <h5>{!! $item->courses->name !!}</h5>
                                            <p>
                                                {!! $item->courses->course->resume != '' ? str_limit(strip_tags($item->courses->course->resume), 255) : str_limit(strip_tags($item->courses->course->description), 255) !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="box-title">
                                        <h4>
                                            Valor do Curso
                                            <a href="{!! route('front.remove.cart.item', base64_encode(json_encode(CryptoJSEncrypt($item->id)))) !!}"
                                               class="btn btn-danger btn-xs pull-right">
                                                Remover item
                                            </a>
                                        </h4>
                                    </div>
                                    <div class="box-body">
                                        @if ($item->value_discount && $item->value_discount > 0)
                                            <h3>
                                                <small>R$
                                                </small> {!! bcoToCoin($item->value_discount * $item->quantity) !!}</h3>
                                        @else
                                            <h3>
                                                <small>R$</small> {!! bcoToCoin($item->value * $item->quantity) !!}</h3>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="box-product-2">
                            <div class="box-title">
                                <h4>TOTAL</h4>
                            </div>
                            <div class="box-body">
                                <ul class="list-group">
                                    <li class="list-group-item"><h5>Subtotal</h5>
                                        <span>R$ {!! isset($abandoned_order) ? bcoToCoin($abandoned_order->sub_total) : '0,00' !!}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <h5>Cupom</h5>
                                        <span class="desconto" id="desconto">{!! isset($abandoned_order) ? $abandoned_order->coupon_type : '0,00' !!}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <h3>TOTAL</h3>
                                        <span id="total">R$ {!! isset($abandoned_order) ? $abandoned_order->total_with_discount : '0,00' !!}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="box-product-2">
                            <div class="box-title">
                                <h4>VOCÊ POSSUI UM CUPOM?</h4>
                            </div>
                            <div class="box-body">
                                {!! Form::open(['route' => isset($abandoned_order) && $abandoned_order->coupon != '' ? 'cart.delete_coupon' :'cart.use_coupon', 'class' => 'form-inline', 'id' => 'form-cupom', 'method' => 'POST']) !!}
                                <div class="form-group">
                                    <div class="input-group">
                                        {!! Form::text('coupon', isset($abandoned_order) && $abandoned_order->coupon != '' ? $abandoned_order->coupon : null, ['required', 'class' => 'form-control', 'placeholder' => 'Digite aqui seu cupom']) !!}
                                    </div>
                                </div>
                                @if (isset($abandoned_order) && $abandoned_order->coupon != '')
                                    <button type="submit" class="btn btn-danger" title="Remove CUPOM do carrinho">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </button>
                                @else
                                    <button type="submit" class="btn btn-primary" title="Adicionar CUPOM no carrinho">
                                        <span class="glyphicon glyphicon-repeat"></span>
                                    </button>
                                @endif
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <img src="{!! url('/images/icon-cartoes-2.png') !!}" class="img-responsive center-block">
                    </div>
                    <div class="col-xs-12">
                        <a href="{!! route('carrinhos.finaliza') !!}" class="btn btn-success btn-carrinho btn-next">PRÓXIMA
                            ETAPA »</a>
                    </div>
                </div>
            </div>
        </section>
    @else
        <section class="section-cursos pg-curso">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h5>O Carrinho está vazio. <a href="{!! route('front.home') !!}" class="text-success">Clique
                                aqui e veja nossos cursos em destaque.</a></h5>
                    </div>
                </div>
            </div>
        </section>
    @endif

@endsection
