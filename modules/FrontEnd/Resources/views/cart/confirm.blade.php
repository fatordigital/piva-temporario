@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li>
                <a href="{!! route('front.home') !!}">Página inicial</a>
              </li>
              <li class="active">Pagamento</li>
          </ol>
          <div class="col-xs-12">
              <h1>Confirmação do pagamento</h1>
          </div>
      </div>
  </div>
@endsection

@section('content')

  <section class="section-cursos pg-curso">
      <div class="container">
          <div class="row">
              <div class="col-xs-12 table-responsive">
                  <table class="table box-product">
                      <thead>
                      <tr class="box-title">
                          <th style="width: 65%">Curso</th>
                          <th class="text-right">Valor do Curso</th>
                      </tr>
                      </thead>
                      <tbody>
                      @foreach($order->orderItems as $item)
                          <tr class="box-body">
                              <td>
                                  <p>{!! $item->name !!}</p>
                              </td>
                              <td class="text-right">
                                  @if ($item->sale > 0)
                                      <small class="text-muted">De <del>R$ {!! bcoToCoin($item->value) !!}</del></small>
                                      {{--<p><small>R$</small> {!! bcoToCoin($item->value_discount) !!}</p>--}}
                                  @else
                                      <p><small>R$</small> {!! bcoToCoin($item->value) !!}</p>
                                  @endif
                              </td>
                          </tr>
                      @endforeach
                      </tbody>
                  </table>
              </div><!-- end of .cols -->
          </div><!-- end of.row -->
          <div class="row">
              <div class="col-xs-12 col-sm-6 table-responsive">
                  <table class="table box-product">
                      <thead>
                      <tr class="box-title">
                          <th>Informações</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr class="box-body">
                          <td>
                              <p>
                                  {!! auth()->user()->socialize->district !!}<br />
                                  {!! auth()->user()->socialize->route_number !!}, {!! auth()->user()->socialize->route !!}<br />
                                  {!! auth()->user()->socialize->city !!} - {!! auth()->user()->socialize->state !!}, {!! auth()->user()->socialize->country !!}
                              </p>
                          </td>
                      </tr>
                      </tbody>
                  </table>
              </div><!-- end of .cols -->
              <div class="col-xs-12 col-sm-6 table-responsive">
                  <table class="table box-product">
                      <thead>
                      <tr class="box-title">
                          <th colspan="2">Total</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr class="box-body">
                          <td>
                              Subtotal
                          </td>
                          <td class="text-right">
                              <small>R$</small> {!! bcoToCoin($order->sub_total) !!}</small>
                          </td>
                      </tr>
                      <tr class="box-body">
                          <td>
                              Cupom desconto
                          </td>
                          <td class="text-right">
                              <small>R$</small> {!! $order->coupon != '' ? bcoToCoin($order->discount) : '0,00' !!}</small>
                          </td>
                      </tr>
                      <tr class="box-body">
                          <td>
                              Total
                          </td>
                          <td class="text-right">
                              <small>R$</small> {!! bcoToCoin($order->total) !!}</small>
                          </td>
                      </tr>
                      </tbody>
                  </table>
              </div><!-- end of .cols -->
          </div><!-- end of.row -->
          <div class="row">
              <div class="col-xs-12">
                  @if ($order->pagseguro->code >= 201 && $order->pagseguro->code <= 220)
                      <a target="_blank" href="{!! $order->payment_link !!}" class="btn btn-info pull-right">IMPRIMIR BOLETO</a>
                  @elseif ($order->pagseguro->code >= 301 && $order->pagseguro->code <= 307)
                      <a target="_blank" href="{!! $order->payment_link !!}" class="btn btn-info pull-right">EFETUAR PAGAMENTO</a>
                  @endif
                  <a href="{!! url('/') !!}" class="btn btn-send pull-right">Página inicial</a>
              </div>
          </div>
      </div><!-- end of .container -->
  </section>

@endsection