@extends('layouts.front')


@section('top')
    <div class="banner-curso">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('front.home') !!}">Página inicial</a>
                </li>
                <li>
                    <a href="{!! route('carrinhos.index') !!}">Carrinho</a>
                </li>
                <li class="active">Pagamento</li>
            </ol>
            <div class="col-xs-12">
                <h1>Pagamento</h1>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <style>
        #form-pagamento label {
            width: auto;
        }
    </style>

    <br />

    <div id="cart" amount="{!! $abandoned_order->total_discount !!}">
        <main class="pages info interna">
            <div class="container">


                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table box-product">
                            <thead>
                            <tr class="box-title">
                                <th colspan="2">Curso</th>
                                <th></th>
                                <th width="20%">Valor do Curso</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($abandoned_order->AbandonedOrderItem)
                                @foreach($abandoned_order->AbandonedOrderItem as $item)
                                    <tr class="box-body">
                                        <td>
                                            <figure>
                                                @if (file_exists(public_path('assets/team/' . $item->courses->image_show)))
                                                    <img src="{!! url('assets/team/' . $item->courses->image_show) !!}" class="img-responsive" width="120">
                                                @endif
                                            </figure>
                                        </td>
                                        <td>
                                            <p>
                                                <strong>{!! $item->name !!}</strong><br/>
                                                {!! str_limit($item->courses->course->resume, 255) !!}
                                            </p>
                                        </td>
                                        <td></td>
                                        <td class="box-body">
                                            @if ($item->value_discount > 0)
                                                <small class="text-muted">De <del>R$ {!! bcoToCoin($item->value) !!}</del></small>
                                                <h3 style="margin-top: 5px !important"><small>R$</small> {!! bcoToCoin($item->value_discount) !!}</h3>
                                            @else
                                                <h3><small>R$</small> {!! bcoToCoin($item->value) !!}</h3>
                                            @endif
                                        </td>
                                        <td class="box-options"></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 table-responsive">
                        <table class="table box-product">
                            <thead>
                            <tr>
                                @if ($abandoned_order->coupon)
                                    <td>Cupom</td>
                                    <td>Cupom Valor</td>
                                @endif
                                <td>Sub Total</td>
                                <td>Total</td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                @if ($abandoned_order->coupon)
                                    <td>{!! $abandoned_order->coupon !!}</td>
                                    <td>{!! $abandoned_order->coupon_type !!}</td>
                                @endif
                                <td>R$ {!! bcoToCoin($abandoned_order->sub_total) !!}</td>
                                <td>R$ {!! bcoToCoin($abandoned_order->total_discount) !!}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        @if ($errors->count())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    <p class="text-white" style="color: #FFF;">{!! $error !!}</p>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>

                <form action="{!! route('cart.checkout_store') !!}" method="post" id="form-pagamento" v-show="payment_wait_ready == 1">
                    {!! Form::hidden('pagseguro_hash', null, ['v-model' => 'pagseguro_hash']) !!}
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <div class="bg-form">
                            <p class="text-center text-muted"><strong>ESCOLHA A FORMA DE PAGAMENTO</strong></p>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="method" v-on:click="set_option('CREDIT_CARD')"
                                           value="CREDIT_CARD"
                                           v-bind:checked="current_method == 'CREDIT_CARD' ? true : false"/>
                                    <img src="{!! url('/assets/images/icons/card-1.jpg') !!}"
                                         class="img-responsive center-block">
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="method" v-on:click="set_option('ONLINE_DEBIT')"
                                           value="ONLINE_DEBIT"
                                           v-bind:checked="current_method == 'ONLINE_DEBIT' ? true : false"/>
                                    <img src="{!! url('/assets/images/icons/card-2.jpg') !!}"
                                         class="img-responsive center-block">
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                <input type="radio" name="method" v-on:click="set_option('BOLETO')" value="BOLETO"
                                v-bind:checked="current_method == 'BOLETO' ? true : false"/>
                                <img src="{!! url('/assets/images/icons/card-3.jpg') !!}" class="img-responsive center-block">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-9">
                        <div class="bg-form">
                            <div class="radio radio-inline" v-for="(k, o) in method_options">
                                <label for="">
                                    <input type="radio" name="method_option" v-on:click="select_brand(o.name.toLowerCase())" v-bind:value="o.name" v-bind:checked="brand == o.name.toLowerCase() ? true : false" />
                                    <div class="text-center">
                                        <img v-bind:src="image_url + o.images.SMALL.path" v-bind:alt="o.displayName" v-bind:title="o.displayName" />
                                    </div>
                                </label>
                            </div>
                            <br /><br />
                            <div class="col-xs-12" v-if="current_method == 'CREDIT_CARD'">
                                {!! Form::hidden('card_token', null, ['v-model' => 'card_token']) !!}
                                <div class="row">
                                    <div class="my-line">
                                        <label for="">
                                            Nome <strong class="text-danger">*</strong> (Impresso no cartão)
                                        </label>
                                        {!! Form::text('card_name', null, ['class' => 'form-control', 'required', 'v-model' => 'card_name']) !!}
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="my-line">
                                                <label for="">
                                                    CPF <strong class="text-danger">*</strong> (Títular do cartão)
                                                </label>
                                                {!! Form::text('card_national_id', null, ['class' => 'form-control cpf-mask', 'required', 'v-model' => 'card_cpf']) !!}
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="my-line">
                                                <label for="">
                                                    Data de Nascimento <strong class="text-danger">*</strong> (Títular do Cartão)
                                                </label>
                                                {!! Form::text('card_birth_date', null, ['class' => 'form-control date-mask', 'required', 'v-model' => 'card_birth']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="">
                                                Número do cartão <strong class="text-danger">*</strong> (Impresso no cartão)
                                            </label>
                                            {!! Form::text('card_number', null, ['class' => 'form-control', 'v-model' => 'card_number', 'required']) !!}
                                        </div>
                                        <div class="col-xs-3">
                                            <label for="">
                                                Mês <strong class="text-danger">*</strong> (Validade)
                                            </label>
                                            {!! Form::select('card_month', [
                                            '01' => '1',
                                            '02' => '2',
                                            '03' => '3',
                                            '04' => '4',
                                            '05' => '5',
                                            '06' => '6',
                                            '07' => '7',
                                            '08' => '8',
                                            '09' => '9',
                                            '10' => '10',
                                            '11' => '11',
                                            '12' => '12'
                                            ], '01', ['class' => 'form-control month-number', 'required', 'v-model' => 'card_month']) !!}
                                        </div>
                                        <div class="col-xs-3">
                                            <label for="">
                                                Ano <strong class="text-danger">*</strong> (Validade)
                                            </label>
                                            {!! Form::select('card_year', $years, null, ['class' => 'form-control year-number', 'required', 'v-model' => 'card_year']) !!}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label for="">
                                                Parcelas <strong class="text-danger">*</strong>
                                            </label>
                                            <select name="plots" class="form-control" required v-model="card_plots">
                                                <option value="">Selecione o parcelamento*</option>
                                                <option v-for="(index, key) in plots" v-bind:value="key.quantity + '#' + key.installmentAmount + '#' + key.totalAmount + '#' + key.interestFree">
                                                    @{{ key.quantity == 1 ? 'À vista de ' + moeda(key.totalAmount) : key.quantity + 'x de ' + moeda(key.installmentAmount) }} @{{ key.quantity > 1 ? (key.interestFree == false ? 'com juros' : 'sem juros') : '' }}
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-xs-3">
                                            <label for="">
                                                Código de Segurança
                                            </label>
                                            {!! Form::text('card_security', null, ['class' => 'form-control', 'v-model' => 'card_cvv', 'v-bind:maxlength' => 'cvvSize', 'required']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12" v-if="current_method == 'BOLETO'">
                                <div class="my-line">
                                    <strong>Após finalizar o pedido, será exibido na tela de confirmação o link para o boleto.</strong>
                                </div>
                                <br /><br />
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <span>Esta compra esta sendo feita no </span>
                        <strong>Brasil</strong>
                        {!! HTML::image('images/1483998903_Brazil_flat.png', 'Compra feita no Brasil', ['width' => 22]) !!}
                    </div>
                    <div class="col-xs-12">
                        <button type="button" v-on:click="submit($event)" class="btn btn-info pull-right">
                            @{{ wait == false ? 'FINALIZAR PEDIDO »' : 'Processando...' }}
                        </button>
                    </div>
                </form>

                <div class="text-center" v-if="payment_wait_ready == 0">
                    {!! HTML::image('/images/carregando.gif') !!}
                </div>

            </div>
        </main>
    </div>
    <br /><br />
@endsection

@section('js')
    {!! HTML::script('js/components/cart/index.js?time='.date('dmyhisu')) !!}
@endsection