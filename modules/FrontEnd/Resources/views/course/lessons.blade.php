@if ($lessons)
    <section class="section-cursos pg-curso">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <br><br>
                    <p>
                        {!! $show->course->resume !!}
                    </p>
                    <br><br>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        @foreach($disciplines as $discipline)
                        <div class="panel panel-default" style="border: none;">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{!! $discipline->slug !!}" aria-expanded="true" aria-controls="collapseOne">
                                        {!! $discipline->name !!} ({!! $discipline->Lessons->count() !!}) <span class="pull-right">EXPANDIR O CONTEÚDO DESSE MÓDULO</span>
                                    </a>
                                </h4>
                            </div>
                            <div id="{!! $discipline->slug !!}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="{!! $discipline->slug !!}">
                                <div class="panel-body">
                                    <p>
                                        {!! $discipline->description !!}
                                    </p>
                                    <ul class="lista-material">
                                        @foreach(\Modules\Team\Entities\TeamLesson::TeamDiscipline(session('current_team'), $discipline->id) as $index => $lesson)
                                        <?php $access = valideLessonDate($show->id, $lesson->lesson->id); ?>
                                        <li>
                                            @if ($access === true)
                                                @if ($lesson->lesson->content_type_count && ($lesson->lesson->content_type_count['videos'] || $lesson->lesson->content_type_count['files'] || $lesson->lesson->content_type_count['texts']))
                                                    <a href="{!! route('front.lesson.view', [$show->slug, $discipline->slug, $lesson->lesson->slug]) !!}">
                                                @else
                                                    <a href="javascript:void(0);">
                                                @endif
                                            @endif
                                                <p>
                                                    {!! $index + 1 !!}. <span class="icon-file-text-o"></span> {!! $lesson->lesson->name !!}
                                                    @if ($access === true)
                                                        @if ($lesson->lesson->content_type_count && ($lesson->lesson->content_type_count['videos'] || $lesson->lesson->content_type_count['files'] || $lesson->lesson->content_type_count['texts']))
                                                            <time>
                                                                <span class="glyphicon glyphicon-play"></span> {!! $lesson->lesson->content_type_count ?  $lesson->lesson->content_type_count['videos'] : 0 !!} Vídeos
                                                            </time>
                                                            <time>
                                                                <span class="glyphicon glyphicon-folder-open"></span> {!! $lesson->lesson->content_type_count ?  $lesson->lesson->content_type_count['files'] : 0 !!} Arquivos
                                                            </time>
                                                            <time>
                                                                <span class="glyphicon glyphicon-font"></span> {!! $lesson->lesson->content_type_count ?  $lesson->lesson->content_type_count['texts'] : 0 !!} Textos
                                                            </time>
                                                        @else
                                                            <time>
                                                                <strong class="text-danger">
                                                                    Não disponível no momento
                                                                </strong>
                                                            </time>
                                                        @endif
                                                    @else
                                                        @if ($access)
                                                        <time>
                                                            <strong class="text-danger">
                                                                Conteúdo disponível dia {!! dateToSql($access->begin_date, 'Y-m-d', 'd/m/Y') !!}
                                                            </strong>
                                                        </time>
                                                        @endif
                                                    @endif
                                                </p>
                                            @if ($access === true)
                                            </a>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif

