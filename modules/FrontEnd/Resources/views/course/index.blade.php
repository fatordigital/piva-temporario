@extends('layouts.front')

@section('top')
  <div class="banner-curso">
      <div class="container">
          <ol class="breadcrumb">
              <li><a href="{!! route('front.home') !!}">Página inicial</a></li>
              <li class="active">Cursos {!! $category->name !!}</li>
          </ol>
          <div class="col-xs-12">
              <h1>Cursos {!! $category->name !!}</h1>
          </div>
          <form action="{!! request()->url() !!}" method="get" class="form-inline">
              {!! Form::hidden('q', $category->slug) !!}
              <div class="form-group">
                  <div class="input-group">
                      {!! Form::text('search', old('search'), ['class' => 'form-control', 'maxlength' => 60, 'placeholder' => 'Buscar']) !!}
                  </div>
              </div>
              <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
          </form>
      </div>
  </div>
@endsection

@section('content')
  <section class="section-cursos pg-curso">
    <div class="container">
        <div class="row">
          @if (isset($courses) && $courses->count())
            @include('frontend::sections.courses', ['content' => true])
          @else
            <div class="col-lg-12">
              <h3>:(</h3>
              <p>Nenum cadastro efetuado no momento, em breve mais cursos e conteúdos para você.</p>
            </div>
          @endif
        </div>
    </div>
  </section>
@endsection