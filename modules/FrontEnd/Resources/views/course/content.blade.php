@extends('layouts.front')

@section('top')
    <div class="banner-curso-detail"{!! (in_array($show->id, $user_team_id)) ? ' style="height: auto;"' : ''!!}>
        @if ($show->image && file_exists(public_path('assets/team/'. $show->image)))
            {!! HTML::image('assets/team/'. $show->image, $show->name, ['width' => 1903, 'height' => 564, 'class' => 'img-news-detail']) !!}
        @else
            <img src="{!! url('/images/news/news-1.jpg') !!}" class="img-news-detail">
        @endif

        <img src="{!! url('/images/bg-curso.png') !!}" class="filter">
        <div class="container">
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('front.home') !!}">Página inicial</a>
                </li>
                @if ($show->category)
                    <li><a href="{!! route('cursos.index') !!}?q={!! $show->category->slug !!}">Curso
                            - {!! $show->category->name !!}</a></li>
                @endif
                <li class="active">{!! str_limit($show->name, 50) !!}</li>
            </ol>
            <div class="col-xs-12">
                <h1>{!! $show->name !!}</h1>
                @if (!in_array($show->id, $user_team_id))
                    <p>
                        {!! $show->course->resume ? str_limit(strip_tags($show->course->resume), 300) : str_limit(strip_tags($show->description), 300) !!}
                    </p>
                @endif
                @if (!in_array($show->id, $user_team_id))
                    @if ($show->value == 0 && $show->value_discount == 0)
                        <a href="javascript:void(0);" class="btn btn-default btn-curso"><br>CURSO GRÁTIS</a>
                    @else
                        @if ($show->value_discount > 0)
                            <a href="javascript:void(0);" class="btn btn-default btn-curso">
                                <small>de
                                    <del>R$ {!! bcoToCoin($show->value) !!}</del>
                                </small>
                                <br>
                                <small>por</small>
                                R$ {!! bcoToCoin($show->value_discount) !!}</a>
                        @else
                            <a href="javascript:void(0);" class="btn btn-default btn-curso">
                                <small>à vista</small>
                                <br> R$ {!! bcoToCoin($show->value) !!}</a>
                        @endif
                    @endif

                    @if ($show->value > 0 || $show->value_discount > 0)
                        <a href="javascript:void(0);" class="btn btn-default btn-curso">
                            <small>até</small>
                            <br/> {!! s('Parcelas') !!}x sem juros</a>
                    @else
                        <a href="javascript:void(0);" style="opacity: 0;" class="btn btn-default btn-curso"><br>-</a>
                    @endif
                    {!! Form::open(['route' => 'carrinhos.store', 'id' => 'buyForm']) !!}
                    {!! Form::hidden('course',  base64_encode(json_encode(CryptoJSEncrypt($show->id)))) !!}
                    <button type="submit" id="sendContact" class="btn btn-success btn-curso">
                        @if ($show->value == 0 && $show->value_discount == 0)
                            @if (auth()->guest())
                                <a href="{!! route('front.login') !!}" style="color: #FFF;">FAÇA SEU LOGIN ANTES</a>
                            @else
                                CLIQUE PARA FAZER O CURSO
                            @endif
                        @else
                            COMPRAR
                        @endif
                    </button>
                    {!! Form::close() !!}
                @endif
            </div>
        </div>

    </div>
    @if (in_array($show->id, $user_team_id))
        @include('frontend::sections.user-bar', ['active' => 'home'])
    @endif
@endsection

@section('content')
    <section class="section-cursos pg-curso" id="target-course">
        <div class="container">
            <div class="row" style="min-height: 460px !important">
                <div class="col-lg-9">
                    <a href="{!! route('cursos.show', $show->slug) !!}" class="pull-left"><< Voltar para o curso</a>
                    <?php $next = false; ?>
                    @foreach($contents->contents as $content)
                        @if ($next == true)
                            <a class="pull-right" href="{!! route('front.lesson.view', [$show->slug, $lesson->Discipline->slug, $lesson->slug,$content->contents ? $content->contents->slug : $content->tests->slug]) !!}">Próxima aula >></a>
                            <?php $next = false; ?>
                        @endif
                        @if ($content->contents && $principal_content->id == ($content->contents ? $content->contents->id : $content->tests->id))
                            <?php $next = true; ?>
                        @endif
                    @endforeach
                </div>
                <div class="clearfix"></div>
                @if ($principal_content)
                    <div class="col-lg-9 effect-vertical-line-yellow video-style{!! !$principal_content->video ?:' ' !!}"{!! $principal_content->text ? ' style="max-height: 357px; overflow-y: scroll"' : ' style="max-height: 357px;"' !!}>
                        @if ($principal_content->video)
                            @if ($principal_content->video_status)
                                @if (!strstr($principal_content->video_link, 'player'))
                                    <iframe width="102%" height="430" webkitallowfullscreen mozallowfullscreen
                                            allowfullscreen src="{!! str_replace('https://vimeo.com/', '//player.vimeo.com/video/', $principal_content->video_link) !!}"
                                            frameborder="0"></iframe>
                                @else
                                    <iframe width="102%" height="430" webkitallowfullscreen mozallowfullscreen
                                        allowfullscreen src="{!! $principal_content->video_link !!}"
                                        frameborder="0"></iframe>
                                @endif
                            @else
                                <h3 class="text-center" style="color: #FFF;">Você já
                                    visualizou {!! $show->video_repeat !!} {!! $show->video_repeat > 1 ? 'vezes' : 'vez' !!}</h3>
                                <p class="text-center text-white">O máximo de visualizações por vídeo
                                    é {!! $show->video_repeat !!} {!! $show->video_repeat > 1 ? 'vezes' : 'vez' !!}</p>
                            @endif
                        @elseif ($principal_content->file)
                            <a target="_blank" href="{!! url('/assets/contents/'.$principal_content->content) !!}" class="text-center">
                                <img height="357" src="{!! url('/img/download-icon.png') !!}" alt=""
                                     class="center-block">
                                <h3 style="color: #283e4e !important">CLIQUE AQUI PARA BAIXAR</h3>
                            </a>
                        @elseif ($principal_content->text)
                            {!! $principal_content->content !!}
                        @elseif ($principal_content->jump_question == 0 || $principal_content->jump_question == 1)
                            <a href="{!! route('access.simulate', $principal_content->slug) !!}" class="text-center">
                                <img height="400" src="{!! url('/img/prova-da-ordem-gerar-prova-etapa-02.svg') !!}" alt=""
                                     class="center-block">
                                <h3 style="color: #283e4e !important">CLIQUE AQUI PARA FAZER O SIMULADO</h3>
                            </a>
                        @endif
                    </div>
                    <div class="col-lg-3 bg-lesson">
                        <div class="row">
                            <div class="col-lg-12 effect-horizontal-lesson">
                                <h5 class="text-center">AULAS DISPONÍVEIS</h5>
                            </div>
                            <div class="col-lg-12 scroll-lesson">
                                <?php $next = false; ?>
                                @foreach($contents->contents as $content)
                                    @if ($content->contents || $content->tests)
                                        <div class="row lesson-template effect-horizontal-lesson{!! $principal_content->id == ($content->contents ? $content->contents->id : $content->tests->id) ? ' principal-box' : '' !!}">
                                            <a {!! $next == true && $content->contents && $content->contents->video ? 'id="next"' : '' !!} href="{!! route('front.lesson.view', [$show->slug, $lesson->Discipline->slug, $lesson->slug,$content->contents ? $content->contents->slug : $content->tests->slug]) !!}">
                                                <div class="col-lg-2 yellow-font-icon">
                                                    <i class="glyphicon glyphicon-play"></i>
                                                </div>
                                                <div class="col-lg-10 box-lesson">
                                                    <span class="size">
                                                        {!! $content->contents ? $content->contents->name : $content->tests->name !!}
                                                    </span>
                                                    <span class="help-block legend">
                                                        {!! substr(strip_tags($content->contents ? $content->contents->description : $content->tests->subject), 0, 50) !!}
                                                    </span>
                                                </div>
                                            </a>
                                        </div>
                                        @if ($next == true)
                                            <?php $next = false; ?>
                                        @endif
                                        @if ($content->contents && $principal_content->id == $content->contents->id)
                                            <?php $next = true; ?>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row lesson-description">
                <div class="col-lg-12">
                    {!! $principal_content->description !!}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h5 class="pull-left">Avalie essa aula:</h5>
                    <div class="pull-left" style="margin-top: 5px;">
                    <span class="star-cb-group">
                      @for($i = 5 ; $i >= 1; $i--)
                        <input type="radio" id="rating-{!! $i !!}" name="rating" {!! $lesson->rating == $i ? 'checked' : '' !!} value="{!! $i !!}"/><label for="rating-{!! $i !!}">{!! $i !!}</label>
                      @endfor
                      <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear"/><label for="rating-0">0</label>
                    </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    {!! HTML::script('https://player.vimeo.com/api/player.js') !!}
    <script type="text/javascript">
        var CURRENT_LESSON = '{!! $lesson->id !!}';
    </script>
    @if ($principal_content->video)
        <script type="text/javascript">
            $(document).ready(function(){
                if ($(document).find('iframe').length) {
                    var iframe = $("iframe")[0];
                    var player = new Vimeo.Player(iframe);
                    player.on('ended', function (data) {
                        $.post('{!! route('api.post.update-status-content') !!}', {lesson_id: '{!! $lesson->id !!}', team_id: '{!! $show->id !!}', content_id: '{!! $principal_content->id !!}', status: true});
                        if ($(document).find("#next").length) {
                            swal({
                                title: "Deseja ir para o próximo vídeo?",
                                type: "success",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Sim",
                                closeOnConfirm: false
                            }, function () {
                                window.location.href = $(document).find('#next').attr('href');
                            });
                        }
                    })
                }
            });
        </script>
    @endif
    <script type="text/javascript">
        $(document).ready(function () {
            $('html, body').animate({
                scrollTop: $("#target-course").offset().top
            }, 200);
        })
    </script>
@endsection