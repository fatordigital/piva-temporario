@extends('layouts.front')

<?php $videos = getContent($show->lessons ? $show->lessons->lists('id')->toArray() : [], 'video')->count(); ?>
<?php $tests = getTest($show->lessons ? $show->lessons->lists('id')->toArray() : [])->count(); ?>

@section('top')
  <div class="banner-curso-detail">
      @if ($show->image && file_exists(public_path('assets/team/'. $show->image)))
        {!! HTML::image('assets/team/'.$show->image, $show->name, ['class' => 'img-news-detail']) !!}
      @else
        <img src="{!! url('/images/news/news-1.jpg') !!}" class="img-news-detail">
      @endif

      <img src="{!! url('/images/bg-curso.png') !!}" class="filter">
      <div class="container">


          <ol class="breadcrumb">
              <li>
                <a href="{!! route('front.home') !!}">Página inicial</a>
              </li>
              @if ($show->category)
                <li><a href="{!! route('cursos.index') !!}?q={!! $show->category->slug !!}">Curso - {!! $show->category->name !!}</a></li>
              @endif
              <li class="active">{!! str_limit($show->name, 50) !!}</li>
          </ol>


          <div class="col-xs-12">
              <h1>{!! $show->name !!}</h1>
              @if (!in_array($show->id, $user_team_id))
                <p>
                    {!! $show->course->resume ? str_limit(strip_tags($show->course->resume), 300) : str_limit(strip_tags($show->description), 300) !!}
                </p>
              @endif
              @if (!in_array($show->id, $user_team_id))
                @if ($show->value == 0 && $show->value_discount == 0)
                  <a href="javascript:void(0);" class="btn btn-default btn-curso"><br>CURSO GRÁTIS</a>
                @else
                    @if ($show->value_discount > 0)
                      <a href="javascript:void(0);" class="btn btn-default btn-curso"><small>de <del>R$ {!! bcoToCoin($show->value) !!}</del></small><br><small>por</small> R$ {!! bcoToCoin($show->value_discount) !!}</a>
                    @else
                      <a href="javascript:void(0);" class="btn btn-default btn-curso"><small>à vista</small><br> R$ {!! bcoToCoin($show->value) !!}</a>
                    @endif
                @endif

                @if ($show->value > 0 || $show->value_discount > 0)
                    <a href="javascript:void(0);" class="btn btn-default btn-curso"><small>até</small> <br /> {!! s('Parcelas') !!}x sem juros</a>
                @else
                    <a href="javascript:void(0);" style="opacity: 0;" class="btn btn-default btn-curso"><br>-</a>
                @endif

                @if ($show->value == 0 && $show->value_discount == 0)
                    {!! Form::open(['route' => ['front.curso-gratis', $show->slug]]) !!}
                        @if (auth()->guest())
                            <a href="{!! route('front.login') !!}?redirect={!! base64_encode(request()->fullUrl()) !!}" class="btn btn-success btn-curso" style="color: #FFF; float: right; margin-top: -60px;">FAÇA SEU LOGIN ANTES</a>
                        @else
                            <button class="btn btn-success btn-curso" type="submit">
                                CLIQUE PARA FAZER O CURSO
                            </button>
                        @endif
                    {!! Form::close() !!}
                @else
                    {!! Form::open(['route' => 'carrinhos.store', 'id' => 'buyForm']) !!}
                      {!! Form::hidden('course',  base64_encode(json_encode(CryptoJSEncrypt($show->id)))) !!}
                      <button type="submit" id="sendContact" class="btn btn-success btn-curso">
                          COMPRAR
                      </button>
                    {!! Form::close() !!}
                @endif
              @endif
          </div>
      </div>

  </div>
@endsection

@if (!in_array($show->id, $user_team_id))
  @section('js')
      @if (isset($alert_end_team))
          <script type="text/javascript">
              $(document).ready(function(){
                  swal('Aviso', '{!! $alert_end_team !!}', 'info');
              })
          </script>
      @endif
    {!! HTML::script('/js/professorpiva/cart/cart-default.js') !!}
  @endsection
@endif

@section('content')
    <section class="section-curso">
      <div class="container full-curso">
          <div class="row">
              <div>
                  <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#apresentacao" aria-controls="apresentacao" role="tab" data-toggle="tab"><span class="icon-stats"></span> APRESENTAÇÃO</a></li>
                      @if ($show->course->structure)
                        <li role="presentation"><a href="#estrutura" aria-controls="estrutura" role="tab" data-toggle="tab"><span class="icon-file-text2"></span> ESTRUTURA CURRICULAR</a></li>
                      @endif
                      @if ($show->team_teachers->count())
                        <li role="presentation"><a href="#professores" aria-controls="professores" role="tab" data-toggle="tab"><span class="icon-profile"></span> PROFESSORES</a></li>
                      @endif
                      @if (strlen($show->course->contest) > 11 && $show->category->id != '4')
                        <li role="presentation"><a href="#concurso" aria-controls="concurso" role="tab" data-toggle="tab"><span class="icon-briefcase"></span> O CONCURSO</a></li>
                      @endif
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                      <div role="tabpanel" class="tab-pane active" id="apresentacao">
                          <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                              <h5>{!! $show->course->name !!}</h5>

                              {!! $show->course->resume !!}

                              <br />

                              <p>
                                  {!! $show->course->description !!}
                              </p>
                          </div>
                          @if ($show->about)
                            <?php $abouts = json_decode($show->about); ?>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                <h5>Sobre o Curso</h5>
                                <ul class="list-group">
                                  @foreach($abouts as $about)
                                    <li class="list-group-item">{!! $about->icon ? '<span class="'.$about->icon.'"></span>' : '' !!} {!! $about->title !!}<strong class="pull-right">{!! $about->info !!}</strong></li>
                                  @endforeach
                                </ul>
                                @if (in_array($show->id, $user_team_id))
                                    <a href="{!! route('estudar.index', $show->slug) !!}" class="btn btn-primary center-block">ESTUDAR</a>
                                @endif
                            </div>
                          @endif
                      </div>
                      @if ($show->course->structure)
                        <div role="tabpanel" class="tab-pane" id="estrutura">
                          <div class="col-lg-12">
                            {!! $show->course->structure !!}
                          </div>
                        </div>
                      @endif
                      @if ($show->team_teachers->count())
                        <div role="tabpanel" class="tab-pane" id="professores">
                          <div class="col-xs-12">
                            @foreach($show->team_teachers as $teacher)
                              @if ($teacher->user)
                                <div class="col-sm-6 col-md-3 col-xs-12">
                                    <a href="{!! route('teacher.detail', $teacher->user->slug) !!}">
                                        <div class="thumbnail thumb-cursos">
                                            @if ($teacher->user->filename)
                                              <img src="{!! url('/assets/users/'. $teacher->user->filename) !!}" class="img-main" alt="{!! $teacher->user->name !!}" style="height: 300px;" />
                                            @else
                                            @endif
                                            <div class="caption">
                                                <h5>{!! $teacher->user->name !!}</h5>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                              @endif
                            @endforeach
                          </div>
                        </div>
                      @endif
                      @if ($show->course->contest)
                        <div role="tabpanel" class="tab-pane" id="concurso">
                          <div class="col-lg-12">
                            {!! $show->course->contest !!}
                          </div>
                        </div>
                      @endif
                  </div>
              </div>

              <div class="col-xs-12">
                  <hr><br>
                  @include('frontend::sections.learn-with-us')
              </div>
              @if (isset($interest_courses) && $interest_courses->count())
                @include('frontend::sections.interest-courses', ['content' => true, 'interest_courses' => $interest_courses])
              @endif
          </div>
      </div>
    </section>
@endsection