@extends('layouts.front')

@section('top')
  <div class="container-fluid">
      <div class="row">
        <div id="banner-principal" class="carousel slide" data-ride="carousel">
              <ol class="carousel-indicators">
                  <?php $i = 0; ?>
                  @foreach ($banners as $banner)
                    <li data-target="#carousel-example-generic" data-slide-to="0"{!! $i == 0 ? ' class="active"' : '' !!}></li>
                    <?php $i++; ?>
                  @endforeach
                  <?php if (isset($teste)) {
                          echo '<rererere>';
                          echo '<teste de tempo>';
                      } ?>
              </ol>
              <div class="carousel-inner" role="listbox">
                  <?php $i = 0; ?>
                  @foreach ($banners as $banner)
                      <div class="item{!! $i == 0 ? ' active' : '' !!}">
                          {!! HTML::image('assets/team/' . $banner->image, $banner->name) !!}
                          <div class="col-lg-5 col-md-6 col-sm-7 col-xs-12 pull-right">
                              <div class="title-banner">
                                  <h2>{!! $banner->course->name !!}</h2>
                              </div>
                              <div class="price-banner">
                                  <div class="meio">
                                      @if ($banner->value > 0 || $banner->value_discount > 0)
                                          @if ($banner->category && $banner->category->name == 'Presencial')
                                              <p style="font-size: 16px; margin-top: 21px;" title="{!! $banner->category ? $banner->category->name : '' !!}">{!! $banner->category ? $banner->category->name : '' !!}</p>
                                          @else
                                              <p style="font-size: 23px" title="{!! $banner->category ? $banner->category->name : '' !!}">{!! $banner->category ? $banner->category->name : '' !!}</p>
                                          @endif
                                      @else
                                          <p style="font-size: 28px">Grátis</p>
                                      @endif
                                  </div>
                                  @if ($banner->value > 0 || $banner->value_discount > 0)
                                      <div class="meio">
                                          @if ($banner->value_discount && $banner->value_discount > 0)
                                            <?php $valor_parcela = number_format($banner->value_discount / s('Parcelas'), 2); ?>
                                            <h2 style="font-size: 42px">
                                                <small>{!! s('Parcelas') !!}x de</small>
                                                <small style="font-size: 16px;">R$</small>{!! substr($valor_parcela, 0, strlen($valor_parcela) - 3) !!}<small>,{!! getDecimal($valor_parcela) !!}</small>
                                                {{--<small>sem juros</small>--}}
                                            </h2>
                                          @else
                                            <?php $valor_parcela = number_format($banner->value / s('Parcelas'), 2); ?>
                                            <h2 style="font-size: 42px">
                                                <small>{!! s('Parcelas') !!}x de</small>
                                                <small style="font-size: 16px;">R$</small>{!! substr($valor_parcela, 0, strlen($valor_parcela) - 3) !!}
                                                <small>,{!! getDecimal($valor_parcela) !!}</small>
                                                {{--<small>sem juros</small>--}}
                                            </h2>
                                          @endif
                                      </div>
                                  @endif
                              </div>
                              <a href="{!! route('cursos.show', $banner->slug) !!}" class="btn btn-warning">
                                  {!! isset($user_team_id) && in_array($banner->id, $user_team_id) ? 'Estudar' : 'Saiba mais' !!}
                              </a>
                          </div>
                      </div>
                      <?php $i++; ?>
                  @endforeach
                  @if (isset($banners) && $banners->count() > 1)
                  <a class="left carousel-control" href="#banner-principal" role="button" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#banner-principal" role="button" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                  </a>
                  @endif
              </div>
          </div>
      </div>
  </div>
@endsection


@section('content')

  @include('frontend::sections.courses', ['home' => true])

  <section class="section-curso-free">
      <div class="container">
          <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <h2>CONHEÇA NOSSOS </h2>
                  <h3>
                      ARQUIVOS, CURSOS E <br>
                      SIMULADOS <strong>GRÁTIS </strong>
                   </h3>
                   <p>
                       Ao acessar a área cursos gratuitos, simulados, arquivos gratuitos e realizar o cadastro, você terá acesso a dezenas de materiais básicos de alto nível para sua preparação.
                       São videoaulas, simulados e apostilas constantemente atualizados e revisados, que servirão de apoio para o início dos seus estudos.
                   </p>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  {!! Form::open(['route' => 'front.fast-register', 'method' => 'POST', 'id' => 'formc', 'class' => 'form-curso-free']) !!}
                      <div class="ball">
                          <h5>Crie sua conta <br /> E tenha acesso aos nossos cursos grátis</h5>
                      </div>
                      <div class="form-group">
                          {!! Form::text('name', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Nome (*)']) !!}
                      </div>
                      <div class="form-group">
                          {!! Form::text('email', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'E-mail (*)']) !!}
                      </div>
                      <div class="form-group div-meia">
                          {!! Form::password('password', ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Senha (*)']) !!}
                      </div>
                      <div class="form-group div-meia">
                          {!! Form::password('r_password', ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Repetir Senha (*)']) !!}
                      </div>
                      <div class="clearfix"></div>
                      <div class="form-group">
                        <button type="submit" id="sendRegister" class="btn btn-warning pull-left">Criar Registro</button>
                      </div>
                  {!! Form::close() !!}
              </div>
          </div>
      </div>
  </section>

  <section class="section-news">
      <div class="container">
          <div class="row">
            <div class="col-xs-12">
                <h3>ÚLTIMAS NOVIDADES</h3>
            </div>
            @include('frontend::sections.news')
            <div class="col-xs-12">
                <a href="{!! route('novidades.index') !!}" class="read-more">CONFIRA TODAS AS NOTÍCIAS</a>
            </div>
          </div>
      </div>
  </section>

  <section class="section-teacher">
      <div class="bar-blue">
          <div class="container">
              <div class="row">
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                      <img src="{!! url('/images/professores/professor-1.png') !!}" class="img-responsive">
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                      <h3>Os melhores professores reunidos em um único local.</h3>
                      <p>
                          A proposta do professor Professor Piva sempre foi a de reunir cursos preparatórios de alta qualidade
                          com os melhores professores do mercado. E eles estão aqui.
                      </p>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                      <a href="{!! route('front.teachers') !!}" class="btn btn-warning">CONHEÇA TODOS OS PROFESSORES</a>
                  </div>
              </div>
          </div>
      </div>
  </section>

@endsection

@section('js')
  {!! HTML::script('js/professorpiva/home/home.js') !!}
@endsection