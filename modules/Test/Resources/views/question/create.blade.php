@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Nova Questão</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li>
                <a href="{!! route('fatorcms.question.index') !!}">Questões</a>
            </li>
            <li class="active">
                <strong>Nova Questão</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <p>
        Todos os campos com <strong class="text-danger">(*)</strong> são obrigatórios.
      </p>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.question.index') !!}"
           class="btn btn-warning">
            Voltar
        </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="ibox float-e-margins">
    <div class="ibox-content">
      {!! Form::open(['route' => 'fatorcms.question.store', 'files' => true, 'id' => 'form']) !!}
        @include('test::question.form')
      {!! Form::close() !!}
    </div>
  </div>
@endsection
