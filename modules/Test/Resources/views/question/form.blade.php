<div class="row">

  <div class="col-lg-6">

    <div class="form-group {!! ($errors->first('discipline_id')) ? 'has-error' : '' !!}">
      <label for="examining_board">
          Banca <strong class="text-success">(Opcional)</strong>
      </label>
      {!! Form::select('examining_board', ['' => 'Selecione uma banca'] + \Modules\Test\Entities\ExaminingBoard::lists('name', 'id')->toArray(), null, ['style' => 'display: none;', 'class' => 'tag-autocomplete']) !!}
      {!! $errors->first('examining_board', '<span class="help-block">:message</span>') !!}
    </div>

    <div class="form-group{!! $errors->first('difficulty') ? ' has-error' : '' !!}">
      <label for="">
         Dificuldade <strong class="text-danger">(*)</strong>
      </label>
      {!! Form::select('difficulty', [1 => 'Fácil', 2 => 'Média', 3 => 'Difícil'], null, ['class' => 'form-control load-select2']) !!}
      {!! $errors->first('difficulty', '<span class="help-block text-danger">:message</span>') !!}
    </div>

  </div>

  <div class="col-lg-6">
    <div class="form-group">
      <p>
        <label for="status">
          Questão terá recurso?<strong class="text-danger">(*)</strong>
        </label>
      </p>
      <div class="radio radio-success radio-inline">
          {!! Form::radio('resource', 1, false, ['id' => 'on']) !!}
        <label for="on">
              Sim
          </label>
      </div>
      <div class="radio radio-danger radio-inline">
          {!! Form::radio('resource', 0, true, ['id' => 'off']) !!}
        <label for="off">
              Não
          </label>
      </div>
    </div>
  </div>

  <div class="col-lg-12">
    <div class="form-group">
      <p>
        <label for="status">
          Justificativa <strong class="text-success">(Opcional)</strong>
        </label>
      </p>
      {!! Form::textarea('justify', null, ['class' => 'form-control summernote']) !!}
    </div>
  </div>

</div>

<div class="clearfix"></div>

<div class="hr-line-dashed"></div>

<div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
  <label for="name">
    <i class="fa fa-question-circle"
       data-toggle="tooltip"
       title="Necessário para identificar as questões ao criar um simulado"></i> Título/Código
  </label>
  {!! Form::text('name', null, ['class' => 'form-control', 'maxlength' => 120]) !!}
  {!! $errors->first('name', '<span class="help-block text-danger">:message</span>') !!}
</div>

<div class="form-group {!! ($errors->first('discipline_id')) ? 'has-error' : '' !!}">
  <label for="icon">
      Disciplina <strong class="text-danger">(*)</strong>
  </label>
  {!! Form::select('discipline_id', ['' => 'Selecione um disciplina'] + \Modules\Lesson\Entities\Discipline::lists('name', 'id')->toArray(), null, ['required' => 'required', 'class' => 'form-control load-select2']) !!}
  {!! $errors->first('discipline_id', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group">
  <label for="text">
    <i class="fa fa-question-circle"
       data-toggle="tooltip"
       title="A questão aceita texto, imagens, vídeos e arquivos."></i> Questão
    <strong class="text-danger">(*)</strong>
  </label>
  {!! Form::textarea('text', null, ['class' => 'form-control summernote', 'id' => 'question-text']) !!}
  {!! $errors->first('text', '<span class="help-block text-danger">:message</span>') !!}
</div>

<div class="form-group">
  <button type="submit" class="btn btn-primary">
    Salvar Questão
  </button>
  <button type="button" data-toggle="modal" data-target="#create-answer" class="btn btn-success">
    Adicionar Resposta
  </button>
</div>

<div id="answers" data-edit="{!! isset($edit) ? $edit->id : 'false' !!}"></div>

@section('cjs')
  {!! HTML::script(elixir('js/cms/components/answer/answer.min.js')) !!}
  <script type="text/javascript">
    $(document).ready(function(){
      $('#form').submit(function(){
        var $field = $('#question-text');
        if ($field.code().length == 11 || $field.code() < 11){
          swal('Ops', 'A Questão não foi preenchida. Preencha antes de salvar o registro.', 'error');
          return false;
        }
      });
    });
  </script>
@endsection
