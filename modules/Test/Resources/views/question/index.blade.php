@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Questões</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Questões</strong>
            </li>
        </ol>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.question.create') !!}"
           class="btn btn-success">
            {!! trans('dictionary.create') !!} Questão
        </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
      <div class="col-lg-12">
        <div class="ibox-content table-responsive">
          {!! Form::open(['route' => 'fatorcms.question.index', 'method' => 'GET']) !!}
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="name">
                  Nome/Título
                </label>
                {!! Form::text('search[name]', old('search[name]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
              </div>
            </div>
            <div class="col-lg-3">
              <div class="form-group">
                <label for="name">
                  Dificuldade
                </label>
                {!! Form::select('search[difficulty]', ['' => '', '1' => 'Fácil', '2' => 'Médio', '3' => 'Difícil'], old('search[difficulty]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="name">
                  Disciplina
                </label>
                <div class="input-group">
                  {!! Form::select('search[discipline_id]', ['' => ''] + \Modules\Lesson\Entities\Discipline::lists('name', 'id')->toArray() , old('search[discipline_id]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary">Procurar</button>
                  </span>
                  <span class="input-group-btn">
                    <a class="btn btn-warning" href="{!! route('fatorcms.question.index') !!}">Limpar</a>
                  </span>
                </div>
              </div>
            </div>
          </div>
          {!! Form::close() !!}
          @if ($list->count())
          <table class="table table-bordered table-striped">
              <thead>
              <tr>
                  <th>Questão</th>
                  <th>Tipo de Resposta</th>
                  <th>Disciplina</th>
                  <td>Bancada</td>
                  <td>Dificuldade</td>
                  <td>Recurso</td>
                  <td>Qtd. Simulados <i data-title="title" title="Quantidade de provas que essa questão já foi utilizada" class="fa fa-question-circle"></i> </td>
                  <th width="8%">Opções</th>
              </tr>
              </thead>
              <tbody>
              @foreach($list as $row)
                <tr>
                    <td>{!! str_limit(strip_tags($row->text)) !!}</td>
                    <td>{!! $row->answers->count() ? 'Objetiva' : 'Dissertativa' !!}</td>
                    <td>{!! $row->discipline ? $row->discipline->name : 'Pergunta sem disciplina' !!}</td>
                    <td>{!! $row->ExaminingBoard ? $row->ExaminingBoard->name : '' !!}</td>
                    <td>{!! $row->DifficultyText !!}</td>
                    <td>{!! $row->resource ? 'Sim' : 'Não' !!}</td>
                    <td>{!! $row->countTests->count() !!}</td>
                    <td>
                      <a href="{!! route('fatorcms.question.edit', $row->id) !!}" class="btn btn-xs btn-primary pull-left margin-right">
                        <i class="fa fa-pencil"></i>
                      </a>
                      {!! Form::open(['route' => ['fatorcms.question.destroy', $row->id], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deletar a questão '. $row->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                      <button data-toggle="tooltip" title="Excluir está questão" class="btn btn-danger btn-xs pull-left">
                        <i class="fa fa-trash-o"></i>
                      </button>
                      {!! Form::close() !!}
                    </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          @include('layouts.elements.paginator', ['paginator' => $list])
          @endif
          </div>
      </div>
  </div>
@endsection