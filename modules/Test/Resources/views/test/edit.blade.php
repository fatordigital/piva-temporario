@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Editando Simulado</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li>
                <a href="{!! route('fatorcms.test.index') !!}">Simulados</a>
            </li>
            <li class="active">
                <strong>Editando Simulado</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <p>
        Todos os campos com <strong class="text-danger">(*)</strong> são obrigatórios.
      </p>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.test.index') !!}"
           class="btn btn-warning">
            Voltar
        </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="ibox float-e-margins">
    <div class="tabs-container">
       <ul class="nav nav-tabs">
         <li class="active"><a data-toggle="tab" href="#tab-1">Dados do simulado</a></li>
         <li class="">
          <a data-toggle="tab" href="#tab-2">
            Questões
          </a>
         </li>
       </ul>
    </div>
    <div class="ibox-content">
      {!! Form::model($edit,['route' => ['fatorcms.test.update', $edit->slug], 'method' => 'PATCH', 'files' => true]) !!}
      @include('test::test.form')
      {!! Form::close() !!}
    </div>
  </div>
@endsection