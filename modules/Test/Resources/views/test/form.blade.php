
@if ($errors->count())
<div class="alert alert-danger">
    @foreach($errors->all() as $error)
        {!! $error !!}
    @endforeach
</div>
@endif

<div class="tabs-container">
    <div class="tab-content">
        <div id="tab-1" class="tab-pane active">

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="filename">
                            <i class="fa fa-question-circle" data-toggle="tooltip" title="Adicione uma imagem de 1900 x 564"></i> Foto de capa <strong class="text-success">(Opcional)</strong>
                        </label>
                        <div class="clearfix"></div>
                        <label title="Adicionar/editar foto" for="filename">
                            {!! Form::file('file', ['class' => 'hide', 'id' => 'filename', 'accept' => 'image/jpeg']) !!}
                            {!! HTML::image('/img/1468049205_25.Camera-Front.png', 'Adicionar/editar foto') !!}
                        </label>
                        <div style="overflow: hidden;{!! isset($edit) &&  !empty($edit->filename) ? '' : '' !!}">
                            @if (!isset($edit))
                                <img id="foto" class="img-responsive" style="width: 100%" />
                            @else
                                @if (file_exists(public_path('assets/simulado/'.$edit->filename)) && $edit->filename != '')
                                    {!! HTML::image(resize_image('assets/simulado/', $edit->filename, '232x232', '232x232'), $edit->name, ['id' => 'foto', 'class' => 'img-responsive']) !!}
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="status" class="control-label">Tem correção?</label>
                        <br/>
                        <div class="radio radio-success radio-inline">
                            {!! Form::radio('return_response', 1, true) !!}
                            <label for="radio4">
                                Sim
                            </label>
                        </div>
                        <div class="radio radio-danger radio-inline">
                            {!! Form::radio('return_response', 0, false) !!}
                            <label for="radio4">
                                Não
                            </label>
                        </div>
                    </div>
                </div>


                <div class="col-lg-6">

                    <div class="form-group">
                        <label for="status" class="control-label">Conteúdo Grátis?</label>
                        <br/>
                        <div class="radio radio-success radio-inline">
                            {!! Form::radio('free', 1, false) !!}
                            <label for="radio4">
                                Sim
                            </label>
                        </div>
                        <div class="radio radio-danger radio-inline">
                            {!! Form::radio('free', 0, true) !!}
                            <label for="radio4">
                                Não
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <p>
                            <label for="status">
                                Descontar ponto ao pular questão <strong class="text-danger">(*)</strong>
                            </label>
                        </p>
                        <div class="radio radio-success radio-inline">
                            {!! Form::radio('jump_question', 1, false, ['id' => 'on']) !!}
                            <label for="on">
                                Sim
                            </label>
                        </div>
                        <div class="radio radio-danger radio-inline">
                            {!! Form::radio('jump_question', 0, true, ['id' => 'off']) !!}
                            <label for="off">
                                Não
                            </label>
                        </div>
                    </div>

                    <div class="form-group" id="datepicker">
                        <label class="font-noraml">
                            Data de início e fim do simulado <strong class="text-success">(Opcional)</strong>
                        </label>
                        <div class="input-daterange input-group" id="datepicker">
                            {!! Form::text('begin_date', null, ['class' => 'input-sm form-control', 'placeholder' => date('d/m/Y')]) !!}
                            <span class="input-group-addon">{!! trans('dictionary.to') !!}</span>
                            {!! Form::text('end_date', null, ['class' => 'input-sm form-control', 'placeholder' => date('d/m/Y')]) !!}
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <p>
                            <label for="status">
                                Descontar ponto ao errar uma questão <strong class="text-danger">(*)</strong>
                            </label>
                        </p>
                        <div class="radio radio-success radio-inline">
                            {!! Form::radio('error_question', 1, false, ['id' => 'on']) !!}
                            <label for="on">
                                Sim
                            </label>
                        </div>
                        <div class="radio radio-danger radio-inline">
                            {!! Form::radio('error_question', 0, true, ['id' => 'off']) !!}
                            <label for="off">
                                Não
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="course_id">
                            Curso ao final do simulado <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::select('course_id', ['' => 'Selecione o curso'] + \Modules\Course\Entities\Course::lists('name', 'id')->toArray(), null, ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group{!! $errors->first('duration') ? ' has-error' : '' !!}">
                        <label for="name">
                            Horas de duração <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::text('duration', null, ['class' => 'form-control time-mask']) !!}
                        {!! $errors->first('duration', '<span class="help-block text-danger">:message</span>') !!}
                    </div>
                </div>
            </div>

            <div class="form-group{!! $errors->first('name') ? ' has-error' : '' !!}">
                <label for="name">
                    Título <strong class="text-danger">(*)</strong>
                </label>
                {!! Form::text('name', null, ['required' => 'required', 'class' => 'form-control', 'maxlength' => 120]) !!}
                {!! $errors->first('name', '<span class="help-block text-danger">:message</span>') !!}
            </div>

            <div class="form-group{!! $errors->first('subject') ? ' has-error' : '' !!}">
                <label for="name">
                    Assunto <strong class="text-danger">(*)</strong>
                </label>
                {!! Form::text('subject', null, ['required' => 'required', 'class' => 'form-control', 'maxlength' => 120]) !!}
                {!! $errors->first('subject', '<span class="help-block text-danger">:message</span>') !!}
            </div>
        </div>

        <div id="tab-2" class="tab-pane">

            <div class="row">
                <div id="test-questions" data-edit="{!! isset($edit) ? $edit->id : 'false' !!}" data-boards={!! isset($examining_board) ? $examining_board : '' !!} ></div>
            </div>

        </div>

    </div>
</div>

<hr/>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <a href="{!! route('fatorcms.test.index') !!}"
               class="btn btn-warning">
                Voltar
            </a>
            <button type="submit" class="btn btn-primary">
                Salvar registros
            </button>
        </div>
    </div>
</div>

@section('cjs')
    {!! HTML::script(elixir('js/cms/components/test-questions/test-questions.min.js')) !!}
@endsection