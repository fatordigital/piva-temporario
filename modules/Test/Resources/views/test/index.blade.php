@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Simulados</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Simulados</strong>
            </li>
        </ol>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.test.create') !!}"
           class="btn btn-success">
            {!! trans('dictionary.create') !!} Simulado
        </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    @if ($list->count())
      <div class="col-lg-12">
        <div class="ibox-content">
          <table class="table table-bordered table-striped">
              <thead>
              <tr>
                  <th>Título</th>
                  <th>Assunto</th>
                  <th>Data de início</th>
                  <th>Data final</th>
                  <th width="8%">Opções</th>
              </tr>
              </thead>
              <tbody>
              @foreach($list as $row)
                <tr>
                    <td>{!! $row->name !!}</td>
                    <td>{!! $row->subject !!}</td>
                    <td>{!! $row->Begin !!}</td>
                    <td>{!! $row->End !!}</td>
                    <td>
                      <a href="{!! route('fatorcms.test.edit', $row->slug) !!}" class="btn btn-xs btn-primary pull-left margin-right">
                        <i class="fa fa-pencil"></i>
                      </a>
                      {!! Form::open(['route' => ['fatorcms.test.destroy', $row->slug], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deletar o simulado '. $row->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                      <button data-toggle="tooltip" title="Excluir Simulado" class="btn btn-danger btn-xs pull-left">
                        <i class="fa fa-trash-o"></i>
                      </button>
                      {!! Form::close() !!}
                    </td>
                </tr>
              @endforeach
              </tbody>
            </table>
          @include('layouts.elements.paginator', ['paginator' => $list])
          </div>
      </div>
    @endif
  </div>
@endsection