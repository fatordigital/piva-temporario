<?php namespace Modules\Test\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestQuestion extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["test_id", "question_id"];

    public function question()
    {
        return $this->belongsTo('Modules\Test\Entities\Question', 'question_id');
    }


}