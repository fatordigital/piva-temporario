<?php namespace Modules\Test\Entities;
   
use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestTeamUser extends BaseModel {

    use SoftDeletes;

    protected $fillable = ["user_id","team_id","begin_date","end_date"];

}