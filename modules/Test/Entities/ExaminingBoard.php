<?php namespace Modules\Test\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExaminingBoard extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["name", "slug"];

    public function customCreate($data)
    {
        if (!empty($data)) {
            if (!is_int(filter_var($data, FILTER_VALIDATE_INT))) {
                $create = $this->firstOrCreate(['name' => $data, 'slug' => str_slug($data, '-')]);
                return $create->id;
            } else {
                $create = $this->find($data);
                return $create->id;
            }
        }
        return '';
    }

}