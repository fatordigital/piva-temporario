<?php namespace Modules\Test\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["user_create_id", "question_id", "check", "text"];

}