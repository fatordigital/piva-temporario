<?php namespace Modules\Test\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["return_response", "free", "filename", "course_id", "duration", "user_create_id", "name", "slug", "jump_question", "error_question", "subject"];

    protected $appends = [
        'Begin',
        'End',
        'broke_duration',
        'correcao'
    ];

    public function getBrokeDurationAttribute()
    {
        return explode(':', $this->duration);
    }

    public function customCreate($request, $controller)
    {
        $return = $this->create($request->except(['_token', 'TestQuestion']));
        if ($return) {
            if ($request->hasFile('file')) {
                $filename = $controller->customFileName($request, 'assets/simulado/', 'file');
                if ($request->file('file')->move(public_path('assets/simulado/'), $filename)) {
                    $request->merge(['filename' => $filename]);
                }
            }
            foreach ($request->get('TestQuestion')['question_id'] as $item) {
                TestQuestion::firstOrCreate(['test_id' => $return->id, 'question_id' => $item]);
            }
        }
        return $return;
    }

    public function customUpdate($edit, $request, $controller)
    {
        $relations = $edit->questions->lists('question_id')->toArray();
        if ($request->has('TestQuestion')) {
            $this->updateRelations($edit, new TestQuestion(), $relations, $request->get('TestQuestion')['question_id'], 'question_id', 'test_id');
        } else if (count($relations)) {
            if (!TestQuestion::whereLessonId($edit->id)->delete())
                return false;
        }
        if ($request->hasFile('file')) {
            $filename = $controller->customFileName($request, 'assets/simulado/', 'file');
            if ($request->file('file')->move(public_path('assets/simulado/'), $filename)) {
                $request->merge(['filename' => $filename]);
            }
        }
        if ($edit->update($request->except(['_token', 'TestQuestion'])))
            return true;
        return false;
    }

    public function questions()
    {
        return $this->hasMany('Modules\Test\Entities\TestQuestion', 'test_id');
    }

    public function getCorrecaoAttribute()
    {
        foreach ($this->questions as $question) {
//            if ($question->Question->id > 524 && $question->Question->id < 533) {
//                dd($question->Question);
//            }
            if (strlen($question->Question->justify) > 11) {
                return true;
            }
        }
        return false;
    }

    public function media($totalQuestions, $totalCorrects)
    {
        $totalQuestions = $totalQuestions / 2;
        return $totalCorrects >= $totalQuestions;
    }
}