<?php namespace Modules\Test\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["name", "user_create_id", "discipline_id", "examining_board_id", "text", "difficulty", "justify", "resource"];

    protected $appends = ["LimitedText"];

    public function getLimitedTextAttribute()
    {
        return str_limit(strip_tags($this->text));
    }

    public function getDifficultyTextAttribute()
    {
        if (isset($this->difficulty)) {
            switch ($this->difficulty) {
                case 1 :
                    return 'Fácil';
                    break;
                case 2 :
                    return 'Médio';
                    break;
                case 3 :
                    return 'Difícil';
                    break;
            }
        }
    }

    public function discipline()
    {
        return $this->belongsTo('Modules\Lesson\Entities\Discipline', 'discipline_id');
    }

    public function answers()
    {
        return $this->hasMany('Modules\Test\Entities\Answer', 'question_id');
    }

    public function countTests()
    {
        return $this->hasMany('Modules\Test\Entities\TestQuestion', 'question_id')->groupBy('test_id');
    }

    public function user_choose()
    {
        return $this->hasOne('Modules\User\Entities\UserQuestionChoose', 'question_id');
    }

    public function ExaminingBoard()
    {
        return $this->belongsTo('Modules\Test\Entities\ExaminingBoard', 'examining_board_id');
    }

    public function customCreate($request)
    {
        $examining_board = new ExaminingBoard();

        $request->merge(['examining_board_id' => $examining_board->customCreate($request->get('examining_board'))]);

        $return = $this->create($request->except(['_token', 'answers', 'answer', 'editAnswer']));
        if (!$return)
            return false;
        else {
            if ($request->has('answers') && $request->get('answers')) {
                foreach ($request->get('answers') as $answer) {
                    $edit = json_decode($answer);
                    Answer::create(['user_create_id' => $request->get('user_create_id'), 'question_id' => $return->id, 'text' => $edit->text, 'check' => $edit->check]);
                }
            }
        }
        return $return;
    }

    public function customUpdate($edit, $request)
    {
        $examining_board = new ExaminingBoard();

        $request->merge(['examining_board_id' => $examining_board->customCreate($request->get('examining_board'))]);
        
        $return = $edit->update($request->except(['_token', 'answers', 'answer', 'editAnswer']));
        if (!$return)
            return false;
        else {
            if (!$request->has('answers') || !$request->get('answers')) {
                Answer::whereQuestionId($edit->id)->delete();
            } else {
                foreach ($request->get('answers') as $answer) {
                    $item = json_decode($answer);
                    if (!is_int($item->id))
                        Answer::create(['user_create_id' => $request->get('user_create_id'), 'question_id' => $edit->id, 'text' => $item->text, 'check' => $item->check]);
                    else {
                        $answer_edit = Answer::find($item->id);
                        if ($answer_edit) {
                            $answer_edit->update(['user_create_id' => $request->get('user_create_id'), 'question_id' => $edit->id, 'text' => $item->text, 'check' => $item->check]);
                        } else {
                            Answer::create(['user_create_id' => $request->get('user_create_id'), 'question_id' => $edit->id, 'text' => $item->text, 'check' => $item->check]);
                        }
                    }
                }
            }
        }
        return $return;
    }

}