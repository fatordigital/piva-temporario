<?php namespace Modules\Test\Entities;

use App\BaseModel;

class QuestionReport extends BaseModel
{

    protected $fillable = ["user_id", "question_id", "report", "ok"];

    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'user_id');
    }

    public function question()
    {
        return $this->belongsTo('Modules\Test\Entities\Question', 'question_id');
    }

    public static function ok($question_id, $user_id)
    {
        $exist = self::whereQuestionId($question_id)->whereUserId($user_id)->first();
        if ($exist)
            $exist->update(['ok' => true]);
    }

}