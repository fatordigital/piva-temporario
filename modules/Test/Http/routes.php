<?php

    Route::group([
        'prefix'     => 'fatorcms',
        'middleware' => [
            'acl',
            'auth',
            'cms'
        ],
        'namespace'  => 'Modules\Test\Http\Controllers'
    ], function () {
        Route::resource('test', 'TestController');
        Route::resource('question', 'QuestionController');

        Route::post('get-answers.json', ['as' => 'get-answers.json', 'uses' => 'QuestionController@getAnswers']);
    });