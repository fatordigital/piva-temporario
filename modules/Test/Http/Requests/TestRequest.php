<?php namespace Modules\Test\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'TestQuestion' => 'required',
            'duration' => 'date_format:H:i:s'
        ];
    }

    public function messages()
    {
        return [
            'TestQuestion.required' => 'É necessário adicionar questões nesta prova.',
            'duration.date_format' => 'O formato do campo é H:i:s ou 00:00:00'
        ];
    }

}
