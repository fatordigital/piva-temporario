<?php namespace Modules\Test\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Illuminate\Http\Request;
use Modules\Test\Entities\Answer;
use Modules\Test\Entities\Question;
use Modules\Test\Entities\QuestionReport;

class QuestionController extends BaseModuleController
{

    public function index(Question $model)
    {
        $list = $model->orderBy('id', 'desc');
        if (request()->has('search')) {
            foreach(request()->get("search") as $key => $value) {
                if (!empty($value) && $key != 'type') {
                    $list->where($key, 'LIKE', '%' . $value . '%');
                } else if ($key == 'type' && !empty($value)) {
                    $list->where($value, '=', true);
                }
            }
        }
        request()->session()->flashInput(request()->all());
        $list = $list->paginate(50);
        return view('test::question.index', compact('list'));
    }

    public function create()
    {
        return view('test::question.create');
    }

    public function store(Request $request, Question $model)
    {
        $request->merge(['user_create_id' => \Auth::user()->id]);
        if ($return = $model->customCreate($request)) {
            n(route('fatorcms.question.edit', $return->id), 'Nova Questão', 'Uma nova questão foi criada por '. userHistorical());
            return redirect()->route('fatorcms.question.index')->with('success', 'Nova questão foi criada com sucesso.');
        }
        return redirect()->back()->with('error', 'Ops, falha ao criar questão');
    }

    public function edit($id, Question $model)
    {
        $edit = $model->find($id);
        if (!$edit)
            return redirect()->route('fatorcms.question.index')->with('error', 'Ops, registro não foi encontrado.');
        if (request()->has('ok'))
            QuestionReport::ok($id, request('ok'));
        session(['answers' => $edit->answers->toArray()]);
        return view('test::question.edit', compact('edit'));
    }

    public function update($id, Request $request, Question $model)
    {
        $request->merge(['user_create_id' => \Auth::user()->id]);
        $edit = $model->find($id);
        if ($model->customUpdate($edit, $request)) {
            n(route('fatorcms.question.edit', $edit->id), 'Questão atualizada', 'Questão atualizada por '. userHistorical());
            session()->forget('answers');
            return redirect()->route('fatorcms.question.index')->with('success', 'Questão foi atualizada com sucesso.');
        }
        return redirect()->back()->with('error', 'Ops, falha ao criar questão');
    }

    public function destroy($id, Question $model)
    {
        $destroy = $model->find($id);
        if (!$destroy)
            return redirect()->route('fatorcms.question.index')->with('error', 'Ops, registro não foi encontrado.');
        if ($destroy->delete()) {
            n(route('fatorcms.question.destroy', $id), 'Nova Aula', 'A questão' . $destroy->name . ', foi deletado por ' . userHistorical());
            return redirect()->route('fatorcms.question.index')->with('success', 'A Questão ' . $destroy->name . ' foi deletada com sucesso.');
        } else
            return redirect()->route('fatorcms.question.index')->with('error', 'Ops, não foi possível deletar o registro, tente novamente.');
    }

    public function getAnswers(Request $request, Answer $model)
    {
        $data = $model->whereQuestionId($request->get('question_id'))->get();
        if ($data) {
            return response()->json(['error' => false, 'msg' => 'Questão objetiva', 'data' => $data->toArray()], 200);
        } else {
            return response()->json(['error' => false, 'msg' => 'Questão considerada dissertativa.'], 200);
        }
    }

}