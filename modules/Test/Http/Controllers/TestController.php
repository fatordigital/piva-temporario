<?php namespace Modules\Test\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use Modules\Test\Entities\ExaminingBoard;
use Modules\Test\Entities\Question;
use Modules\Test\Entities\Test;
use Modules\Test\Http\Requests\TestRequest;

class TestController extends BaseModuleController
{

    private function load_json()
    {
        view()->share([
            'examining_board' => ExaminingBoard::lists('name', 'id')->toJson()
        ]);
    }

    public function index(Test $model)
    {
        $list = $model->orderBy('id', 'desc')->paginate(15);
        return view('test::test.index', compact('list'));
    }

    public function create()
    {
        $this->load_json();
        if (!Question::all()->count())
            return redirect()->route('fatorcms.question.create')->with('error', 'Você foi direicionado para a seção de cadastro de questões. Antes de criar uma simulado é necessário que tenha questões registradas no sistema.');
        return view('test::test.create');
    }

    public function store(TestRequest $request, Test $model)
    {
        $request->merge(['user_create_id' => \Auth::user()->id, 'slug' => $model->notRepeatModelString(str_slug($request->get('name', 'slug'), '-'), $model)]);
        if ($save = $model->customCreate($request, $this)) {
            n(route('fatorcms.test.edit', $save->slug), 'Novo Simulado', 'Simulado criado por ' . userHistorical());
            return redirect()->route('fatorcms.test.edit', $save->slug)->with('success', 'A prova foi criada com sucesso.');
        }
        return redirect()->route('fatorcms.test.create')->with('error', 'Ops, não foi possível salvar os registros, tente novamente.')->withInput($request->all());
    }

    public function edit($slug, Test $model)
    {
        $this->load_json();
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.test.index')->with('error', 'Ops, registro não foi encontrado.');
        return view('test::test.edit', compact('edit'));
    }

    public function update($slug, TestRequest $request, Test $model)
    {
        $request->merge(['user_create_id' => \Auth::user()->id, 'slug' => $model->notRepeatModelString(str_slug($request->get('name', 'slug'), '-'), $model)]);
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.test.index')->with('error', 'Ops, registro não foi encontrado.');
        if ($edit->customUpdate($edit, $request, $this)) {
            n(route('fatorcms.test.edit', $request->get('slug')), 'Simulado atualizado', 'O simulado ' . $request->get('name') . ', foi criada por ' . userHistorical());
            return redirect()->route('fatorcms.test.edit', $request->get('slug'))->with('success', 'O simulado' . $edit->name . ' foi atualizada com sucesso.');
        } else
            return redirect()->back()->with('error', 'Ops, não foi possível atualizar a aula, verifique todos os campos e tente novamente.')->withInput($request->except(['_token', 'user_create_id', 'slug']));
    }

    public function destroy($slug, Test $model)
    {
        $destroy = $model->whereSlug($slug)->first();
        if (!$destroy)
            return redirect()->route('fatorcms.test.index')->with('error', 'Ops, registro não foi encontrado.');
        if ($destroy->delete()) {
            n(route('fatorcms.test.destroy', $slug), 'Nova Aula', 'O Simulado' . $destroy->name . ', foi deletado por ' . userHistorical());
            return redirect()->route('fatorcms.test.index')->with('success', 'O simulado ' . $destroy->name . ' foi deletada com sucesso.');
        } else
            return redirect()->route('fatorcms.test.index')->with('error', 'Ops, não foi possível deletar o registro, tente novamente.');
    }

}