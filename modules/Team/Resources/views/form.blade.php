<div class="tabs-container">
    <div class="tab-content">
        <div id="tab-1" class="tab-pane active">
            <div class="row">

                @include('sections.cropper', ['name' => 'file', 'image' => isset($edit) && $edit->image != '' ? url('assets/team/'.$edit->image) : ''])

                <div class="col-lg-12">
                    <div class="form-group">
                        {!! $errors->first('file', '<span class="text-danger">:message</span>') !!}
                    </div>
                </div>


                <div class="col-lg-12">
                    <div class="form-group {!! ($errors->first('image_showw')) ? 'has-error' : '' !!}">
                        <label for="filename">
                            <i class="fa fa-question-circle" data-toggle="tooltip" title="Adicione uma imagem de 261x146"></i> Imagem de apresentação <strong class="text-success">(Opcional)</strong>
                        </label>
                        <input type="file" name="image_showw" class="form-control" />
                        <span class="help-block">Adicione uma imagem de 261x146</span>
                        <div style="overflow: hidden;{!! isset($edit) &&  !empty($edit->image_show) ? '' : '' !!}">
                            @if (!isset($edit))
                                <img id="foto" class="img-responsive" style="width: 100%"/>
                            @else
                                @if ($edit->image_show != '' && file_exists(public_path('assets/team/'. $edit->image_show)))
                                    {!! HTML::image(resize_image('assets/team', $edit->image_show, '261x146', '261x146'), $edit->image_show, ['id' => 'foto', 'class' => 'img-responsive']) !!}
                                @endif
                            @endif
                        </div>
                        {!! $errors->first('image_showw', '<strong class="help-block text-danger">:message</strong>') !!}
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="filename">
                            <i class="fa fa-question-circle" data-toggle="tooltip"
                               title="Adicione um ícone de 82 x 82"></i> Ícone concurso <strong class="text-success">(Opcional)</strong>
                        </label>
                        <div class="clearfix"></div>
                        <label title="Adicionar/editar foto" for="icone">
                            {!! Form::file('icone', ['accept' => 'image/jpeg', 'class' => 'form-control']) !!}
                        </label>
                        <span class="help-block">Adicione uma imagem de 82x82</span>
                        <div style="overflow: hidden;{!! isset($edit) &&  !empty($edit->filename) ? ' height: 400px;' : '' !!}">
                            @if (!isset($edit))
                                <img id="foto" class="img-responsive" style="width: 100%"/>
                            @else
                                @if ($edit->icon != '' && file_exists(public_path('assets/team/'. $edit->icon)))
                                    {!! HTML::image(resize_image('assets/team', $edit->icon, '82x82', '82x82'), $edit->name, ['id' => 'foto', 'class' => 'img-responsive']) !!}
                                @endif
                            @endif
                        </div>
                    </div>
                </div>


                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="status" class="control-label">Apresentar na página inicial?</label>
                        <br/>
                        <div class="radio radio-success radio-inline">
                            {!! Form::radio('home_status', 1, false) !!}
                            <label for="radio4">
                               Sim
                            </label>
                        </div>
                        <div class="radio radio-danger radio-inline">
                            {!! Form::radio('home_status', 0, true) !!}
                            <label for="radio4">
                               Não
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="status" class="control-label">Apresentar como banner?</label>
                        <br/>
                        <div class="radio radio-success radio-inline">
                            {!! Form::radio('banner_status', 1, false) !!}
                            <label for="radio4">
                                Sim
                            </label>
                        </div>
                        <div class="radio radio-danger radio-inline">
                            {!! Form::radio('banner_status', 0, true) !!}
                            <label for="radio4">
                                Não
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group {!! ($errors->first('name')) ? 'has-error' : '' !!}">
                        <label for="name">
                            Nome/Título <strong class="text-danger">(*)</strong>
                        </label>
                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group {!! ($errors->first('partner_id[]')) ? 'has-error' : '' !!}">
                        <label for="icon">
                            Parceiros <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::select('partner_id[]', ['' => 'Selecione os parceiros aqui'] + \App\User::whereIn('group_id', [7, 11])->lists('name', 'id')->toArray(),
                        isset($edit) && $edit->Partners ? $edit->Partners->lists('user_id')->toArray() : null, ['multiple', 'style' => 'width: 100%', 'class' => 'form-control load-select2']) !!}
                        {!! $errors->first('partner_id[]', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group {!! ($errors->first('course_category_id')) ? 'has-error' : '' !!}">
                        <label for="icon">
                            Categoria <strong class="text-danger">(*)</strong>
                        </label>
                        {!! Form::select('course_category_id', ['' => 'Escolha uma categoria'] + \Modules\Course\Entities\CourseCategory::orderBy('name')->lists('name', 'id')->toArray(), null, ['style' => 'width: 100%', 'class' => 'form-control load-select2']) !!}
                        {!! $errors->first('course_category_id', '<span class="help-block">:message</span>') !!}
                    </div>
                    @if (isset($edit) && $edit->course_category_id == 1)
                        <div id="students" style="display: none;"
                             class="form-group {!! ($errors->first('max_student')) ? 'has-error' : '' !!}">
                            <label for="name">
                                Máximo de alunos <strong class="text-danger">(*)</strong>
                            </label>
                            {!! Form::text('max_student', 99999999, ['required' => 'required', 'class' => 'form-control']) !!}
                            {!! $errors->first('max_student', '<span class="help-block">:message</span>') !!}
                        </div>
                    @else
                        <div id="students"
                             class="form-group {!! ($errors->first('max_student')) ? 'has-error' : '' !!}">
                            <label for="name">
                                Máximo de alunos <strong class="text-danger">(*)</strong>
                            </label>
                            {!! Form::text('max_student', !isset($edit) ? 1 : null, ['required' => 'required', 'class' => 'form-control']) !!}
                            {!! $errors->first('max_student', '<span class="help-block">:message</span>') !!}
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="video_repeat">
                            Quantidade de vezes que repete o vídeo <strong class="text-danger">(*)</strong>
                        </label>
                        {!! Form::text('video_repeat', !isset($edit) ? 1 : null, ['type' => 'number', 'class' => 'form-control']) !!}
                    </div>
                    <div class="form-group {!! $errors->first('value') ? 'has-error' : '' !!}">
                        <label class="font-noraml">
                            Preço <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::text('value', null, ['class' => 'form-control money']) !!}
                        {!! $errors->first('value', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {!! $errors->first('end_date') ? 'has-error' : '' !!}">
                        <label class="font-noraml">
                            <i class="fa fa-question-circle" data-toggle="tooltip"
                               title="Caso a turma não tenha um limite de data, deixe o campo em branco."></i>
                            Data limite do curso <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::text('end_date', null, ['class' => 'form-control date-mask s-datepicker']) !!}
                        {!! $errors->first('end_date', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group {!! ($errors->first('course_id')) ? 'has-error' : '' !!}">
                        <label for="icon">
                            Cursos associados <strong class="text-danger">(*)</strong>
                        </label>
                        {!! Form::select('course_id', ['' => 'Relacionar um curso a turma'] + $courses, null, ['style' => 'width: 100%', 'required' => 'required', 'class' => 'form-control load-select2']) !!}
                        {!! $errors->first('course_id', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="" class="font-noraml">
                            Ordenação <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::text('order', !isset($edit) ? \Modules\Team\Entities\Team::get()->count() + 1 : null, ['class' => 'form-control', 'maxlength' => 3]) !!}
                    </div>
                    <div class="form-group {!! $errors->first('value_discount') ? 'has-error' : '' !!}">
                        <label class="font-noraml">
                            Preço promoção <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::text('value_discount', null, ['class' => 'form-control money']) !!}
                        {!! $errors->first('value_discount', '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group {!! $errors->first('end_date') ? 'has-error' : '' !!}">
                        <label class="font-noraml">
                            <i class="fa fa-question-circle" data-toggle="tooltip"
                               title="Após a compra do curso, o aluno terá X dias de acesso após a compra"></i>
                            Limite de dias <strong class="text-success">(Opcional)</strong> *apenas números*
                        </label>
                        {!! Form::text('end_days', null, ['type' => 'number', 'class' => 'form-control']) !!}
                        {!! $errors->first('end_days', '<span class="help-block">:message</span>') !!}
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="description">
                            Descrição da turma <strong class="text-success">(Opcional)</strong>
                        </label>
                        {!! Form::textarea('structure', null, ['class' => 'form-control summernote']) !!}
                    </div>
                </div>
            </div>
        </div>

        <div id="tab-2" class="tab-pane">

            <div class="row">
                <div id="team-lessons" data-edit="{!! isset($edit) ? $edit->id : null !!}"></div>
            </div>

        </div>

        <div id="tab-3" class="tab-pane">

            <div class="row">
                <div id="teachers" data-edit="{!! isset($edit) ? $edit->id : null !!}"></div>
            </div>

        </div>

        <div id="tab-4" class="tab-pane">
            <div id="about-team" data-edit='{!! isset($edit) ? $edit->about : null !!}'></div>
        </div>

        @if (isset($edit))
            <div id="tab-5" class="tab-pane">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="dd" id="nestable2">
                            <ol class="dd-list">
                                @foreach($disciplines as $discipline)
                                    <li class="dd-item" data-id="d-{!! $discipline->id !!}">
                                        <div class="dd-handle">
                                            <span class="label label-info"><i class="fa fa-hand-o-up"></i></span> {!! $discipline->name !!}
                                        </div>
                                        <ol class="dd-list">
                                            @foreach(\Modules\Team\Entities\TeamLesson::TeamDiscipline($edit->id, $discipline->id) as $lesson)
                                                <li class="dd-item" data-id="l-{!! $lesson->lesson_id !!}">
                                                    <div class="dd-handle">
                                                        Ordem <strong class="text-info">({!! $lesson->order !!})</strong> - {!! $lesson->lesson->name !!} - Data de lançamento - {!! $lesson->Begin !!}
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ol>
                                    </li>
                                @endforeach
                            </ol>
                        </div>
                        <div class="hide">
                        <div class="m-t-md">
                            <h5>Serialised Output</h5>
                        </div>
                        <textarea id="nestable2-output" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>
</div>

<hr/>

<div class="row">
    <div class="col-lg-12">
        <div class="form-group">
            <a href="{!! route('fatorcms.team.index') !!}"
               class="btn btn-warning">
                Voltar
            </a>
            <button type="submit" class="btn btn-primary">
                Salvar registros
            </button>
        </div>
    </div>
</div>


@section('ccss')
    {!! HTML::style('css/plugins/crop/cropper.min.css') !!}
@endsection

@section('cjs')
    {!! HTML::script('js/plugins/nestable/jquery.nestable.js') !!}
    {!! HTML::script('js/plugins/crop/cropper.min.js') !!}
    {!! HTML::script('js/plugins/crop/cropper-init.js') !!}
    {!! HTML::script(elixir('js/cms/components/team-lessons/grid.min.js')) !!}
    {!! HTML::script(elixir('js/cms/components/teachers/teachers.min.js')) !!}
    {!! HTML::script('js/cms/scripts/team/team.js') !!}
    {!! HTML::script('js/cms/components/about-team/about-team.min.js') !!}
    @if (isset($edit))
    <script>
        $(document).ready(function(){

            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target),
                        output = list.data('output');
                if (window.JSON) {
                    output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                } else {
                    output.val('JSON browser support required for this demo.');
                }

                $.ajax({
                    url: b + '/fatorcms/update-order-discipline' ,
                    dataType: 'json',
                    data: { list: list.nestable('serialize'), team_id: '{!! $edit->id !!}' },
                    method: 'post'
                });
            };
            // activate Nestable for list 2
            $('#nestable2').nestable({
                group: 1
            }).on('change', updateOutput);

            updateOutput($('#nestable2').data('output', $('#nestable2-output')));

            $('#nestable-menu').on('click', function (e) {
                var target = $(e.target),
                        action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });
        });
    </script>
    @endif
@endsection
