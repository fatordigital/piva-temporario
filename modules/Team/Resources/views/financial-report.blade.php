@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Relatório financeiro</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Relatório financeiro</strong>
            </li>
        </ol>
    </div>
  </div>
  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.team.index') !!}"
           class="btn btn-warning">
            <i class="fa fa-arrow-left"></i> Voltar
        </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <div class="ibox-content table-responsive">
        @if ($list->orderItems->count())
          <table class="table table-bordered">
            <thead>
            <tr>
                <th>Status do pagamento</th>
                <th>Usuário</th>
                <th>Turma</th>
                <th>Curso</th>
                <th>Tipo</th>
                <th>Quantidade</th>
                <th>Valor</th>
            </tr>
            </thead>
            <tbody>
              @foreach($list->orderItems as $item)
                <tr>
                  <td>{!! $item->Order->orderStatus->name !!}</td>
                  <td>{!! $item->Order->user->name !!}</td>
                  <td>{!! $item->name !!}</td>
                  <td>{!! $item->Item->course->name !!}</td>
                  <td>{!! $item->Item->category->name !!}</td>
                  <td>{!! $item->quantity !!}</td>
                  <td>R$ {!! bcoToCoin($item->value) !!}</td>
                </tr>
              @endforeach
            </tbody>
            <tfoot>
            <tr>
              <td colspan="4"></td>
              <td>Total</td>
              <td>R$ {!! bcoToCoin($list->orderItems->sum('value')) !!}</td>
            </tr>
            </tfoot>
          </table>
        @endif
      </div>
    </div>
</div>
@endsection