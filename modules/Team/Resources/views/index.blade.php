@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-lg-9">
      <h2>Turmas</h2>
      <ol class="breadcrumb">
          <li>
              <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
          </li>
          <li class="active">
              <strong>Turmas</strong>
          </li>
      </ol>
  </div>
</div>

  @if (auth()->user()->isPartner() == false)
    <div class="row page-heading white-bg">
      <div class="col-lg-9">
          <br />
          <a href="{!! URL::route('fatorcms.team.create') !!}" class="btn btn-success">
              {!! trans('dictionary.create') !!} Turma
          </a>
          <a href="#" data-target="#myModal5" class="btn btn-default" data-title="title" data-toggle="modal" title="Avisos, notificações, alertas...">
            <i class="fa fa-paper-plane"></i> Notificações
          </a>
          <a href="{!! route('fatorcms.team.index', 'export=true') !!}" class="btn btn-primary">
              Exportar
          </a>
      </div>
    </div>
  @endif
@endsection

@section('content')


    {!! Form::open(['route' => 'fatorcms.team.index', 'method' => 'GET']) !!}

      <div class="col-lg-12">
        <div class="ibox-content">
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <label for="name">
                  Nome/Título
                </label>
                {!! Form::text('search[name]', old('search[name]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
              </div>
            </div>
            <div class="col-lg-9">
              <div class="form-group">
                <label for="name">
                  Categoria
                </label>
                <div class="input-group">
                  {!! Form::select('search[course_category_id]', ['' => ''] + $categories->lists('name', 'id')->toArray(), old('search[course_category_id]'), ['class' => 'form-control', 'autocomplete' => 'off']) !!}
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary">Procurar</button>
                  </span>
                  <span class="input-group-btn">
                    <a class="btn btn-warning" href="{!! route('fatorcms.team.index') !!}">Limpar</a>
                  </span>
                </div>
              </div>
            </div>
            <div class="col-lg-12">
              <?php $rand = ['success', 'danger', 'warning', 'primary', 'info']; ?>
              @foreach($categories as $category)
                <label data-category-id="{!! $category->id !!}" data-toggle="tooltip" title="Apenas {!! $category->name !!}" class="cursor-pointer label label-{!! $rand[rand(0, 3)] !!}">{!! $category->name !!}</label>
              @endforeach
              <label class="label label-default cursor-pointer" data-category-id="*">Todos</label>
            </div>
          </div>
        </div>
      </div>
    {!! Form::close() !!}

  @include('team::modal')

    <?php $b = 1; ?>
  <div class="row">
      <div class="col-lg-12">
      @if ($list->count())
        @foreach($list as $row)
          @if ($row->userPartner() || !in_array(auth()->user()->group_id, [7, 11]))
          <?php $errors = \Modules\Team\Entities\Team::previousError($row) ?>
          @if ($row->begin_date < $row->end_date)
            @if ($row->begin_date == date('Y-m-d'))
              <?php $percent = 0; ?>
            @else
                @if (!empty($row->end_date))
                    <?php //$percent = round((time() - strtotime(date('Y-m-d'))) / (strtotime($row->end_date) - strtotime(date('Y-m-d'))) * 100) ?>
                    <?php $percent = 0 ?>
                @endif
            @endif
          @else
            <?php $percent = 100; ?>
          @endif
          <?php $percent = ($percent > 100) ? 100 : $percent ?>
          <div class="col-lg-4{!! $errors !== false ? ' text-danger' : '' !!}" data-category-childrens="{!! $row->course_category_id !!}">
              <div class="ibox" data-title="title" title="{!! $errors !== true ? $errors['msg'] : '' !!}">
                  <div class="ibox-title">
                      <span data-toggle="tooltip" title="Total de estudantes na turma" class="label label-success pull-right">
                        {!! $row->students ? $row->students->count() : 0 !!}
                      </span>
                      <span data-toggle="tooltip" title="Data final" class="label label-danger pull-right">
                        {!! $row->End !!}
                      </span>
                      <h5>{!! $row->name !!}</h5>
                  </div>
                  <div class="ibox-content">
                      @if (auth()->user()->isPartner() == false && $row->students->count())
                          <?php $i = 0; ?>
                          <div class="team-members">
                            @foreach($row->students as $student)
                              @if ($student->user)
                                @if ($i < 5)
                                  <a href="#" data-toggle="tooltip" title="{!! $student->user->name !!}">
                                    @if ($student->user->filename && fex('assets/users', $student->user->filename))
                                      <a href="{!! route('fatorcms.user.edit', $student->user->id) !!}">
                                        {!! HTML::image('assets/users/'. $student->user->filename, $student->user->name, ['class' => 'img-circle']) !!}
                                      </a>
                                    @else
                                      @if ($student->user->socialize && $student->user->socialize->gender !== '')
                                        @if ($student->user->socialize->gender == 'M')
                                          <img alt="member" class="img-circle" src="{!! url('img/male.png') !!}">
                                        @elseif($student->user->socialize->gender == 'F')
                                          <img alt="member" class="img-circle" src="{!! url('img/female.png') !!}">
                                        @endif
                                      @else
                                        <img alt="member" class="img-circle" src="{!! url('img/unisex.png') !!}">
                                      @endif
                                    @endif
                                  </a>
                                @endif
                              @endif
                              <?php $i++; ?>
                            @endforeach
                          </div>
                      @else
                        <div class="team-members">
                          <strong class="text-warning">A turma está vazia no momento.</strong>
                        </div>
                      @endif
                      <h4>{!! $row->course ? $row->course->name : '' !!}</h4>
                      <div>
                          <span>Status do curso:</span>
                          <div class="stat-percent">{!! $percent !!}%</div>
                          <div class="progress progress-mini">
                              <div style="width:{!! $percent !!}%;" class="progress-bar"></div>
                          </div>
                      </div>
                      <div class="row  m-t-sm">
                          <div class="col-sm-4">
                              <div class="font-bold">AULAS</div>
                              {!! $row->lessons->count() !!}
                          </div>
                          <div class="col-sm-4">
                              <div class="font-bold">-</div>
                              -
                          </div>
                          <div class="col-sm-4 text-right">
                              <div class="font-bold">TOTAL DA TURMA</div>
                              -
                              {{--- <i class="fa fa-level-up text-navy"></i>--}}
                          </div>
                      </div>
                  </div>
                  <div class="ibox-footer">
                      @if (auth()->user()->isPartner() == false)
                      <a class="btn btn-xs btn-rounded btn-default" href="{!! route('fatorcms.team.show', $row->slug) !!}" data-title="title" title="Gerenciador de alunos">
                        <i class="fa fa-users"></i>
                      </a>
                      {!! Form::open(['route' => ['fatorcms.team.destroy', $row->id], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deleta a turma '. $row->name.'?', 'method' => 'DELETE', 'class' => 'form-horizontal pull-right']) !!}
                      <button data-toggle="tooltip" title="Excluir Turma" class="btn no-rounded btn-danger btn-xs">
                          <i class="fa fa-trash-o"></i>
                      </button>
                      {!! Form::close() !!}
                      <a href="{!! route('fatorcms.team.edit', $row->slug) !!}" class="btn btn-xs no-rounded btn-success pull-right">
                        <i class="fa fa-pencil"></i>
                      </a>
                      @endif

                      <a href="{!! route('fatorcms.team.financial', $row->slug) !!}"
                         data-title="title"
                         title="Relatório financeiro da turma"
                         class="btn btn-xs btn-warning no-rounded pull-right">
                        <i class="fa fa-dollar"></i>
                      </a>
                      <div class="clearfix"></div>
                  </div>
              </div>
          </div>
          @if ($b % 3 == 0)
          <div class="clearfix"></div>
          @endif
          <?php $b++; ?>
          @endif
        @endforeach
      @else
        <div class="col-lg-12">
          <p>Nenhum registro no momento :(</p>
        </div>
      @endif
    </div>
    <div class="col-lg-12">
      @include('layouts.elements.paginator', ['paginator' => $list])
    </div>
  </div>


@endsection

@section('cjs')
  {!! HTML::script('js/cms/scripts/team/index.js') !!}
@endsection