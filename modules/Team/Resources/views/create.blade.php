@extends('layouts.cms')

@section('title')
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Nova Turma</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
            </li>
            <li>
                <a href="{!! route('fatorcms.team.index') !!}">Turmas</a>
            </li>
            <li class="active">
                <strong>Nova Turma</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <p>
        Todos os campos com <strong class="text-danger">(*)</strong> são obrigatórios.
      </p>
    </div>
  </div>

  <div class="row page-heading white-bg">
    <div class="col-lg-9">
        <br/>
        <a href="{!! URL::route('fatorcms.team.index') !!}"
           class="btn btn-warning">
            Voltar
        </a>
    </div>
  </div>
@endsection

@section('content')
  <div class="ibox float-e-margins">
    <div class="tabs-container">
       <ul class="nav nav-tabs">
         <li class="active"><a data-toggle="tab" href="#tab-1">Dados da turma</a></li>
         <li class="">
          <a data-toggle="tab" href="#tab-4">
            Sobre a turma
          </a>
         </li>
         <li class="">
          <a data-toggle="tab" href="#tab-2">
            Aulas
          </a>
         </li>
         <li class="">
          <a data-toggle="tab" href="#tab-3">
            Professores
          </a>
         </li>
       </ul>
    </div>
    <div class="ibox-content">
      {!! Form::open(['route' => 'fatorcms.team.store', 'files' => true]) !!}
      @include('team::form')
      {!! Form::close() !!}
    </div>
  </div>
@endsection