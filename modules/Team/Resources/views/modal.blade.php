<div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        {!! Form::open(['route' => 'team.alert']) !!}
      <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Alerta de turma ou usuários</h4>
                <p class="text-success">Segure CONTROL (Ctrl) para poder selecionar mais de uma turma ou usuário.</p>
            </div>

            <div class="modal-body">

                <div class="form-group">
                    <label for="status" class="control-label">Tipo de envio</label>
                    <br/>
                    <div class="radio radio-success radio-inline">
                      {!! Form::radio('team', 1, true) !!}
                      <label for="radio4">
                        Turmas
                      </label>
                    </div>
                    <div class="radio radio-danger radio-inline">
                      {!! Form::radio('team', 0, false) !!}
                      <label for="radio4">
                        Usuários
                      </label>
                    </div>
                </div>

                <div class="form-group" id="data_1">
                    <label class="font-noraml">
                      <i class="fa fa-question-circle" data-toggle="tooltip" title="Caso esteja vazio, o alerta será recebido apenas por e-mail."></i> Manter aviso até dia:
                    </label>
                    <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span> {!! Form::text('end_date', null, ['class' => 'form-control s-datepicker', 'autocomplete' => 'off']) !!}
                    </div>
                </div>

                <div data-team="1" class="form-group modalTeam {!! ($errors->first('course_id')) ? 'has-error' : '' !!}">
                  <label for="icon">
                      Turmas <strong class="text-danger">(*)</strong>
                  </label>
                  <br />
                  {!! Form::select('team_id[]', $teams, null, ['multiple', 'class' => 'form-control load-select2', 'style' => 'width: 100%:']) !!}
                  {!! $errors->first('team_id[]', '<span class="help-block">:message</span>') !!}
                </div>

                <div data-team="0" class="form-group modalTeam {!! ($errors->first('course_id')) ? 'has-error' : '' !!}" style="display: none">
                  <label for="icon">
                      Usuários <strong class="text-danger">(*)</strong>
                  </label>
                  <br />
                  {!! Form::select('user_id[]', $users, null, ['multiple', 'class' => 'form-control load-select2', 'style' => 'width: 100%:']) !!}
                  {!! $errors->first('user_id[]', '<span class="help-block">:message</span>') !!}
                </div>

                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                      <label for="description">
                        <i class="fa fa-question-circle" data-toggle="tooltip" title="Evite a usar imagem, algumas caixas de e-mail entendem como spam."></i> Mensagem <strong class="text-success">(Opcional)</strong>
                      </label>
                      {!! Form::textarea('description', null, ['class' => 'form-control summernote']) !!}
                      <p><a href="#" data-toggle="modal" data-target="#helpAlert">Clique aqui</a> e veja como criar um e-mail com dados existente.</p>
                    </div>
                  </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
        </div>
      {!! Form::close() !!}
    </div>
</div>


<div class="modal inmodal fade" id="helpAlert" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Ajuda do sistema</h4>
                <p class="text-success">Abaixo é possível ver comandos e exemplos de uso.</p>
            </div>

            <div class="modal-body">

              <h3>Comandos</h3>

              <ul class="list-group">
                <li class="list-group-item">{usuario.nome} - Irá mostra o nome do usuário</li>
                <li class="list-group-item">{turma.nome} - Irá mostrar o nome da turma</li>
                <li class="list-group-item">{turma.data_inicial} - Irá mostrar a data de início da turma</li>
                <li class="list-group-item">{turma.data_final} - Irá mostrar a data final da turma</li>
              </ul>

              <h3>Exemplos</h3>
              <p class="text-success">Os exemplos são baseados na mensagem do envio.</p>
              <code>
                Olá {usuario.nome}, <br />
                Aviso que semana que vem irá ser efetuado o simulado para que possamos ver como você se saiu no curso.
              </code>
              <div class="clearfix"></div> <br /><br />
              <code>
                Bom dia {usuario.nome},<br />
                Apenas para te lembrar que a sua turma encerra {turma.data_final}.<br /> Esperamos que você tenha gostado do nosso curso e que seu conhecimento tenha se aprimorado com ele. <br />

                A sua turma teve início no dia: {turma.data_inicial} e finaliza {turma.data_final} e é conhecida como {turma.nome} <br />
                Agradecemos por tudo professorpiva
              </code>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>