@extends('layouts.cms')

@section('title')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-9">
            <h2>Gerenciador de alunos - {!! $edit->name !!}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{!! route('fatorcms.dashboard') !!}">Dashboard</a>
                </li>
                <li>
                    <a href="{!! route('fatorcms.team.index') !!}">Turmas</a>
                </li>
                <li class="active">
                    <strong>Gerenciador de alunos - {!! $edit->name !!}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-12">
            <p>
                Você pode adicionar usuários nesta turma e removê-los
            </p>
        </div>
    </div>

    <div class="row page-heading white-bg">
        <div class="col-lg-9">
            <br/>
            <a href="{!! URL::route('fatorcms.team.index') !!}"
               class="btn btn-warning">
                <i class="fa fa-arrow-left"></i> Voltar
            </a>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-content">

                {!! Form::open(['route' => ['fatorcms.save.user.team', $edit->slug], 'method' => 'POST']) !!}
                <div class="form-group">
                    <label for="">Vincular usuários a está turma</label>
                    <div class="clearfix"></div>
                    {!! Form::select('user_id[]', \Modules\User\Entities\User::whereNotIn('id', \Modules\Team\Entities\TeamUser::where('team_id', '=', $edit->id)->lists('user_id')->toArray())->get()->lists('name_email', 'id')->toArray(),  null, ['class' => 'form-control select', 'multiple' => 'multiple']) !!}
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">
                        <i class="fa fa-save"></i> Salvar
                    </button>
                </div>
                {!! Form::close() !!}

                {!! Form::open(['route' => ['fatorcms.update.user.team', $edit->slug], 'method' => 'POST']) !!}
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td>Nome</td>
                        <td>E-mail</td>
                        <td>Telefone</td>
                        <td>Data de expiração</td>
                        <td>Data/Hora de cadastro</td>
                        <td>Opções</td>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($users as $user)
                        @if ($user->user)
                            <tr>
                                <td>{!! $user->user->name !!}</td>
                                <td>{!! $user->user->email !!}</td>
                                <td>{!! $user->user->socialize ?  $user->user->socialize->phone : '' !!}</td>
                                <td>
                                    {!! Form::hidden('save['.$user->user->id.'][user_id]', $user->user->id) !!}
                                    {!! Form::text('save['.$user->user->id.'][expire]',  $user->end_date ? dateToSql($user->end_date, 'Y-m-d', 'd/m/Y') : null,['class' => 'form-control s-datepicker','placeholder' => 'Experirar em']) !!}
                                </td>
                                <td>
                                    {!!  dateToSql($user->created_at, 'Y-m-d H:i:s', 'd/m/Y H:i:s') !!}
                                </td>
                                <td>
                                    {{--{!! Form::open(['route' => ['fatorcms.user.destroyUser', $user->user->id, $edit->id], 'onsubmit' => 'return false;', 'data-delete' => 'Deseja realmente deleta o usuário '. $user->name.'?', 'method' => 'DELETE']) !!}--}}
                                    <a class="btn btn-xs btn-danger" href="{!! route('fatorcms.user.destroyUser', [$user->user->id, $edit->id]) !!}">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                    {{--{!! Form::close() !!}--}}
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-lg-3">
                        <button class="btn btn-success" type="submit">
                            Salvar datas de expiração
                        </button>
                    </div>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>

@endsection