<?php namespace Modules\Team\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamPartner extends BaseModel
{

    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'team_id'
    ];

}