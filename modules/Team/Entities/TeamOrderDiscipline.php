<?php namespace Modules\Team\Entities;

use App\BaseModel;
use Modules\Lesson\Entities\Discipline;
use Modules\Lesson\Entities\Lesson;

class TeamOrderDiscipline extends BaseModel
{

    protected $fillable = ['discipline_id', 'team_id', 'order'];

    public function Discipline()
    {
        return $this->belongsTo(Discipline::class, 'discipline_id');
    }

    public function Lesson()
    {
        return $this->hasMany(Lesson::class, 'discipline_id', 'discipline_id');
    }

}