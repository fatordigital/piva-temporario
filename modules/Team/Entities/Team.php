<?php namespace Modules\Team\Entities;

use App\BaseModel;
use App\Events\FilesEvent;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Modules\Team\Entities\TeamPartner;

class Team extends BaseModel
{

    use SoftDeletes;

    protected $fillable = [
        'home_status', 'video_repeat', 'end_days', 'banner_status', 'course_category_id', 'course_id',
        'structure', 'value', 'value_discount', 'order', 'slug', 'name', 'max_student', 'begin_date', 'end_date', 'image', 'image_show',
        'icon', 'about', 'test', 'status'
    ];

    public $excel = [
        'Nome', 'Valor', 'Valor com desconto', 'Status', 'Data de registro', 'Data de alteração no registro'
    ];

    public $excelModel = [
        'name', 'value', 'value_discount', 'status', 'created_at', 'updated_at'
    ];

    public function TeamOrderDiscipline()
    {
        return $this->hasMany(TeamOrderDiscipline::class, 'team_id')->orderBy('order', 'asc');
    }


    public function loadTeam($slug)
    {
        $key = 'turma-' . $slug;

        cache()->forget($key);

        if (!cache()->has($key)) {

            $team = $this->has('TeamOrderDiscipline')->with(['TeamOrderDiscipline' => function ($team_order_discipline) {
                return $team_order_discipline->has('Discipline')->with(['Discipline' => function($discipline) {
                    return $discipline->select(['id', 'name', 'slug', 'description']);
                }])->select(['id', 'order', 'discipline_id', 'team_id']);
            }])->whereSlug($slug)
                ->whereRaw('( (DATE(`begin_date`) <= ? || ( `begin_date` IS NULL || `begin_date` = "0000-00-00" ) ) AND ( DATE(`end_date`) >= ? || ( `end_date` IS NULL || `end_date` = "0000-00-00" ) ) )', [Carbon::today(), Carbon::today()])
                ->select([
                    'id', 'course_id', 'video_repeat', 'about', 'image',
                    'course_category_id', 'name', 'slug'
                ])
                ->first();

            if (!$team)
                return false;

            cache()->put($key, $team, Carbon::now()->addDay());
        } else {
            $team = cache($key);
        }
        return $team;
    }


    public function category()
    {
        return $this->belongsTo('Modules\Course\Entities\CourseCategory', 'course_category_id');
    }

    public function course()
    {
        return $this->belongsTo('Modules\Course\Entities\Course', 'course_id');
    }

    public function students()
    {
        return $this->hasMany('Modules\Team\Entities\TeamUser', 'team_id');
    }

    public function lessons()
    {
        return $this->hasMany('Modules\Team\Entities\TeamLesson', 'team_id');
    }

    public function team_teachers()
    {
        return $this->hasMany('Modules\Team\Entities\TeamTeacher', 'team_id');
    }

    public function orderItems()
    {
        return $this->hasMany('Modules\Order\Entities\OrderItem', 'ref');
    }

    public function Partners()
    {
        return $this->hasMany('Modules\Team\Entities\TeamPartner', 'team_id');
    }

    public function getPriceAttribute()
    {
        return $this->value_discount != '' && $this->value_discount > 0 ? $this->value_discount : $this->value;
    }

    private function teamlesson($request, $id)
    {
        if ($request->has('lesson')) {
            $ids = [];
            $i = 0;
            foreach ($request->get('lesson') as $item) {
                if (isset($item['id']) && isset($id)) {
                    $edit = TeamLesson::whereTeamId($id)->whereLessonId($item['id'])->first();
                    if ($edit) {
//                    $edit->update(['user_create_id' => Auth::user()->id, 'order' => isset($item['order']) && $item['order'] > 0 ? $item['order'] : $i, 'begin_date' => dateToSql($item['begin_date']), 'end_date' => dateToSql($item['end_date'])]);
                        $edit->update([
                            'user_create_id' => Auth::user()->id,
//                        'order' => isset($item['order']) && $item['order'] > 0 ? $item['order'] : $i,
                            'begin_date' => dateToSql($item['begin_date'])
                        ]);
                        $ids[] = $edit->id;
                    } else {
//                    $return = TeamLesson::create(['user_create_id' => Auth::user()->id, 'order' => isset($item['order']) && $item['order'] > 0 ? $item['order'] : $i, 'team_id' => $id, 'lesson_id' => $item['id'], 'begin_date' => dateToSql($item['begin_date']), 'end_date' => dateToSql($item['end_date'])]);
//                    $return = TeamLesson::create(['user_create_id' => Auth::user()->id, 'order' => isset($item['order']) && $item['order'] > 0 ? $item['order'] : $i, 'team_id' => $id, 'lesson_id' => $item['id'], 'begin_date' => dateToSql($item['begin_date'])]);
                        $return = TeamLesson::create(['user_create_id' => Auth::user()->id, 'team_id' => $id, 'lesson_id' => $item['id'], 'begin_date' => dateToSql($item['begin_date'])]);
                        $ids[] = $return->id;
                    }
                }
                $i++;
            }

            $findC = TeamLesson::whereTeamId($id)->whereNotIn('id', $ids)->get();
            if ($findC->count()) {
                TeamLesson::whereTeamId($id)->whereNotIn('id', $ids)->delete();
            }
        } else {
            $findTeacher = TeamTeacher::whereTeamId($id)->get();
            if ($findTeacher->count()) {
                TeamTeacher::whereTeamId($id)->delete();
            }
        }
    }

    private function teamteachers($request, $id)
    {
        if ($request->has('teacher')) {
            $ids = [];
            foreach ($request->get('teacher') as $key => $item) {
                $edit = TeamTeacher::whereTeamId($id)->where('user_id', '=', $key)->first();
                if ($edit) {
                    $edit->update(['team_id' => $id, 'partner' => isset($item['partner']) ? true : false, 'user_id' => $key]);
                    $ids[] = $edit->id;
                } else {
                    $return = TeamTeacher::create(['team_id' => $id, 'partner' => isset($item['partner']) ? true : false, 'user_id' => $key]);
                    $ids[] = $return->id;
                }
            }
            $f1 = TeamTeacher::whereTeamId($id)->whereNotIn('id', $ids)->get();
            if ($f1->count())
                TeamTeacher::whereTeamId($id)->whereNotIn('id', $ids)->delete();
        } else {
            $f2 = TeamTeacher::whereTeamId($id)->get();
            if ($f2->count())
                TeamTeacher::whereTeamId($id)->delete();
        }
    }

    public function customCreate($request, $controller)
    {
        $request->merge(['slug' => $this->notRepeatModelString(str_slug($request->get('name', 'slug'), '-'), $this)]);
        $request->merge(['value' => coinToBco($request->get('value'))]);
        $request->merge(['value_discount' => coinToBco($request->get('value_discount'))]);
        if ($request->has('begin_date')) {
            $request->merge(['begin_date' => Carbon::createFromFormat('d/m/Y', $request->get('begin_date'))->format('Y-m-d')]);
            $request->merge(['end_date' => Carbon::createFromFormat('d/m/Y', $request->get('end_date'))->format('Y-m-d')]);
        }
        if ($request->hasFile('file')) {
            event(new FilesEvent($request->file('file'), array('1900x564', '1280x1024', '350x350'), 'assets/team/', function ($filename) use ($request) {
                $request->merge(['image' => $filename]);
            }));
//            $filename = $controller->customFileName($request, 'assets/team/', 'file');
//            if ($request->file('file')->move(public_path('assets/team/'), $filename)) {
//                $request->merge(['image' => $filename]);
//            }
        }
        if ($request->hasFile('image_showw')) {
            event(new FilesEvent($request->file('image_showw'), array('261x146'), 'assets/team/', function ($filename) use ($request) {
                $request->merge(['image_show' => $filename]);
            }, 'image_showw'));
        }
        if ($request->hasFile('icone')) {
            $filename = $controller->customFileName($request, 'assets/team/', 'icone');
            if ($request->file('icone')->move(public_path('assets/team/'), $filename)) {
                $request->merge(['icon' => $filename]);
            }
        }
        $return = $this->create($request->except(['_token']));
        $this->teamlesson($request, $return->id);
        $this->teamteachers($request, $return->id);
        return $return;
    }

    public function customUpdate($edit, $request, $controller)
    {
        $request->merge(['slug' => str_slug($request->get('name'), '-')]);
        $request->merge(['value' => coinToBco($request->get('value'))]);
        $request->merge(['value_discount' => coinToBco($request->get('value_discount'))]);
        if ($request->has('begin_date')) {
            $request->merge(['begin_date' => Carbon::createFromFormat('d/m/Y', $request->get('begin_date'))->format('Y-m-d')]);
            $request->merge(['end_date' => Carbon::createFromFormat('d/m/Y', $request->get('end_date'))->format('Y-m-d')]);
        }

        if ($request->hasFile('file')) {
            event(new FilesEvent($request->file('file'), array('1900x564', '1280x1024', '350x350'), 'assets/team/', function ($filename) use ($request) {
                $request->merge(['image' => $filename]);
            }));
        }
        if ($request->hasFile('image_showw')) {
            event(new FilesEvent($request->file('image_showw'), array('261x146'), 'assets/team/', function ($filename) use ($request) {
                $request->merge(['image_show' => $filename]);
            }, 'image_showw'));
        }
        if ($request->hasFile('icone')) {
            $filename = $controller->customFileName($request, 'assets/team/', 'icone');
            if ($request->file('icone')->move(public_path('assets/team/'), $filename)) {
                $request->merge(['icon' => $filename]);
            }
        }
        $this->teamlesson($request, $edit->id);
        $this->teamteachers($request, $edit->id);
        $this->teampartners($request, $edit->id);
        return $edit->update($request->except(['_token']));
    }

    public function teampartners($request, $id)
    {
        if ($request->has('partner_id')) {
            foreach ($request->get('partner_id') as $partner) {
                if (!empty($partner)) {
                    TeamPartner::firstOrCreate(['user_id' => $partner, 'team_id' => $id]);
                }
            }
        }
    }

    public static function previousError($row)
    {
        if ($row) {
            if (!$row->course)
                return ['msg' => 'A turma não está relacionada a um curso, caso esteja em andamento os alunos não terão acesso as aulas.', 'error' => true];
            else if (!$row->course_category_id)
                return ['msg' => 'A turma não está categorizada, não será possível apresentar para o usuário.', 'error' => true];
        }
        return false;
    }

    public function validTeams($id = [], $home = false)
    {
        $ids = [];
        if (!$home) {
            $teams = Team::has('course')->has('category')->whereStatus(true)
                ->whereNotIn('id', $id)
                ->whereRaw("(DATE(NOW()) <= DATE(`end_date`) OR `end_date` IS NULL OR `end_date` = '0000-00-00')")
                ->get(['id', 'max_student']);
        } else {
            $teams = Team::has('course')->has('category')->whereStatus(true)
                ->where('home_status', '=', true)
                ->whereNotIn('id', $id)
                ->whereRaw("(DATE(NOW()) <= DATE(`end_date`) OR `end_date` IS NULL OR `end_date` = '0000-00-00')")
                ->get(['id', 'max_student']);
        }

//        dd($teams->toArray());

        foreach ($teams as $team) {
            if ($team->max_student >= TeamUser::whereTeamId($team->id)->count())
                $ids[] = $team->id;
        }

        return Team::whereIn('id', $ids)->orderByRaw('CASE WHEN teams.order = 1 THEN teams.created_at ELSE teams.order END DESC');
    }

    public function userPartner()
    {
        return in_array(auth()->user()->id, $this->Partners ? $this->Partners->lists('user_id')->toArray() : []);
    }

}