<?php namespace Modules\Team\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Lesson\Entities\Discipline;
use Modules\Lesson\Entities\Lesson;
use Modules\User\Entities\UserTest;

class TeamUser extends BaseModel
{

    use SoftDeletes;

    protected $fillable = [
        'course_id',
        'team_id',
        'user_id',
        'alert',
        'end_date',
        'status'
    ];

    public function setEndDateAttribute($value)
    {
        if ($value && typeDate($value, 'd/m/Y'))
            $this->attributes['end_date'] = dateToSql($value);
        else {
            $this->attributes['end_date'] = $value;
        }
    }

    public function team()
    {
        return $this->belongsTo('Modules\Team\Entities\Team', 'team_id');
    }

    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'user_id', 'id');
    }

    public function lessons()
    {
        return $this->hasMany('Modules\Team\Entities\TeamLesson', 'team_id');
    }

    public static function getLessonContent($lesson_id, $without = null)
    {
        if ($lesson_id) {
            $tests = UserTest::whereUserId(auth()->user()->id)->whereNotNull('end_test')->lists('test_id')->toArray();
            $lessons = Lesson::has('contents')->with(['discipline', 'contents' => function ($q) use ($without, $tests) {
                if ($tests) {
                    $q->has('contents')->with(['contents', 'tests'])->whereRaw('(DATE(`begin_date`) < NOW())')->orderBy('order', 'asc');
                } else {
                    $q->has('contents')->with(['contents', 'tests'])->whereRaw('(DATE(`begin_date`) < NOW())')->orderBy('order', 'asc');
                }
            }])->whereId($lesson_id)->orderBy('created_at', 'desc')->first();
            return $lessons;
        }
        return false;
    }

    public function UserContentEnds()
    {
        return $this->hasMany('Modules\User\Entities\UserContentEnd', 'user_id', 'user_id')->where('team_id', '=', $this->team_id)->where('status', '=', 1);
    }


    public static function getDisciplines($team_id, $discipline_id = null)
    {
        $team = self::whereTeamId($team_id)->first();
        $order = TeamOrderDiscipline::whereTeamId($team_id)->orderBy('order')->get()->lists('discipline_id');
        if ($team) {
            $lessons = TeamLesson::whereTeamId($team->team_id)->orderBy('order', 'desc')->get()->lists(['lesson_id'])->toArray();
            if ($lessons) {
                if (!$discipline_id) {
                    if (!$order->count()) {
                        return Discipline::whereIn('id', Lesson::whereIn('id', $lessons)->lists('discipline_id')->toArray())->get();
                    } else {
                        return Discipline::whereIn('id', Lesson::whereIn('id', $lessons)->lists('discipline_id')->toArray())
                            ->orderByRaw('FIELD(id, "' . implode('","', $order->toArray()) . '")')->get();
                    }
                } else {
                    if ($order->count()) {
                        return Discipline::whereId($discipline_id)->orderByRaw('FIELD(id, "' . implode('","', $order->toArray()) . '")')->get();
                    } else {
                        return Discipline::whereId($discipline_id)->get();
                    }
                }
            }
        }
        return [];
    }

    public static function getLessonIds($team_id, $user_id, $validate = false, $discipline_id = null)
    {
        $discipline = null;
        if (request()->has('disciplina')) {
            $discipline = Discipline::whereSlug(request('disciplina'))->first();
        }
        $team = self::whereTeamId($team_id)->whereUserId($user_id)->first();
        if ($team) {
            $lessons = TeamLesson::whereTeamId($team->team_id)->get()->lists(['lesson_id'])->toArray();
            if ($lessons) {
                $tests = UserTest::whereUserId(auth()->user()->id)->whereNotNull('end_test')->lists('test_id')->toArray();
                if ($validate)
                    return true;
                if (!$discipline && !$discipline_id) {
                    return Lesson::with(['contents' => function ($q) use ($tests) {
                        if ($tests) {
                            $q->with(['contents', 'tests'])->whereRaw('(`test_id` NOT IN (?) OR `test_id` IS NULL)', [$tests])->orderBy('created_at', 'desc');
                        } else {
                            $q->with(['contents', 'tests'])->whereRaw('(`test_id` IS NULL)', [$tests])->orderBy('created_at', 'desc');
                        }
                    }])->whereIn('id', $lessons)->get();
                } else if ($discipline_id) {
                    return Lesson::with(['contents' => function ($q) use ($tests) {
                        if ($tests) {
                            $q->with(['contents', 'tests'])->whereRaw('(`test_id` NOT IN (?) OR `test_id` IS NULL)', [$tests])->orderBy('created_at', 'desc');
                        } else {
                            $q->with(['contents', 'tests'])->whereRaw('(`test_id` IS NULL)', [$tests])->orderBy('created_at', 'desc');
                        }
                    }])->where('discipline_id', '=', $discipline_id)->whereIn('id', $lessons)->get();
                } else {
                    return Lesson::with(['contents' => function ($q) use ($tests) {
                        if ($tests) {
                            $q->with(['contents', 'tests'])->whereRaw('(`test_id` NOT IN (?) OR `test_id` IS NULL)', [$tests])->orderBy('created_at', 'desc');
                        } else {
                            $q->with(['contents', 'tests'])->whereRaw('(`test_id` IS NULL)', [$tests])->orderBy('created_at', 'desc');
                        }
                    }])->where('discipline_id', '=', $discipline->id)->whereIn('id', $lessons)->get();
                }
            }
        }
        return [];
    }

    public static function getLessonsByDisciplines($disciplines)
    {
        $lesson_ids = TeamLesson::whereTeamId(session('current_team'))->get()->lists(['lesson_id'])->toArray();
        $tests = UserTest::whereUserId(auth()->user()->id)->whereNotNull('end_test')->lists('test_id')->toArray();
        $lessons = Discipline::with(['Lessons' => function ($q) use ($lesson_ids, $tests) {
            $q->with(['contents' => function($q) use ($lesson_ids, $tests) {
                if ($tests) {
                    $q->with(['contents', 'tests'])->whereRaw('(`test_id` NOT IN (?) OR `test_id` IS NULL)', [$tests])->orderByRaw('CASE WHEN lesson_contents.order = 1 THEN lesson_contents.created_at ELSE lesson_contents.order END DESC');
                } else {
                    $q->with(['contents', 'tests'])->orderByRaw('CASE WHEN lesson_contents.order = 1 THEN lesson_contents.created_at ELSE lesson_contents.order END DESC');
                }
            }])->whereIn('id', $lesson_ids);
        }])->whereIn('id', $disciplines)->orderBy('created_at', 'desc')->get();
        return $lessons;
    }

}