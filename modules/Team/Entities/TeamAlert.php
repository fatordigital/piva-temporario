<?php namespace Modules\Team\Entities;

use App\BaseModel;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Mail;

class TeamAlert extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'read',
        'user_id',
        'team_id',
        'message',
        'end_date'
    ];

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function Team()
    {
        return $this->belongsTo('Modules\Team\Entities\Team', 'team_id');
    }

    public function sendTeam($request)
    {
        if (count($request->get('team_id'))) {
            $users = User::whereIn('id', TeamUser::whereIn('team_id', $request->get('team_id'))->lists('user_id')->toArray())->get();
            $this->loadSend($users, $request->get('description'));
            foreach ($request->get('team_id') as $team) {
                TeamAlert::create(['team_id' => $team, 'message' => $request->get('description'), 'end_date' => $request->get("end_date")]);
            }
            return true;
        }
        return false;
    }

    public function sendUser($request)
    {
        if (count($request->get('user_id'))) {
            $users = User::whereIn('id', $request->get('user_id'))->get();
            $this->loadSend($users, $request->get('description'));
            foreach ($request->get('user_id') as $user) {
                TeamAlert::create(['user_id' => $user, 'message' => $request->get('description'), 'end_date' => $request->get("end_date")]);
            }
            return true;
        }
        return false;
    }

    public function loadSend($users, $description)
    {
        foreach ($users as $user) {
            $view = str_replace(array('{usuario.nome}', '{turma.nome}', '{turma.data_inicial}', '{turma.data_final}'), array($user->name), $description);
            Mail::send('team::mail', ['content' => $view], function ($m) use ($user) {
//                $m->from('andrew.rodrigues@fatordigital.com.br', 'Andrew Rodrigues Brunoro');
                $m->from(env('CLIENT_EMAIL'), env('CLIENT_NAME'));
                $m->subject('Notificação - Professor Piva');
                $m->to($user->email);
            });
        }
    }

}