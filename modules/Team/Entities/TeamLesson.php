<?php namespace Modules\Team\Entities;

use App\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamLesson extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["user_create_id", "order", "team_id", "lesson_id", "begin_date", "end_date"];

    protected $appends = [
        'Begin',
        'End'
    ];


    public static function TeamDiscipline($team_id, $discipline_id)
    {
        $key = $team_id . '-' . $discipline_id;

        cache()->forget($key);

        if (!cache()->has($key)) {
            $cache = self::has('lesson')->with('lesson')->whereHas('lesson', function ($q) use ($discipline_id) {
                return $q->where('discipline_id', '=', $discipline_id);
            })->whereTeamId($team_id)->orderBy('order')->get(['*', \DB::raw('( ( DATE(`begin_date`) <= "' . Carbon::today() . '" || ( `begin_date` IS NULL || `begin_date` = "0000-00-00" ) ) && ( DATE(`end_date`) <= "' . Carbon::today() . '" || ( `end_date` IS NULL || `end_date` = "0000-00-00" ) ) ) AS Access')]);

            if (!$cache)
                return [];

            cache()->put($key, $cache, Carbon::now()->addDay());
        } else {
            $cache = cache($key);
        }
        return $cache;
    }

    public function team()
    {
        return $this->belongsTo('Modules\Team\Entities\Team', 'team_id');
    }

    public function lesson()
    {
        return $this->belongsTo('Modules\Lesson\Entities\Lesson', 'lesson_id');
    }


}