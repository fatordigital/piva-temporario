<?php namespace Modules\Team\Entities;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class TeamTeacher extends BaseModel
{

    use SoftDeletes;

    protected $fillable = ["user_id", "partner", "team_id"];

    public function user()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'user_id');
    }

    public function team()
    {
        return $this->belongsTo('Modules\Team\Entities\Team', 'team_id');
    }

}