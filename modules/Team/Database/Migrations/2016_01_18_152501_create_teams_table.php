<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateTeamsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('teams', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_id')->nullable();
                $table->integer('module_id')->nullable();
                $table->string('name');
                $table->integer('approved')->nullable();
                $table->integer('disapproved')->nullable();
                $table->integer('max_student')->nullable();
                $table->date('begin_date')->nullable();
                $table->date('end_date')->nullable();
                $table->boolean('status')->default(true);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('teams');
        }

    }
