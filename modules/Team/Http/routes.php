<?php

    Route::group([
        'prefix'     => 'fatorcms',
        'middleware' => [
            'auth',
            'acl',
            'cms'
        ],
        'namespace'  => 'Modules\Team\Http\Controllers'
    ], function () {
        Route::resource('team', 'TeamController');
        Route::patch('/team/status/{id}', [
            'as'   => 'fatorcms.team.status',
            'uses' => 'TeamController@status'
        ]);
        Route::post('/team-alert', [
            'uses' => 'TeamController@alert',
            'as'   => 'team.alert'
        ]);
        Route::post('save-user-to-team/{slug}', ['as' => 'fatorcms.save.user.team', 'uses' => 'TeamController@saveUserTeam']);
    });

    Route::post('/fatorcms/update-user-to-team/{slug}', ['as' => 'fatorcms.update.user.team', 'uses' => 'Modules\Team\Http\Controllers\TeamController@updateUserTeam']);

    Route::post('/fatorcms/update-order-discipline', ['as' => 'fatorcms.update.order.discipline', 'uses' => 'Modules\Team\Http\Controllers\TeamController@updateDisciplineOrder']);

    Route::get('/fatorcms/{slug}/financial-report', ['middleware' => 'cms', 'as' => 'fatorcms.team.financial', 'uses' => 'Modules\Team\Http\Controllers\TeamController@financialReport']);
    Route::get('/fatorcms/get-disciplines.json', ['as' => 'fatorcms.disciplines.json', 'uses' => 'Modules\Team\Http\Controllers\TeamController@getDisciplines']);

    Route::post('/fatorcms/adicionar-professor/{slug}', ['as' => 'turma.professor', 'uses' => 'Modules\Team\Http\Controllers\TeamController@adicionarProfessor']);