<?php namespace Modules\Team\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Modules\Course\Entities\Course;
use Modules\Course\Entities\CourseCategory;
use Modules\Lesson\Entities\Discipline;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamAlert;
use Modules\Team\Entities\TeamLesson;
use Modules\Team\Entities\TeamOrderDiscipline;
use Modules\Team\Entities\TeamUser;
use Modules\Team\Http\Requests\TeamRequest;

class TeamController extends BaseModuleController
{

    public function getDisciplines()
    {
        return response()->json(['error' => false, 'data' => Discipline::all()->toArray()], 200);
    }

    public function lists()
    {
        $courses = Course::orderBy('id', 'desc')->lists('name', 'id')->toArray();
        $teams = Team::orderBy('name')->lists('name', 'id')->toArray();
        if (auth()->user()->isAdmin())
            $users = User::orderBy('name')->get()->lists('NameEmail', 'id')->toArray();
        else
            $users = User::where('group_id', '!=', 1)->orderBy('name')->get()->lists('NameEmail', 'id')->toArray();

        View::share(compact('courses', 'modules', 'users', 'teams'));
    }

    public function index()
    {
        $this->lists();
        $categories = CourseCategory::orderBy('name')->get();
        $list = Team::with('course')->orderByRaw('CASE WHEN teams.order = 1 THEN teams.created_at ELSE teams.order END DESC');
        if (request()->has('search')) {
            foreach (request()->get("search") as $key => $value) {
                if (!empty($value) && $key != 'type') {
                    $list->where($key, 'LIKE', '%' . $value . '%');
                } else if ($key == 'type' && !empty($value)) {
                    $list->where($value, '=', true);
                }
            }
        }
        request()->session()->flashInput(request()->all());
        if (request()->has('export')) {
            $this->exportSimpleExcel(new Team(), $list, 'Turmas');
        }
        $list = $list->paginate(15);
        return view('team::index', compact('list', 'categories'));
    }

    public function show($slug, Team $model)
    {
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.material.index')->with('error', 'Ops, registro não foi encontrado.');
        return view('team::show', [
            'edit'  => $edit,
            'users' => TeamUser::where('team_id', '=', $edit->id)->get()
        ]);
    }

    public function create()
    {
        $this->lists();
        if (request()->has('course'))
            request()->session()->flashInput(['course_id' => request('course')]);
        return view('team::create');
    }

    public function store(TeamRequest $request, Team $model)
    {
        cache()->flush();

        if ($save = $model->customCreate($request, $this))
            return redirect()->route('fatorcms.team.edit', $save->slug)->with('success', 'Turma ' . $request->get('name') . ', foi registrada com sucesso.');
        else
            return redirect()->route('fatorcms.team.create')->with('error', 'Ops, não foi possível salvar a turma, tente novamente.')->withInput($request->except(['_token']));
    }

    public function edit($slug, Team $model)
    {
        cache()->flush();

        $this->lists();
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.material.index')->with('error', 'Ops, registro não foi encontrado.');
        
        $disciplines = TeamUser::getDisciplines($edit->id);
        if ($edit->begin_date != '0000-00-00' && $edit->begin_date != '') {
            $edit->begin_date = Carbon::createFromFormat('Y-m-d', $edit->begin_date)->format('d/m/Y');
        }
        if ($edit->end_date != '0000-00-00' && $edit->end_date != '') {
            $edit->end_date = Carbon::createFromFormat('Y-m-d', $edit->end_date)->format('d/m/Y');
        }
        if (!Auth::user()->isAdmin()) {
            $users = TeamUser::where('team_id', '=', $edit->id)->where('user_id', '!=', 1)->get();
        } else {
            $users = TeamUser::where('team_id', '=', $edit->id)->get();
        }
        return view('team::edit', compact('edit', 'users', 'disciplines'));
    }

    public function update(TeamRequest $request, Team $model, $slug)
    {
        cache()->flush();

        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.team.edit', $slug)->with('error', 'Ops, registro não foi encontrado.');
        if ($model->customUpdate($edit, $request, $this)) {
            return redirect()->route('fatorcms.team.edit', $slug)->with('success', trans('messages.update.success', ['name' => $request->get('name')]));
        } else
            return redirect()->back()->with('error', 'Ops, não foi possível atualizar a turma, tente novamente.');
    }

    public function status($id)
    {
        $status = Team::findOrFail($id);
        $to = ($status->status) ? 0 : 1;
        $status->status = $to;
        $status->save();
        return redirect()->route('fatorcms.team.index')->with('success', trans('messages.status.success', [
            'status' => ($to) ? trans('dictionary.status_text.active') : trans('dictionary.status_text.inactive')
        ]));
    }

    public function destroy(Team $model, $id)
    {
        $destroy = $model->find($id);
        if ($destroy)
            $return = $destroy->delete();
        else
            $return = false;
        if ($return)
            return redirect()->route('fatorcms.team.index')->with('success', trans('messages.delete.success'));
        else
            return redirect()->route('fatorcms.team.index')->with('error', 'Não foi possível deletar o registro, tente novamente.');
    }

    public function alert(Request $request, TeamAlert $model)
    {
        if ($request->get('team_id'))
            $model->sendTeam($request);
        else
            $model->sendUser($request);

        if (strstr(URL::previous(), 'team-alert')) {
            return redirect()->route('fatorcms.team.index')->with('success', 'Os e-mails estão sendo enviados.');
        } else {
            return redirect(URL::previous())->with('success', 'Os e-mails estão sendo enviados');
        }
    }

    public function saveUserTeam($slug, Team $model)
    {
        $edit = $model->whereSlug($slug)->first();
        if (!$edit)
            return redirect()->route('fatorcms.material.index')->with('error', 'Ops, registro não foi encontrado.');

        if (request()->has('user_id')) {
            foreach (request('user_id') as $id) {
                $exists = TeamUser::whereTeamId($edit->id)->whereUserId($id)->first();
                if (!$exists) {
                    TeamUser::create([
                        'end_date' => $edit->end_days && $edit->end_days > 0 ? date('Y-m-d', strtotime('+' . $edit->end_days . ' days')) : $edit->end_date,
                        'user_id' => $id,
                        'team_id' => $edit->id,
                        'course_id' => $edit->course_id
                    ]);
                }
            }
        }
        return redirect()->route('fatorcms.team.show', $edit->slug)->with('success', 'Usuários vinculados com sucesso.');
    }

    public function financialReport($slug, Team $model)
    {
        $list = $model->with('OrderItems')->whereSlug($slug)->first();
        if (!$list)
            return redirect()->route('fatorcms.material.index')->with('error', 'Ops, registro não foi encontrado.');
        return view('team::financial-report', compact('list'));
    }

    public function updateUserTeam($slug)
    {
        if (request()->has('save')) {
            $team = new Team();
            $team = $team->whereSlug($slug)->first();
            foreach (request('save') as $item) {
                $team_user = TeamUser::whereUserId($item['user_id'])->whereTeamId($team->id)->first();
                if ($team_user) {
                    $status = $team_user->update(['end_date' => $item['expire'], 'alert' => 0]);
                }
            }
        } else {
            return redirect()->back()->with('error', 'Ops!');
        }
        return redirect()->route('fatorcms.team.show', $slug)->with('success', 'Todos os registros foram atualizados.');
    }

    public function updateDisciplineOrder()
    {
        if (request()->has('list') && request()->has('team_id')) {
            foreach (request('list') as $order => $item) {
                $d = str_replace('d-', '', $item['id']);
                $tod = TeamOrderDiscipline::whereTeamId(request('team_id'))
                    ->whereDisciplineId($d)->first();
                if (!$tod) {
                    TeamOrderDiscipline::create([
                        'team_id' => request('team_id'),
                        'discipline_id' => $d,
                        'order' => $order
                    ]);
                } else {
                    $tod->update(['order' => $order]);
                }

                if (isset($item['children'])) {
                    foreach ($item['children'] as $order_child => $child) {
                        $id_child = str_replace('l-', '', $child['id']);
                        $tol = TeamLesson::whereTeamId(request('team_id'))
                            ->whereLessonId($id_child)
                            ->first();
                        if ($tol) {
                            $tol->update(['order' => $order_child]);
                        }
                    }
                }
            }
        }
    }

    public function adicionarProfessor($slug, Request $request)
    {
        dd($slug, $request->all());
    }

}