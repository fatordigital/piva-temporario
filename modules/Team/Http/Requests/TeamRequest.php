<?php namespace Modules\Team\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Support\Facades\Validator;

class TeamRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'file' => 'dimensions:1900,564'
        ];
    }

    public function messages()
    {
        return [
            'file.dimensions' => 'A imagem tem que ter o tamanho de 1900 x 564',
            'image_showw.dimensions' => 'A imagem tem que ter o tamanho de 261 x 146'
        ];
    }

}
