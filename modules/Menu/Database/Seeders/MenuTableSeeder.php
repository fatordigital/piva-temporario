<?php namespace Modules\Menu\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Menu\Entities\Menu;

class MenuTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = Menu::firstOrCreate([
            'parent_id'   => 0,
            'user_id'     => 1,
            'icon_id'     => 0,
            'name'        => 'Menu Principal',
            'route'       => '',
            'seo_url'     => 'menu-principal',
            'filename'    => '',
            'attribute'   => '',
            'description' => '',
            'status'      => true
        ]);
        Menu::firstOrCreate([
            'parent_id'   => $menu->id,
            'user_id'     => 1,
            'icon_id'     => 0,
            'name'        => 'Menus',
            'route'       => 'menu.index',
            'seo_url'     => 'menus',
            'filename'    => '',
            'attribute'   => '',
            'description' => '',
            'status'      => true
        ]);

    }

}