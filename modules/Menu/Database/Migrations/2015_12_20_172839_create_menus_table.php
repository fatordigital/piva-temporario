<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateMenusTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('menus', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('parent_id')->default(0);
                $table->integer('user_id')->nullable()->unsigned();
                $table->integer('icon_id')->nullable();
                $table->string('name', '30');
                $table->string('route', '30');
                $table->string('seo_url', '100')->nullable();
                $table->string('filename', '255')->nullable();
                $table->string('attribute', '255')->default('target="_self"');
                $table->text('description')->nullable();
                $table->integer('order')->default(0);
                $table->boolean('status')->default(1);
                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('menus');
        }

    }
