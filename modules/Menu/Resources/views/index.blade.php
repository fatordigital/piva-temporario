@extends('layouts.cms')

@section('content')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a href="{!! URL::route('fatorcms.menu.create') !!}"
               class="btn btn-success pull-right">
                {!! trans('dictionary.create') !!} Menu
            </a>

            <h2>Menus</h2>
        </div>
        <div class="ibox-content">

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover datatable">
                    <thead>
                    <tr>
                        <th data-ref="name">{!! trans('menu::db.name') !!}</th>
                        <th data-ref="route">{!! trans('menu::db.route') !!}</th>
                        <th data-ref="seo_url">{!! trans('menu::db.seo_url') !!}</th>
                        <th data-ref="params">{!! trans('menu::db.attribute') !!}</th>
                        <th data-ref="status">{!! trans('dictionary.status') !!}</th>
                        <th data-ref="created_at">{!! trans('dictionary.created_at') !!}</th>
                        <th width="11%">{!! trans('dictionary.options') !!}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if ($list->count())
                        @foreach($list as $row)
                            <tr>
                                <td>{!! $row->name !!}</td>
                                <td>{!! $row->route !!}</td>
                                <td>{!! $row->seo_url !!}</td>
                                <td>{!! $row->attribute !!}</td>
                                <td>{!! $row->StatusText !!}</td>
                                <td>{!! $row->created_at !!}</td>
                                <td>
                                    <a class="btn btn-primary btn-xs pull-left margin-right"
                                       href="{!! URL::route('fatorcms.menu.edit', $row->id) !!}">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    {!! Form::open(['route' => ['fatorcms.menu.status', $row->id], 'method' => 'PATCH', 'class' => 'form-horizontal pull-left']) !!}
                                    <button class="btn {!! ($row->status) ? 'btn-success' : 'btn-danger' !!} btn-xs margin-right"
                                            title="{!! $row->StatusText !!}">
                                        <i class="glyphicon {!! ($row->status) ? 'glyphicon-ok' : 'glyphicon-off' !!}"></i>
                                    </button>
                                    {!! Form::close() !!}

                                    @if ($row->parents->count())
                                    <button data-url="{!! URL::route('fatorcms.menu.destroy', $row->id) !!}"
                                            data-text="{!! ($row->parents->count()) ? trans('messages.delete.alert') : '' !!}"
                                            class="btn btn-danger btn-xs sweetBt-delete">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                    @else
                                        {!! Form::open(['route' => ['fatorcms.menu.destroy', $row->id], 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                                        <button class="btn btn-danger btn-xs">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                            @if ($row->parents->count())
                                @foreach($row->parents as $parent)
                                    <tr>
                                        <td>{!! $parent->name !!}</td>
                                        <td>{!! $parent->route !!}</td>
                                        <td>{!! $parent->seo_url !!}</td>
                                        <td>{!! $parent->attribute !!}</td>
                                        <td>{!! $parent->StatusText !!}</td>
                                        <td>{!! $parent->created_at !!}</td>
                                        <td>
                                            <a class="btn btn-primary btn-xs pull-left margin-right"
                                               href="{!! URL::route('fatorcms.menu.edit', $parent->id) !!}">
                                                <i class="fa fa-pencil"></i>
                                            </a>

                                            {!! Form::open(['route' => ['fatorcms.menu.status', $parent->id], 'method' => 'PATCH', 'class' => 'form-horizontal pull-left']) !!}
                                            <button class="btn {!! ($parent->status) ? 'btn-success' : 'btn-danger' !!} btn-xs margin-right"
                                                    title="{!! $parent->StatusText !!}">
                                                <i class="glyphicon {!! ($parent->status) ? 'glyphicon-ok' : 'glyphicon-off' !!}"></i>
                                            </button>
                                            {!! Form::close() !!}

                                            @if ($parent->parents->count())
                                                <button data-url="{!! URL::route('fatorcms.menu.destroy', $parent->id) !!}"
                                                        data-text="{!! ($parent->parents->count()) ? trans('messages.delete.alert') : '' !!}"
                                                        class="btn btn-danger btn-xs sweetBt-delete">
                                                <i class="fa fa-trash-o"></i>
                                                </button>
                                            @else
                                                {!! Form::open(['route' => ['fatorcms.menu.destroy', $row->id], 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                                                <button class="btn btn-danger btn-xs">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                                {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                    @if ($parent->parents->count())
                                        @foreach($parent->parents as $third)
                                            <tr>
                                                <td>{!! $third->name !!}</td>
                                                <td>{!! $third->route !!}</td>
                                                <td>{!! $third->seo_url !!}</td>
                                                <td>{!! $third->attribute !!}</td>
                                                <td>{!! $third->StatusText !!}</td>
                                                <td>{!! $third->created_at !!}</td>
                                                <td>
                                                    <a class="btn btn-primary btn-xs pull-left margin-right"
                                                       href="{!! URL::route('fatorcms.menu.edit', $third->id) !!}">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>

                                                    {!! Form::open(['route' => ['fatorcms.menu.status', $third->id], 'method' => 'PATCH', 'class' => 'form-horizontal pull-left']) !!}
                                                    <button class="btn {!! ($third->status) ? 'btn-success' : 'btn-danger' !!} btn-xs margin-right"
                                                            title="{!! $third->StatusText !!}">
                                                        <i class="glyphicon {!! ($third->status) ? 'glyphicon-ok' : 'glyphicon-off' !!}"></i>
                                                    </button>
                                                    {!! Form::close() !!}

                                                    {!! Form::open(['route' => ['fatorcms.menu.destroy', $row->id], 'method' => 'DELETE', 'class' => 'form-horizontal pull-left']) !!}
                                                        <button class="btn btn-danger btn-xs">
                                                        <i class="fa fa-trash-o"></i>
                                                    </button>
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7">Nenhum registro encontrado.</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                @include('layouts.elements.paginator', ['paginator' => $list])
            </div>

        </div>
    </div>
@endsection