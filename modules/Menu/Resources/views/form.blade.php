<div class="col-lg-6">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="form-group">
                <label for="status"
                       class="control-label">{!! trans('dictionary.status') !!}</label>
                <br/>

                <div class="radio radio-success radio-inline">
                    {!! Form::radio('status', 1, true) !!}
                    <label for="radio4">
                        {!! trans('dictionary.status_text.active') !!}
                    </label>
                </div>
                <div class="radio radio-danger radio-inline">
                    {!! Form::radio('status', 0, false) !!}
                    <label for="radio4">
                        {!! trans('dictionary.status_text.inactive') !!}
                    </label>
                </div>
            </div>

            <div class="form-group {!! ($errors->first('name')) ? 'has-error' : '' !!}">
                <label for="name">{!! trans('menu::db.name'). ' (*)' !!}</label>
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => trans('menu::db.name'). ' (*)', 'required' => 'required']) !!}
                {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group {!! ($errors->first('route')) ? 'has-error' : '' !!}">
                <label for="parent_id">
                    {!! trans('menu::db.route') !!} ({!! trans('dictionary.optional') !!})
                </label>
                {!! Form::select('route', ['' => trans('dictionary.select', ['name' => trans('menu::db.route')])] + $routes, null, ['class' => 'form-control', 'id' => 'select']) !!}
                {!! $errors->first('route', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group {!! ($errors->first('url')) ? 'has-error' : '' !!}">
                <label for="url">
                    Params
                </label>
                {!! Form::text('url', null, ['class' => 'form-control']) !!}
                {!! $errors->first('url', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group {!! ($errors->first('filename')) ? 'has-error' : '' !!}">
                <label for="filename">{!! trans('menu::db.filename') !!} ({!! trans('dictionary.optional') !!}
                    )</label>
                {!! Form::file('filename', ['class' => 'form-control']) !!}
                {!! $errors->first('filename', '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>
</div>

<div class="col-lg-6">
    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="form-group {!! ($errors->first('parent_id')) ? 'has-error' : '' !!}">
                <label for="parent_id">Menu ({!! trans('dictionary.optional') !!})</label>
                {!! Form::select('parent_id', ['' => trans('dictionary.select', ['name' => 'menu'])] + $parents, null, ['class' => 'form-control', 'id' => 'select']) !!}
                {!! $errors->first('parent_id', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group {!! ($errors->first('attribute')) ? 'has-error' : '' !!}">
                <label for="attribute">{!! trans('menu::db.attribute') !!}
                    ({!! trans('dictionary.optional') !!})
                </label>
                {!! Form::text('attribute', null, ['class' => 'form-control', 'placeholder' => trans('menu::db.attribute')]) !!}
                {!! $errors->first('attribute', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group {!! ($errors->first('icon_id')) ? 'has-error' : '' !!}">
                <label for="icon">{!! trans('menu::db.icon') !!} ({!! trans('dictionary.optional') !!})
                    <i id="brand"></i>
                </label>
                {!! Form::select('icon_id', ['' => trans('dictionary.icon')] + $icons, null, ['class' => 'form-control', 'id' => 'select-icon']) !!}
                {!! $errors->first('icon_id', '<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group {!! ($errors->first('order')) ? 'has-error' : '' !!}">
                <label for="order">
                    {!! trans('dictionary.order') !!} ({!! trans('dictionary.optional') !!})
                </label>
                {!! Form::text('order', null, ['class' => 'form-control', 'placeholder' => trans('dictionary.order'), 'required' => 'required']) !!}
                {!! $errors->first('order', '<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 menu">
    <div class="ibox">
        <div class="ibox-content">
            <h2>{!! trans('menu::db.description') !!}</h2>
            {!! Form::textarea('description', null, ['id' => 'description', 'placeholder' => '...']) !!}
            {!! $errors->first('description', '<span class="help-block">:message</span>') !!}
            <button class="btn btn-success pull-right">{!! trans('menu::db.btn.submit') !!}</button>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

@section('cjs')
    {!! HTML::script('js/cms/scripts/menu/menu.js') !!}
@endsection