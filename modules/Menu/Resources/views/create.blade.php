@extends('layouts.cms')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox-title">
                <h4 class="panel-title">{!! trans('dictionary.create') !!} Menu</h4>
                <p>{!! trans('messages.form.alert.fields_required') !!}</p>
            </div>
        </div>
        {!! Form::open(['route' => 'fatorcms.menu.store']) !!}
        @include('menu::form')
        {!! Form::close() !!}
    </div>
@endsection