<?php

    $return = [

        'name'        => 'Nome',
        'route'       => 'Rota',
        'seo_url'     => 'Url',
        'icon'        => 'Ícone',
        'description' => 'Descrição do Menu',
        'attribute'   => 'Atributos',
        'filename'    => 'Imagem',
        'alert'       => [
            'add_links' => 'Adicionar Links'
        ],
        'btn'         => [
            'submit' => 'Salvar Menu'
        ]
    ];

    return $return;