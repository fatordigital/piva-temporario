<?php

    $return = [
        'name'        => 'Name',
        'route'       => 'Route',
        'seo_url'     => 'Url',
        'icon'        => 'Icon',
        'description' => 'Menu Description',
        'attribute'   => 'Attribute',
        'filename'    => 'Image',
        'alert'       => [
            'add_links' => 'Add Links'
        ],
        'btn'         => [
            'submit' => 'Save Menu'
        ]
    ];

    return $return;