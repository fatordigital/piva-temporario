<?php namespace Modules\Menu\Entities;

use App\BaseModel;
use Illuminate\Support\Facades\Auth;
use Modules\Acl\Entities\Action;

class Menu extends BaseModel
{

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'url',
        'user_id',
        'status',
        'parent_id',
        'route',
        'seo_url',
        'icon_id',
        'filename',
        'attribute',
        'description',
        'order'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('Modules\Menu\Entities\Menu', 'parent_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parents()
    {
        return $this->hasMany('Modules\Menu\Entities\Menu', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function icon()
    {
        return $this->belongsTo('App\Icon', 'icon_id');
    }


    /**
     * @param int $status
     * @return mixed
     */
    public static function getMenu($status = 1)
    {
        if (Auth::user() && !Auth::user()->isAdmin()) {
            $menu = Menu::with([
                'parents' => function ($q) use ($status) {
                    $q->whereIn('route', Auth::user()->getActions())->whereStatus($status)->orderBy('order', 'asc');
                }
            ])->whereParentId(0)->whereStatus($status)->orderBy('order', 'asc')->get();
        } else {
            $menu = Menu::with('parent')->whereParentId(0)->whereStatus($status)->orderBy('order', 'asc')->get();
        }
        return $menu;
    }

}