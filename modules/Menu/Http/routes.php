<?php
    Route::group([
        'prefix'     => 'fatorcms',
        'middleware' => [
            'auth',
            'acl',
            'cms'
        ],
        'namespace'  => 'Modules\Menu\Http\Controllers'
    ], function () {
        Route::resource('menu', 'MenuController');
        Route::patch('menu/status/{id}', [
            'as'   => 'fatorcms.menu.status',
            'uses' => 'MenuController@status'
        ]);
    });