<?php namespace Modules\Menu\Http\Controllers;

use App\Http\Controllers\BaseModuleController;
use App\Icon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Modules\Acl\Entities\Action;
use Modules\Menu\Entities\Menu;
use Modules\Menu\Http\Requests\MenuRequest;

class MenuController extends BaseModuleController
{

    /**
     * Compartilha com as views um array de select do parents (menus) e icones da tabela icons
     */
    public function lists()
    {
        $routes = Action::all()->lists('route', 'route')->toArray();
        $parents = Menu::all()->lists('name', 'id')->toArray();
        $icons = Icon::all()->lists('icon', 'id')->toArray();
        View::share(compact('parents', 'icons', 'routes'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $list = Menu::with([
            'parents' => function ($q) {
                if (!Auth::user()->isAdmin()) {
                    $q->whereIn('route', Auth::user()->getActions())->orderBy('order', 'asc');
                }
            }
        ])->whereParentId(0)->orderBy('order', 'asc')->paginate(10);
        return view('menu::index', compact('list'));
    }

    /**
     * @return View
     */
    public function create()
    {
        $this->lists();
        return view('menu::create');
    }

    /**
     * @param MenuRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * Recebe o request do formulário do menu (apenas novos inserts) METODO POST
     */
    public function store(MenuRequest $request)
    {
        $request->merge([
            'seo_url' => Str::slug($request->get('name'), '-'),
            'user_id' => Auth::user()->id
        ]);
        Menu::create($request->except(['_token']));
        return redirect()->route('fatorcms.menu.index')->with('success', trans('messages.insert.success'));
    }

    /**
     * @param $id
     * @return View
     */
    public function edit($id)
    {
        $this->lists();
        $edit = Menu::findOrFail($id);
        return view('menu::edit', compact('edit'));
    }

    /**
     * @param MenuRequest $request
     * @param             $id
     * @return \Illuminate\Http\RedirectResponse
     * Recebe o request do formulário do menu (apenas atualizações) METODO PATCH
     */
    public function update(MenuRequest $request, $id)
    {
        $request->merge([
            'seo_url' => Str::slug($request->get('name'), '-'),
            'user_id' => Auth::user()->id
        ]);
        $update = Menu::with('icon')->findOrFail($id);
        $update->update($request->except(['_token']));
        return redirect()->route('fatorcms.menu.index')->with('success', trans('messages.update.success'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * Ativa ou Inativa o menu
     */
    public function status($id)
    {
        $status = Menu::findOrFail($id);
        $to = ($status->status) ? 0 : 1;
        $status->status = $to;
        $status->save();
        return redirect()->route('menu.index')->with('success', trans('messages.status.success', [
            'status' => ($to) ? trans('dictionary.status_text.active') : trans('dictionary.status_text.inactive')
        ]));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * Deleta o registro do banco de dados
     */
    public function destroy($id)
    {
        Menu::destroy($id);
        return redirect()->route('fatorcms.menu.index')->with('success', trans('messages.delete.success'));
    }

}