<?php namespace Modules\Menu\Http\Requests;

use App\Http\Requests\BaseRequest;

class MenuRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|min:2|max:20',
            'filename' => 'jpeg|jpg|png',
            'status'   => 'boolean',
            $this->editRequest('menus')
        ];
    }

}
