<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{

    use SoftDeletes;

    protected $fillable = ['key', 'value'];

    public function loadSettings($clear_cache = false)
    {
        if ($clear_cache) {
            cache()->forget('settings');
            return true;
        }

        if (!cache()->has('settings')) {
            $items = [];
            foreach ($this->all() as $settings) {
                $items[$settings->key] = $settings->value;
            }
            cache()->put('settings', $items, Carbon::now()->addDay());
        }

        return cache()->get('settings');
    }

}
