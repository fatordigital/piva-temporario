<?php

namespace App\Listeners;

use App\Events\FilesEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class SaveFilesListener
{
    public $event;

    public function __construct()
    {
        //
    }

    public function handle(FilesEvent $event)
    {
        $this->event = $event;
        if (count($this->event->sizes) > 0) {
            $this->save_files();
        }
        $this->original($this->event->request_file, $event->callback);
    }

    private function save_files()
    {
        foreach ($this->event->sizes as $size) {
            $size = strstr($size, '*') ? explode('*', $size) : explode('x', $size);
            if (isset($size[0]) && isset($size[1])) {
//                Image::make($this->event->request_file)->resize($size[0], $size[1])->save($this->event->path . implode('-', $size) . '_' . $this->event->request_file->getClientOriginalName());
                Image::make($_FILES[$this->event->input_name]['tmp_name'])->resize($size[0], $size[1])->save(public_path($this->event->path . implode('-', $size) . '_' . $this->event->request_file->getClientOriginalName()));
            }
        }
    }

    private function original($request_file, $callback)
    {
        $filename = $this->no_repeat($this->event->path);
        if (is_callable($callback)) {
            $callback($filename);
        }
        $request_file->move(public_path($this->event->path), $filename);
    }

    private function no_repeat($path)
    {
        $i = 0;
        $e = str_slug(pathinfo($this->event->request_file->getClientOriginalName(), PATHINFO_FILENAME), '-');
        do {
            if ($i) {
                $filename = str_slug($e, '-') . '-' . $i . '.' . $this->event->request_file->getClientOriginalExtension();
            } else {
                $filename = $e . '.' . $this->event->request_file->getClientOriginalExtension();
            }
            $i++;
        } while (file_exists(public_path($path . $filename)));
        return $filename;
    }
}
