<?php

namespace App\Listeners;

use App\Events\ImageEvent;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\File;

class ImageListener
{

    private $img, $event, $json;

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ImageEvent $event
     * @return void
     */
    public function handle(ImageEvent $event)
    {
        $this->event = $event;
        if ($event->image && $event->collect->count() && $event->collect->has('path')) {
            $this->manage_directory($event->collect->get('path'));
            $this->img = \Image::make($event->image->getPathname());
            if ($event->collect->has('responsive_sizes')) {
                $this->filter_sizes($event, 'responsive_sizes');
                $this->responsive_sizes($event->collect->get('responsive_sizes'));
            }
            if ($event->collect->has('sizes')) {
                $this->filter_sizes($event, 'sizes');
                $this->sizes($event->collect->get('sizes'));
            }

            $this->img->save(public_path($this->event->collect->get('path') . '/' . $event->image->getClientOriginalName()));

            $event->request->merge([
                'return_upload' => ['name' => $event->image->getClientOriginalName()] + $this->json
            ]);
        }
    }

    private function filter_sizes($event, $name)
    {
        if (!is_array($event->collect->get($name))) {
            json_decode($event->collect->get($name));
            if (json_last_error() == JSON_ERROR_NONE) {
                $event->collect->put($name, json_decode($event->collect->get($name), true));
            }
        }
    }

    private function manage_directory($path)
    {
        if (!File::exists(public_path($path)))
            File::makeDirectory(public_path($path), 0775, true);
    }

    private function responsive_sizes($responsive)
    {
        $name = null;
        foreach ($responsive as $item) {
            if (isset($item['w']) && !empty($item['w']) && isset($item['h']) && !empty($item['h'])) {
                $this->img->fit($item['w'], $item['h'], function ($constraint) {
                    $constraint->upsize();
                });
                $name = Carbon::now()->timestamp . '-' . $item['w'] . 'x' . $item['h'] . '-' . $this->event->image->getClientOriginalName();
                $this->json[isset($item['name']) ? $item['name'] : 'wh'] = $name;
                $this->img->save(public_path($this->event->collect->get('path') . '/' . $name));
                dd('here');
            } else if (isset($item['w']) && !empty($item['w']) && !isset($item['h'])) {
                $this->img->fit($item['w']);
                $name = Carbon::now()->timestamp . '-' . $item['w'] . '-' . $this->event->image->getClientOriginalName();
                $this->json[isset($item['name']) ? $item['name'] : 'w'] = $name;
                $this->img->save(public_path($this->event->collect->get('path') . '/' . $name));
            } else if (isset($item['h']) && !empty($item['h']) && !isset($item['w'])) {
                $this->img->fit($item['h']);
                $name = Carbon::now()->timestamp . '-' . $item['h'] . '-' . $this->event->image->getClientOriginalName();
                $this->json[isset($item['name']) ? $item['name'] : 'h'] = $name;
                $this->img->save(public_path($this->event->collect->get('path') . '/' . $name));
            }
        }
    }

    private function sizes($sizes)
    {
        $name = null;
        foreach ($sizes as $item) {
            if (isset($item['w']) && !empty($item['w']) && isset($item['h']) && !empty($item['h'])) {
                $this->img->resize($item['w'], $item['h'], function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $name = Carbon::now()->timestamp . '-' . $item['w'] . 'x' . $item['h'] . '-' . $this->event->image->getClientOriginalName();
                $this->json[isset($item['name']) ? $item['name'] : 'wh'] = $name;
                $this->img->save(public_path($this->event->collect->get('path') . '/' . $name));
            }
            if (isset($item['w']) && !empty($item['w']) && !isset($item['h'])) {
                $this->img->resize($item['w'], null, function ($constraint) {
                    $constraint->upsize();
                });
                $name = Carbon::now()->timestamp . '-' . $item['w'] . '-' . $this->event->image->getClientOriginalName();
                $this->json[isset($item['name']) ? $item['name'] : 'w'] = $name;
                $this->img->save(public_path($this->event->collect->get('path') . '/' . $name));
            }
            if (isset($item['h']) && !empty($item['h']) && !isset($item['w'])) {
                $this->img->resize($item['h'], null, function ($constraint) {
                    $constraint->upsize();
                });
                $name = Carbon::now()->timestamp . '-' . $item['h'] . '-' . $this->event->image->getClientOriginalName();
                $this->json[isset($item['name']) ? $item['name'] : 'h'] = $name;
                $this->img->save(public_path($this->event->collect->get('path') . '/' . $name));
                dd('here');
            }
        }
    }

}
