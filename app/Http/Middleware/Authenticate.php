<?php

    namespace App\Http\Middleware;

    use Closure;
    use Illuminate\Contracts\Auth\Guard;

    class Authenticate
    {
        /**
         * The Guard implementation.
         *
         * @var Guard
         */
        protected $auth;

        /**
         * @param Guard $auth
         * return @void
         */
        public function __construct(Guard $auth)
        {
            $this->auth = $auth;
        }

        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  \Closure                 $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            if ($this->auth->guest()) {
                if ($request->ajax()) {
                    return response('Unauthorized.', 401);
                } else {
                    if (preg_match('/fatorcms/', $request->url()))
                        return redirect()->route('fatorcms.login');
                    return redirect()->route('fatorcms.dashboard')->with('error', 'Desculpe, não está logado.');
                }
            }
            return $next($request);
        }
    }
