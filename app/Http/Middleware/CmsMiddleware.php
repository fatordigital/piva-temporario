<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Modules\Menu\Entities\Menu;

class CmsMiddleware
{

    private function getCurrentRoute()
    {
        if (Route::getCurrentRoute()) {
            $route = Route::getCurrentRoute()->getName();
            $currentMenu = Menu::with('parent')->whereRoute($route)->get()->first();
            Session::put('currentMenu', (!is_null($currentMenu)) ? $currentMenu->toArray() : Session::get('currentMenu'));
        }
    }

    public function loadBeforeMenu()
    {
        $menus = Menu::getMenu();
//        dd($menus);
        $this->getCurrentRoute();
        view()->share(compact('menus'));
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->loadBeforeMenu();
        return $next($request);
    }
}
