<?php

    namespace App\Http\Middleware;

    use Closure;
    use Illuminate\Contracts\Auth\Guard;

    class RedireftIfAuthenticateFront
    {
        /**
         * The Guard implementation.
         *
         * @var Guard
         */
        protected $auth;

        /**
         * @param Guard $auth
         */
        public function __construct(Guard $auth)
        {
            $this->auth = $auth;
        }

        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  \Closure                 $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            if ($this->auth->check()) {
                return redirect()->route('front.home')->with('success', 'Você já está logado.');
            }

            return $next($request);
        }
    }
