<?php

    namespace App\Http\Middleware;

    use Closure;
    use Illuminate\Contracts\Auth\Guard;
    use Illuminate\Support\Facades\Route;

    class AuthFrontMiddleware
    {
        /**
         * The Guard implementation.
         *
         * @var Guard
         */
        protected $auth;

        /**
         * @param Guard $auth
         * return @void
         */
        public function __construct(Guard $auth)
        {
            $this->auth = $auth;
        }

        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  \Closure                 $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            if (!auth()->guest() && auth()->user()->email == 'dev@fd.com.br') {
                config(['app.debug' => true]);
            }
            if (session()->has('routePay')) {
                Route::post(session()->get('routePay'), ['as' => 'route.pay', 'uses' => 'Modules\FrontEnd\Http\Controllers\CartController@requestPay']);
            }
            if ($this->auth->guest()) {
                if ($request->ajax()) {
                    return response('Unauthorized.', 401);
                } else {
                    return redirect()->route('front.login', 'redirect='. base64_encode($request->fullUrl()))->with('error', 'Efetue seu login para prosseguir.');
                }
            }
            return $next($request);
        }
    }
