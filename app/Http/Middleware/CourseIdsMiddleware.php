<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Laravel\Socialite\One\User;
use Modules\Team\Entities\TeamUser;
use Modules\User\Entities\UserTest;

class CourseIdsMiddleware
{

    private function saveTestUser()
    {
        if (!auth()->guest() && session()->has('add_user_test')) {
            $user_test = UserTest::whereUserId(auth()->user()->id)->whereTestId(session('add_user_test'))->get();
            if (!$user_test) {
                UserTest::create([
                    'user_id' => auth()->user()->id,
                    'test_id' => session('add_user_test')
                ]);
            }
        }
    }

    private function loadTeamUsers()
    {
//        $this->saveTestUser();
        $return = [];
        if (!auth()->guest()) {

            $query = TeamUser::whereUserId(auth()->user()->id)
                ->whereRaw("((DATE(`end_date`) >= DATE(NOW()) OR `end_date` IS NULL OR `end_date` = \"0000-00-00\"))")
                ->lists('team_id')
                ->toArray();

            if ($query)
                $return = $query;
            $this->loadAlertsEndAccess();
        }
        session(['user_team_id' => $return]);
        view()->share('user_team_id', $return);
    }

    public function loadAlertsEndAccess()
    {
        $teams = TeamUser::whereUserId(auth()->user()->id)
            ->whereRaw("(DATE(`end_date`) <= DATE(NOW()) AND `end_date` IS NOT NULL)")
            ->where('alert', '=', 0)
            ->get();


        if ($teams) {
            $array = [];
            foreach ($teams as $team) {
                $array[] = $team->id;
                $team->update(['alert' => 1]);
            }
            session(['alert_team' => $array]);
        } else {
            session()->forget('alert_team');
        }
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->loadTeamUsers();
        return $next($request);
    }
}
