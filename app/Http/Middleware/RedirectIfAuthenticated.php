<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            $permissionsAvaible = $this->auth->user()->getActions();
            if (in_array('fatorcms.dashboard', $permissionsAvaible) || in_array('*', $permissionsAvaible)) {
                return redirect()->route('fatorcms.dashboard');
            } else {
                $this->auth->user()->try_cms_access = 1;
                $this->auth->user()->save();
                $this->auth->logout();
                return redirect()->route('fatorcms.dashboard')->with('error', 'Está área é restrita.');
            }
        }

        return $next($request);
    }
}
