<?php

    namespace App\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Support\Facades\Session;

    class BaseRequest extends FormRequest
    {
        /**
         * @param null   $db
         * @param string $key
         * @param string $field
         * @return null
         */
        public function editRequest($db = null, $key = 'id', $field = 'id')
        {
            $rules = '';
            if ($this->method() == 'PATCH' && $db == null) {
                $rules[$field] = 'required|unique:' . $db . ',' . $key . ',' . $this->getSegmentFromEnd() . ',' . $key;
            }
            return $rules;
        }

        /**
         * @param int $position_from_end
         * @return mixed
         */
        public function getSegmentFromEnd($position_from_end = 1)
        {
            $segments = $this->segments();
            return $segments[sizeof($segments) - $position_from_end];
        }
    }
