<?php

Route::get(trans('routes.login'), [
    'as' => 'fatorcms.login',
    'uses' => 'Auth\AuthController@index'
]);

Route::post(trans('routes.auth') . '/{ref?}', [
    'as' => 'auth',
    'uses' => 'Auth\AuthController@postLogin'
]);

Route::get(trans('routes.logout') . '/{ref?}', [
    'as' => 'logout',
    'uses' => 'Auth\AuthController@getLogout'
]);

Route::get('/i/{path}/{size}/{name}/{crop?}/{fit?}/{position?}', function ($path = null, $size = null, $name = null, $crop = null, $fit = null, $position = null) {
    if (!is_null($size) && !is_null($name) && !is_null($path)) {
        $size = explode('x', $size);
        $cache_image = Intervention\Image\Facades\Image::cache(function ($image) use ($path, $size, $name, $crop, $fit, $position) {
            if (!is_null($crop)) {
                $crop = explode('x', $crop);
                if (!is_null($position)) {
                    //0 = x, 1 = y
                    $position = explode('x', $position);
                    $fit = explode('x', $fit);
                    return $image->make(url(base64_decode($path) . '/' . $name))->fit($fit[0], $fit[1])->crop($crop[0], $crop[1], $position[0], $position[1]);
//                        return $image->make(url(base64_decode($path) . '/' . $name))->fit(800, 261)->crop($crop[0], $crop[1], $position[0], $position[1]);
                    //->crop($crop[0], $crop[1], $position[0], $position[1])
//                        return $image->make(url(base64_decode($path) . '/' . $name))->resize($size[0], $size[1])->crop($crop[0], $crop[1], $position[0], $position[1]);
                } else {
                    return $image->make(url(base64_decode($path) . '/' . $name))->resize($size[0], $size[1])->crop($crop[0], $crop[1]);
                }
            } else {
                return $image->make(url(base64_decode($path) . '/' . $name))->resize($size[0], $size[1]);
            }
        }, 10);
        return response()->make($cache_image, 200, ['Content-Type' => 'image']);
    } else {
        abort(404);
    }
});


Route::get('imagem/{slug}/{type?}', function ($slug = null, $type = null) {
    if (!is_null($slug) && !is_null($type)) {
//        dd($type);
        if ($type == 1) {
            $size = [1900, 564];
        } else if ($type == 2) {
            $size = [800, 600];
        }
        $cache_image = Intervention\Image\Facades\Image::cache(function ($image) use ($size, $slug, $type) {
            $team = \Modules\Team\Entities\Team::whereSlug($slug)->first();
            if ($size[0] == 1900) {
                return $image->make(url('assets/team/' . $team->image))->resize($size[0], $size[1], function ($constraint) {
                    $constraint->upsize();
                });
            } else {
                return $image->make(url('assets/team/'. $team->image_show))->resize($size[0], $size[1], function ($constraint) {
                    $constraint->upsize();
                });
            }
        }, 10);

        return response()->make($cache_image, 200, ['Content-Type' => 'image']);
    } else {
        abort(404);
    }
});



Route::group([
    'prefix' => 'fatorcms',
    'middleware' => 'cms'
], function () {
    Route::post('/module/ajax/clear/files', [
        'as' => 'fatorcms.module.ajax.clear.files',
        'uses' => 'Controller@ajaxClearTempFiles'
    ]);

    Route::get('lessons.json', ['as' => 'json.lesson', 'uses' => 'Json\JsonController@lessons']);
    Route::post('lesson-team-save.json', ['as' => 'json.lesson-team-save', 'uses' => 'Json\JsonController@lessonTeamSave']);
    Route::post('lesson-team-save-date.json', ['as' => 'json.lesson-team-save-date', 'uses' => 'Json\JsonController@lessonTeamLessonUpdate']);
    Route::delete('lesson-team-destroy.json', ['as' => 'json.lesson-team-destroy', 'uses' => 'Json\JsonController@lessonTeamLessonDestroy']);

    Route::get('disciplines.json', ['as' => 'json.discipline', 'uses' => 'Json\JsonController@getDiscipline']);
    Route::post('get-questions.json', ['as' => 'json.get-questions', 'uses' => 'Json\JsonController@getQuestions']);

    Route::delete('destroy-question.json', ['as' => 'json.destroy-question', 'uses' => 'Json\JsonController@destroyQuestion']);
    Route::post('load-edit-test.json', ['as' => 'json.load-edit-test', 'uses' => 'Json\JsonController@loadEditTest']);
});


Route::group([
    'middleware' => 'cms'
], function () {
    Route::resource('/fatorcms/notifications', 'NotificationController');
    Route::get('getIcons.json', ['as' => 'geticons.json', 'uses' => 'Json\JsonController@getIcons']);
});
