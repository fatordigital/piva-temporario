<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
//            \App\Http\Middleware\VerifyCsrfToken::class
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'order' => \Modules\FrontEnd\Http\Middleware\OrderMiddleware::class,
        'auth' => \App\Http\Middleware\Authenticate::class,
        'cms' => \App\Http\Middleware\CmsMiddleware::class,
        'auth-front' => \App\Http\Middleware\AuthFrontMiddleware::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'guest-front' => \App\Http\Middleware\RedireftIfAuthenticateFront::class,
        'acl' => \Modules\Acl\Http\Middleware\AclMiddleware::class,
        'course-ids' => \App\Http\Middleware\CourseIdsMiddleware::class,
        'team_check' => \Modules\FrontEnd\Http\Middleware\TeamUserCheck::class
    ];
}
