<?php namespace App\Http\Controllers\Json;

use App\Http\Controllers\BaseModuleController;
use App\Http\Requests\LessonTeamSaveRequest;
use App\Icon;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Lesson\Entities\Discipline;
use Modules\Lesson\Entities\Lesson;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamLesson;
use Modules\Test\Entities\Question;
use Modules\Test\Entities\TestQuestion;

class JsonController extends BaseModuleController
{

//    public function lessons(Request $request, Lesson $lesson, Team $team)
//    {
//        $keys = $request->has('keys') ? $request->get('keys') : [];
//        if ($request->has('slug')) {
//            $exists = $team->whereSlug($request->get('slug'))->first();
//            $keys = TeamLesson::whereTeamId($exists->id)->lists('lesson_id')->toArray();
//        }
//        if ($request->has('type')) {
//            if ($request->get('type') == 'lists') {
//                return response()->view('json.index', ['content' => $lesson->whereNotIN('id', $keys)->get()->toJson()])->header('Content-type', 'application/json');
//            }
//        }
//        abort(404);
//    }

    public function lessons(Request $request, Lesson $lesson, Team $team)
    {
        if ($request->has('id')) {
            return response()->json(['error' => false, 'data' => TeamLesson::with(['lesson' => function ($q) {
                $q->with('discipline');
            }])->whereTeamId($request->get('id'))->orderBy('order', 'desc')->get()->toArray()], 200);
        } else {
            $lesson = $lesson->with(['discipline']);
            if (request()->has('discipline')) {
                $lesson = $lesson->where('discipline_id', '=', request('discipline'));
            }
            if (request()->has('chooses'))
                $lesson = $lesson->whereNotIn('id', request('chooses'));
            return response()->json(['error' => false, 'data' => $lesson->get()->toArray()], 200);
        }
    }

    public function lessonTeamSave(LessonTeamSaveRequest $request, TeamLesson $model)
    {
        $team = Team::whereSlug($request->get('team_id'))->first();
        if (!TeamLesson::whereTeamId($team->id)->whereLessonId($request->get('lesson_id'))->first()) {
            $request->merge(['user_create_id' => \Auth::user()->id, 'team_id' => $team->id]);
            if ($save = $model->create($request->except(['_token', 'slug'])))
                return response()->view('json.index', ['content' => TeamLesson::whereId($save->id)->with(['team', 'lesson'])->first()->toJson()])->header('Content-type', 'application/json');
        }
        return response()->json(['error' => true, 'msg' => 'Turma já está vinculada a está aula.'], 400);
    }

    public function lessonTeamLessonUpdate(Request $request, TeamLesson $model)
    {
        $edit = $model->find($request->get('id'));
        if ($request->get('begin_date') != 'Data não definida') {
            $request->merge(['begin_date' => Carbon::createFromFormat('d/m/Y', $request->get('begin_date'))->format('Y-m-d')]);
        }
        if ($request->get('end_date') != 'Data não definida') {
            $request->merge(['end_date' => Carbon::createFromFormat('d/m/Y', $request->get('end_date'))->format('Y-m-d')]);
        }
        if ($edit->update($request->except(['_token', 'id'])))
            return response()->json(['error' => false, 'msg' => 'Update efetuado com sucesso.'], 200);
        else
            return response()->json(['error' => true, 'msg' => 'Falha ao atualizar, tente novamente.'], 400);
    }

    public function lessonTeamLessonDestroy(Request $request, TeamLesson $model)
    {
        $destroy = $model->find($request->get('id'));
        if ($destroy->delete())
            return response()->json(['error' => false, 'msg' => 'Vínculo removido com sucesso.'], 200);
        else
            return response()->json(['error' => true, 'msg' => 'Falha ao deletar, tente novamente.'], 400);
    }

    public function getDiscipline()
    {
        return response()->json(['data' => Discipline::orderBy('name')->get()->lists('discipline_questions', 'id')], 200);
    }

    public function getQuestions(Request $request, Question $model)
    {
        $query = $model->with('discipline')->orderBy('name');
        if ($request->get('difficulty'))
            $query = $query->whereDifficulty($request->get('difficulty'));
        if ($request->get('board'))
            $query = $query->whereExaminingBoardId($request->get('board'));
        if ($request->get('quantity'))
            $query = $query->limit($request->get('quantity'));
        if ($request->get('notIn'))
            $query = $query->whereNotIn('id', $request->get('notIn'));
        $query = $query->whereDisciplineId($request->get('discipline'));
        if (!$query->get()->count())
            return response()->json(['error' => true, 'msg' => 'Nenhuma questão encontrada com os filtros passados.'], 200);
        return response()->json(['error' => false, 'data' => $query->get()->toArray()], 200);
    }

    public function loadEditTest(Request $request, TestQuestion $model, Question $model_question)
    {
        $relations = $model->whereTestId($request->get('id'))->lists('question_id')->toArray();
        if ($relations) {
            return response()->json(['error' => false, 'msg' => 'Algumas questões no vínculo', 'data' => $model_question->with('discipline')->whereIn('id', $relations)->get()->toArray()], 200);
        }
        return response()->json(['error' => true, 'msg' => 'Nenhuma questão vinculada para este simulado.']);
    }

    public function destroyQuestion(Request $request, TestQuestion $model)
    {
        if ($request->has('test_id') && $request->get('test_id')) {
            if ($model->whereTestId($request->get('test_id'))->whereQuestionId($request->get('question_id'))->delete()) {
                return response()->json(['error' => false, 'msg' => 'Questão foi removida com sucesso.']);
            }
        }
        return response()->json(['error' => false, 'msg' => 'Vínculo não encontrado.'], 200);
    }

    public function getIcons()
    {
        return response()->json(['error' => false, 'data' => Icon::where('icon', 'LIKE', '%glyphicon%')->get(['icon'])->toArray()]);
    }
}