<?php

    namespace App\Http\Controllers;

    use App\Http\Requests;
    use App\Notification;
    use App\NotificationUser;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;

    class NotificationController extends BaseModuleController
    {

        public function index(Notification $model)
        {
            if (Auth::user()) {
                $notifications = NotificationUser::whereUserId(Auth::user()->id)->lists('notification_id')->toArray();
                $list = $model->whereNotIn('id', $notifications)->orderBy('created_at', 'desc')->paginate(10);
                return response()->json(['error' => false] + $list->toArray(), 200);
            }
        }

        public function store(Request $request, Notification $model)
        {
            NotificationUser::firstOrCreate(['notification_id' => $request->get('id'), 'user_id' => auth()->user()->id, 'read' => true]);
            $notifications = NotificationUser::whereUserId(auth()->user()->id)->lists('notification_id')->toArray();
            $list = $model->whereNotIn('id', $notifications)->orderBy('created_at', 'desc')->paginate(10);
            return response()->json(['error' => false] + $list->toArray(), 200);
        }

    }
