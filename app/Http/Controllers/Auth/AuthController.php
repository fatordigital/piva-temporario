<?php

    namespace App\Http\Controllers\Auth;

    use App\Http\Requests\LoginRequest;
    use App\Http\Controllers\Controller;
    use App\Http\Requests\RegisterRequest;
    use Illuminate\Foundation\Auth\ThrottlesLogins;
    use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
    use Illuminate\Support\Facades\Auth;

    class AuthController extends Controller
    {

        use AuthenticatesAndRegistersUsers, ThrottlesLogins;

        private function verifyEmptyPassAndChange($email)
        {
            $user = User::where('email', '=', $email)->first();
            if ($user && $user->password == '') {
                if (request()->has('pass'))
                    $user->update(['password' => bcrypt(request('pass'))]);
                else {
                    $user->update(['password' => bcrypt(request('password'))]);
                }
            }
        }

        /**
         * Create a new authentication controller instance.
         */
        public function __construct()
        {
            $this->middleware('guest', ['except' => ['getLogout', 'postLogin']]);
        }

        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            return view('cms.auth.index');
        }

        /**
         * @param LoginRequest $request
         * @return $this|\Illuminate\Http\RedirectResponse
         */
        public function postLogin(LoginRequest $request)
        {
            if (Auth::attempt($request->only(['email', 'password']))) {
                return redirect()->route('fatorcms.dashboard');
            }
            return redirect()->route('fatorcms.login')
                ->withInput(['email' => $request->get('email')])
                ->withErrors(['password' => trans('auth.failed')]);
        }

        public function getLogout($ref = '')
        {
            Auth::logout();
            return redirect()->route('fatorcms.login');
        }


    }
