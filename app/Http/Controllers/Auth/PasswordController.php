<?php

    namespace App\Http\Controllers\Auth;

    use App\Http\Controllers\Controller;
    use Illuminate\Foundation\Auth\ResetsPasswords;
    use Illuminate\Http\Request;
    use Illuminate\Mail\Message;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Password;

    class PasswordController extends Controller
    {

        use ResetsPasswords;

        /**
         *
         */
        public function __construct()
        {
            $this->middleware('guest');
        }

        /**
         * @param Request $request
         * @return \Illuminate\Http\RedirectResponse
         */
        public function postEmail(Request $request)
        {
            $this->validate($request, ['email' => 'required|email']);
            $response = Password::sendResetLink($request->only(['email']), function (Message $message) {
                $message->subject($this->getEmailSubject());
            });
            switch ($response) {
                case Password::RESET_LINK_SENT:
                    return redirect()->back()->with('success', 'Foi enviado um e-mail com as instruções para ' . $request->get("email"));

                case Password::INVALID_USER:
                    return redirect()->back()->withErrors(['email' => trans($response)]);
            }
        }

        /**
         * Get the e-mail subject line to be used for the reset link email.
         *
         * @return string
         */
        protected function getEmailSubject()
        {
            return isset($this->subject) ? $this->subject : 'Resetar Senha';
        }

        /**
         * @param null $token
         * @return $this
         * @throws NotFoundHttpException
         */
        public function getReset($token = null)
        {
            if (is_null($token)) {
                throw new NotFoundHttpException;
            }
            return view('frontend::user.reset')->with('token', $token);
        }

        /**
         * Reset the given user's password.
         *
         * @param  \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function postReset(Request $request)
        {
            $this->validate($request, [
                'token'    => 'required',
                'email'    => 'required|email',
                'password' => 'required|confirmed',
            ]);

            $credentials = $request->only([
                'password', 'password_confirmation', 'token', 'email'
            ]);

            $response = Password::reset($credentials, function ($user, $password) {
                $this->resetPassword($user, $password);
            });

            switch ($response) {
                case Password::PASSWORD_RESET:
                    return redirect()->route('front.home')->with('success', 'Sua senha foi resetada com sucesso.');

                default:
                    return redirect()->back()
                        ->withInput($request->only('email'))
                        ->withErrors(['email' => trans($response)]);
            }
        }

        /**
         * Reset the given user's password.
         *
         * @param  \Illuminate\Contracts\Auth\CanResetPassword $user
         * @param  string                                      $password
         * @return void
         */
        protected function resetPassword($user, $password)
        {

            $user->password = bcrypt($password);

            $user->save();

            Auth::login($user);
        }

    }
