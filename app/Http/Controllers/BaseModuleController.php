<?php

    namespace App\Http\Controllers;

    use App\Http\Requests;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Route;
    use Illuminate\Support\Facades\Session;
    use Illuminate\Support\Facades\View;
    use Maatwebsite\Excel\Facades\Excel;
    use Modules\Menu\Entities\Menu;
    use Pingpong\Modules\Routing\Controller as BsController;

    class BaseModuleController extends BsController
    {


        /**
         * @param        $request
         * @param string $path
         * @param string $key
         * @return string
         */
        public function customFileName(Request $request, $path = 'img/redactor/', $key = 'file')
        {
            $i = 0;
            $e = str_slug(pathinfo($request->file($key)->getClientOriginalName(), PATHINFO_FILENAME), '-');
            do {
                if ($i) {
                    $filename = str_slug($e, '-') . '-' . $i . '.' . $request->file($key)->getClientOriginalExtension();
                } else {
                    $filename = $e . '.' . $request->file($key)->getClientOriginalExtension();
                }
                $i++;
            } while (file_exists(public_path($path . $filename)));
            return $filename;
        }

        /**
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse
         * função padrão para o redactor inserir imagens
         */
        public function ajaxRedactorImage(Request $request)
        {
            if ($request->hasFile('file')) {
                $filename = $this->customFileName($request);
                $request->file('file')->move('img/redactor/', $filename);
                return response()->json(['status' => 'success', 'message' => 'Imagem salva com sucesso', 'filelink' => \URL::to('/img/redactor/' . $filename)]);
            }
            return response()->json(['status' => 'error', 'message' => 'Desculpe, não foi possível salvar a imagem. Tente novamente.']);
        }

        /**
         * @param Request $request
         * @return \Illuminate\Http\JsonResponse
         * função padrão para o redactor inserir arquivos
         */
        public function ajaxRedactorFile(Request $request)
        {
            if ($request->hasFile('file')) {
                $filename = $this->customFileName($request);
                $request->file('file')->move('img/redactor', $filename);
                return response()->json(['status' => 'success', 'message' => 'Imagem salva com sucesso', 'filelink' => \URL::to('/img/redactor/' . $filename), 'filename' => $filename]);
            }
            return response()->json(['status' => 'error', 'message' => 'Desculpe, não foi possível salvar o arquivo. Tente novamente.']);
        }

        public function exportSimpleExcel($model, $objectQuery, $title = '')
        {

            if ($model->excel) {
                $title = $title ? $title : 'Professor Piva';
                Excel::create($model->getTable().'-'.date('dmyhisu'), function ($excel) use ($model, $objectQuery, $title) {
                    $excel->setTitle($title);
                    $excel->sheet(str_limit($title, 18, ''), function ($sheet) use ($model, $objectQuery) {
                        $sheet->fromArray($model->excel);
                        $i = 2;
                        foreach ($objectQuery->limit(60)->get()->toArray() as $row) {
                            $store = [];
                            foreach($model->excelModel as $r) {
                                $store[] = $row[$r];
                            }
                            if (isset($row['created_at']) && !empty($row['created_at'])) {
                                $row['created_at'] = dateToSql($row['created_at'], 'Y-m-d H:i:s', 'd/m/Y H:i:s');
                            }
                            if (isset($row['updated_at']) && !empty($row['updated_at'])) {
                                $row['updated_at'] = dateToSql($row['updated_at'], 'Y-m-d H:i:s', 'd/m/Y H:i:s');
                            }
                            $sheet->row($i, $store);
                            $i++;
                        }
                    });
                })->download('xls');
            }
        }

        public function export_excel(array $data, $title = '', array $header = array(), array $header_footer = array(), array $footer_data = array())
        {
            $title = $title ? $title : 'Professor Piva';
            Excel::create(str_slug($title).'-'.date('d-m-Yhis'), function($excel) use ($header_footer, $footer_data, $data, $title, $header) {
                $excel->setTitle($title);
                $excel->sheet($title, function($sheet) use ($header_footer, $footer_data, $data, $header) {
                    $i = 2;
                    foreach ($data as $item) {
                        if ($i == 2) {
                            if ($header) {
                                $sheet->fromArray($header);
                            }else {
                                $sheet->fromArray(array_flip($item));
                            }
                        }
                        $sheet->row($i, $item);
                        $i++;
                    }
                    if ($header_footer) {
                        $i++;
                        $sheet->row($i, $header_footer);
                    }
                    if ($footer_data) {
                        $i++;
                        foreach ($footer_data as $data_footer) {
                            $sheet->row($i, $data_footer);
                            $i++;
                        }
                    }
                });
            })->download('xls');
        }

    }
