<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Modules\Team\Entities\Team;
use Modules\Team\Entities\TeamUser;
use Modules\Test\Entities\Test;
use Modules\User\Entities\UserTest;
use Pingpong\Modules\Routing\Controller as BsController;

class FrontDefaultController extends BsController
{

    private $old = [];
    protected $rules = [];
    protected $messages = [];
    protected $cart = false;

    public function requestRoute($route, $back = false)
    {
        if ($back == false) {
            if (!request()->has('redirect')) {
                if (Route::has($route)) {
                    return redirect()->route($route);
                }
            } else if (base64_decode(request()->get('redirect'), true)) {
                $request = base64_decode(request()->get('redirect'));
                return redirect($request);
            }
        } else {
            if (!request()->has('redirect')) {
                if (Route::has($route)) {
                    return redirect()->route($route);
                }
            } else if (base64_decode(request()->get('redirect'), true)) {
                $request = base64_decode(request()->get('redirect'));
                if (Route::has($request)) {
                    return redirect()->route($route, 'redirect=' . base64_encode($request));
                }
            }
        }
        return redirect()->back();
    }

    public function captchaValidate($request)
    {
        if ($request->exists('captcha')) {
            if (!$request->get('captcha'))
                return true;
            else {
                return false;
            }
        }
    }

    public function customEncryptValidate($request)
    {
        $return = true;
        if ($this->cart == false && $request->has('request') != null && $request->get('request') != '')
            $request->merge(CryptoJSDecrypt($request->get('request')));
        else if ($request->has('request')) {
            $request->merge(CryptoJSDecrypt($request->get('request')));
            $request->merge(['course' => CryptoJSDecrypt(base64_decode($request->get('course')))]);
        } else {
            return false;
        }
        if (!$request->has('captcha')) {
            foreach ($this->rules as $rules) {
                if (!$request->has($rules)) {
                    $this->old[$rules] = $this->messages[$rules];
                    $return = false;
                } else if ($rules == 'email' && !filter_var($request->get($rules), FILTER_VALIDATE_EMAIL)) {
                    $this->old[$rules] = 'Por favor, preencha um e-mail válido.';
                    $return = false;
                } else if ($rules == 'password' && $request->get($rules) != $request->get('r_password')) {
                    $this->old[$rules] = 'As senhas não coincidem';
                    $this->old['r_password'] = 'x';
                    $return = false;
                }
            }
            $request->session()->flashInput($this->old);
        } else {
            $request->session()->flashInput(['error' => 'Desculpe, houve um erro inesperado ao enviar o contato. Por favor, atualize a página antes de continuar.']);
            $return = false;
        }
        return $return;
    }

    public function loadCourses($model, $ids = [], $home = false)
    {
        $teams = $model->validTeams($ids, $home)->where('status', '=', 1);
        view()->share(['courses' => $teams->get(['id', 'icon', 'image_show', 'image', 'name', 'course_category_id', 'course_id', 'slug', 'value', 'value_discount'])]);
    }

    public function loadNews($model, $principal = false)
    {
        if (!$principal)
            $news = $model->orderBy('created_at', 'desc')->limit(3)->get(['new_category_id', 'name', 'slug', 'text', 'image']);
        else {
            $news = $model->where('principal', '=', true)->orderBy('created_at', 'desc')->limit(3)->get(['new_category_id', 'name', 'slug', 'text', 'image']);
            if ($news->count() == 0) {
                $news = $model->orderBy('created_at', 'desc')->limit(3)->get(['new_category_id', 'name', 'slug', 'text', 'image']);
            }
        }
        View::share(compact('news'));
    }

    public function loadBanners($model, $ids = array())
    {
        $banners = $model->validTeams($ids)->where('banner_status', '=', 1)->get();
        View::share(compact('banners'));
    }

    public function loadUserCourses($user_id, $return = false)
    {
        $no_more = TeamUser::whereUserId(auth()->user()->id)
            ->whereRaw("DATE(`end_date`) <= DATE(NOW()) && `end_date` != '0000-00-00'")
            ->get()->lists(['id'])->toArray();

//        $user_courses = TeamUser::with(['team' => function ($q) {
//            $q->with(['lessons', 'course', 'category'])
//                ->whereRaw("(DATE(`end_date`) >= DATE(NOW()) OR `end_date` IS NULL OR `end_date` = '0000-00-00')");
//        }])->whereUserId($user_id)
//            ->whereNotIn('id', $no_more)
//            ->whereRaw("(DATE(`end_date`) >= DATE(NOW()) OR `end_date` IS NULL OR `end_date` = '0000-00-00')")
//            ->groupBy('team_id')
//            ->get();

        $user_courses = TeamUser::with(['team' => function($q) {
            $q->with(['lessons', 'course', 'category']);
        }])->whereUserId($user_id)
            ->whereNotIn('id', $no_more)
            ->whereRaw('((DATE(`end_date`) >= DATE(NOW()) OR `end_date` IS NULL OR `end_date` = "0000-00-00"))')
            ->groupBy('team_id')
            ->get();


        foreach ($user_courses as $index => $user_course) {
            if (!$user_course->team)
                unset($user_courses[$index]);
        }

        if (!$return)
            view()->share(compact('user_courses'));
        else
            return $user_courses;
    }

    public function loadTeamUsers()
    {
        $return = [];
        if (!auth()->guest()) {
            $query = TeamUser::whereUserId(auth()->user()->id)
                ->lists('team_id')->toArray();
            if ($query)
                $return = $query;
        }
        session(['user_team_id' => $return]);
        session()->save();
        view()->share('user_team_id', $return);
    }

    public function loadEndTests($user_id)
    {
        $user_tests = UserTest::where('user_id', '=', $user_id)
            ->groupBy(['test_id'])
            ->orderBy('created_at', 'asc')->get();
        view()->share(compact('user_tests'));
    }

    public function loadFreeTests($user_id)
    {
        $tests = Test::whereFree(1)->get();
        foreach ($tests as $test) {
            $user_tests = UserTest::whereUserId($user_id)
                ->whereTestId($test->id)
                ->first();
            if (!$user_tests) {
                UserTest::create(['end_date' => sum_hours(time_to_seconds($test->duration)), 'user_id' => $user_id, 'test_id' => $test->id]);
            }
        }
    }

}