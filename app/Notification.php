<?php

    namespace App;

    use Carbon\Carbon;
    use Illuminate\Database\Eloquent\SoftDeletes;

    class Notification extends BaseModel
    {

        use SoftDeletes;

        protected $fillable = [
            'route',
            'type',
            'title',
            'text'
        ];

        protected $hidden = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];

        protected $appends = [
            'DateHour'
        ];

        public function getDateHourAttribute()
        {
            return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y H:i:s');
        }

        public function myAlerts()
        {
            return $this->hasMany('App\NotificationUser', 'notification_id');
        }

    }
