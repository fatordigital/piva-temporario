<?php

    namespace App;

    use Carbon\Carbon;
    use Illuminate\Support\Facades\Schema;
    use LaravelArdent\Ardent\Ardent;

    class BaseModel extends Ardent
    {

        public function getBeginAttribute()
        {
            if (isset($this->begin_date) && $this->begin_date != '0000-00-00' && $this->begin_date != '')
                return Carbon::createFromFormat('Y-m-d', $this->begin_date)->format('d/m/Y');
            else {
                return date('d/m/Y');
            }
        }

        public function getEndAttribute()
        {
            if (isset($this->end_date) && $this->end_date != '0000-00-00' && $this->end_date != '')
                return Carbon::createFromFormat('Y-m-d', $this->end_date)->format('d/m/Y');
            else {
                return date('d/m/Y');
            }
        }

        public function getCreatedAttribute()
        {
            if (isset($this->created_at) && $this->created_at != '0000-00-00')
                return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/y H:i:s');
            else {
                return 'Data não definida';
            }
        }

        public function getUpdatedAttribute()
        {
            if (isset($this->updated_at) && $this->updated_at != '0000-00-00')
                return Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at)->format('d/m/y H:i:s');
            else {
                return 'Data não definida';
            }
        }

        /**
         * @return string
         * Atributo criado, caso exista um campo 'status', retorna um campo StatusText
         */
        public function getStatusTextAttribute()
        {
            $return = '';
            if (isset($this->status)) {
                switch ($this->status) {
                    case 1 :
                        $return = trans('dictionary.status_text.active');
                        break;
                    case 0 :
                        $return = trans('dictionary.status_text.inactive');
                        break;
                    default:
                        $return = trans('dictionary.status_text.not_active');
                        break;
                }
            }
            return $return;
        }

        /**
         * @return string
         * Atributo criado, caso exista um campo 'early_description', retornao o texto de 1 (sim) ou 0 (não)
         */
        public function getEarlyDescriptionTextAttribute()
        {
            $return = '';
            if (isset($this->early_description)) {
                switch ($this->early_description) {
                    case 1 :
                        $return = trans('dictionary.yes');
                        break;
                    case 0 :
                        $return = trans('dictionary.no');
                        break;
                    default:
                        $return = trans('dictionary.no');
                        break;
                }
            }
            return $return;
        }

        public function notRepeatModelString($string, $model, $field = 'slug')
        {
            $i = 0;
            $slugName = $string;
            do {
                $group = $model->where($field, '=', $string)->first();
                if ($group)
                    $string = str_slug($slugName . ' ' . $i, '-');
                $i++;
            } while ($group);
            return $string;
        }

        public function updateRelations($model, $model_relation, $array_database, $array_request, $first = 'content_id', $second = 'material_id')
        {
            $return = [];
            foreach (array_diff($array_request, $array_database) as $create) {
                $status = $model_relation->firstOrCreate([$first => $create, $second => $model->id]);
                if ($status)
                    $return['create'][$status->id] = true;
                else
                    $return['create'][$status->id] = false;
            }
            foreach (array_diff($array_database, $array_request) as $remove) {
                $status = $model_relation->where($first, '=', $remove)->where($second, '=', $model->id)->delete();
                if ($status)
                    $return['delete'][$model->id] = true;
                else
                    $return['delete'][$model->id] = false;
            }
            return $return;
        }


        public function beforeCreate()
        {
            if ($this->offsetExists('begin_date') && $this->attributes['begin_date'] != '' &&  typeDate($this->attributes['begin_date']) && Schema::hasColumn($this->getTable(), 'begin_date'))
                $this->attributes['begin_date'] = Carbon::createFromFormat('d/m/Y', $this->attributes['begin_date'])->format('Y-m-d');
            if ($this->offsetExists('end_date') && $this->attributes['end_date'] != '' && typeDate($this->attributes['end_date']) && Schema::hasColumn($this->getTable(), 'end_date'))
                $this->attributes['end_date'] = Carbon::createFromFormat('d/m/Y', $this->attributes['end_date'])->format('Y-m-d');
            if (Schema::hasColumn($this->getTable(), 'user_create_id'))
                $this->setAttribute('user_create_id', \Auth::user()->id);
        }

        public function beforeUpdate()
        {
            if ($this->offsetExists('begin_date') && typeDate($this->attributes['begin_date']) && Schema::hasColumn($this->getTable(), 'begin_date'))
                $this->attributes['begin_date'] = Carbon::createFromFormat('d/m/Y', $this->attributes['begin_date'])->format('Y-m-d');
            if ($this->offsetExists('end_date') && typeDate($this->attributes['end_date']) && Schema::hasColumn($this->getTable(), 'end_date'))
                $this->attributes['end_date'] = Carbon::createFromFormat('d/m/Y', $this->attributes['end_date'])->format('Y-m-d');
            if (Schema::hasColumn($this->getTable(), 'user_create_id'))
                $this->setAttribute('user_create_id', \Auth::user()->id);
        }

    }
