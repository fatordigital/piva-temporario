<?php

if (!function_exists('valideLessonDate')) {
    function valideLessonDate($team_id, $lesson_id)
    {
        $team_lesson = \Modules\Team\Entities\TeamLesson::whereTeamId($team_id)
            ->whereLessonId($lesson_id)
            ->whereRaw('(DATE(begin_date) <= DATE(NOW()) OR begin_date IS NULL)')
            ->count();
        if ($team_lesson)
            return true;

        $team_lesson = \Modules\Team\Entities\TeamLesson::whereTeamId($team_id)
            ->whereLessonId($lesson_id)
            ->select(['begin_date'])
            ->first();

        return $team_lesson;
    }
}

if (!function_exists('videoView')) {
    function videoView($user_id, $team, $video)
    {
        $model = new \Modules\User\Entities\UserContent();
        $user_contents = $model->whereTeamId($team->id)->whereContentId($video->id)->whereUserId($user_id)->get();
        if ($team->video_repeat == 0 || $user_contents->count() < $team->video_repeat) {
            l('Visualizando o conteúdo ' . $video->name, $video->id, $team->id, $team->course_id);
            return true;
        }
        return false;
    }
}

if (!function_exists('time_to_seconds')) {
    function time_to_seconds($time, $current_date = 'TODAY')
    {
        return strtotime($time) - strtotime($current_date);
    }
}

if (!function_exists('seconds_to_time')) {
    function seconds_to_time($seconds, $format = 'H:i:s')
    {
        return gmdate($format, $seconds);
    }
}

if (!function_exists('sum_hours')) {
    function sum_hours($seconds, $output = 'Y-m-d H:i:s')
    {
        return date($output, time() + $seconds);
    }
}

if (!function_exists('s')) {
    function s($key, $json = false)
    {
        $settings = new \App\Setting();
//        if (env('APP_DEBUG') == true) {
//            $settings->loadSettings(true);
//        }

        $settings = $settings->loadSettings();
        if (isset($settings[$key])) {
            return $json ? json_decode($settings[$key]) : $settings[$key];
        }
        if (env('DEBUG') == true)
            return 'Configurações não encontrada ' . $key;
        return '';
    }
}

if (!function_exists('checkRoute')) {
    function checkRoute($route)
    {
        if (!auth()->user()->isAdmin()) {
            if (!$route)
                return true;
            $actions = auth()->user()->getActions();
            if (in_array($route, $actions)) {
                return true;
            }
            return false;
        }
        return true;
    }
}

if (!function_exists('resize_img')) {
    function resize_image($path, $image, $size, $cropSize = false, $fit = false, $position = false)
    {
        if ($image && $path) {
            if (file_exists(public_path($path . '/' . $image))) {
                if ($cropSize) {
                    if ($fit && $position) {
                        return url('i/' . base64_encode($path) . '/' . $size . '/' . $image . '/' . $cropSize . '/' . $fit . '/' . $position);
                    } else {
                        return url('i/' . base64_encode($path) . '/' . $size . '/' . $image . '/' . $cropSize);
                    }
                } else {
                    return url('i/' . base64_encode($path) . '/' . $size . '/' . $image);
                }
            }
        }
        return '';
    }
}

if (!function_exists('resize_img_curso')) {
    function resize_img_curso($path, $image, $size, $cropSize = false, $fit = false, $position = false)
    {
        if ($image && $path) {
            if (file_exists(public_path($path . '/' . $image))) {
                if ($cropSize) {
                    if ($fit && $position) {
                        return url('i/' . base64_encode($path) . '/' . $size . '/' . $image . '/' . $cropSize . '/' . $fit . '/' . $position);
                    } else {
                        return url('i/' . base64_encode($path) . '/' . $size . '/' . $image . '/' . $cropSize);
                    }
                } else {
                    return url('i/' . base64_encode($path) . '/' . $size . '/' . $image);
                }
            }
        }
        return '';
    }
}

if (!function_exists('getContent')) {
    function getContent($lesson_id, $return = 'video')
    {
        $contents = \Modules\Lesson\Entities\LessonContent::with(['contents' => function ($q) use ($return) {
            $q->where($return, '=', true);
        }])->where('content_id', '!=', '')->whereIn('lesson_id', $lesson_id)->get();
        if ($contents) {
            return $contents;
        }
        return false;
    }
}

if (!function_exists('getTest')) {
    function getTest($lesson_id)
    {
        $tests = \Modules\Lesson\Entities\LessonContent::with('tests')->where('test_id', '!=', '')->whereIn('lesson_id', $lesson_id)->get();
        if ($tests) {
            return $tests;
        }
        return false;
    }
}

if (!function_exists('dateToSql')) {
    function dateToSql($date, $format = 'd/m/Y', $output = 'Y-m-d')
    {
        if ($date)
            return \Carbon\Carbon::createFromFormat($format, $date)->format($output);
        else {
            return date('Y-m-d');
        }
    }
}

if (!function_exists('typeDate')) {
    function typeDate($date, $format = 'd/m/Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }
}

if (!function_exists('userHistorical')) {
    function userHistorical()
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        return $user->name ? $user->name : $user->email;
    }
}

if (!function_exists('sumHours')) {
    function sumHours($hi, $he)
    {
        $data_login = strtotime('2009-12-15 ' . $hi);
        $data_logout = strtotime('2009-12-15 ' . $he);

        $tempo_con = mktime(date('H', $data_logout) - date('H', $data_login), date('i', $data_logout) - date('i', $data_login), date('s', $data_logout) - date('s', $data_login));

        return date('H:i:s', $tempo_con);
    }
}

if (!function_exists('fex')) {
    function fex($path, $filename)
    {
        if (!empty($filename) && !is_null($filename)) {
            $base_path = env('APP_DEBUG') == false ? base_path() . '/' . $path . '/' . $filename : $path . '/' . $filename;
            return \Illuminate\Support\Facades\File::exists($base_path);
        }
        return false;
    }
}

if (!function_exists('getStates')) {
    function getStates()
    {
        return array(
            '' => 'Selecione um Estado (*)',
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins'
        );
    }
}

if (!function_exists('brokeInstallment')) {
    function brokeInstallment($string, $return)
    {
        if ($string) {
            $r = explode('#', $string);
            return $r[$return];
        }
    }
}

if (!function_exists('onlyNumber')) {
    function onlyNumber($string)
    {
        return preg_replace('/\D/', '', $string);
    }
}

if (!function_exists('brokePhone')) {
    function brokePhone($phone)
    {
        $return = ['area_code', 'phone_number'];
        preg_match('/\(([0-9\)]*)\) ([0-9-]+)/', $phone, $area_code);
        if (isset($area_code[1]) && isset($area_code['2'])) {
            $return = [
                'area_code' => trim($area_code[1]),
                'phone_number' => str_replace('-', '', trim($area_code[2]))
            ];
        }
        return $return;
    }
}


if (!function_exists('getInstallmentArray')) {
    function getInstallmentArray($installmentMAX)
    {
        $return = [];
        for ($i = 2; $i <= $installmentMAX; $i++) {
            $return[$i] = 'Parcelar em ' . $i . 'x sem/juros';
        }
        return $return;
    }
}

if (!function_exists('simpleInstallment')) {
    function simpleInstallment($value, $installment)
    {
        $division = coinToBco($value) / (int)$installment;
        return bcoToCoin($division);
    }
}

/**
 * Verifica se a pasta existe, caso não exista, é criada.
 * @param $folder
 * @return mixed
 */
if (!function_exists('folderExists')) {
    function folderExists($folder)
    {
        if (!is_dir($folder)) {
            mkdir($folder);
        }
        return $folder;
    }
}

if (!function_exists('noRepeatFile')) {
    function noRepeatFile($name, $ext, $folder)
    {
        $i = 0;
        do {
            if ($i) {
                $noRepeatFile = $i . '_' . $name . '_' . $ext;
            } else {
                $noRepeatFile = $name . '_' . $ext;
            }
            $i++;
        } while (Illuminate\Support\Facades\File::exists($folder . $noRepeatFile));
        return $noRepeatFile;
    }
}

if (!function_exists('bcoToCoin')) {
    function bcoToCoin($value)
    {
        return number_format($value, 2, ',', '.');
    }
}

if (!function_exists('route_field')) {
    function route_field()
    {
        return Form::hidden('page', request()->getUri());
    }
}

if (!function_exists('simple_coin')) {
    function simple_coin($number)
    {
        return number_format((float)$number, 2, '.', '');
    }
}

if (!function_exists('coinToBco')) {
    function coinToBco($get_valor)
    {
        if (strstr($get_valor, ',')) {
            $source = array(
                '.',
                ','
            );
            $replace = array(
                '',
                '.'
            );
            $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
        } else
            $valor = $get_valor;

        return $valor; //retorna o valor formatado para gravar no banco
    }
}

if (!function_exists('in_array_r')) {
    function in_array_r($needle, $haystack, $strict = true)
    {
        if (is_array($haystack)) {
            foreach ($haystack as $value) {
                if (($strict ? $value === $needle : $value == $needle) || (is_array($value) && in_array_r($needle, $value, $strict))) {
                    return true;
                }
            }
        }

        return false;
    }
}

if (!function_exists('currentRoute')) {
    function currentRoute($route, $attr = 'class="active"')
    {
        $currentPath = \Illuminate\Support\Facades\Session::get('currentMenu');
        if (in_array_r($route, $currentPath)) {
            return $attr;
        }
        return '';
    }
}

if (!function_exists('currentFrontRoute')) {
    function currentFrontRoute($route, $attr = 'class="active"', $fullurl = false)
    {
        if (!$fullurl) {
            $currentRoute = request()->route()->getName();
            if ($route == $currentRoute) {
                return $attr;
            }
        } else {
            if ($route == request()->fullUrl()) {
                return $attr;
            }
        }
        return '';
    }
}

if (!function_exists('effectRoute')) {
    function effectRoute()
    {
        $route = \Route::getCurrentRoute()->getName();
        if (strstr($route, '.')) {
            $routes = [
                'create' => trans('dictionary.create'),
                'edit' => trans('dictionary.edit')
            ];
            $route = explode('.', $route);
            if (isset($routes[$route[1]])) {
                return ' - ' . $routes[$route[1]];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

if (!function_exists('CryptoJSDecrypt')) {
    function CryptoJSDecrypt($jsonString, $passphrase = false)
    {
        $passphrase = (env('DECRYPT_FRONT')) ? env('DECRYPT_FRONT') : $passphrase;
        $jsondata = json_decode($jsonString, true);
        try {
            $salt = hex2bin($jsondata["s"]);
            $iv = hex2bin($jsondata["iv"]);
        } catch (Exception $e) {
            return null;
        }
        $ct = base64_decode($jsondata["ct"]);
        $concatedPassphrase = $passphrase . $salt;
        $md5 = array();
        $md5[0] = md5($concatedPassphrase, true);
        $result = $md5[0];
        for ($i = 1; $i < 3; $i++) {
            $md5[$i] = md5($md5[$i - 1] . $concatedPassphrase, true);
            $result .= $md5[$i];
        }
        $key = substr($result, 0, 32);
        $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
        return json_decode($data, true);
    }
}

if (!function_exists('CryptoJSEncrypt')) {
    function CryptoJSEncrypt($value, $passphrase = false)
    {
        $passphrase = (env('DECRYPT_FRONT')) ? env('DECRYPT_FRONT') : $passphrase;
        $salt = openssl_random_pseudo_bytes(8);
        $salted = '';
        $dx = '';
        while (strlen($salted) < 48) {
            $dx = md5($dx . $passphrase . $salt, true);
            $salted .= $dx;
        }
        $key = substr($salted, 0, 32);
        $iv = substr($salted, 32, 16);
        $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
        $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
        return $data;
    }
}

if (!function_exists('apiV')) {
    function apiV($url, $returnAll = false, $field = 'duration', $type = 'json')
    {
        $embed = 'http://vimeo.com/api/oembed';
        $typeOembed = [
            'json' => '.json?url=',
            'xml' => '.xml?url='
        ];
        $callURL = $embed . $typeOembed[$type] . rawurlencode($url);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $callURL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET"
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return false;
        } else {
//                $return = json_decode($response, true);
//                if ($returnAll)
//                    return $return;
//                if ($field == 'duration') {
//                    $seconds = $return[$field];
//                    return gmdate('H:i:s', $seconds);
//                }
            return $response;
        }
    }
}

if (!function_exists('apiYC')) {
    function apiYC($json, $return = false)
    {
        $response = $json;

        if ($response) {
            $duration = new DateInterval($response['contentDetails']['duration']);

            $sum = 0;
            if ($duration->h > 0)
                $sum += (int)$duration->h * 3600;
            if ($duration->i > 0)
                $sum += (int)$duration->i * 60;
            if ($duration->s > 0)
                $sum += $duration->s;

            $seconds = $sum;
            if (!$return)
                return gmdate('H:i:s', $seconds);
            return $response;
        }
        return '00:00:00';
    }
}

if (!function_exists('apiY')) {
    function apiY($url, $key = 'AIzaSyBEhPW4-2s-RV_ENFd2wHbw7Fkl1SQT3LY', $fields = 'snippet,contentDetails,statistics,status')
    {
        $content = (class_exists('\Modules\Course\Entities\Content')) ? new \Modules\Course\Entities\Content() : null;
        parse_str(parse_url($url, PHP_URL_QUERY), $vars);

        if (!isset($vars['v'])) {
            if (preg_match('/embed\/([a-zA-Z0-9]+)/', $url, $r)) {
                if (isset($r[1])) {
                    $vars['v'] = $r[1];
                } else {
                    $var['v'] = false;
                }
            } else {
                $var['v'] = false;
            }
        }

        if (isset($vars['v'])) {

            $embed = 'https://www.googleapis.com/youtube/v3/videos?id=' . $vars['v'] . '&key=' . $key . '&part=' . $fields;

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $embed,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET"
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return false;
            } else {
                $response = json_decode($response, true);
                $response = $response['items'][0];
                $duration = new DateInterval($response['contentDetails']['duration']);
//                    $seconds = ((int)$duration->i*60)+$duration->s;
                $sum = 0;
                if ($duration->h > 0)
                    $sum += (int)$duration->h * 3600;
                if ($duration->i > 0)
                    $sum += (int)$duration->i * 60;
                if ($duration->s > 0)
                    $sum += $duration->s;

                $seconds = $sum;

                if (!is_null($content))
                    $content->setSeconds($seconds);

                return gmdate('H:i:s', $seconds);
            }

        } else {
            return false;
        }
    }
}

if (!function_exists('n')) {
    function n($route, $title, $text)
    {
        \App\Notification::create([
            'route' => $route,
            'title' => $title,
            'text' => $text
        ]);
    }
}

if (!function_exists('resize')) {
    function resize($filename, $path, $width = 300, $height = 200, $ext = 'jpg')
    {
        if (\Illuminate\Support\Facades\File::exists($path . $filename)) {
            $image = \Intervention\Image\Facades\Image::make($path . $filename)->resize($width, $height);
            return $image->dirname . '/' . $image->basename;
        } else {
            return '';
        }
    }
}

if (!function_exists('reportToMe')) {
    function reportToMe($dev_email, array $messageArray, $alert = 'Urgente')
    {
        dd($messageArray);
//        if (count($messageArray)) {
//            \Illuminate\Support\Facades\Mail::send('email.alert', ['errors' => $messageArray], function ($message) use ($messageArray, $dev_email, $alert) {
//                $message->subject($alert);
//                $message->to($dev_email, $alert);
//            });
//        }
    }
}

if (!function_exists('date_interval')) {
    function date_interval($date1, $date2)
    {
        $html = '';
        $d1 = new DateTime($date1);
        $d2 = new DateTime($date2);
        $r = $d2->diff($d1);
        if ($r->y)
            $html .= ($r->y == 1) ? $r->y . ' ano ' : $r->y . ' anos ';
        if ($r->m) {
            if (!$r->y)
                $html .= ($r->m == 1) ? $r->m . ' mês ' : $r->m . ' meses ';
            else {
                $html .= ', ';
                $html .= ($r->m == 1) ? $r->m . ' mês ' : $r->m . ' meses ';
            }
        }
        if ($r->d) {
            if ($r->m == 0)
                $html .= ($r->d == 1) ? $r->d . ' dia ' : $r->d . ' dias ';
            else {
                $html .= ' e ';
                $html .= ($r->d == 1) ? $r->d . ' dia ' : $r->d . ' dias ';
            }
        }
        return $html;
    }
}

if (!function_exists('cpfValidate')) {
    function cpfValidate($cpf = null)
    {

        // Verifica se um número foi informado
        if (empty($cpf)) {
            return false;
        }

        // Elimina possivel mascara
        $cpf = preg_replace('/[^0-9]/', '', $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999'
        ) {
            return false;
            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        } else {

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }
}
if (!function_exists('cepValidate')) {
    function cepValidate($cep)
    {
        $cep = trim($cep);
        $avaliaCep = preg_match("/^[0-9]{5}-[0-9]{3}$/", $cep);
        if (!$avaliaCep) {
            return false;
        } else {
            return true;
        }
    }
}

if (!function_exists('onlyNumber')) {
    function onlyNumber($string)
    {
        return preg_replace('/\D/', '', $string);
    }
}

if (!function_exists('getDDD')) {
    function getDDD($phone)
    {
        $return = '';
        preg_match('/\(([0-9\)]*)\)/', $phone, $area_code);
        if (isset($area_code[1]))
            $return = $area_code[1];
        return onlyNumber($return);
    }
}

if (!function_exists('getPHONE')) {
    function getPHONE($phone)
    {
        $return = '';
        preg_match('/ ([0-9-]+)/', $phone, $area_code);
        if (isset($area_code[1]))
            $return = $area_code[1];
        return onlyNumber($return);
    }
}

if (!function_exists('bcoToCoin')) {
    function bcoToCoin($value)
    {
        return number_format($value, 2, ',', '.');
    }
}

if (!function_exists('simple_coin')) {
    function simple_coin($number)
    {
        return number_format((float)$number, 2, '.', '');
    }
}

if (!function_exists('coinToBco')) {
    function coinToBco($get_valor)
    {
        if (strstr($get_valor, ',')) {
            $source = array(
                '.',
                ','
            );
            $replace = array(
                '',
                '.'
            );
            $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
        } else
            $valor = $get_valor;

        return $valor; //retorna o valor formatado para gravar no banco
    }
}

if (!function_exists('getDecimals')) {
    function getDecimal($value)
    {
        return substr($value, strlen($value) - 2, strlen($value));
    }
}

if (!function_exists('l')) {
    function l($log, $content_id = null, $team_id = null, $course_id = null, $type = null, $status = false)
    {
        if (!auth()->guest()) {
            \Modules\User\Entities\UserContent::create(['content_id' => $content_id, 'team_id' => $team_id, 'course_id' => $course_id, 'user_id' => auth()->user()->id, 'log' => $log]);
            if (!is_null($type)) {
                $user_content_end = new \Modules\User\Entities\UserContentEnd();
                $query_exists = $user_content_end->where('user_id', '=', auth()->user()->id)->whereContentId($content_id)->first();
                if (!$query_exists) {
                    if ($type->text || $type->file) {
                        $status = true;
                    }
                    $user_content_end->create(['content_id' => $content_id, 'team_id' => $team_id, 'course_id' => $course_id, 'user_id' => auth()->user()->id, 'status' => $status]);
                } else if ($query_exists && $status) {
                    $query_exists->update(['status' => $status]);
                }
            }
        }
    }
}


if (!function_exists('cr')) {
    function cr($user_id, $content_id, $point)
    {
        if ($user_id && $content_id && $point) {
            $model = new \Modules\Lesson\Entities\ContentRating();
            if ($point < 0)
                $point = 0;
            if ($point > 5)
                $point = 5;

            $exist = $model->where('user_id', '=', $user_id)->where('lesson_id', '=', $content_id)->first();
            if (!$exist)
                $model->create(['user_id' => $user_id, 'lesson_id' => $content_id, 'point' => $point]);
            else {
                $exist->update(['point' => $point]);
            }
        }
    }
}

if (!function_exists('cache')) {
    /**
     * Get / set the specified cache value.
     *
     * If an array is passed, we'll assume you want to put to the cache.
     *
     * @param  dynamic  key|key,default|data,expiration|null
     * @return mixed
     *
     * @throws \Exception
     */
    function cache()
    {
        $arguments = func_get_args();

        if (empty($arguments)) {
            return app('cache');
        }

        if (is_string($arguments[0])) {
            return app('cache')->get($arguments[0], isset($arguments[1]) ? $arguments[1] : null);
        }

        if (is_array($arguments[0])) {
            if (!isset($arguments[1])) {
                throw new Exception(
                    'You must set an expiration time when putting to the cache.'
                );
            }

            return app('cache')->put(key($arguments[0]), reset($arguments[0]), $arguments[1]);
        }
    }
}
