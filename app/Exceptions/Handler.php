<?php

    namespace App\Exceptions;

    use Doctrine\DBAL\Query\QueryException;
    use Exception;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Session\TokenMismatchException;
    use Illuminate\Support\Facades\Log;
    use Illuminate\Support\Facades\Mail;
    use Symfony\Component\HttpKernel\Exception\HttpException;
    use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
    use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
    use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

    class Handler extends ExceptionHandler
    {
        /**
         * A list of the exception types that should not be reported.
         *
         * @var array
         */
        protected $dontReport = [
            HttpException::class,
            ModelNotFoundException::class
        ];

        /**
         * Report or log an exception.
         *
         * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
         *
         * @param  \Exception $e
         * @return void
         */
        public function report(Exception $e)
        {
            return parent::report($e);
        }

        /**
         * Render an exception into an HTTP response.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  \Exception               $e
         * @return \Illuminate\Http\Response
         */
        public function render($request, Exception $e)
        {
            if (!env('APP_DEBUG')) {
                if ($e instanceof MethodNotAllowedHttpException) {
                    return response()->view('errors.404', [], 500);
                }

                if ($e instanceof TokenMismatchException) {
                    return response()->view('errors.404', [], 500);
                }

                if ($e instanceof \Illuminate\Database\QueryException) {
                    reportToMe(env('DEVELOPER_EMAIL'), [$e->getSql() . ' bindings - ' . implode(',', $e->getBindings()), $e->getMessage()], 'Urgente');
                    return response()->view('errors.model', [], 500);
                }

                if ($e instanceof \ErrorException) {
                    reportToMe(env('DEVELOPER_EMAIL'), [$e->getMessage() . ' - file - ' . $e->getFile() . ':line:' . $e->getLine() .' - '. json_encode($_SERVER)]);
                    return redirect()->back()->with('error', 'Desculpe, um erro inesperado aconteceu em nossos servidores. Caso o erro persista, aguarde alguns minutos e tente novamente.');
                }
            } else {
                if ($e instanceof ModelNotFoundException) {
                    $e = new NotFoundHttpException($e->getMessage(), $e);
                }
            }
            return parent::render($request, $e);
        }
    }
