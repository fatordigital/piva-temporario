<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Facades\File;

    class Icon extends Model
    {
        public $timestamps = false;

        protected $fillable = [
            'icon'
        ];

        public static function findIconsFile($link, $regex = '/fa ([a-zA-Z-]+)/', $inline = false)
        {
            $rows = file($link);
            if ($inline) {
                preg_match_all($regex, $rows[0], $a);
                foreach ($a[0] as $icon) {
                    $save[] = [
                        'icon' => $icon
                    ];
                }
            } else {
                foreach ($rows as $row) {
                    if (preg_match($regex, $row, $a)) {
                        if (isset($a[0]) && !empty($a[0])) {
                            $save[] = [
                                'icon' => $a[0]
                            ];
                        }
                    }
                }
            }
            if (isset($save) && count($save))
                Icon::insert($save);
        }

    }


