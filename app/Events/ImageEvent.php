<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class ImageEvent extends Event
{
    use SerializesModels;

    public $image, $request, $collect;

    /**
     * ImageEvent constructor.
     * @param $request
     * @param $collect
     * @param $key_request
     *
     * responsive_sizes = array (  array( w => value, h => value ) || array( w => value ) || array( h => value ) )
     * sizes = array (  array( w => value, h => value ) || array( w => value ) || array( h => value ) )
     * path = caminho que será salvo os arquivos
     * callback = função de retorno
     *
     */
    public function __construct($request, $collect, $key_request = 'file')
    {
        $this->request = $request;
        $this->image = $request->file($key_request);
        $this->collect = collect($collect);
    }

}
