<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FilesEvent extends Event
{

    public $sizes, $path, $input_name, $request_file, $callback;

    /**
     * FilesEvent constructor.
     * @param $request_file
     * @param $sizes 1024x768, 800x600 ou 1024*768, 800*600
     * @param $path
     * @param string $input_name
     * @param $callback
     */
    public function __construct($request_file, array $sizes, $path, $callback, $input_name = 'file')
    {
        $this->request_file = $request_file;
        $this->sizes = $sizes;
        $this->path = $path;
        $this->input_name = $input_name;
        $this->callback = $callback;
    }

}
