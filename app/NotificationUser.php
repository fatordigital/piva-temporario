<?php

    namespace App;

    class NotificationUser extends BaseModel
    {

        protected $fillable = [
            'notification_id',
            'user_id',
            'read'
        ];

    }
