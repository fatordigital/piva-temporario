<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Auth;
use Modules\Acl\Entities\Group;
use Modules\Order\Entities\AbandonedOrder;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_id',
        'name',
        'email',
        'try_cms_access',
        'filename',
        'notifications',
        'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    private $actions = [];

    public function setPasswordAttribute($value)
    {
        if (!empty($value))
            $this->attributes['password'] = bcrypt($value);
        else {
            unset($this->attributes['password']);
        }
    }

    public function getNameEmailAttribute()
    {
        return $this->email . ' - ' . $this->name;
    }

    public function isPartner()
    {
        if ($this->group_id == 7 || $this->group_id == 11) {
            return true;
        }
        return false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function action_groups()
    {
        return $this->hasMany('Modules\Acl\Entities\ActionGroup', 'group_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function groups()
    {
        return $this->belongsTo('Modules\Acl\Entities\Group', 'group_id');
    }

    public function partner()
    {
        return $this->hasMany('Modules\Team\Entities\TeamTeacher', 'user_id')->where('partner', '=', 1)->lists('team_id')->toArray();
    }

    /**
     * @param string $id
     * @return array
     */
    public function getActions($id = 'route')
    {
        $return = [];
        $actions = Auth::user()->groups->actions;
        if ($actions) {
            foreach ($actions as $action) {
                if ($action->route) {
                    $return[] = $action->$id;
                }
            }
        }
        return $return;
    }

    public function ihp($route)
    {
        if (!$this->actions)
            $this->actions = $this->getActions();
        if (in_array($route, $this->actions) || in_array('*', $this->actions)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public function getGroups()
    {
        $g = [];
        $groups = Group::whereId(Auth::user()->group_id)->orWhere('parent_id', '=', Auth::user()->group_id)->get(['id'])->toArray();
        if ($groups) {
            foreach ($groups as $group) {
                $g[] = $group['id'];
            }
        }
        return $g;
    }

    /**
     * @return bool
     * Verifica se o usuário é admin
     */
    public function isAdmin()
    {
        if (in_array('*', $this->getActions())) {
            return true;
        }
        return false;
    }

    public static function getCartTotal($coin = true)
    {
        if (!session()->has('order_id')) {
            $order = AbandonedOrder::with('items')->whereUserId(Auth::user()->id)->get()->first();
        } else {
            $order = AbandonedOrder::with('items')->find(session('order_id'));
        }
        if ($order) {
            $total = $order->total;
        } else {
            $total = 0.00;
        }

        return ($coin) ? bcoToCoin($total) : $total;
    }


    public function socialize()
    {
        return $this->hasOne('Modules\User\Entities\UserSocialize', 'user_id');
    }

}
