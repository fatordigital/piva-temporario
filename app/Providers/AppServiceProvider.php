<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('dimensions', function($attribute, $value, $parameters, $validator)
        {
            $return = true;
            $file = request()->file($attribute);
            $image_info = getimagesize($file);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            if( $image_width > $parameters[0]) {
                $return = false;
            }
            if( $image_height > $parameters[1] ) {
                $return = false;
            }

            if ($return == false)
                return false;
            else {
                return true;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
