$(document).ready(function () {


    var dTable = $('.datatable').DataTable({
        lengthChange: false,
        "oLanguage": {
            "sInfo": lang.dataTable.info,
            "sSearch": lang.search,
            "oPaginate": {
                "sNext": lang.next,
                "sPrevious": lang.previous
            },
            "sEmptyTable": "Nenhum registro no momento"
        },
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            },
            {extend: 'copy', text: 'Copiar'},
            {extend: 'csv', text: 'CSV'},
            {extend: 'excel', text: 'EXCEL', title: 'ExampleFile'},
            {extend: 'pdf', text: 'PDF', title: 'ExampleFile'}
        ]
    });



});