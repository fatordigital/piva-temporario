$(document).ready(function () {
    var $actions = $('[data-extend-action]');
    var $selectAll = $('[data-select-all]');
    var $up = "fa fa-angle-up";
    var $down = "fa fa-angle-double-down";
    $actions.click(function () {
        var $id = $(this).data('extendAction');
        var $action = $('[data-action-id="' + $id + '"]');
        if (!$action.is(":visible")) {
            $('[data-arrow="' + $id + '"]').removeClass($down).addClass($up);
            $action.show();
        } else {
            $('[data-arrow="' + $id + '"]').removeClass($up).addClass($down);
            $action.hide();
        }
    });
    $selectAll.click(function () {
        var $id = $(this).data('selectAll');
        $('[data-action-id="' + $id + '"]').each(function () {
            if (!$(this).is(':checked')) {
                $(this).click();
            } else {
                $(this).click();
            }
        });
    })
});