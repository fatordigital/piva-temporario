$(document).ready(function () {
    var $year = new Date().getFullYear().toString();
    $('#datepicker .input-daterange').datepicker({
        format: 'dd/mm/yyyy',
        startDate: $year,
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
    $('select[name="course_category_id"]').change(function () {
        if ($(this).val() == 1) {
            $('#students').hide();
            $('input[name="max_student"]').val('99999999');
        } else {
            $('#students').show();
            $('input[name="max_student"]').val('1');
        }
    });
});
