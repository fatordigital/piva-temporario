$(document).ready(function(){
    $('input[name="team"]').change(function(){
        $('.modalTeam').hide();
        $('[data-team="'+$(this).val()+'"]').show();
    });
    $('[data-category-id]').click(function(e){
        e.preventDefault();
        var $id = $(this).data('categoryId');
        if ($id != '*') {
            $.each($('[data-category-childrens]'), function (key, element) {
                if ($(this).data('categoryChildrens') == $id) {
                    $(this).fadeIn();
                } else {
                    $(this).fadeOut();
                }
            });
        } else {
            $('[data-category-childrens]').fadeIn('slow');
        }
    });
});