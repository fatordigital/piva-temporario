$(document).ready(function () {
    $('#filename').change(function(event) {
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            var dataURL = reader.result;
            var output = document.getElementById('foto');
            output.src = dataURL;
        };
        reader.readAsDataURL(input.files[0]);
        $('#foto').parent().css({'height': '300px', 'overflow': 'hidden'});
    });
});

