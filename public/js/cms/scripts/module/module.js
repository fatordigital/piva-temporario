/**
 * Created by root on 18/01/16.
 */
$(document).ready(function () {
    $('.summernote').summernote();

    /**
     * Salva a imagem temporariamente
     */
    $('input[name=fakeFile]').change(function () {
        var $code = $('input[name=fakeCode]');
        if ($code.val() == "") {
            $('#fakeCode').click();
        }
        var $form = new FormData();
        $form.append('file', $(this)[0].files[0]);
        $form.append('code', $code.val());
        $.ajax({
            url: '/module/ajax/file',
            type: 'post',
            data: $form,
            contentType: false,
            processData: false
        });
    });

    $(".touchspin").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%',
        buttondown_class: 'btn btn-white',
        buttonup_class: 'btn btn-white'
    });
});

function fnDelteModule($el) {
    swal({
        title: lang.sweet_alert.delete.title,
        text: lang.sweet_alert.success.text,
        type: "warning",
        showCancelButton: true,
        cancelButtonText: lang.cancel,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: lang.sweet_alert.delete.confirm,
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (confirm) {
        if (confirm) {

            $('[data-content-id="' + $($el).data('contentId') + '"]').remove();

            $('<input>').attr({
                'type': 'hidden',
                'name': 'content_deletes[]',
                'value': $($el).data('contentId')
            }).appendTo('form');

            swal(lang.sweet_alert.delete.success.title, lang.sweet_alert.delete.success.text, 'success');
        } else {
            swal(lang.sweet_alert.cancel.title, lang.sweet_alert.cancel.text, 'success');
        }
    });
}

/**
 * Limpa todos os campos do formulário ao salvar as mudanças do conteúdo
 */
function clear() {
    var $each = $('input[name^="fake"], textarea[name^="fake"], select[name^="fake"]');
    $.each($each, function () {
        $(this).val('');
        $('.note-editable').html('');
    });
}
/**
 * Função simples que pega a alteração do tipo de conteúdo e insere no textbox
 * @param node
 * @param target
 */
function fnSelect(node, target) {
    var $node = $(node);
    var $target = $(target);
    $target.val($node.data('value'));
}
/**
 * Insere alteração na dataTable
 * @returns {boolean}
 */
function fnClickAddRow() {
    var $continue = true;
    var $each = $('input[name^="fake"], textarea[name^="fake"], select[name^="fake"]');
    $.each($each, function (i, $el) {
        var el = $($el);
        if (el.data('required') && el.val() == "" && !el.data('disabled')) {
            el.css({border: '1px solid red'});
            $continue = false;
        } else {
            el.css({border: '1px solid #e5e6e7'});
        }
    });
    if ($continue == true) {
        swal({
            title: lang.sweet_alert.success.title,
            text: lang.sweet_alert.success.text,
            type: "success"
        }, function (confirm) {
            if (confirm) {
                var $edit = $('#editable');
                var json = JSON.stringify({
                    type: $('input[name=fakeType]').val(),
                    name: $('input[name=fakeName]').val(),
                    value: $('input[name=fakeValue]').val(),
                    code: $('input[name=fakeCode]').val(),
                    file: $('input[name=fakeFile]').val(),
                    video: $('input[name=fakeVideo]').val(),
                    link: $('input[name=fakeLink]').val(),
                    text: $('#summermodal').code(),
                    status: $('input[name=fakeStatus]:checked').val()
                });
                $('<input>').attr({type: 'hidden', value: json, name: 'json[]'}).appendTo('form');
                $edit.dataTable().fnAddData([
                    $('input[name=fakeType]').val(),
                    $('input[name=fakeName]').val(),
                    $('input[name=fakeCode]').val(),
                    $('input[name=fakeFile]').val(),
                    $('input[name=fakeVideo]').val(),
                    $('input[name=fakeLink]').val(),
                    '<button class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>'
                ]);
                $('#myModal').modal('hide');
                clear();
            }
        });
    } else {
        return false;
    }
}