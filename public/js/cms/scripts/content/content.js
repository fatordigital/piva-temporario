function parseVideo(url) {
    return url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);
}

$(document).ready(function(){
    var $class = '';
    $('input[name="type"]').click(function(){
        $class = $(this).val();
        $.each($('[data-option]'), function (i, el) {
            if ($(el).data('option') != $class) {
                $(el).hide();
                $(el).find('textarea').attr('disabled', 'disabled');
                $(el).find('input').attr('disabled', 'disabled');
            } else {
                $(el).show();
                $(el).find('textarea').removeAttr('disabled');
                $('.note-editable').attr("contenteditable", true);
                $(el).find('input').removeAttr('disabled');
            }
        });
    });
    $('input[name="content"]').focusout(function(){
        if (!parseVideo($(this).val())) {
            swal('Ops', 'Adicione um link do vimeo', 'error');
        }
    })
});