var $return = true;
$(document).ready(function () {
    var $question = $('input[name=question]'),
        $addQuestion = $('[data-question]'),
        $content = $('#type_content');
    $('.clockpicker').clockpicker();
    $('#datepicker .input-daterange').datepicker({
        format: 'dd/mm/yy',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
    $addQuestion.click(function () {
        if ($question.val() != '') {
            typeContent($(this).data('question'), $content);
            $question.css({border: '1px solid #e5e6e7'});
        } else {
            $question.css({border: '1px solid red'});
        }
    });
});

function sendToTrashThis($el, e) {
    var $target, $ID;
    if ($el != null) {
        $target = $('[data-set-questions="' + e + '"]');
        $ID = $('[data-quest-id="' + $el + '"]');
    } else {
        $target = $('[data-set-questions="' + e + '"]');
        $ID = null;
    }
    deleteIt($ID, $target);
}

function find(id, father) {
    var $father = $('[data-id="' + father + '"]');
    var $json = JSON.parse($father.val());
    $('<input>').attr({
        type: 'hidden',
        name: 'delete_json[]',
        value: JSON.stringify($json.objective[id])
    }).appendTo('#append-form');
    $json.objective.splice(id, 1);
    $father.val(JSON.stringify($json));
}

function deleteIt($ID, $target) {
    if ($ID != null) {
        find($ID.data('questId'), $target.data('setQuestions'));
        $ID.remove();
    } else {
        $('<input>').attr({
            type: 'hidden',
            name: 'delete_json[]',
            value: $('[data-id="' + $target.data('setQuestions') + '"]').val()
        }).appendTo('#append-form');
        $('[data-content="' + $target.data('setQuestions') + '"]').remove();
    }
}

function findDataRules(flagName) {
    var $inputs = $('input[name^=' + flagName + ']');
    $.each($inputs, function () {
        if (!fnRequired($(this))) {
            $return = false;
        }
    });
}

function fnRequired($el, $falseCss, $trueCss) {
    $falseCss = ($falseCss) ? $falseCss : {border: '1px solid red'};
    $trueCss = ($trueCss) ? $trueCss : {border: '1px solid #e5e6e7'};
    if ($el.data('required') == 1 && $el.val() == '') {
        $el.css($falseCss);
        return false;
    } else {
        $el.css($trueCss);
        return true;
    }
}

function saveTest() {
    $return = true;
    findDataRules('fake');
    if ($return == true) {
        success();
    }
    return $return;
}

function fnView() {
    $('#content-view').html('');
    $.each($('input[name="json[]"]'), function (i, el) {
        var $el = JSON.parse($(el).val());
        if (!$('[data-content="' + convertToSlug($el.typeQuestion) + '"]').length) {
            fnNewHtml($el);
        }
        fnUpdateHtml($el, convertToSlug($el.typeQuestion));
    });
}

function fnUpdateHtml($json, $target) {
    var $html = '';
    if ($json.discursive) {
        $html += '<div class="form-group" data-quest-id="' + $target + '">';
        $html += '<div class="input-group">';
        $html += '<div>' + $json.discursive + '</div>';
        $html += '<span class="input-group-btn">';
        $html += '<button type="button" onclick="sendToTrashThis(null,' + $target + ')" class="btn btn-danger trash">';
        $html += '<i class="fa fa-trash"></i>';
        $html += '</button>';
        $html += '</span>';
        $html += '</div>';
        $html += '</div>';
    } else if ($json.part) {
        $html += '<div class="form-group" data-quest-id="' + $target + '">';
        $html += '<div class="input-group">';
        $html += '<div>' + $json.part + '</div>';
        $html += '<span class="input-group-btn">';
        $html += '<button type="button" class="btn btn-danger" onclick="sendToTrashThis(null,' + $target + ')">';
        $html += '<i class="fa fa-trash"></i>';
        $html += '</button>';
        $html += '</span>';
        $html += '</div>';
        $html += '</div>';
    } else {
        $.each($json.objective, function (i, el) {
            $html += '<div class="form-group" data-quest-id="' + i + '">';
            $html += '<div class="input-group">';
            $html += '<input type="text" class="form-control" readonly="readonly" value="' + el.value + '">';
            $html += '<span class="input-group-btn">';
            $html += '<button type="button" class="btn btn-danger" onclick="sendToTrashThis(' + i + ',' + $target + ')">';
            $html += '<i class="fa fa-trash"></i>';
            $html += '</button>';
            $html += '</span>';
            $html += '</div>';
            $html += '</div>';
        });
    }

    $('[data-set-questions="' + $target + '"]').append($html);
}

function fnNewHtml($json) {
    var $html = '';
    $html += '<div class="ibox-content" data-content="' + convertToSlug($json.typeQuestion) + '">';
    $html += '<h2>' + $json.typeQuestion + '</h2>';
    $html += '<label>' + $json.question + '</label>';
    $html += '<div data-set-questions="' + convertToSlug($json.typeQuestion) + '">';
    $html += '</div>';
    $html += '</div>';
    $('#content-view').append($html);
}


function success() {
    swal({
        title: lang.sweet_alert.success.title,
        text: lang.sweet_alert.success.text,
        type: "success"
    }, function () {
        $('<input>').attr({
            type: 'hidden',
            value: makeJsonTest(),
            'data-id': convertToSlug($('input[name=fakeType]').val()),
            name: 'json[]'
        }).appendTo('#append-form');
        fnView();
        clear();
    });
}

function makeJsonTest() {
    var $r = {
        typeQuestion: $('input[name=fakeType]').val(),
        question: $('input[name=fakeQuestion]').val(),
        discursive: $('textarea[name=fakeDiscursive]').code(),
        part: $('textarea[name=fakePart]').code(),
        objective: []
    };
    $.each($('input[name^=fakeObjective]'), function (i, el) {
        var $i = $('input[name^=check]:checked').data('ref');
        $r.objective.push({
            value: $(this).val(),
            checked: i == $i
        });
    });
    return JSON.stringify($r);
}


function clear() {
    var $each = $('input[name^="fake"], textarea[name^="fake"], select[name^="fake"]');
    $('.objective-quest').remove();
    $('[data-question]').show();
    $('.textFake').remove();
    $.each($each, function () {
        $(this).val('');
        $('.note-editable').html('');
    });
}

function objective(target) {
    var $html = '<div class="form-group objective-quest"><label for="objective">' + lang.answer + '</label>' +
        '<div class="input-group">' +
        '<span class="input-group-addon">' +
        '<input type="radio" data-ref="' + $('input[name^=check]').length + '" name="check[]">' +
        '</span>' +
        '<input type="text" name="fakeObjective[]" placeholder="' + lang.answer + '" data-required="1" class="form-control">' +
        '</div></div>';
    target.append($html)
}

function part(target) {
    var $html = '<div class="form-group textFake">' +
        '<label for="part">' + lang.part + '</label>' +
        '<textarea name="fakePart" class="summernote" />' +
        '</div>';
    if (!$('textarea[name=fakePart]').length) {
        target.append($html);
    }
}

function discursive(target) {
    var $html = '<div class="form-group textFake">' +
        '<label for="discursive">' + lang.discursive + '</label>' +
        '<textarea class="summernote" name="fakeDiscursive" />' +
        '</div>';
    if (!$('textarea[name=fakeDiscursive]').length) {
        target.append($html);
    }
}

function hideOtherTypes(t) {
    $.each($('[data-question]'), function () {
        if ($(this).data('question') != t) {
            $(this).hide();
        }
    });
}

function typeContent(type, target) {
    if (type != "" && type != undefined) {
        hideOtherTypes(type);
        window[type](target);
        $('.summernote').summernote();
    }
}