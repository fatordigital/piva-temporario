$(document).ready(function () {
    var $brand = $('#brand');
    $('#description').summernote({
        height: 200
    });
    $('#select, #select-icon').select2();
    $('#select-icon').change(function () {
        $brand.removeClass().addClass($(this).find(':selected').text());
    });
});