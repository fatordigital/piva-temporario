function timeOut(element) {
    setTimeout(function () {
        element.button('reset');
        element.removeClass('md-btn disabled');
    }, 4000);
}

var openFile = function(event, id) {
    var input = event.target;
    var reader = new FileReader();
    reader.onload = function(){
        var dataURL = reader.result;
        var output = document.getElementById(id);
        output.src = dataURL;
    };
    reader.readAsDataURL(input.files[0]);
};

$(document).ready(function () {

    $('.cpf-mask').mask('000.000.000-00');
    $('.cep-mask').mask('00000-000');

    $('.date-mask').mask('00/00/0000');

    $('.date-mask-us').mask('00-00-0000');

    $('.time-mask').mask('00:00:00', {
        placeholder: '00:00:00'
    });


    $('.tag-autocomplete').selectize({
        delimiter: ',',
        persist: false,
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });

    $('.text-autocomplete').selectize({
        create: true,
        sortField: 'text'
    });

    $('#datepicker .input-daterange').datepicker({
        format: 'dd/mm/yyyy',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('.s-datepicker').datepicker({
        format: 'dd/mm/yyyy',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('.load-select2').select2();

    function formatState (state) {
        return '<span><i class='+ state.id +'></i>' + state.text + '</span>';
    }

    var $select = $('.load-select2-icon').select2();
    $select.change(function(){
        $('#see-icon').removeAttr('class').addClass($(this).val());
    });

    $('a.btn').click(function () {
        if ($(this).data("loading") != false) {
            $(this).addClass('btn disabled').attr('data-loading-text', "<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i> " + $(this).text()).button('loading');
            timeOut($(this));
        }
    });
    $('.cLoading').click(function () {
        if ($(this).data("loading") != false) {
            $(this).addClass('disabled').attr('data-loading-text', "<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i>").button('loading');
            timeOut($(this));
        }
    });
    $('form').submit(function () {
        if ($(this).data("loading") != false) {
            $(this).find('button[type="submit"]').attr('data-loading-text', "<i class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></i> Aguarde...").addClass('disabled').button('loading');
            timeOut($(this).find('button[type="submit"]'));
        }
    });

    $('.select').select2();
    $('.summernote').summernote({
        height: 200
    });
    $('[data-delete]').submit(function () {
        var $this = $(this);
        swal({
            title: $this.data('title') ? $this.data('title') : 'Aviso',
            text: $this.data('delete'),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Continuar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                swal("Sucesso", "Ação foi deletada com sucesso", "success");
                $this.removeAttr('onsubmit');
                $this.submit();
            } else {
                swal("Cancelado", "Não se preocupe, nada aconteceu :)", "success");
                return false;
            }
        });
    });
    //
    //$('.value').maskMoney({
    //    symbol: 'R$ ',
    //    showSymbol: true, thousands: '.', decimal: ',', symbolStay: true
    //});

    $('.money').mask('000.000.000.000.000,00', {reverse: true});

    //$(this).tooltip({
    //    selector: "[data-toggle=tooltip]",
    //    container: "body",
    //    html: true
    //});

    $(document).find('[data-toggle="tooltip"],[data-title="title"]').qtip({
        position: {
            my: 'bottom left',
            at: 'top left'
        }
    });

});

function fnToastr(title, text, type) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    var $fn = window['toastr'];
    $fn[type](text, title);
}

function convertToSlug(Text) {
    console.log(Text);
    return Text
        .toLowerCase()
        .replace(/[^\w ]+/g, '')
        .replace(/ +/g, '-')
        ;
}

function genCode() {
    var text = "";
    var possible = "Qg9f3iGyIaw1yjh1ME0HJNL0bFeodMvk";
    for (var i = 0; i < 13; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text.toUpperCase();
}

