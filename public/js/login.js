var sendLogin = document.getElementById('sendLoginTop');
sendLogin.onclick = function () {
    var request = $f('#form-login-top').call({
        multiClick: false,
        clear: false,
        loadingText: 'Aguarde',
        complete: function () {
            var r = request.returnData;
            if (r.error == true) {
                swal('Professor Piva', r.msg, 'error');
            } else {
                request.form.reset();
                request.form.style.display = 'none';
                $('#guest-area').remove();
                $('#student-area').attr("href", r.studentAreaUrl).show();
                $(request.form).after(r.append);
                // swal('Professor Piva', r.msg, 'success');
                window.location.href = r.redirectUrl;
                // swal({
                //     title: 'Professor Piva',
                //     type: 'success',
                //     allowEscapeKey: false,
                //     text: r.msg,
                //     timer: 1000
                // }, function (isC) {
                //     if (isC == true || isC == null)
                //         window.location.href = r.redirectUrl;
                // });
            }
        }
    });
};