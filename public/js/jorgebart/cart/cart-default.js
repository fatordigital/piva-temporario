$(document).ready(function(){
    var buy = document.getElementById('sendContact');
    if (buy.length) {
        buy.onclick = function () {
            var request = $f('#buyForm').call({
                multiClick: false,
                complete: function () {
                    var r = request.returnData;
                    if (r.error == false) {
                        swal({
                                title: r.msg,
                                text: "",
                                type: "success",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "Sim",
                                cancelButtonText: "Não",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                            function (isConfirm) {
                                if (isConfirm) {
                                    window.location.href = r.redirectUrl;
                                } else {
                                    return true;
                                }
                            });
                    } else {
                        swal('Professor Piva', r.msg, 'info');
                    }
                }
            });
        }

    }
});