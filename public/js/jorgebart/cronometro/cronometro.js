function cronometro() {

    var hours = parseInt($("#horas").text());
    var minuto = parseInt($("#minutos").text());
    var segundo = parseInt($("#segundos").text());

    if (minuto > 0 && segundo === 0) {
        segundo = 60;
        minuto--;
    }
    if ((segundo - 1) >= 0) {
        segundo = segundo - 1;
        if (segundo == 0 && minuto == 0) {
            time = "00:00";
            $('#btn').removeClass('disabled');
        } else if (segundo < 10 && minuto === 0) {
            time = "0" + minuto + ":" + "0" + segundo;
        } else if (minuto >= 1) {
            time = (minuto < 10 ? '0' + minuto : minuto) + ":" + (segundo < 10 ? '0' + segundo : segundo);
        } else {
            time = "0" + minuto + ":" + segundo;
        }
        $("#time").text(time);
        $("#segundos").text(segundo);
        $("#minutos").text(minuto);
        setTimeout('cronometro();', 1000);
    }

}