$(document).ready(function () {
    var send = document.getElementById('send');
    send.onclick = function () {
        var request = $f('#form').call({
            multiClick: false,
            clear: false,
            complete: function () {
                var r = request.returnData;
                if (r.error == true) {
                    setErrors($('#form'), r);
                    swal('Professor Piva', r.msg, 'error');
                } else {
                    request.form.reset();
                    swal('Professor Piva', r.msg, 'success');
                }
            }
        });
    };
});

