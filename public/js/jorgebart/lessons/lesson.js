var id = 0;
$(document).ready(function () {

    $('select[name="orderBy"]').change(function () {
        if ($(this).val() != '')
            window.location.href = document.location.origin + document.location.pathname + '?disciplina=' + $(this).val();
        else {
            window.location.href = document.location.origin + document.location.pathname;
        }
    });

    function sendTimeAndClear(time, content_id) {
        window.timeUser = 0;
        id = 0;
        clearInterval(window.timeView);
        $.ajax({
            url: b + 'time-view.json',
            dataType: 'json',
            method: 'post',
            data: {_token: $('#_token').data('content'), request: send({time: time, id: content_id})}
        });
    }

    $('#myModal').on('hidden.bs.modal', function () {
        sendTimeAndClear(window.timeUser, id);
        $('#content-show').html('');
        $('#content-title').html('');
    });


    $('.content').click(function () {
        var $this = $(this);
        if ($this.data('security')) {
            $.ajax({
                url: b + 'get-content.json',
                dataType: 'json',
                method: 'post',
                data: {_token: $('#_token').data('content'), request: $this.data('security')},
                success: function (res) {
                    var r = response(res);
                    if (r.error == false) {
                        var data = r.data;
                        id = data.id;
                        $('#content-title').html(data.name);
                        if (data.video) {
                            var video = r.video;
                            if (video.html)
                                $('#content-show').html(video.html);
                            else {
                                $('#content-show').html(video);
                            }
                        } else {
                            $('#content-show').html(data.content);
                        }
                        $('#myModal').modal('show');
                        window.timeUser = 0;
                        window.timeView = setInterval(function () {
                            window.timeUser++;
                        }, 1000);
                        if (data.rating)
                            $('input[name="rating"][value="'+data.rating.point+'"]').prop('checked', true)
                    } else {
                        swal('Professor Piva', r.msg, 'error');
                    }
                }
            });
        }
    })
})