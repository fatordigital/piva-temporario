$(document).ready(function () {
    var sendRegister = document.getElementById('sendRegister');
    sendRegister.onclick = function () {
        var request = $f('#formc').call({
            multiClick: false,
            clear: false,
            complete: function () {
                var r = request.returnData;
                if (r.error == true) {
                    setErrors($('#formc'), r);
                    swal('Professor Piva', r.msg, 'error');
                } else {
                    request.form.reset();
                    swal({
                        title: 'Professor Piva',
                        text: r.msg,
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'OK'
                    }, function(c) {
                        if (c == true && r.redirectUrl) {
                            window.location.href = r.redirectUrl;
                        }
                    })
                }
            }
        });
    };
});

