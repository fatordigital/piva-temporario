/**
 * jQuery Menu and Manage Menu Plugin - v 0.0.1
 *
 * @author Andrew Rodrigues Brunoro <andrewrbrunoro@gmail.com>, 2015
 * @author FatorDigital http://fatordigital.com.br, 2015
 * @license MIT License <http://www.opensource.org/licenses/mit-license.php>
 */
(function ($, window, document, undefined) {

    var defaults = {
        selector: 'data-menu-id',
        class: 'active',
        data: 'menuId',
        parent: 'parentId',
        check: 'data-menu-check'
    };

    function FdMenu(input, options) {
        this.options = $.extend(true, {}, defaults, options);
        this.input = input;
        this.$input = $(input);
        this._defaults = defaults;
        this._name = 'fdmenu';
        this.init();
    }

    $.extend(FdMenu.prototype, {

        init: function () {
            this.initMenus();
            this.clickMenu();
        },

        initMenus: function () {
            if (localStorage.getItem('parent')) {
                $('[' + defaults.check + '="' + localStorage.getItem('parent') + '"]').addClass(defaults.class);
            }
            $('[' + defaults.check + '="' + localStorage.getItem('menu') + '"]').addClass(defaults.class);
        },

        clickMenu: function () {
            $('[' + defaults.selector + ']').click(function () {
                $('[' + defaults.check + ']').removeClass(defaults.class);
                localStorage.setItem('menu', $(this).data(defaults.data));
                localStorage.setItem('parent', $(this).data(defaults.parent));
                $(this).parent('li').addClass(defaults.class);
                if (localStorage.getItem('parent')) {
                    $('[' + defaults.check + '="' + localStorage.getItem('parent') + '"]').addClass(defaults.class);
                }
                if($(this).closest('ul')){
                    $(this).closest('ul').addClass('collapse').addClass('in');
                }
            });
        }
    });

    $.fn.fdmenu = function (options) {
        var attribute = 'plugin_fdmenu';
        if (typeof options == "object") {
            return this.each(function () {
                var instance = $.data(this, attribute);
                if (!instance) {
                    instance = new FdMenu(this, options);
                    $.data(this, attribute, instance)
                }
            });
        } else {
            var instance = $.data(this, attribute);
            if (!instance) {
                instance = new FdMenu(this, options);
                $.data(this, attribute, instance)
            }
        }
    }

})(jQuery, window, document);

$(document).ready(function () {
    $(this).fdmenu();
});