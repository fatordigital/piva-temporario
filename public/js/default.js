$f.pass = 'ta8iTL8sZ8V7hMSnBxciGFsYuJAZXMaq';
$f('form').prepareForm();

var sendNewsletter = document.getElementById('sendNewsletter');


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

// Vue.config.debug = true;
// new Vue({
//     el: 'body',
//     props: ['popup'],
//     data: {
//         popup_open: true
//     },
//     methods: {
//         close: function (e, popup_id) {
//             e.preventDefault();
//             this.popup_open = false;
//             var $cookies = getCookie('popups');
//             if (!$cookies)
//                 setCookie('popups', JSON.stringify([popup_id]));
//             else {
//                 $cookies = JSON.parse($cookies);
//                 if ($cookies.indexOf(popup_id) == -1) {
//                     var popups = [];
//                     popups.push(popup_id);
//                     $cookies.map(function(k, i) {
//                         popups.push(k);
//                     });
//                     setCookie('popups', JSON.stringify(popups));
//                 }
//             }
//         }
//     },
//     beforeCompile: function () {
//         var $cookies = getCookie('popups');
//         if ($cookies) {
//             $cookies = JSON.parse($cookies);
//             $cookies.map(function(k, i) {
//                 if (k == this.popup) {
//                     this.popup_open = false;
//                 }
//             }.bind(this));
//         }
//     }
// });



sendNewsletter.onclick = function () {
    var request = $f('#formn').call({
        multiClick: false,
        clear: false,
        complete: function () {
            var r = request.returnData;
            if (r.error == true) {
                setErrors($('#formn'), r);
                swal('Professor Piva', r.msg, 'error');
            } else {
                request.form.reset();
                swal('Professor Piva', r.msg, 'success');
            }
        }
    });
};


function setErrors(form, errors) {
    removeErrors(form);
    $.each(errors, function (key, value) {
        if (key != 'error' && key != 'msg') {
            if (form.find('[name="' + key + '"]').length) {
                var current = form.find('[name="' + key + '"]');
                current.parents('.form-group').addClass('has-error form-return');
                current.after('<span class="help-block form-return">' + value + '</span>');
            }
        }
    });
}

function removeErrors(form) {
    form.find('.help-block').remove();
    form.find('.form-return').removeClass('form-return').removeClass('has-error');
}


function send(data) {
    return CryptoJS.AES.encrypt(JSON.stringify(data), $f.pass, {format: CryptoJSAesJson}).toString();
}

function response(res) {
    return JSON.parse(CryptoJS.AES.decrypt(JSON.stringify(res), $f.pass, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8))
}