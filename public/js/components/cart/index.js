var Helper = {
    palavra_composta: function (value) {
        var er = /[A-z][ ][A-z]/;
        return er.test(value);
    },
    verifica_cpf: function(cpf) {
        cpf = cpf.replace(/[^\d]+/g,'');

        if(cpf == '') return false;

        // Elimina CPFs invalidos conhecidos
        if (cpf.length != 11 || cpf == "00000000000"
            || cpf == "11111111111" || cpf == "22222222222"
            || cpf == "33333333333" || cpf == "44444444444"
            || cpf == "55555555555" || cpf == "66666666666"
            || cpf == "77777777777" || cpf == "88888888888"
            || cpf == "99999999999")
            return false;

        // Valida 1o digito
        var add = 0;
        var i = 0;
        for (i=0; i < 9; i ++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
        var rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;

        // Valida 2o digito
        add = 0;
        for (i = 0; i < 10; i ++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return false;

        return true;
    }
};

new Vue({
    el: '#cart',
    props: ['amount'],
    data: {
        payment_wait_ready: 0,
        termos: false,
        pagseguro_hash: '',
        wait: false,
        brand: '',
        card_name: '',
        card_cpf: '',
        card_birth: '',
        card_month: '',
        card_token: '',
        card_year: '',
        card_plots: '',
        card_cvv: '',
        plots: [],
        cvvSize: 4,
        card_number: '',
        pay_methods: [],
        method_options: [],
        allow_methods: ['CREDIT_CARD', 'ONLINE_DEBIT', 'BOLETO'],
        current_method: 'CREDIT_CARD',
        image_url: 'https://stc.pagseguro.uol.com.br/',
        pagseguro: null,
        fd_open: false,
        fd_modal_small: $('.fd-modal-small'),
        fd_modal_overlay: $('.fd-modal-small-overlay')
    },
    methods: {
        __construct: function () {
            this.pagseguro = PagSeguroDirectPayment;
            this.loadMethods();
        },
        set_options: function (option) {
            this.method_options = [];
            if (option == 'CREDIT_CARD') {
                this.method_options = this.pay_methods[2].options;
            } else if (option == 'ONLINE_DEBIT') {
                this.method_options = this.pay_methods[1].options;
            }
        },
        select_brand: function (brand) {
            this.brand = brand;
            this.loadPlots();
        },
        set_option: function (val) {
            this.current_method = val;
            this.set_options(val);
        },
        loadMethods: function () {
            var self = this;
            this.pagseguro.getPaymentMethods({
                amount: self.amount,
                error: function (r) {
                    window.location.reload();
                },
                success: function (r) {
                    Object.keys(r.paymentMethods).map(function (i, k) {
                        if (self.allow_methods.indexOf(i) > -1) {
                            self.pay_methods.push(r.paymentMethods[i]);
                            if (i == 'CREDIT_CARD') {
                                self.set_options('CREDIT_CARD');
                            }
                        }
                    });
                    self.payment_wait_ready = 1;
                }
            });
        },
        loadPlots: function () {
            var self = this;
            this.pagseguro.getInstallments({
                amount: self.amount,
                brand: self.brand,
                success: function (r) {
                    self.plots = r.installments[self.brand];
                }
            });
        },
        moeda: function (value) {
            return "R$ " + value
                    .toFixed(2)
                    .replace(".", ",")
                    .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
        },
        credit_card_validate: function () {
            if (this.card_name == '') {
                toastr.error('Preencha o NOME IMPRESSO NO CARTÃO');
                return false;
            } else if (!Helper.palavra_composta(this.card_name)) {
                toastr.error('O Nome precisa estar igual ao impresso no cartão.');
                return false;
            }
            if (this.card_cpf == '') {
                toastr.error('Preencha o CPF DO TITULAR DO CARTÃO');
                return false;
            } else if (!Helper.verifica_cpf(this.card_cpf)) {
                toastr.error('Digite um CPF válido.');
                return false;
            }
            if (this.card_birth == '') {
                toastr.error('Preencha a DATA DE NASCIMENTO DO TITULAR');
                return false;
            } else if (!/(\d{2})[-.\/](\d{2})[-.\/](\d{4})/.exec(this.card_birth)) {
                toastr.error('Digite uma data de nascimento válida.');
                return false;
            }
            if (this.card_number == '') {
                toastr.error('Preencha o NÚMERO IMPRESSO NO CARTÃO');
                return false;
            }
            if (this.card_month == '') {
                toastr.error('Preencha o MÊS DE VALIDADE DO CARTÃO');
                return false;
            } else if (!/(\d{2})/.exec(this.card_month)) {
                toastr.error('Preencha o mês de validade do cartão com 2 dígitos.');
                return false;
            }
            if (this.card_year == '') {
                toastr.error('Preencha o O ANO DE VALIDADE DO CARTÃO');
                return false;
            } else if (!/(\d{4})/.exec(this.card_year)) {
                toastr.error('Preencha o ano de validade do cartão com 4 dígitos.');
                return false;
            }
            if (this.card_plots == '') {
                toastr.error('Selecione a QUANTIDADE PARCELAS');
                return false;
            }
            if (this.card_cvv == '') {
                toastr.error('Preencha o CÓDIGO DE SEGURANÇA DO CARTÃO');
                return false;
            }
            return true;
        },
        submit: function (e) {
            var self = this;
            if (this.wait == false) {
                this.wait = true;
                $('input[name="pagseguro_hash"]').val(PagSeguroDirectPayment.getSenderHash());
                if (this.current_method == 'CREDIT_CARD') {
                    if (this.credit_card_validate()) {
                        var callToken = new Promise(function (resolve, reject) {
                            self.pagseguro.createCardToken({
                                cardNumber: self.card_number,
                                cvv: self.card_cvv,
                                expirationMonth: self.card_month,
                                expirationYear: self.card_year,
                                success: function (r) {
                                    self.card_token = r.card.token;
                                    resolve(r.card);
                                },
                                error: function (r) {
                                    resolve(true);
                                }
                            });
                        });
                        callToken.then(function (r) {
                            $('#form-pagamento').submit();
                        });
                    } else {
                        self.wait = false;
                    }
                } else {
                    $('#form-pagamento').submit();
                }
            }
        },
        open_modal: function (status) {
            this.fd_open = status;
        }
    },
    beforeCompile: function () {
        this.__construct();
    },
    watch: {
        'card_number': function (val, old_val) {
            if (val.length == 6) {
                var self = this;
                this.pagseguro.getBrand({
                    cardBin: val,
                    success: function (r) {
                        self.brand = r.brand.name;
                        self.cvvSize = r.brand.cvvSize;
                        self.loadPlots();
                    }
                });
            }
        },
        'fd_open': function (val, old) {
            if (val == true) {
                this.fd_modal_small.fadeIn(300).addClass('open');
                this.fd_modal_overlay.fadeIn(300).addClass('open');
            } else {
                this.fd_modal_small.fadeOut(300).removeClass('open');
                this.fd_modal_overlay.fadeOut(300).removeClass('open');
            }
        }
    }
});