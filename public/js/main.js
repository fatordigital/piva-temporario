function setPoints (id, e) {
    var $this = $(e);
    $.ajax({
        url: b + 'set-content-rating.json',
        dataType: 'json',
        method: 'post',
        data: {_token: $('#_token').data('content'), request: send({lesson_id: id, points: $this.val()})},
        complete: function() {
            swal('Obrigado', 'Sua pontuação nos ajuda a criar conteúdos com maior qualidade.', 'success')
        }
    })
}

$(document).ready(function ($) {


    $('#trigger').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active')
            $(".nav-mobile").slideUp();
        } else {
            $(this).addClass('active')
            $(".nav-mobile").slideDown();
        }
    });

    $('.s-datepicker').datepicker({
        format: 'dd/mm/yyyy',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('.cpf-mask').mask('000.000.000-00');
    $('.cep-mask').mask('00000-000');

    $('.date-mask').mask('00/00/0000');

    $('input[name="rating"]').change(function(){
        setPoints(CURRENT_LESSON, this);
    });

    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    $('.phone-mask').mask(SPMaskBehavior, spOptions);

    /* Imagens de background de banners*/

    var img_detail = $(".img-news-detail").attr("src");
    $(".banner-news").css({
        "background-image": "url(" + img_detail + ")",
    });


    var img_detail = $(".img-news-detail").attr("src");
    $(".banner-curso-detail").css({
        "background-image": "url(" + img_detail + ")",
    });


    // questões e progress bar
    //var num_q = $("ul.num-questoes").find("li");
    //var progresso = 0;
    //var q;
    //for (var i = num_q.length - 1; i >= 0; i--) {
    //    var minha_questao = num_q[i];
    //    if ($(minha_questao).hasClass("btn-primary")) {
    //        progresso = parseInt(progresso) + 1;
    //        q = progresso;
    //    }
    //}
    //;
    //progresso = (progresso * 100) / parseInt(num_q.length);
    //$(".my-simulado .progress-bar").width(progresso + "%");
    //$(".progress-status").text("Progresso: " + q + "/" + num_q.length);

    var tempo = 0;
    var hora = 0;
    var minuto = 0;
    var segundo = 0;
    var aux;

    // click das   questoes next
    $("#my-next").click(function () {

        var num_q = $("ul.num-questoes").find("li");
        var num_q_p = $("ul.num-questoes li.btn-primary");

        if (parseInt(num_q.length) > parseInt(num_q_p.length)) {
            for (var y = 0; y <= num_q_p.length; y++) {
                var my_btn = num_q[y];
                if (y == num_q_p.length) {
                    $(my_btn).removeClass("btn-default");
                    $(my_btn).addClass("btn-primary");
                }
            }
        }
        var progresso = 0;
        for (var i = num_q.length - 1; i >= 0; i--) {
            var minha_questao = num_q[i];
            if ($(minha_questao).hasClass("btn-primary")) {
                progresso = parseInt(progresso) + 1;
                q = progresso;
            }
        }
        ;
        progresso = (progresso * 100) / parseInt(num_q.length);
        $(".my-simulado .progress-bar").width(progresso + "%");
        $(".progress-status").text("Progresso: " + q + "/" + num_q.length);

    })

    // click das   questoes prev
    $("#my-prev").click(function () {
        var num_q = $("ul.num-questoes").find("li");
        var num_q_p = $("ul.num-questoes li.btn-primary");

        if (parseInt(num_q.length) > parseInt(num_q_p.length)) {
            for (var y = 0; y < num_q_p.length; y++) {
                var my_btn = num_q[y];
                if (y == (num_q_p.length - 1)) {
                    $(my_btn).removeClass("btn-primary");
                    $(my_btn).addClass("btn-default");
                }
            }
        }
        var progresso = 0;
        for (var i = num_q.length - 1; i >= 0; i--) {
            var minha_questao = num_q[i];
            if ($(minha_questao).hasClass("btn-primary")) {
                progresso = parseInt(progresso) + 1;
                q = progresso;
            }
        }
        ;
        progresso = (progresso * 100) / parseInt(num_q.length);
        $(".my-simulado .progress-bar").width(progresso + "%");
        $(".progress-status").text("Progresso: " + q + "/" + num_q.length);

    })


    /* Transforma tabs em collapsse*/
    var tela = $(window).width();
    if (tela <= 767) {
        $('li.active').removeClass('active');
        $(".full-curso .nav-tabs li").removeClass('active');
        var lista_tabs = $(".full-curso .nav-tabs").find('li');
        for (var i = lista_tabs.length - 1; i >= 0; i--) {
            var id_tab = $(lista_tabs[i]).find("a").attr("href");
            var conteudo_antigo = $(lista_tabs[i]).html();

            if (id_tab == '#apresentacao') {
                $('a[href="#apresentacao"]').parents('li').addClass('active')
                $('a[href="#apresentacao"]').next().css({display: 'block'});
            }


            $(lista_tabs[i]).html(conteudo_antigo + "<div class='box-collapse'>" + $(id_tab).html() + "</div>");

            if ($(lista_tabs[i]).hasClass('active')) {
                $(lista_tabs[i]).find('div.box-collapse').slideDown();
            }
            else {
                $(lista_tabs[i]).find('div.box-collapse').hide();
            }
        }
        ;


        $(".full-curso .nav-tabs li a").click(function (event) {
            if (!$(this).parent().hasClass('active')) {
                $("div.box-collapse").slideUp();
                var element = $(this).parent().find("div.box-collapse");
                $(element).slideDown();
            }
        });
    }
    else {
        $("div.box-collapse").remove();
    }


    if (tela > 767) {
        $("#form-pagamento .col-lg-3 .bg-form").height($("#form-pagamento .col-lg-9 .bg-form").height());
    }
    else {
        $("#form-pagamento .col-lg-3 .bg-form").css({
            height: 'auto',
            "margin-bottom": "10px"
        });
    }

    $("more-cursos").height($(".meus-cursos").height());

});

$(window).resize(function (event) {

    /* Transforma tabs em collapsse*/
    var tela = $(window).width();
    if (tela <= 767) {


        $(".full-curso .nav-tabs li").removeClass('active');
        var lista_tabs = $(".full-curso .nav-tabs").find('li');
        for (var i = lista_tabs.length - 1; i >= 0; i--) {
            var id_tab = $(lista_tabs[i]).find("a").attr("href");
            var conteudo_antigo = $(lista_tabs[i]).html();

            $(lista_tabs[i]).html(conteudo_antigo + "<div class='box-collapse'>" + $(id_tab).html() + "</div>");

            if ($(lista_tabs[i]).hasClass('active')) {
                $(lista_tabs[i]).find('div.box-collapse').slideDown();
            }
            else {
                $(lista_tabs[i]).find('div.box-collapse').hide();
            }
        }
        ;


        $(".full-curso .nav-tabs li a").click(function (event) {
            if (!$(this).parent().hasClass('active')) {
                $("div.box-collapse").slideUp();
                var element = $(this).parent().find("div.box-collapse");
                $(element).slideDown();
            }
        });
    }
    else {
        $("div.box-collapse").remove();
    }

    if (tela > 767) {
        $("#form-pagamento .col-lg-3 .bg-form").height($("#form-pagamento .col-lg-9 .bg-form").height());
    }
    else {
        $("#form-pagamento .col-lg-3 .bg-form").css({
            height: 'auto',
            "margin-bottom": "10px"
        });
    }

    $("more-cursos").height($(".meus-cursos").height());

});


    


