var lang = {
    "options": "Options",
    "created_at": "Created at",
    "group": "Group",
    "status": "Status",
    "answer": "Answer",
    "objective": "Objective",
    "discursive": "Discursive",
    "part": "Pro Part",
    "info": "Information",
    "warning": "Warning",
    "error": "Error",
    "success": "Success",
    "search": "Search",
    "cancel": "Cancel",
    "next": "Next",
    "previous": "Previous",
    "dataTable": {
        "info": "Showing _START_ to _END_ of _TOTAL_ registers"
    },
    "sweet_alert": {
        "delete": {
            "title": "Are you sure?",
            "confirm": "Delete!",
            "success": {
                "text": "You register has been deleted with success",
                "title": "Register has been deleted"
            }
        },
        "cancel": {
            "title": "Action Canceled",
            "text": "Item not be deleted"
        },
        "warning": {
            "title": "Ops!",
            "confirmButton": "Ok!"
        },
        "success": {
            "title": "Uhul!",
            "text": "Data has been save with success"
        }
    },
    'code': 'Code',
    'value': 'Value',
    'file': 'File',
    'video': 'Video',
    'link': 'Link',
    'text': 'Text',
    'course_date': 'Course Date',
    'code_exists': 'Code exist',
    'section': 'Section',
    'name': 'Name'
};