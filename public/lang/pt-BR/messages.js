var lang = {
    "options": "Opções",
    "created_at": "Criado em",
    "group": "Grupo",
    "status": "Situação",
    "answer": "Resposta",
    "objective": "Objetive",
    "discursive": "Discursive",
    "part": "Peça Profissional",
    "info": "Informação",
    "warning": "Cuidado",
    "error": "Erro",
    "success": "Sucesso",
    "search": "Procurar",
    "cancel": "Cancelar",
    "next": "Próximo",
    "previous": "Anterior",
    "dataTable": {
        "info": "De _START_ para _END_ de _TOTAL_ registros"
    },
    "sweet_alert": {
        "delete": {
            "title": "Você tem certeza?",
            "confirm": "Deletar!",
            "success": {
                "text": "O seu registro foi deletado com sucesso",
                "title": "Registro foi deletado"
            }
        },
        "cancel": {
            "title": "Ação Cancelada!",
            "text": "Item não foi removido"
        },
        "warning": {
            "title": "Ops!",
            "confirmButton": "Entendi!"
        },
        "success": {
            "title": "Uhul!",
            "text": "O registro foi salvo com sucesso."
        }
    },
    'code': 'Código',
    'value': 'Valor',
    'file': 'Arquivo',
    'video': 'Vídeo',
    'link': 'Link',
    'text': 'Texto',
    'course_date': 'Data do curso',
    'code_exists': 'Código já existe',
    'section': 'Seção',
    'name': 'Nome'
};