@if ($paginator->count() > 1)
  <p>
      <a href="{{ $paginator->url(1) }}" class="btn btn-outline{{ ($paginator->currentPage() == 1) ? ' btn-default disabled' : ' btn-success' }}">
        <i class="fa fa-arrow-left"></i>
      </a>

      @for ($i = 1; $i <= $paginator->lastPage(); $i++)
        <a href="{{ $paginator->url($i) }}" class="btn btn-outline btn-primary{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">{!! $i !!}</a>
      @endfor

      <a href="{{ $paginator->url($paginator->currentPage()+1) }}" class="btn btn-outline{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' btn-default disabled' : ' btn-success' }}">
        <i class="fa fa-arrow-right"></i>
      </a>
  </p>
@endif