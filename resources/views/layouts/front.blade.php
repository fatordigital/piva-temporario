<!DOCTYPE html>
<html lang="pt-br">
	<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" >
    <meta http-equiv="X-UA-Compatible" content="IE=8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta id="_token" data-content="{!! csrf_token() !!}">
    <meta name="author" content="Fator Digital">
    <meta name="keywords" content="{!! s('Keywords') !!}">
    <meta name="description" content="{!! s('Description') !!}">
    <link rel="shortcut icon" type="image/x-icon" href="{!! url('/assets/images/favicon.ico') !!}" />

    <meta property="og:locale" content="pt_BR">
    <meta property="og:url" content="{!! route('front.home') !!}">
    <meta property="og:title" content="{!! !isset($titleHead) ?  'Cursos Professor Piva | Concursos e Exames' : $titleHead . ' - Cursos Professor Piva' !!}">
    <meta property="og:site_name" content="Professor Piva">
    <meta property="og:description" content="{!! s('Description') !!}">

    @if (request()->route()->getName() == "front.home")
      <meta property="og:type" content="website">
      <meta property="og:image" content="http://professorpiva.dev/images/logo.png">
      <meta property="og:image:type" content="image/png">
      <meta property="og:image:width" content="207">
      <meta property="og:image:height" content="72">
    @elseif(in_array(request()->route()->getName(), ['novidades.show', "cursos.show"]))
      <meta property="og:type" content="article">
      <meta property="article:author" content="Professor Piva">
      <meta property="article:section" content="{!! isset($section) ? $section : '' !!}">
      <meta property="article:tag" content="{!! isset($tags) ? $tags : '' !!}">
      <meta property="article:published_time" content="{!! isset($publish) ? $publish : '' !!}">

      @if (!isset($current_image))
        <meta property="og:image" content="http://professorpiva.dev/images/no-image.jpg">
        <meta property="og:image:type" content="image/jpg">
        <meta property="og:image:width" content="360">
        <meta property="og:image:height" content="205">
      @else
        <meta property="og:image" content="{!! $current_image !!}">
      @endif
    @endif

		<title>
            {!! !isset($titleHead) ?  'Cursos Professor Piva | Concursos e Exames' : $titleHead . ' - Cursos Professor Piva' !!}
        </title>

		{!! HTML::style('css/professorpiva/lib/bootstrap.min.css') !!}
        {!! HTML::style('css/plugins/sweetalert/sweetalert.css') !!}
        {!! HTML::style('js/plugins/toastr/toastr.min.css') !!}
        {!! HTML::style('css/style.min.css') !!}
        {!! HTML::style('css/custom.me.css') !!}

		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
        <script type="text/javascript">var b = '{!! url('/') !!}/';</script>

        <script>
            window.b = '{!! url('/') !!}/';
            window.Laravel = {
                url: '{!! url('/') !!}'
            };
        </script>

        @if (isset($pagseguro_id) && isset($pagseguro_url))
            {!! HTML::script($pagseguro_url) !!}
            <script type="text/javascript">
                PagSeguroDirectPayment.setSessionId('{!! $pagseguro_id !!}');
            </script>
            {!! HTML::script('/js/plugins/vue/vue.min.js') !!}
            <script>
                Vue.config.debug = true;
            </script>
            {!! HTML::script('/js/plugins/vue/vue-resource.js') !!}
        @endif
	</head>
	<body popup="{!! isset($popup) && $popup ? $popup->id : '' !!}">
	
	@if (isset($popup) && $popup)
		<div class="fd-modal-overlay show" style="display: none;"></div>

		<div id="modal-popup" v-show="popup_open">
			<div class="pull-left">
				<a href="#" class="btn btn-danger btn-xs no-rounded" v-on:click="close($event, '{!! $popup->id !!}')" type="button">
					Fechar
				</a>
			</div>
			@if (isset($popup->image['wh']))
				<a href="{!! $popup->url !!}" target="_blank">
					<img src="{!! url('/assets/popup/' . $popup->image['wh']) !!}" class="img-responsive"/>
				</a>
			@endif
		</div>
	@endif

        <div class="loading-page">
          <div class="counter">
            <p></p>
            <h1></h1>
            <svg id="loader-pencil" xmlns="http://www.w3.org/2000/svg" width="667" height="182" viewBox="0 0 677.34762 182.15429">
              <g>
                <path id="body-pencil" d="M128.273 0l-3.9 2.77L0 91.078l128.273 91.076 549.075-.006V.008L128.273 0zm20.852 30l498.223.006V152.15l-498.223.007V30zm-25 9.74v102.678l-49.033-34.813-.578-32.64 49.61-35.225z">
                </path>
                <path id="line" d="M134.482 157.147v25l518.57.008.002-25-518.572-.008z">
                </path>
              </g>
            </svg>
          </div>
        </div>
        <header>
			<div class="container-fluid">
				<div class="row row-login">
					<div class="col-lg-7 col-md-7 col-ms-7 col-xs-12">
						<ul class="nav nav-pills">
							<li>
								<p>
									Precisa de ajuda?
								</p>
							</li>
							<li>
								<p>
									<span class="icon-phone"></span> (51) -
								</p>
							</li>

                            <li>
								<p>
									<span class="glyphicon glyphicon-shopping-cart"></span> <a href="{!! route('carrinhos.index') !!}" style="color: #FFF !important;">Carrinho</a>
								</p>
							</li>
						</ul>
					</div>
					<div class="col-lg-5 col-md-5 col-ms-5 col-xs-12">
                        @if (auth()->guest())
                          <p id="guest-area">
                            <span class="icon-graduation-cap"></span> ÁREA DO ALUNO
                          </p>
                          <a href="" id="student-area" style="display: none;">
                            <p>
                              <span class="icon-graduation-cap"></span> ACESSAR ÁREA DO ALUNO
                            </p>
                          </a>
                        @else
                          <a href="{!! route('front.user.home') !!}" id="student-area">
                            <p>
                              <span class="icon-graduation-cap"></span> ACESSAR ÁREA DO ALUNO
                            </p>
                          </a>
                        @endif
                        @if (auth()->guest())
                        {!! Form::open(['route' => 'front.verify.ajax', 'id' => 'form-login-top', 'class' => 'form-inline']) !!}
                          <div class="form-group">
                            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail(*)']) !!}
                          </div>
                          <div class="form-group">
                            {!! Form::password('pass', ['class' => 'form-control', 'placeholder' => 'Senha (*)']) !!}
                          </div>
                          <button type="submit" id="sendLoginTop" class="btn btn-default">Entrar</button>
                          <a href="{!! route('front.user.register.guest') !!}" class="btn btn-cadastro">Cadastre-se</a>
						{!! Form::close() !!}
						@else
                        <div class="logado" style="display: block;">
							<p class="pull-left">
                              {!! str_limit(auth()->user()->name, 25) !!}
                            </p>
                            <a href="{!! route('front.logout') !!}" class="logout pull-right">SAIR</a>
						</div>
                        @endif
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-menu">
						<a href="{!! route('front.user.home') !!}" class="logo">
							<img src="{!! url('/assets/images/logo-piva.png') !!}" class="img-responsive">
						</a>
						<ul class="nav nav-pills menu">

							<li {!! currentFrontRoute(route('cursos.index', 'q=presenciais'), 'class="active"', true) !!}>
                              <a href="{!! route('cursos.index', 'q=presenciais') !!}">CURSOS PRESENCIAIS</a>
                            </li>
							<li {!! currentFrontRoute(route('cursos.index', 'q=ead'), 'class="active"', true) !!}>
                              <a href="{!! route('cursos.index', 'q=ead') !!}">CURSOS EAD</a>
                            </li>
							<li {!! currentFrontRoute('conteudos.index') !!}>
                              <a href="{!! route('conteudos.index') !!}">CONTEÚDO GRÁTIS</a>
                            </li>
							<li {!! currentFrontRoute(route('cursos.index', 'q=capacitacoes'), 'class="active"', true) !!}>
                              <a href="{!! route('cursos.index', 'q=capacitacoes') !!}">CURSOS DE CAPACITAÇÃO</a>
                            </li>
							<li {!! currentFrontRoute('novidades.index') !!}>
                              <a href="{!! route('novidades.index') !!}">NOTÍCIAS</a>
                            </li>
							<li>
                              <a href="{!! route('front.contact') !!}">CONTATO</a>
                            </li>
						</ul>
                        <div class="menu-mobile">
							<a class="trigger" id="trigger">
								<span class="glyphicon glyphicon-menu-hamburger"></span>
							</a>
							<ul class="nav-mobile">
                                @if (auth()->guest())
                                  <li class="bg-warning">
                                    <a href="{!! route('front.login') !!}">Área do aluno</a>
                                  </li>
                                @else
                                  <li class="bg-warning">
                                    <a href="{!! route('front.user.home') !!}" id="student-area">
                                      <p>
                                        <span class="icon-graduation-cap"></span> ACESSAR ÁREA DO ALUNO
                                      </p>
                                    </a>
                                  </li>
                                @endif
								<li {!! currentFrontRoute(route('cursos.index', 'q=presenciais'), 'class="active"', true) !!}>
                                  <a href="{!! route('cursos.index', 'q=presenciais') !!}">CURSOS PRESENCIAIS</a>
                                </li>
                                <li {!! currentFrontRoute(route('cursos.index', 'q=ead'), 'class="active"', true) !!}>
                                  <a href="{!! route('cursos.index', 'q=ead') !!}">CURSOS EAD</a>
                                </li>
                                <li {!! currentFrontRoute('conteudos.index') !!}>
                                  <a href="{!! route('conteudos.index') !!}">CONTEÚDO GRÁTIS</a>
                                </li>
                                <li {!! currentFrontRoute(route('cursos.index', 'q=capacitacoes'), 'class="active"', true) !!}>
                                  <a href="{!! route('cursos.index', 'q=capacitacoes') !!}">CURSOS DE CAPACITAÇÃO</a>
                                </li>
                                <li {!! currentFrontRoute('novidades.index') !!}>
                                  <a href="{!! route('novidades.index') !!}">NOTÍCIAS</a>
                                </li>
                                <li {!! currentFrontRoute('novidades.index') !!}>
                                  <a href="{!! route('front.contact') !!}">CONTATO</a>
                                </li>
                                @if (!auth()->guest())
                                  <li>
                                    <a href="{!! route('front.logout') !!}" class="logout">SAIR</a>
                                  </li>
                                @endif
							</ul>
						</div>
					</div>
				</div>
			</div>
			@yield('top')
		</header>

        @yield('content')

        <footer>
          <div class="container">
              <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                      <p>
                          <span class="icon-phone"></span>(51) 3223.0033 <br>
                          <span class="icon-phone"></span> (51) 3029.8522 <br>
                          <span class="icon-phone"></span> (51) 99411.9896
                      </p>
                      <p> <span class="icon-envelope"></span> teste@teste.com.br</p>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                      <h4>Compre online e pague com</h4>
                      <img src="{!! url('/images/icon-cartoes.png') !!}" class="img-responsive center-block">
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                      <h4>Cadastre seu e-mail para receber novidades e promoções!</h4>
                      {!! Form::open(['route' => 'front.form.newsletter', 'id' => 'formn', 'method' => 'POST']) !!}
                      <div class="form-group">
                          {!! Form::text('name', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Nome (*)']) !!}
                      </div>
                      <div class="form-group">
                          {!! Form::text('email', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'E-mail (*)']) !!}
                      </div>
                      <button type="submit" id="sendNewsletter" class="btn btn-warning"> CADASTRAR</button>
                      {!! Form::close() !!}
                  </div>
              </div>
          </div>


		</footer>


        <footer>
          <div class="container">
            <div class="row">
              <section class="fd pull-right" style="margin-right: 40px; margin-top: -60px;">
                <script type="text/javascript" src="https://www.gstatic.com/swiffy/v7.4/runtime.js"></script>
                {!! HTML::script('js/assignature.js') !!}
                <div id="assinatura_fator" style="width: 100px; height: 50px">
                </div>
                <script>
                  var stage = new swiffy.Stage(document.getElementById('assinatura_fator'),
                          swiffyobject, {});
                  stage.setBackground(null);
                  stage.start();
                </script>
            </section>
            </div>
          </div>
        </footer>


        {!! HTML::script('js/aes.js') !!}
        {!! HTML::script('js/cms/jquery-2.1.1.js') !!}
        {!! HTML::script('js/cms/bootstrap.min.js') !!}
        {!! HTML::script('js/plugins/sweetalert/sweetalert2.min.js') !!}
        {!! HTML::script('js/plugins/datapicker/bootstrap-datepicker.js') !!}
        {!! HTML::script('js/plugins/jquery-mask-plugin/jquery.mask.js') !!}
        {!! HTML::script('js/plugins/toastr/toastr.min.js') !!}
        {!! HTML::script('js/aes-json-format.js') !!}
        {!! HTML::script('js/fd.min.js') !!}
        {!! HTML::script('js/default.js') !!}
        {{--<script type="text/javascript" src="http://updateyourbrowser.net/asn.js"> </script>--}}
        @if(auth()->guest())
            {!! HTML::script('js/login.js') !!}
        @endif
        <script type="text/javascript">
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-full-width",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        </script>
        @yield('js')
        {!! HTML::script('js/main.js') !!}
        <script type="text/javascript">
        @if(session()->has('success'))
          toastr.success('{!! session('success') !!}');
        @elseif(session()->has('info'))
          toastr.info('{!! session('info') !!}');
        @elseif(session()->has('warning'))
          toastr.warning('{!! session('warning') !!}');
        @elseif(session()->has('error'))
          toastr.error('{!! session('error') !!}');
        @endif
        </script>

	</body>
</html>