<div class="row border-bottom">
    <nav class="navbar navbar-static-top"
         role="navigation"
         style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button"
                    class="navbar-minimalize minimalize-styl-2 btn btn-primary">
                <i class="fa fa-bars"></i>
            </button>
        </div>
      {{--@if (\Auth::user())--}}
      {{--<div id="notification" data-name="{!! \Auth::user()->name !!}"></div>--}}
      {{--@endif--}}
      <ul class="nav navbar-top-links navbar-right">
          @if (!auth()->guest())
          <li>
              <span class="m-r-sm text-muted welcome-message">{!! auth()->user()->name !!}</span>
          </li>
          @endif
          <li>
              <a href="{!! route('logout') !!}">
                  <i class="fa fa-sign-out"></i> Sair!
              </a>
          </li>
      </ul>
    </nav>
</div>

