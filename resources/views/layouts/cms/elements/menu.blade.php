<nav class="navbar-default navbar-static-side"
     role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu"
            id="side-menu">
            <li class="nav-header text-center">
                <div class="dropdown profile-element">
                    <span>
                        @if (auth()->user() && fex('assets/users/', auth()->user()->filename))
                            <img alt="image"
                                 width="124"
                                 class="img-circle"
                                 src="{!! URL::to('assets/users/', auth()->user()->filename) !!}"/>
                        @else
                            <img alt="image"
                                 class="img-circle"
                                 src="{!! URL::to('img/profile.jpg') !!}"/>
                        @endif
                    </span>
                    <a href="{!! route('fatorcms.user.edit', auth()->user()->id) !!}">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">
                                    {!! Auth::user()->name !!}
                                </strong>
                            </span>
                            <span class="text-muted text-xs block">{!! (Auth::user()->user_information) ? Auth::user()->user_information->operation : '' !!}
                                {{--<b class="caret"></b>--}}
                            </span>
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                    FD
                </div>
            </li>
            @if (isset($menus))
                @foreach($menus as $menu)
                    @if (auth()->user()->group_id == 7 && auth()->user()->isPartner() == 0)
                      @if ($menu->seo_url != 'site')
                        @if (!empty($menu->route) || $menu->parents->count())
                          @if (checkRoute($menu->route))
                            <li {!! currentRoute($menu->seo_url) !!}>
                                      <a href="{!! ($menu->route) ? URL::route($menu->route) : 'javascript:void(0);' !!}">
                                          {!! ($menu->icon) ? '<i class="'.$menu->icon->icon.'"></i>' : '' !!}
                                        <span class="nav-label">
                                          {!! $menu->name !!}
                                      </span>
                                        {!! ($menu->parents->count()) ? '<span class="fa arrow"></span>' : '' !!}
                                      </a>
                              @if ($menu->parents->count())
                                <ul class="nav nav-second-level collapse">
                                              @foreach($menu->parents as $parent)
                                    <li {!! currentRoute($parent->seo_url) !!}>
                                                      <a href="{!! ($parent->route) ? URL::route($parent->route) : 'javascript:void(0);' !!}">
                                                          {!! ($parent->icon) ? '<i class="'.$parent->icon->icon.'"></i>' : '' !!}
                                                        {!! $parent->name !!}{!! effectRoute() !!}
                                                        {!! ($parent->parents->count()) ? '<span class="fa arrow"></span>' : '' !!}
                                                      </a>
                                      @if ($parent->parents->count())
                                        <ul class="nav nav-third-level">
                                                              @foreach($parent->parents as $third)
                                            <li {!! currentRoute($third->seo_url) !!}>
                                                                      <a href="{!! ($third->route) ? URL::route($third->route) : 'javascript:void(0);' !!}">
                                                                          {!! ($third->icon) ? '<i class="'.$third->icon->icon.'"></i>' : '' !!}
                                                                        {!! $third->name !!}{!! effectRoute() !!}
                                                                      </a>
                                                                  </li>
                                          @endforeach
                                                          </ul>
                                      @endif
                                                  </li>
                                  @endforeach
                                          </ul>
                              @endif
                                  </li>
                          @endif
                        @endif
                      @endif
                    @else
                      @if (!empty($menu->route) || $menu->parents->count())
                        @if (checkRoute($menu->route))
                          <li {!! currentRoute($menu->seo_url) !!}>
                                    <a href="{!! ($menu->route) ? URL::route($menu->route) : 'javascript:void(0);' !!}">
                                        {!! ($menu->icon) ? '<i class="'.$menu->icon->icon.'"></i>' : '' !!}
                                      <span class="nav-label">
                                        {!! $menu->name !!}
                                    </span>
                                      {!! ($menu->parents->count()) ? '<span class="fa arrow"></span>' : '' !!}
                                    </a>
                            @if ($menu->parents->count())
                              <ul class="nav nav-second-level collapse">
                                            @foreach($menu->parents as $parent)
                                  <li {!! currentRoute($parent->seo_url) !!}>
                                                    <a href="{!! ($parent->route) ? URL::route($parent->route) : 'javascript:void(0);' !!}">
                                                        {!! ($parent->icon) ? '<i class="'.$parent->icon->icon.'"></i>' : '' !!}
                                                      {!! $parent->name !!}{!! effectRoute() !!}
                                                      {!! ($parent->parents->count()) ? '<span class="fa arrow"></span>' : '' !!}
                                                    </a>
                                    @if ($parent->parents->count())
                                      <ul class="nav nav-third-level">
                                                            @foreach($parent->parents as $third)
                                          <li {!! currentRoute($third->seo_url) !!}>
                                                                    <a href="{!! ($third->route) ? URL::route($third->route) : 'javascript:void(0);' !!}">
                                                                        {!! ($third->icon) ? '<i class="'.$third->icon->icon.'"></i>' : '' !!}
                                                                      {!! $third->name !!}{!! effectRoute() !!}
                                                                    </a>
                                                                </li>
                                        @endforeach
                                                        </ul>
                                    @endif
                                                </li>
                                @endforeach
                                        </ul>
                            @endif
                                </li>
                        @endif
                      @endif
                    @endif
                @endforeach
            @endif
        </ul>
    </div>
</nav>