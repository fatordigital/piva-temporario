<!DOCTYPE html>
<html lang="{!! \Config::get('app.locale') !!}">
  <head>
      <meta charset="utf-8">
      <meta name="viewport"
            content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
      <meta name="_token"
            data-content="{!! csrf_token() !!}"/>
      <meta name="description"
            content="">
      <meta name="author"
            content="">
      <meta name="_d8aa4b94ba5a823b53cde96be4a3aef7" data-content="{!! \Config::get('app.json-key') !!}">
      <link rel="icon" type="image/x-icon" href="{!! url('/assets/images/favicon.ico') !!}" />
      <title>Área administrativa - {!! s('Project.name') !!}</title>
      {!! HTML::style('/css/cms/plugins/selectize/selectize.default.css') !!}
      <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
      {!! HTML::style('/css/fatorcms.min.css') !!}
      @yield('ccss')
      @if (file_exists('lang/'. \Config::get('app.locale').'/messages.js'))
          {!! HTML::script('lang/'. \Config::get('app.locale').'/messages.js') !!}
      @endif
      <script type="text/javascript">var b = '{!! URL::to('/') !!}';</script>
  </head>
  <body class="pace-done">

    <div id="wrapper">
        @if (auth()->user())
          @include('layouts.cms.elements.menu')
        @endif
        <div id="page-wrapper" class="gray-bg dashbard-1">
            @include('layouts.cms.elements.top')
            @yield('elements')
            @yield('title')
            <div class="row">
                <div class="wrapper wrapper-content animated fadeInRight">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    {!! HTML::script('js/cms/jquery-2.1.1.js') !!}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    {!! HTML::script('js/plugins/slimscroll/jquery.slimscroll.min.js') !!}
    {!! HTML::script('js/plugins/metisMenu/jquery.metisMenu.js') !!}
    {!! HTML::script('js/plugins/select2/select2.full.min.js') !!}
    {!! HTML::script('js/plugins/summernote/summernote.min.js') !!}
    {!! HTML::script('js/plugins/jQtip/jquery.qtip.min.js') !!}
    {!! HTML::script('js/plugins/datapicker/bootstrap-datepicker.js') !!}
    {!! HTML::script('js/plugins/toastr/toastr.min.js') !!}
    {!! HTML::script('js/plugins/selectize/selectize.min.js') !!}
    {!! HTML::script('js/plugins/sweetalert/sweetalert.min.js') !!}
    {!! HTML::script('js/plugins/jquery-mask-plugin/jquery.mask.js') !!}
    @yield('cjs')
    {{--{!! HTML::script(elixir('js/cms/components/notification/notification.min.js')) !!}--}}
    {!! HTML::script('js/cms/inspinia.js') !!}
    {!! HTML::script('js/cms/default.js') !!}
    @if(session()->has('success'))
        <script type="text/javascript">
            $(document).ready(function () {
                fnToastr(lang.success, '{!! session('success') !!}', 'success');
            });
        </script>
    @elseif(session()->has('info'))
        <script type="text/javascript">
            $(document).ready(function () {
                fnToastr(lang.info, '{!! session('info') !!}', 'info');
            });
        </script>
    @elseif(session()->has('warning'))
        <script type="text/javascript">
            $(document).ready(function () {
                fnToastr(lang.warning, '{!! session('warning') !!}', 'warning');
            });
        </script>
    @elseif(session()->has('error'))
        <script type="text/javascript">
            $(document).ready(function () {
                fnToastr(lang.error, '{!! session('error') !!}', 'error');
            });
        </script>
    @endif


  </body>
</html>