<div class="col-sm-12">
    <div class="row">
        <!-- .Your image -->
        <div class="col-md-12">
            <label for="">
                Banner Apresentação
            </label>
            <div class="img-container">
                {!! HTML::image(isset($image) && !empty($image) ? $image : 'assets/img/gallery/Image02.jpg', null, ['id' => 'image', 'class' => 'img-responsive']) !!}
            </div>
        </div>
        <!-- /.Your image -->
    </div>
</div>

<div class="col-sm-12">
    <div class="row">
        <div class="col-md-9 docs-buttons">
            <!-- .btn groups -->
            <div class="btn-group">
                <button type="button" class="btn btn-info" data-method="setDragMode" data-option="move" title="Move">
                    <span class="docs-tooltip" data-toggle="tooltip"
                          title="$().cropper(&quot;setDragMode&quot;, &quot;move&quot;)"> <span
                                class="fa fa-arrows"></span> </span></button>
                <button type="button" class="btn btn-info" data-method="setDragMode" data-option="crop" title="Crop">
                    <span class="docs-tooltip" data-toggle="tooltip"
                          title="$().cropper(&quot;setDragMode&quot;, &quot;crop&quot;)"> <span
                                class="fa fa-crop"></span> </span></button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-success" data-method="zoom" data-option="0.1" title="Zoom In"><span
                            class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;zoom&quot;, 0.1)"> <span
                                class="fa fa-search-plus"></span> </span></button>
                <button type="button" class="btn btn-success" data-method="zoom" data-option="-0.1" title="Zoom Out">
                    <span class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;zoom&quot;, -0.1)"> <span
                                class="fa fa-search-minus"></span> </span></button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-default btn-outline" data-method="move" data-option="-10"
                        data-second-option="0" title="Move Left"><span class="docs-tooltip" data-toggle="tooltip"
                                                                       title="$().cropper(&quot;move&quot;, -10, 0)"> <span
                                class="fa fa-arrow-left"></span> </span></button>
                <button type="button" class="btn btn-default btn-outline" data-method="move" data-option="10"
                        data-second-option="0" title="Move Right"><span class="docs-tooltip" data-toggle="tooltip"
                                                                        title="$().cropper(&quot;move&quot;, 10, 0)"> <span
                                class="fa fa-arrow-right"></span> </span></button>
                <button type="button" class="btn btn-default btn-outline" data-method="move" data-option="0"
                        data-second-option="-10" title="Move Up"><span class="docs-tooltip" data-toggle="tooltip"
                                                                       title="$().cropper(&quot;move&quot;, 0, -10)"> <span
                                class="fa fa-arrow-up"></span> </span></button>
                <button type="button" class="btn btn-default btn-outline" data-method="move" data-option="0"
                        data-second-option="10" title="Move Down"><span class="docs-tooltip" data-toggle="tooltip"
                                                                        title="$().cropper(&quot;move&quot;, 0, 10)"> <span
                                class="fa fa-arrow-down"></span> </span></button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-default btn-outline" data-method="rotate" data-option="-45"
                        title="Rotate Left"><span class="docs-tooltip" data-toggle="tooltip"
                                                  title="$().cropper(&quot;rotate&quot;, -45)"> <span
                                class="fa fa-rotate-left"></span> </span></button>
                <button type="button" class="btn btn-default btn-outline" data-method="rotate" data-option="45"
                        title="Rotate Right"><span class="docs-tooltip" data-toggle="tooltip"
                                                   title="$().cropper(&quot;rotate&quot;, 45)"> <span
                                class="fa fa-rotate-right"></span> </span></button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-default btn-outline" data-method="scaleX" data-option="-1"
                        title="Flip Horizontal"><span class="docs-tooltip" data-toggle="tooltip"
                                                      title="$().cropper(&quot;scaleX&quot;, -1)"> <span
                                class="fa fa-arrows-h"></span> </span></button>
                <button type="button" class="btn btn-default btn-outline" data-method="scaleY" data-option="-1"
                        title="Flip Vertical"><span class="docs-tooltip" data-toggle="tooltip"
                                                    title="$().cropper(&quot;scaleY&quot;, -1)"> <span
                                class="fa fa-arrows-v"></span> </span></button>
            </div>

            <div class="btn-group">
                <button type="button" class="btn btn-default btn-outline" data-method="crop" title="Crop"><span
                            class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;crop&quot;)"> <span
                                class="fa fa-check"></span> </span></button>
                <button type="button" class="btn btn-default btn-outline" data-method="clear" title="Clear"><span
                            class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;clear&quot;)"> <span
                                class="fa fa-remove"></span> </span></button>
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-default btn-outline" data-method="reset" title="Reset"><span
                            class="docs-tooltip" data-toggle="tooltip" title="$().cropper(&quot;reset&quot;)"> <span
                                class="fa fa-refresh"></span> </span></button>
                <label class="btn btn-default btn-outline btn-upload" for="inputImage" title="Upload image file">
                    {!! Form::file($name, ['class' => 'sr-only', 'accept' => 'image/*', 'id' => 'inputImage']) !!}
                    <span class="docs-tooltip" data-toggle="tooltip" title="Selecionar a imagem"> <span
                                class="fa fa-upload"></span> Selecione a imagem </span> </label>
            </div>

            <div class="btn-group btn-group-crop">
                <button type="button" class="btn btn-danger" data-method="getCroppedCanvas"><span class="docs-tooltip"
                                                                                                  data-toggle="tooltip"
                                                                                                  title="$().cropper(&quot;getCroppedCanvas&quot;)"> Baixar CROP </span>
                </button>
                <button type="button" class="btn btn-danger btn-outline" data-method="getCroppedCanvas"
                        data-option="{ &quot;width&quot;: 84, &quot;height&quot;: 84 }"><span class="docs-tooltip"
                                                                                               data-toggle="tooltip"
                                                                                               title="$().cropper(&quot;getCroppedCanvas&quot;, { width: 84, height: 84 })"> 84&times;84 </span>
                </button>
                <button type="button" class="btn btn-danger btn-outline" data-method="getCroppedCanvas"
                        data-option="{ &quot;width&quot;: 800, &quot;height&quot;: 600 }"><span class="docs-tooltip"
                                                                                                data-toggle="tooltip"
                                                                                                title="$().cropper(&quot;getCroppedCanvas&quot;, { width: 800, height: 600 })"> 800&times;600 </span>
                </button>
            </div>

            <!-- Show the cropped image in modal -->
            <div class="modal fade docs-cropped" id="getCroppedCanvasModal" aria-hidden="true"
                 aria-labelledby="getCroppedCanvasTitle" role="dialog" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="getCroppedCanvasTitle">Imagem cropada</h4>
                        </div>
                        <div class="modal-body"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg">Baixar</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.modal -->
        </div>
        <!-- /.btn groups -->
    </div>
</div>

