@extends('layouts.auth_cms')

@section('content')
    <a href="/">
        {!! HTML::image('img/cms/logo.png', 'Fator Digital', ['class' => 'text-center']) !!}
    </a>

    <h4 class="panel-title">
        {!! trans('messages.login.welcome') !!}
    </h4>

    {!! Form::open(['route' => 'auth', 'class' => 'm-t']) !!}
    <div class="form-group">
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => trans('auth.username'), 'required' => 'required']) !!}
        {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
    </div>
    <div class="form-group">
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('auth.password'), 'required' => 'required']) !!}
        {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
    </div>

    <button type="submit" class="btn btn-primary block full-width m-b">
        {!! trans('dictionary.signin') !!}
    </button>
    <a href="#">
        <small>{!! trans('auth.forgot_password') !!}</small>
    </a>
    {!! Form::close() !!}
    <p class="m-t">
        <small>FatorDigital - Todos os direitos reservados @2015</small>
    </p>
@stop