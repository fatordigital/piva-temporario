@extends('layouts.cms')

@section('content')
  <div class="col-md-12">
      <div class="error-template">
          <h1>:(</h1>
          <h2>Um erro inesperado aconteceu no nosso sistema, mas não se preocupe, será resolvido dentro de instantes.</h2>
      </div>
  </div>
@stop