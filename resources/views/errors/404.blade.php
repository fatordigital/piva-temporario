<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Professor Piva - Página não encontrada</title>

  {!! HTML::style('css/professorpiva/lib/bootstrap.min.css') !!}
  {!! HTML::style('css/fatorcms.min.css') !!}

</head>

<body class="gray-bg">

    <div class="middle-box text-center animated fadeInDown">
        <h1>:(</h1>
        <br />
        <h3 class="font-bold">Página não encontrada.</h3>

        <div class="error-desc">
            Clique no botão abaixo para voltar para página inicial
        </div>
        <br />
        <a href="{!! url('/') !!}" class="btn btn-success btn-block">Voltar</a>
    </div>

</body>

</html>
