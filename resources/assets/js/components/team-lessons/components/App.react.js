var React = require('react'),
    Model = require('../stores/Store'),
    Controller = require('../actions/Actions')

var App = React.createClass({

    getInitialState () {
        return {
            options: {},
            selected: {},
            discipline: {},
            chooses: []
        };
    },

    componentDidMount () {
        Controller.getDisciplines();
        Controller.getOptions();
        if (this.props.edit)
            Controller.edit(this.props.edit);
    },

    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    componentDidUpdate() {
        $('.s-datepicker').datepicker({
            format: 'dd/mm/yyyy',
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });
    },

    _onChange: function () {
        this.setState({
            options: Model.getOptions(),
            selected: Model.getSelects(),
            discipline: Model.getDisciplines(),
            chooses: Model.getChooses()
        });
    },

    handleSelect(e) {
        Controller.selectOption(e.target.value);
        $(e.target).val("");
    },

    handleDestroy(id, e) {
        swal({
            title: "Você tem certeza?",
            text: "Deseja realmente remover a aula da turma?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            cancelButtonText: "Não",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                Controller.destroy(id);
                swal("Deletada", "A aula foi removida da turma com sucesso.", "success");
            } else {
                swal("Cancelado", "A aula continua nos registros.", "error");
            }
        });
    },

    handleFilter (e) {
        Controller.getOptions(e.target.value, this.state.chooses);
    },

    handleAll (e) {
        var ids = [];
        Object.keys(this.state.options).map(function (key, row) {
            ids.push(this.state.options[row].id);
        }.bind(this));
        for (var i = 0; i < ids.length; i++) {
            Controller.selectOption(ids[i]);
        }
    },

    render() {
        return (
            <div className="col-lg-12">
                <div className="row">
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label htmlFor="discipline">
                                [Filtro] Disciplina
                            </label>
                            <select className="form-control" onChange={this.handleFilter}>
                                <option value="">Filtre por disciplina</option>
                                {Object.keys(this.state.discipline).map(function (key, row) {
                                    return <option key={key} value={this.state.discipline[row].id}>{this.state.discipline[row].name}</option>
                                }.bind(this))}
                            </select>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className={Object.keys(this.state.options).length ? 'col-lg-12' : 'col-lg-12'}>
                        <div className="form-group">
                            <label htmlFor="select">
                                Selecione as aulas <strong className="text-success">(Opcional)</strong>
                            </label>
                            <div className="input-group">
                                <select onChange={this.handleSelect} className="form-control">
                                    {function(){
                                        if (Object.keys(this.state.options).length) {
                                            return <option value="">Selecione as aulas</option>
                                        } else {
                                            return <option value="">Nenhuma aula cadastrada</option>
                                        }
                                    }.bind(this)()}
                                    {Object.keys(this.state.options).map(function (key, row) {
                                        return <option key={key} value={this.state.options[row].id}>{this.state.options[row].name}</option>
                                    }.bind(this))}
                                </select>
                                <span onClick={this.handleAll} className="input-group-addon" data-toggle="title" title="Adicionar todos da lista">
                                    <i className="fa fa-plus" />
                                </span>
                                <a href={b + '/fatorcms/lesson/create'} className="btn input-group-addon btn-success">Criar aula</a>
                            </div>
                        </div>
                    </div>
                </div>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <td>Aula</td>
                            <td>Disciplina</td>
                            <td>Resumo</td>
                            <td width="4%">Ordem</td>
                            <td width="10%">Data início</td>
                            <td width="5%">Opções</td>
                        </tr>
                    </thead>
                    <tbody>
                    {Object.keys(this.state.selected).map(function(key, row){
                        return (
                            <tr key={key}>
                                <td>{this.state.selected[row].name}</td>
                                <td>{this.state.selected[row].discipline ? this.state.selected[row].discipline.name : 'Não disciplinada'}</td>
                                <td>{this.state.selected[row].resume}</td>
                                <td>
                                    <input type="text" className="form-control" defaultValue={this.state.selected[row].order} name={"lesson[" + this.state.selected[row].id + "][order]"} />
                                </td>
                                <td>
                                    <input type="text" autoComplete="off" defaultValue={this.state.selected[row].Begin} className="form-control s-datepicker" name={"lesson[" + this.state.selected[row].id + "][begin_date]"} />
                                </td>
                                <td>
                                    <button type="button" className="btn btn-xs btn-danger" onClick={this.handleDestroy.bind(this, this.state.selected[row].id)}>
                                        <i className="fa fa-trash" />
                                    </button>
                                    <input type="hidden" name={"lesson[" + this.state.selected[row].id + "][id]"} value={this.state.selected[row].id} />
                                </td>
                            </tr>
                        )
                    }.bind(this))}
                    </tbody>
                </table>
            </div>
        )
    }

});

module.exports = App;
