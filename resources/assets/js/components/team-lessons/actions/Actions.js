var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants')

module.exports = {

    getDisciplines () {
        $.ajax({
            url: b + '/fatorcms/get-disciplines.json',
            dataType: 'json',
            method: 'get',
            success: function (res) {
                if (res.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.DISCIPLINES,
                        data: res.data
                    });
                }
            }
        });
    },

    getOptions (discipline, chooses) {
        $.ajax({
            url: b + '/fatorcms/lessons.json',
            dataType: 'json',
            data: {discipline: discipline, chooses: chooses},
            method: 'get',
            success: function (res) {
                if (res.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.OPTIONS,
                        data: res.data
                    });
                }
            }
        });
    },

    selectOption(id) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.SELECT,
            data: id
        });
    },

    destroy (id) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.DESTROY,
            data: id
        });
    },

    edit (id) {
        $.ajax({
            url: b + '/fatorcms/lessons.json',
            dataType: 'json',
            method: 'get',
            data: {id: id},
            success: function (res) {
                if (res.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.EDIT,
                        data: res.data
                    });
                }
            }
        });
    }

};