var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    assign = require('react/lib/Object.assign'),
    EventEmitter = require('events').EventEmitter,
    Helpers = require('../Helpers/Helpers.react'),
    CHANGE_EVENT = 'change',
    _options = {},
    _select = {},
    _disciplines = {},
    _chooses = []

function setOptions (data) {
    _options = data;
}

function setSelect(data) {
    _select = $.merge([Helpers.getObject(data, _options, 'id')], _select);
    _chooses = $.merge([data], _chooses);
    Helpers.removeObject(data, _options, 'id');
}

function destroy (data) {
    _options = $.merge([Helpers.getObject(data, _select, 'id')], _options);
    Helpers.removeObject(data, _select, 'id');
}

function edit(data) {
    $.each(data, function(key, row) {
        if (row) {
            row.lesson.Begin = row.Begin;
            row.lesson.order = row.order;
            _select = $.merge([row.lesson], _select);
            Helpers.removeObject(row.lesson.id, _options, 'id');
        }
    });
}

function discipline (data) {
    _disciplines = data;
}

var StartStore = assign(EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener (callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    getOptions () {
        return _options;
    },
    getSelects () {
        return _select;
    },
    getDisciplines () {
        return _disciplines;
    },
    getChooses () {
        return _chooses;
    }
});

StartStore.dispatchToken = AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {
        case AppConstants.OPTIONS:
            setOptions(action.data);
            break;
        case AppConstants.SELECT:
            setSelect(action.data);
            break;
        case AppConstants.DESTROY:
            destroy(action.data);
            break;
        case AppConstants.EDIT:
            edit(action.data);
            break;
        case AppConstants.DISCIPLINES:
            discipline(action.data);
            break;
    }
    StartStore.emitChange();
    return true;
});


module.exports = StartStore;
