var React = require('react'),
    Controller = require('../actions/Actions');

var Create = React.createClass({

    getInitialState () {
        return {
            name: '',
            errors: {
                name: ''
            }
        };
    },

    handleSubmit (e) {
        e.preventDefault();
        var name = this.refs.name;
        if (!name.value.trim()) {
            this.setState({errors: {name: 'Este campo é obrigatório, por favor, preencha o campo.'}});
        } else {
            this.setState({errors: {name: ''}});
        }
        Controller.store({name: name.value.trim(), _token: $('meta[name="_token"]').data('content')}, this.props.store);
        this.refs.name.value = '';
    },

    render () {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <form action={this.props.store} method="post" data-loading="false" onSubmit={this.handleSubmit}>
                      <input type="hidden" name="_token" value="" />
                      <div className={(this.state.errors.name) ? 'form-group has-error' : 'form-group'}>
                          <label htmlFor="name">
                              Nome/Título <strong className="text-dange">(*)</strong>
                          </label>
                          <div className="input-group">
                              <input type="text" ref="name" name="name" className="form-control" />
                              <span className="input-group-btn">
                                  <button type="submit" className="btn btn-primary">Salvar Registro</button>
                              </span>
                          </div>
                          <span className="help-block text-danger">{this.state.errors.name}</span>
                      </div>
                    </form>
                </div>
            </div>
        )
    }
});

module.exports = Create;
