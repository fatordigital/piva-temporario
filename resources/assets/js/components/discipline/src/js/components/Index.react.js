var React = require('react'),
    Controller = require('../actions/Actions');

var Index = React.createClass({

    handleDestroy (e) {
        e.preventDefault();
        var $this = $(e.target);
        Controller.destroy($this.attr('href'));
        $this.parents('tr').remove();
    },

    render () {
        var self = this;
        var tBody = this.props.data.map(function(row){
            return (
                <tr key={row.id}>
                   <td>{row.name}</td>
                   <td>
                      <a href={self.props.update.replace('*id*', row.slug)} className="btn btn-xs btn-primary pull-left">
                        <i className="fa fa-pencil" />
                      </a>
                      <a href={self.props.delete.replace('*id*', row.slug)} onClick={self.handleDestroy} className="btn btn-xs btn-danger pull-left">
                        <i className="fa fa-trash" />
                      </a>
                   </td>
               </tr>
            )
        });
        return (
            <div className="row">
                <div className="col-lg-12">
                  <table className="table table-bordered">
                      <thead>
                      <tr>
                          <th>Título</th>
                          <th width="8%">Opções</th>
                      </tr>
                      </thead>
                      <tbody>
                      {tBody}
                      </tbody>
                    </table>
                </div>
            </div>
        )
    }
});
module.exports = Index;