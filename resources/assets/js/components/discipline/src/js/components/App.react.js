var React = require('react'),
    StartStore = require('../stores/Store'),
    Controller = require('../actions/Actions'),
    Index = require('../components/Index.react'),
    Create = require('../components/Create.react'),
    Edit = require('../components/Edit.react');

var App = React.createClass({

    getInitialState () {
        return {
            settings: {store: '', update: '', delete: ''},
            data: []
        };
    },

    componentDidMount () {
        Controller.setSettings(this.props.settings);
        Controller.setData(this.props.data);
    },

    componentWillMount: function () {
        StartStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        StartStore.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            data: StartStore.getData(),
            settings: StartStore.getSettings()
        });
    },

    render() {
        return (
            <div className="modal inmodal fade" id="discipline-index" tabIndex="-1" role="dialog" aria-hidden="true">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header" style={{borderBottom: '0px !important'}}>
                            <button type="button" className="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span className="sr-only">Close</span></button>
                            <h4 className="modal-title">Disciplinas</h4>
                            <ul className="nav nav-tabs">
                              <li className="active"><a data-toggle="tab" href="#tab-discipline-index">Disciplinas</a></li>
                              <li><a data-toggle="tab" href="#tab-discipline-create">Nova Disciplina</a></li>
                            </ul>
                        </div>
                        <div className="modal-body tab-content">
                          <div id="tab-discipline-index" className="tab-pane fade in active">
                            <Index data={this.state.data} update={this.state.settings.update} delete={this.state.settings.delete} />
                          </div>
                          <div id="tab-discipline-create" className="tab-pane fade">
                            <Create store={this.state.settings.store} />
                          </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-warning" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

});

module.exports = App;