var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants');

function setData (data) {
    AppDispatcher.handleViewAction({
        actionType: AppConstants.STORE,
        data: data
    });
}

module.exports = {
    setSettings (data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.SETTINGS,
            data: data
        });
    },
    setData (data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.STORE,
            data: data
        });
    },
    destroy (destroy) {
        $.ajax({
            url: destroy,
            type: 'DELETE',
            data: {_method: 'DELETE', _token: $('meta[name="_token"]').data('content')},
            dataType: 'json',
            success: function (res) {
                swal('Professor Piva', res.msg, 'success');
            },
            error: function (res) {
                res = res.responseJSON;
                swal('Erro', res.msg, 'error');
            }
        });
    },
    store (data, store) {
        $.ajax({
            url: store,
            type: 'post',
            data: data,
            dataType: 'json',
            success: function (res) {
                setData(res.data);
                swal('Professor Piva', res.msg, 'success');
            },
            error: function (res) {
                res = res.responseJSON;
                swal('Erro', res.msg, 'error');
            }
        });
    }
};