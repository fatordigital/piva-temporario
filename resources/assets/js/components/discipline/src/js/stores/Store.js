var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    assign = require('react/lib/Object.assign'),
    EventEmitter = require('events').EventEmitter,
    CHANGE_EVENT = 'change',
    _data = {},
    _settings = {
        update: '',
        store: '',
        delete: ''
    };

function setData(data) {
    _data = $.merge(data, _data);
}

function setSettings (setting) {
    _settings = setting;
}

var StartStore = assign(EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener (callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    getData () {
        return _data;
    },
    getSettings () {
        return _settings;
    }
});

StartStore.dispatchToken = AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {
        case AppConstants.STORE:
            setData(action.data);
            break;
        case AppConstants.SETTINGS:
            setSettings(action.data);
            break;
    }
    StartStore.emitChange();
    return true;
});


module.exports = StartStore;