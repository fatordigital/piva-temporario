var React = require('react'),
    ReactDOM = require('react-dom'),
    App = require('./components/App.react'),
    documentGenerator;
window.react = React;

require('events').EventEmitter.prototype._maxListeners = 100;
documentGenerator = document.getElementById('generator');
ReactDOM.render(<App data={JSON.parse(documentGenerator.getAttribute('data-return'))} settings={JSON.parse(documentGenerator.getAttribute('data-settings'))} />, documentGenerator);
