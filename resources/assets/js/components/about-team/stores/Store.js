var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    assign = require('react/lib/Object.assign'),
    EventEmitter = require('events').EventEmitter,
    Helpers = require('../Helpers/Helpers.react'),
    CHANGE_EVENT = 'change',
    _icons = {},
    _about = {};

function setIcons (icons) {
    _icons = icons;
}

function setAbout (about) {
    _about = $.merge([about], _about);
}

function setEdit (about) {
    _about = about;
}

function deleteAbout (id) {
    Helpers.removeObject(id, _about, 'id');
}

var StartStore = assign(EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener (callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    getIcons () {
        return _icons;
    },
    getAbouts() {
        return _about;
    }
});

StartStore.dispatchToken = AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {
        case AppConstants.ICONS:
            setIcons(action.data);
            break;
        case AppConstants.ADD_ABOUT:
            setAbout(action.data);
            break;
        case AppConstants.EDIT:
            setEdit(action.data);
            break;
        case AppConstants.DELETE:
            deleteAbout(action.data);
            break;
    }
    StartStore.emitChange();
    return true;
});


module.exports = StartStore;
