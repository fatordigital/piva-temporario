var keyMirror = require('keymirror');
module.exports = keyMirror({
    ICONS: null,
    EDIT: null,
    DELETE: null,
    ADD_ABOUT: null
});
