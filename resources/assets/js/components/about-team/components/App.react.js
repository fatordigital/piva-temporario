var React = require('react'),
    Model = require('../stores/Store'),
    Controller = require('../actions/Actions'),
    Helpers = require('../Helpers/Helpers.react');

var App = React.createClass({

    getInitialState () {
        return {
            icons: {},
            abouts: {}
        };
    },

    getDefaultProps() {
        return {
            edit: null
        }
    },

    componentDidMount () {
        Controller.getIcons();
        if (this.props.edit)
            Controller.setEdit(JSON.parse(this.props.edit));
    },

    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            icons: Model.getIcons(),
            abouts: Model.getAbouts()
        });
    },

    handleSelect (e) {
        console.log(e.target.value);
        $('#see-icon').removeAttr('class').addClass(e.target.value);
    },

    handleDelete (id, e) {
        e.preventDefault();
        Controller.delete(id);
    },

    handleSave (e) {
        e.preventDefault();
        var title = $('#titleAbout'),
            info = $('#informationAbout'),
            icon = $('#iconAbout');
        if (!title.val() || !info.val()) {
            swal('Ops!', 'Antes de salvar uma informação sobre a turma, preencha os campos de título e informção.', 'info');
            return false;
        }
        Controller.addAbout({id: Helpers.uuid(), icon: icon.val(), title: title.val(), info: info.val()});
        title.val('');
        info.val('');
        icon.val('');
        $('#see-icon').removeAttr('class');
    },

    render() {


        return (
            <div className="row">
                <input type="hidden" name="about" value={JSON.stringify(this.state.abouts)} />
                <div className="col-lg-3">
                    <div className="form-group">
                        <label htmlFor="">
                            <i className="fa fa-question-circle" data-title="title" title="Para visualizar o ícone basta selecionar" /> Selecione um ícone
                            <strong className="text-success">(Opcional)</strong> <i id="see-icon"/>
                        </label>
                        <select style={{width: '100%'}} id="iconAbout" className="form-control load-select2-icon" onChange={this.handleSelect}>
                            <option value="">Selecione um ícone</option>
                            {Object.keys(this.state.icons).map(function(key, row){
                                return <option key={key} value={this.state.icons[row].icon} dangerouslySetInnerHTML={Helpers.markUp(this.state.icons[row].icon)}></option>
                            }.bind(this))}
                        </select>
                    </div>
                </div>
                <div className="col-lg-3">
                    <div className="form-group">
                        <label htmlFor="">
                            Título
                        </label>
                        <input type="text" id="titleAbout" placeholder="Exemplo: Duração" className="form-control" />
                    </div>
                </div>
                <div className="col-lg-6">
                    <div className="form-group">
                        <label htmlFor="">
                            Informação
                        </label>
                        <div className="input-group">
                            <input type="text" id="informationAbout" placeholder="Exemplo: 3 semanas " className="form-control" />
                            <span className="input-group-btn">
                                <button type="button" className="btn btn-success" onClick={this.handleSave}>
                                    <i className="fa fa-plus" /> Adicionar
                                </button>
                            </span>
                        </div>
                    </div>
                </div>

                <div className="col-lg-12">
                    <table className="table table-bordered">
                        <thead>
                            <tr>
                                <td width="5%">Ícone</td>
                                <td>Título</td>
                                <td>Informação</td>
                                <td width="3%">Opções</td>
                            </tr>
                        </thead>
                        <tbody>
                        {Object.keys(this.state.abouts).map(function(key, row) {
                            return (
                                <tr key={key}>
                                    {function(){
                                        if (this.state.abouts[key].icon != '') {
                                            return <td><i className={this.state.abouts[key].icon}>&nbsp;</i></td>
                                        } else {
                                            return <td></td>;
                                        }
                                    }.bind(this)()}
                                    <td>{this.state.abouts[key].title}</td>
                                    <td>{this.state.abouts[key].info}</td>
                                    <td>
                                        <button type="button" onClick={this.handleDelete.bind(this, this.state.abouts[key].id)} className="btn btn-xs btn-danger no-rounded">
                                            <i className="fa fa-trash-o" />
                                        </button>
                                    </td>
                                </tr>
                            )
                        }.bind(this))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

});

module.exports = App;
