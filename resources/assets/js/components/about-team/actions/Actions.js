var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    Helpers = require('../Helpers/Helpers.react')

module.exports = {

    getIcons () {
        $.ajax({
            url: b + '/getIcons.json',
            dataType: 'json',
            method: 'GET',
            success: function (r) {
                AppDispatcher.handleViewAction({
                    actionType: AppConstants.ICONS,
                    data: r.data
                });
            }
        })
    },

    setEdit (object) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.EDIT,
            data: object
        });
    },

    addAbout (object) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.ADD_ABOUT,
            data: object
        });
    },

    delete (id) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.DELETE,
            data: id
        });
    }

};