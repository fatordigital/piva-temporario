var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    assign = require('react/lib/Object.assign'),
    EventEmitter = require('events').EventEmitter,
    Helpers = require('../Helpers/Helpers.react'),
    CHANGE_EVENT = 'change',
    _lessons = {},
    _disciplines = {},
    _modal_contents = {},
    _current_content = {},
    _embed = null

function setLessons (data) {
    _lessons = data;
}

function setModalContents (data) {
    _modal_contents = data;
}

function setCurrentContent (data) {
    _current_content = data;
}

function setEmbed (data) {
    _embed = data;
}

function setDisciplines (data) {
    _disciplines = data;
}

var StartStore = assign(EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener (callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    getLessons () {
        return _lessons;
    },
    getModalContent () {
        return _modal_contents;
    },
    getCurrentContent () {
        return _current_content;
    },
    getEmbed () {
        return _embed;
    },
    getDisciplines () {
        return _disciplines;
    }
});

StartStore.dispatchToken = AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {
        case AppConstants.LESSON:
            setLessons(action.data);
            setDisciplines(action.disciplines);
            break;
        case AppConstants.MODAL_LESSON:
            setModalContents(action.data);
            setCurrentContent(action.currentContent);
            break;
        case AppConstants.MODAL_EMBED:
            setEmbed(action.data);
            break;
    }
    StartStore.emitChange();
    return true;
});


module.exports = StartStore;
