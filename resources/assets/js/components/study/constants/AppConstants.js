var keyMirror = require('keymirror');
module.exports = keyMirror({
    LESSON: null,
    MODAL_LESSON: null,
    MODAL_EMBED: null
});
