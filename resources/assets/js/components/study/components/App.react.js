var React = require('react'),
    Model = require('../stores/Store'),
    Helpers = require('../Helpers/Helpers.react'),
    Controller = require('../actions/Actions'),
    Modal = require('./Modal.react')

var App = React.createClass({

    getInitialState () {
        return {
            lessons: '',
            modalTitle: '',
            modalContent: {},
            currentContent: {},
            disciplines: {}
        };
    },

    componentDidMount () {
        Controller.lessons();
    },

    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            lessons: Model.getLessons(),
            modalContent: Model.getModalContent(),
            currentContent: Model.getCurrentContent(),
            disciplines: Model.getDisciplines()
        });
    },

    handleLessonModal (lesson, content_id, e) {
        this.setState({modalTitle: lesson.name});
        Controller.lessonModal(lesson.id, content_id);
    },

    selectDiscipline(e){
        var discipline = e.target.value;
        Controller.lessons(discipline);
    },

    render() {
        return (
            <section className="section-cursos pg-curso">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="form-group">
                                <label htmlFor="">
                                    Filtrar por disciplina
                                </label>
                                <select onChange={this.selectDiscipline} className="form-control">
                                    <option value="">Selecione a disciplina que deseja filtrar</option>
                                    {Object.keys(this.state.disciplines).map(function(key, row) {
                                        var current = this.state.disciplines[key];
                                        return <option key={key} value={current.id}>{current.name}</option>
                                    }.bind(this))}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                {Object.keys(this.state.lessons).map(function(key, row){
                    var current = this.state.lessons[key];
                    return (
                        <div className="container" key={key}>
                            <div className="row">
                                <div className="col-xs-12">
                                    <div className="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div className="panel panel-default">
                                            <div className="panel-heading" role="tab" id="headingOne">
                                                <h4 className="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href={"#collapse"+key} aria-expanded="true" aria-controls={"collapse"+key}>
                                                       <span style={{fontSize: '16px'}}>{current.name}</span><span className="pull-right">EXPANDIR O CONTEÚDO DESSE MÓDULO</span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id={"collapse"+key} className="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                            {function(){
                                                if (current.lessons.length) {
                                                    var i = 0;
                                                    return (
                                                        <div className="panel-body">
                                                            <p>{current.description}</p>
                                                            <ul className="lista-material">
                                                            {Object.keys(current.lessons).map(function(key, value){
                                                                var lesson = current.lessons[key];
                                                                i++;
                                                                return (
                                                                    <li key={key}
                                                                        onClick={this.handleLessonModal.bind(this, lesson)}
                                                                        data-toggle="modal"
                                                                        data-target="#myModal">
                                                                        <p>
                                                                            {i}. <span className="icon-film"/> {lesson.name}
                                                                            <time>
                                                                                <span className="glyphicon glyphicon-play" /> {lesson.ContentTypeCount.videos} Vídeos
                                                                            </time>
                                                                            <time>
                                                                                <span className="glyphicon glyphicon-folder-open" /> {lesson.ContentTypeCount.files} Arquivos
                                                                            </time>
                                                                            <time>
                                                                                <span className="glyphicon glyphicon-font" /> {lesson.ContentTypeCount.texts} Textos
                                                                            </time>
                                                                        </p>
                                                                    </li>
                                                                );
                                                            }.bind(this))}
                                                            </ul>
                                                        </div>
                                                    )
                                                }
                                            }.bind(this)()}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                }.bind(this))}
                <Modal title={this.state.modalTitle} modalContent={this.state.modalContent} />
            </section>
        )
    }

});

module.exports = App;
