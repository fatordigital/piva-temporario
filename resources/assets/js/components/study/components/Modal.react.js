var React = require('react'),
    Model = require('../stores/Store'),
    Helpers = require('../Helpers/Helpers.react'),
    Controller = require('../actions/Actions')

var Modal = React.createClass({

    getDefaultProps () {
        return {
            title: '',
            modalContent: {},
            currentContent: null,
            currentText: false
        }
    },

    getInitialState () {
        return {
            current_video: '',
            embed: '',
            current_data: '',
            currentText: ''
        }
    },

    viewVideo (video, data) {
        this.setState({current_video: "//player.vimeo.com/video/" + video, currentText: '', current_data: data});
    },

    setPoints (id, e) {
        var $this = $(e.target);
        $.ajax({
            url: b + 'set-content-rating.json',
            dataType: 'json',
            method: 'post',
            data: {_token: $('#_token').data('content'), request: Helpers.send({content_id: id, points: $this.val()})},
            complete: function() {
                swal('Obrigado', 'Sua pontuação nos ajuda a criar conteúdos com maior qualidade.', 'success')
            }
        })
    },

    closeModal () {
        this.setState({current_video: '', currentText: ''});
    },

    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            embed: Model.getEmbed()
        });
        if (this.props.currentContent && this.props.currentContent.video && !this.state.current_video) {
            var v = JSON.parse(this.props.currentContent.content);
            this.viewVideo(v.video_id);
        }
        if (this.props.currentContent && this.props.currentContent.text) {
            this.handleText(this.props.currentContent);
        }

    },

    handleText (current) {
        this.setState({currentText: current.content});
    },

    render () {
        return (
            <div className="modal fade" data-backdrop="static" id="myModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div className="modal-dialog modal-lg" role="document">
                <div className="modal-content" style={ this.state.current_video ? {} : {width: '100%'} }>
                  <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.closeModal}><span aria-hidden="true">&times;</span></button>
                    <h5 className="modal-title" id="myModalLabel">
                        {this.props.title}
                    </h5>
                  </div>
                  <div className="modal-body">
                    {function(){
                        if (this.state.current_video != '') {
                            console.log(this.state.current_data);
                            return (
                                <div>
                                    <span className="star-cb-group">
                                      <input checked={this.state.current_data.Rating == 5} type="radio" id="rating-5" name="rating" onClick={this.setPoints.bind(this, this.state.current_data.id)} value="5" /><label htmlFor="rating-5">5</label>
                                      <input checked={this.state.current_data.Rating == 4} type="radio" id="rating-4" name="rating" onClick={this.setPoints.bind(this, this.state.current_data.id)} value="4" /><label htmlFor="rating-4">4</label>
                                      <input checked={this.state.current_data.Rating == 3} type="radio" id="rating-3" name="rating" onClick={this.setPoints.bind(this, this.state.current_data.id)} value="3" /><label htmlFor="rating-3">3</label>
                                      <input checked={this.state.current_data.Rating == 2} type="radio" id="rating-2" name="rating" onClick={this.setPoints.bind(this, this.state.current_data.id)} value="2" /><label htmlFor="rating-2">2</label>
                                      <input checked={this.state.current_data.Rating == 1} type="radio" id="rating-1" name="rating" onClick={this.setPoints.bind(this, this.state.current_data.id)} value="1" /><label htmlFor="rating-1">1</label>
                                      <input checked={this.state.current_data.Rating == 0} type="radio" id="rating-0" name="rating" onClick={this.setPoints.bind(this, this.state.current_data.id)} value="0" className="star-cb-clear" /><label htmlFor="rating-0">0</label>
                                    </span>
                                    <div className="clearfix"></div>
                                </div>
                            )
                        }
                    }.bind(this)()}
                    {function(){
                        if (this.state.current_video) {
                            return (
                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <div ref="loadingVideo" style={{width: '100%', position: 'absolute', minHeight: '370px', background: '#FFF', display: 'none', textAlignCenter: 'center'}}>Carregando Aguarde</div>
                                    <iframe id="iframe" src={this.state.current_video + "?autoplay=1"}
                                            width="100%"
                                            height="362"
                                            frameBorder="0"
                                            webkitallowfullscreen
                                            mozallowfullscreen
                                            allowFullScreen></iframe>
                                    <div dangerouslySetInnerHTML={Helpers.markUp(this.state.current_data.description)}></div>
                                </div>
                            )
                        } else if (!this.state.current_video && this.props.currentContent && this.props.currentContent.video) {
                            return (
                                <div className="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <div ref="loadingVideo" style={{width: '100%', position: 'absolute', minHeight: '370px', background: '#FFF', display: 'none', textAlignCenter: 'center'}}>Carregando Aguarde</div>
                                    <iframe id="iframe" src={this.state.current_video + "?autoplay=1"}
                                            width="100%"
                                            height="362"
                                            frameBorder="0"
                                            webkitallowfullscreen
                                            mozallowfullscreen
                                            allowFullScreen></iframe>
                                </div>
                            )
                        }
                    }.bind(this)()}
                    <div className={this.state.current_video ? "col-lg-4 col-md-4 col-sm-4 col-xs-12" : "col-lg-12 col-md-12 col-sm-12 col-xs-12"}>
                      <ul className="list-group" style={!this.state.current_video ? {height: 'auto'} : {}}>
                          {function() {
                              if (Object.keys(this.props.modalContent).length) {
                                  return Object.keys(this.props.modalContent.contents).map(function(key, row){
                                      var current = this.props.modalContent.contents[key].contents ? this.props.modalContent.contents[key].contents : this.props.modalContent.contents[key].tests;
                                      if (current) {
                                          return (
                                              <li className="list-group-item"
                                                  style={current.id == this.state.current_data.id ? {background: '#385069'} : {}}
                                                  key={key}>
                                              {function () {
                                                  if (current.video) {
                                                      var video = JSON.parse(current.content);
                                                      return (
                                                          <div style={{cursor: 'pointer'}}
                                                               onClick={this.viewVideo.bind(this, video.video_id, current)}>
                                                              <div className="col-xs-5">
                                                                <img src={video.thumbnail_url_with_play_button}
                                                                     className="img-responsive"/>
                                                              </div>
                                                              <div className="col-xs-7">
                                                                <p>
                                                                  <strong>{video.title}</strong><br />
                                                                    {video.description}
                                                                </p>
                                                                <div dangerouslySetInnerHTML={Helpers.markUp(current.description)}></div>
                                                              </div>
                                                          </div>
                                                      )
                                                  }
                                                  else if (current.text) {
                                                      return (
                                                          <div style={{cursor: 'pointer'}}
                                                               onClick={this.handleText.bind(this, current)}
                                                               className="col-xs-7">
                                                            <p>
                                                              <strong>{current.name}</strong><br />
                                                            </p>
                                                          </div>
                                                      )
                                                  }
                                                  else if (current.file) {
                                                      return (
                                                          <a target="_blank"
                                                             href={b + 'assets/contents/' + current.content}>
                                                              <div style={{cursor: 'pointer'}}
                                                                   className="col-xs-7">
                                                                <p>
                                                                  <strong>{current.name}</strong><br />
                                                                </p>
                                                              </div>
                                                          </a>
                                                      )
                                                  } else {
                                                      return (
                                                          <a href={b + current.slug + '/simulado'}
                                                             target="_blank">
                                                              <div style={{cursor: 'pointer'}}
                                                                   className="col-xs-7">
                                                                <p>
                                                                  <strong>{current.name}</strong><br />
                                                                </p>
                                                              </div>
                                                          </a>
                                                      )
                                                  }
                                              }.bind(this)()}
                                          </li>
                                          )
                                      }
                                  }.bind(this))
                              }
                          }.bind(this)()}
                      </ul>
                    </div>
                    {function(){
                        if (this.state.currentText) {
                            return (
                                <div className="col-xs-12" dangerouslySetInnerHTML={Helpers.markUp(this.state.currentText)}></div>
                            )
                        } else {
                            if (this.state.current_data == '') {
                                return (
                                    <div className="col-xs-12"
                                         dangerouslySetInnerHTML={Helpers.markUp(this.props.modalContent.text)}></div>
                                )
                            }
                        }
                    }.bind(this)()}
                  </div>
                </div>
              </div>
            </div>
        )
    }
});

module.exports = Modal;