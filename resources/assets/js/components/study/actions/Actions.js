var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    Helpers = require('../Helpers/Helpers.react')

module.exports = {

    lessons (discipline_id) {
        $.ajax({
            url: b + 'api/contents.json',
            dataType: 'json',
            type: 'get',
            data: {discipline_id: discipline_id},
            success: function (res) {
                var r = Helpers.return(res);
                if (r.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.LESSON,
                        data: r.data,
                        disciplines: r.disciplines
                    });
                }
            }
        });
    },

    lessonModal (lesson_id, without) {
        $.ajax({
            url: b + 'api/modal-contents.json',
            dataType: 'json',
            data: {_token: Helpers.getToken(), request: Helpers.send({lesson_id: lesson_id})},
            type: 'post',
            success: function (res) {
                var r = Helpers.return(res);
                if (r.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.MODAL_LESSON,
                        data: r.data,
                        currentContent: r.content
                    });
                }
            }
        });
    },

    embed (iframe) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.MODAL_EMBED,
            data: iframe
        });
    }

};