var keyMirror = require('keymirror');
module.exports = keyMirror({
    CART_DATA: null,

    IS_MULTI: null,

    INSTALLMENTS: null,

    HIDE_FORM_CREDIT_CARD: null,

    METHOD_OPTIONS: null,

    STOP_COMPONENT: null,

    SETTINGS_CARD: null,

    SEND: null,

    FORM_DATA: null,

    HASH_PAGSEGURO: null
});
