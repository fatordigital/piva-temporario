var React = require('react'),
    Helpers = require('../Helpers/Helpers.react'),
    Controller = require('../actions/Actions')

var RequiredFields = React.createClass({

    getDefaultProps() {
        return {
            data: {}
        }
    },

    componentDidMount () {
        this.jqueryLoads();
    },

    jqueryLoads () {
        $('.s-datepicker').datepicker({
            format: 'dd/mm/yyyy',
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });
        $('.cpf-mask').mask('000.000.000-00');
        $('.cep-mask').mask('00000-000');

        var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.phone-mask').mask(SPMaskBehavior, spOptions);
        $('.date-mask').mask('00/00/0000');
    },

    handleChange (e) {
        e.preventDefault();
        if (!e.target.value) {
            Helpers.addErrorField($(e.target), 'Campo obrigatório.', '.my-line');
        } else {
            Helpers.removeError($(e.target), '.my-line');
            Controller.saveRegister($(e.target).attr('name'), e.target.value, $(e.target));
        }
    },

    render () {
        return (
            <div className="col-lg-12">
                <h5>Detectamos que você precisa preencher alguns campos</h5>
                <p className="text-danger">Todos os campos abaixo são obrigatórios.</p>
                {Object.keys(this.props.data).map(function(key, row){
                    return (
                        <div className="my-line" key={key}>
                            <label htmlFor={key} style={{textAlign: 'left'}} dangerouslySetInnerHTML={Helpers.markUp(this.props.data[key].label)} />
                            {function(){
                                if (this.props.data[key].select) {
                                    return (
                                        <select name={key} className="form-control" onChange={this.handleChange}>
                                            {Object.keys(this.props.data[key].select).map(function(k, r){
                                                return <option key={k} value={k}>{this.props.data[key].select[k]}</option>
                                            }.bind(this))}
                                        </select>
                                    )
                                } else {
                                    return <input type="text" onBlur={this.handleChange} name={key} className={this.props.data[key].class ? "form-control " + this.props.data[key].class : "form-control"} />
                                }
                            }.bind(this)()}
                        </div>
                    )
                }.bind(this))}
            </div>
        )
    }

});

module.exports = RequiredFields;