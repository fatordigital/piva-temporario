var React = require('react'),
    Helpers = require('../Helpers/Helpers.react'),
    Controller = require('../actions/Actions')

var Methods = React.createClass({

    getDefaultProps() {
        return {
            data: {}
        }
    },

    optionsAvailable (data, other) {
        var $return = false;
        if (other.name == 'BALANCE')
            return false;
        if (other.name == 'DEPOSIT')
            return false;
        Object.keys(data).map(function (key, row) {
            if (data[key].status == 'AVAILABLE') {
                $return = true;
            }
        });
        return $return;
    },

    nameOrImage (data, name) {
        if (Object.keys(data.options).length == 1 && data.options[Object.keys(data.options)[0]].images)
            return <img src={window.pagseguro_image + data.options[Object.keys(data.options)[0]].images.MEDIUM.path} className="img-responsive center-block" />
        else {
            return Helpers.methodName(name)
        }
    },

    handleSelect (method, e) {
        if (Object.keys(method.options).length > 1 || method.name == 'BOLETO')
            Controller.isMulti(true, method.options, method.name);
        else {
            Controller.isMulti(false, method.options, method.name);
        }
    },

    render () {
        return (
            <div className="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                  <div className="bg-form">
                      <h5 className="text-center">ESCOLHA A FORMA DE PAGAMENTO</h5><br />
                      {Object.keys(this.props.data).map(function(key, row){
                          if (this.optionsAvailable(this.props.data[key].options, this.props.data[key])) {
                              return (
                                  <div className="radio" key={key}>
                                      <label htmlFor="">
                                          <input type="radio" name="method" value={Helpers.send(this.props.data[key].name)} onClick={this.handleSelect.bind(this, this.props.data[key])} />
                                          <div className="text-center">
                                            {this.nameOrImage(this.props.data[key], this.props.data[key].name)}
                                          </div>
                                      </label>
                                  </div>
                              )
                          }
                      }.bind(this))}
                  </div>
            </div>
        )
    }
});

module.exports = Methods;