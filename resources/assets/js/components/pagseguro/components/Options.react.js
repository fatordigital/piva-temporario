var React = require('react'),
    Controller = require('../actions/Actions')

var Options = React.createClass({

    getDefaultProps() {
        return {
            data: {},
            amount: 0.00
        }
    },

    getImage(images) {
        if (images) {
            return <img src={window.pagseguro_image + images.SMALL.path}
                        className="img-responsive"/>
        } else {
            return '';
        }
    },

    getInfo (method) {
        if (method.name == 'BOLETO') {
            return (
                <div key={method.code}>
                    <p>O pedido será confirmado somente após a aprovação do pagamento.</p>
                    <p>Tarifa de boleto = <strong className="text-success">R$ 1,00</strong> Tarifa aplicada para cobrir os custos de gestão de risco do meio de pagamento.</p>
                </div>
            )
        }
        return false;
    },

    handleChange(e) {
        Controller.getInstallments(this.props.amount, e.target.value);
        Controller.setFormData('card', e.target.value);
    },

    render () {
        return (
            <div className="col-xs-12 my-line radio-groups">
                <h5>Escolha a opção de pagamento</h5>
                {Object.keys(this.props.data).map(function (key, row) {
                    var info = this.getInfo(this.props.data[key]);
                    if (!info) {
                        if (this.props.data[key].status == 'AVAILABLE') {
                            return (
                                <label className="radio-inline"
                                       key={key}>
                                  <input type="radio"
                                         onChange={this.handleChange}
                                         name={this.props.type}
                                         value={this.props.data[key].name.toLowerCase()} />
                                    {this.getImage(this.props.data[key].images)}
                                </label>
                            )
                        }
                    } else {
                        return info
                    }
                }.bind(this))}
            </div>
        )
    }

});

module.exports = Options;