var React = require('react'),
    Helpers = require('../Helpers/Helpers.react'),
    Controller = require('../actions/Actions')

var MethodSelected = React.createClass({

    getDefaultProps() {
        return {
            installments: [],
            cvvLength: 3
        };
    },

    componentDidMount () {
        this.jqueryLoads();
    },

    jqueryLoads () {
        $('.s-datepicker').datepicker({
            format: 'dd/mm/yyyy',
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });
        $('.cpf-mask').mask('000.000.000-00');
        $('.cep-mask').mask('00000-000');

        var SPMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            spOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.phone-mask').mask(SPMaskBehavior, spOptions);
        $('.date-mask').mask('00/00/0000');
    },

    handleName (e) {
        var value = e.target.value;
        if (!value) {
            Helpers.addErrorField($(e.target), 'O Nome é obrigatório', '.my-line');
            return false;
        }
        if (!Helpers.compose(value)) {
            Helpers.addErrorField($(e.target), 'O Nome é um campo composto.', '.my-line');
            return false;
        }
        Helpers.removeError($(e.target), '.my-line');
        Helpers.addSuccessField($(e.target), '.my-line');
        this.setData('card_name', value);
    },

    handleBirthDate (e) {
        var value = e.target.value;
        if (!value) {
            Helpers.addErrorField($(e.target), 'Data de nascimento é obrigatório', '.my-line');
            return false;
        }
        Helpers.removeError($(e.target), '.my-line');
        Helpers.addSuccessField($(e.target), '.my-line');
        this.setData('card_birth_date', value);
    },

    handleNationalId (e) {
        var value = e.target.value;
        if (!value) {
            Helpers.addErrorField($(e.target), 'O CPF é obrigatório', '.my-line');
            return false;
        }
        if (!Helpers.validaCPF(value)) {
            Helpers.addErrorField($(e.target), 'O CPF não é válido.', '.my-line');
            return false;
        }
        Helpers.removeError($(e.target), '.my-line');
        Helpers.addSuccessField($(e.target), '.my-line');
        this.setData('card_national_id', value);
    },

    handleCardNumber (e) {
        var value = e.target.value;
        if (!value) {
            Helpers.addErrorField($(e.target), 'O Campo de cartão é obrigatório', '.my-line');
            return false;
        }
        if (value.length < 6) {
            Helpers.addErrorField($(e.target), 'O cartão não é válido', '.my-line');
            return false;
        }
        Controller.getCardBrand(value, $(e.target));
        Controller.setSend(true);
        this.setData('card_number', value);
    },

    handleCvvNumber (e) {
        var value = e.target.value;
        if (!value) {
            Helpers.addErrorField($(e.target), 'O Código de segurançã é obrigatório', '.my-line');
            return false;
        }
        if (value.length < this.props.cvvLength) {
            Helpers.addErrorField($(e.target), 'O Campo precisa ter '+ this.props.cvvLength + ' caracteres.', '.my-line');
            return false;
        }
        Helpers.removeError($(e.target), '.my-line');
        Helpers.addSuccessField($(e.target), '.my-line');
        this.setData('card_cvv', value);
    },

    handleYear (e) {
        var value = e.target.value;
        this.setData('card_year', value);
    },

    handleMonth (e) {
        var value = e.target.value;
        this.setData('card_month', value);
    },

    handleInstallments (e) {
        var value = e.target.value;
        this.setData('card_installment', value);
    },

    setData (name, value, e) {
        Controller.setFormData(name, value);
    },

    render () {
        return (
            <div className="col-xs-12">
                <div className="my-line">
                    <label htmlFor="" style={{width: '100%', textAlign: 'left'}}>
                        Nome <strong className="text-danger">(*)</strong> (Impresso no cartão)
                    </label>
                    <input type="text" onBlur={this.handleName} className="form-control" ref="card_name" />
                </div>
                <div className="row">
                    <div className="col-xs-6">
                        <div className="my-line">
                            <label htmlFor="" style={{width: '100%', textAlign: 'left'}}>
                                CPF <strong className="text-danger">(*)</strong> <strong className="text-primary">(Titular do cartão)</strong>
                            </label>
                            <input type="text" onBlur={this.handleNationalId} className="form-control cpf-mask" ref="card_national_id" />
                        </div>
                    </div>
                    <div className="col-xs-6">
                        <div className="my-line">
                            <label htmlFor="" style={{width: '100%', textAlign: 'left'}}>
                                Data de nascimento <strong className="text-danger">(*)</strong> <strong className="text-primary">(Titular do cartão)</strong>
                            </label>
                            <input type="text" onBlur={this.handleBirthDate} className="form-control s-datepicker date-mask" ref="card_birth_date" />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-6">
                        <div className="my-line">
                            <label htmlFor="" style={{width: '100%', textAlign: 'left'}}>
                                Número do Cartão <strong className="text-danger">(*)</strong> (Impresso no cartão)
                            </label>
                            <input type="text" onBlur={this.handleCardNumber} className="form-control" ref="card_number" />
                        </div>
                    </div>
                    <div className="col-xs-3">
                        <div className="my-line">
                            <label htmlFor="" style={{width: '100%', textAlign: 'left'}}>
                                Mês <strong className="text-danger">(*)</strong> (Validade)
                            </label>
                            <select className="form-control" ref="card_month" onBlur={this.handleMonth}>
                                <option value="">Mês</option>
                                {Object.keys(Helpers.getMonths()).map(function(key, row){
                                    return <option key={key} value={key}>{Helpers.getMonths()[key]}</option>
                                }.bind(this))}
                            </select>
                        </div>
                    </div>
                    <div className="col-xs-3">
                        <div className="my-line">
                            <label htmlFor="" style={{width: '100%', textAlign: 'left'}}>
                                Ano <strong className="text-danger">(*)</strong> (Validade)
                            </label>
                            <select className="form-control" onBlur={this.handleYear} ref="card_year">
                                <option value="">Ano</option>
                                {Helpers.getYears()}
                            </select>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-6">
                        <div className="my-line">
                            <label htmlFor="" style={{width: '100%', textAlign: 'left'}}>
                                Parcelas <strong className="text-danger">(*)</strong>
                            </label>
                            <select className="form-control" onBlur={this.handleInstallments} ref="card_installment">
                                <option value="">Parcelas</option>
                                {this.props.installments}
                            </select>
                        </div>
                    </div>
                    <div className="col-xs-6">
                        <div className="my-line">
                            <label htmlFor="" style={{width: '100%', textAlign: 'left'}}>
                                Código de segurança <strong className="text-danger">(*)</strong> (Impresso no cartão)
                            </label>
                            <input type="text" onChange={this.handleCvvNumber} maxLength={this.props.cvvLength} className="form-control" ref="card_cvv" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }

});

module.exports = MethodSelected;