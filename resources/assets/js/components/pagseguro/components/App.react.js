var React = require('react'),
    Model = require('../stores/Store'),
    Controller = require('../actions/Actions'),
    Methods = require('./Methods.react'),
    Options = require('./Options.react'),
    CreditCard = require('./CreditCard.react'),
    RequiredFields = require('./RequiredFields.react')

var App = React.createClass({

    getInitialState () {
        return {
            isMulti: 'none',
            hashPagseguro: false,
            cart_data: {},
            method_options: {},
            options: {},
            hideCreditForm: false,
            requiredFields: {},
            amount: 0.00,
            installments: [],
            secure: false,
            send: false,
            cvvLength: 3
        };
    },

    componentDidMount () {
        Controller.startLoading();
        Controller.getCartData();
        Controller.loadHash();
    },

    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            amount: Model.getAmount(),
            selected_name: Model.getSelectedName(),
            hashPagseguro: Model.getHashPagseguro(),
            cart_data: Model.getCartData(),
            method_options: Model.getMethodOptions(),
            isMulti: Model.isMulti(),
            options: Model.getSelectedOptions(),
            hideCreditForm: Model.getHideCreditForm(),
            requiredFields: Model.getRequiredFields(),
            installments: Model.getInstallments(),
            secure: Model.getSecure(),
            cvvLength: Model.getCvvLength(),
            send: Model.getSend(),
            formData: Model.getFormData()
        });
    },

    send (e) {
        if (this.state.send == true && !document.getElementsByClassName('has-error').length) {
            e.preventDefault();
            Controller.submit(this.state.formData, this.state.selected_name, this.state.hashPagseguro);
        } else {
            e.preventDefault();
            swal('Professor Piva', 'Verifique se todos os campos estão preenchidos.', 'error');
        }
    },

    render() {
        return (
            <form id="form-pagamento" onSubmit={this.send}>
                <Methods data={this.state.method_options}/>
                <div className="col-lg-9 col-md-9 col-sm-9 col-xs-12" style={{display: this.state.isMulti}}>
                    <div className="bg-form">
                        <Options amount={this.state.amount} data={this.state.options} type={this.state.selected_name}/>
                        {function () {
                            if (Object.keys(this.state.requiredFields).length) {
                                return <RequiredFields data={this.state.requiredFields}/>
                            } else if (this.state.selected_name == 'CREDIT_CARD' && this.state.hideCreditForm == false && this.state.secure == true) {
                                return <CreditCard cvvLength={this.state.cvvLength}
                                                   installments={this.state.installments}/>
                            }
                        }.bind(this)()}
                    </div>
                </div>
                <div className="col-xs-12">
                    <button type="submit" className="btn btn-success btn-carrinho btn-next">FINALIZAR PEDIDO »</button>
                </div>
            </form>
        )
    }

});

module.exports = App;
