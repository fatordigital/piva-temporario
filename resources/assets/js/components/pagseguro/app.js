var React = require('react'),
    ReactDOM = require('react-dom'),
    App = require('./components/App.react'),
    documentGenerator;
window.react = React;
window.pagseguro_image = 'https://stc.pagseguro.uol.com.br';

require('events').EventEmitter.prototype._maxListeners = 100;
documentGenerator = document.getElementById('payment');
ReactDOM.render(<App />, documentGenerator);
