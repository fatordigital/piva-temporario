var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    Helper = require('../Helpers/Helpers.react');

module.exports = {

    startLoading () {
    },

    endLoading () {
    },

    loadHash () {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.HASH_PAGSEGURO,
            data: PagSeguroDirectPayment.getSenderHash()
        });
    },

    isMulti (status, options, name) {
        this.requiredFields();
        if (name == 'BOLETO' || name == 'ONLINE_DEBIT')
            this.setSend(true);
        AppDispatcher.handleViewAction({
            actionType: AppConstants.IS_MULTI,
            data: (status) ? 'block' : 'none',
            options: options,
            name: name
        });
        this.setFormData('type', name);
    },

    requiredFields () {
        $.ajax({
            url: b + 'check-me',
            method: 'post',
            dataType: 'json',
            beforeSend: function () {
                this.startLoading();
            }.bind(this),
            data: {_token: $('#_token').data('content')},
            success: function (res) {
                var r = Helper.return(res);
                if (r.error == true) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.HIDE_FORM_CREDIT_CARD,
                        data: true,
                        fields: r.data
                    });
                } else {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.HIDE_FORM_CREDIT_CARD,
                        data: false,
                        fields: []
                    });
                }
            }.bind(this),
            complete: function () {
                this.endLoading();
            }.bind(this)
        })
    },

    getCartData() {
        $.ajax({
            url: b + 'checkout',
            method: 'post',
            dataType: 'json',
            data: {_token: $('#_token').data('content')},
            success: function (res) {
                var r = Helper.return(res);
                if (r.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.CART_DATA,
                        data: r.data.data
                    });
                    this.getOptions(r.data.data.total);
                    this.endLoading();
                }
            }.bind(this)
        });
    },

    getOptions (amount) {
        PagSeguroDirectPayment.getPaymentMethods({
            amount: amount,
            error (r) {
                AppDispatcher.handleViewAction({
                    actionType: AppConstants.STOP_COMPONENT,
                    data: true
                });
            },
            success(r) {
                AppDispatcher.handleViewAction({
                    actionType: AppConstants.METHOD_OPTIONS,
                    data: r.paymentMethods
                });
            }
        });
    },

    saveRegister (field, value, target) {
        $.ajax({
            url: b + 'save-my-data',
            method: 'post',
            dataType: 'json',
            data: {_token: $('#_token').data('content'), request: Helper.send({field: field, value: value})},
            success: function (res) {
                var r = Helper.return(res);
                if (r.error == false) {
                    Helper.addSuccessField(target, '.my-line');
                    if (r.data) {
                        AppDispatcher.handleViewAction({
                            actionType: AppConstants.HIDE_FORM_CREDIT_CARD,
                            data: true,
                            fields: r.data
                        });
                    } else {
                        AppDispatcher.handleViewAction({
                            actionType: AppConstants.HIDE_FORM_CREDIT_CARD,
                            data: false,
                            fields: {}
                        });
                    }
                } else {
                    Helper.addErrorField(target, r.msg, '.my-line');
                }
            }.bind(this)
        });
    },

    getCardBrand (bin, target) {
        PagSeguroDirectPayment.getBrand({
            cardBin: bin,
            error: function (result) {
                if (result.error == true) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.STOP_COMPONENT,
                        data: true
                    });
                }
                Helper.addErrorField(target, 'O Cartão não é válido', '.my-line');
            },
            success: function (result) {
                AppDispatcher.handleViewAction({
                    actionType: AppConstants.SETTINGS_CARD,
                    data: result.brand
                });
                if (result.brand.config.acceptedLengths.indexOf(bin.length) > -1) {
                    Helper.removeError(target, '.my-line');
                    Helper.addSuccessField(target, '.my-line');
                } else {
                    Helper.addErrorField(target, 'O cartão não é válido', '.my-line');
                }
            }
        });
    },

    getInstallments (amount, brand) {
        PagSeguroDirectPayment.getInstallments({
            amount: amount,
            brand: brand,
            error: function (result) {
                if (result.error == true) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.STOP_COMPONENT,
                        data: true
                    });
                }
            },
            success: function (result) {
                AppDispatcher.handleViewAction({
                    actionType: AppConstants.INSTALLMENTS,
                    data: result.installments[brand]
                });
            }
        });
    },

    getCardToken (data, route, value, bin, brand, cvv, month, year) {
        PagSeguroDirectPayment.createCardToken({
            cardNumber: bin,
            brand: brand,
            cvv: cvv,
            expirationMonth: month < 10 ? '0' + month : month,
            expirationYear: year,
            error: function (result) {

            },
            success: function (result) {
                if (result.card.token) {
                    this.requestPay(data, route, value, result.card.token);
                }
            }.bind(this)
        });
    },

    setSend (bool) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.SEND,
            data: bool
        });
    },

    startLoadingBackground() {
        $('.loading-page').fadeIn();
        $("html, body").animate({scrollTop: 0});
        $('body').addClass('body-loading-page');
        var i = 0;
        window.interval = setInterval(function () {
            if (i <= 99) {
                $('.loading-page .counter p').html(i + '%');
                i++;
            }
        }, 100);
    },

    endLoadingBackground (msg) {
        $('.loading-page').fadeOut();
        $('body').removeClass('body-loading-page');
        window.clearInterval(window.interval);
    },

    submit (data, value, hash) {
        this.startLoadingBackground();
        this.setFormData('hash', PagSeguroDirectPayment.getSenderHash());
        $.ajax({
            url: b + 'pay',
            dataType: 'json',
            method: 'post',
            data: {_token: $('#_token').data('content'), request: Helper.send(data)},
            success: function (res) {
                var r = Helper.return(res);
                if (r.error == false) {
                    this.requestPay(data, r.route, value);
                } else {
                    swal('Professor Piva', r.msg, 'error');
                    this.endLoadingBackground();
                }
            }.bind(this)
        });
    },

    requestPay (data, route, value, token) {
        if (value != 'CREDIT_CARD' || token) {
            $.ajax({
                url: b + route,
                dataType: 'json',
                data: {
                    _token: $('#_token').data('content'),
                    card_token: token,
                    request: Helper.send({
                        type: value,
                        data: data
                    })
                },
                method: 'post',
                success: function (res) {
                    var r = Helper.return(res);
                    if (r.error == false) {
                        swal({
                                title: "Obrigado",
                                text: r.msg,
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonText: "Continuar",
                                closeOnConfirm: false,
                                closeOnCancel: false
                            },
                            function (isConfirm) {
                                if (isConfirm) {
                                    window.location.href = r.redirectUrl;
                                }
                            });
                    } else {
                        swal('Professor Piva', r.msg, 'error');
                    }
                    this.endLoadingBackground();
                }.bind(this)
            });
        } else {
            this.getCardToken(data, route, value, data.card_number, data.card, data.card_cvv, data.card_month, data.card_year);
        }
    },

    setFormData (name, value) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.FORM_DATA,
            data: {name: name, value: value}
        });
    }

};