var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    assign = require('react/lib/Object.assign'),
    EventEmitter = require('events').EventEmitter,
    Helpers = require('../Helpers/Helpers.react'),
    CHANGE_EVENT = 'change',
    _selected_name = '',
    _cart_data = {},
    _options = {},
    _option_selected = {},
    _amount = 0,
    _multi = 'none',
    _credit_form = false,
    _fields = {},
    _installments = [],
    _secure = false,
    _settings_card = {},
    _cvv_length = 3,
    _card_length = [13, 16, 19],
    _send = false,
    _form_data = {},
    _hash_pagseguro = '';

function setCartData(data) {
    _cart_data = data;
    _amount = data.total;
}

function setMethodOptions(data) {
    _options = data;
}

function setMulti(data) {
    _multi = data;
}

function setSelectOptions(data) {
    _option_selected = data;
}

function setSelectedName(data) {
    _selected_name = data;
}

function setHideCreditForm(data, fields) {
    _credit_form = data;
    _fields = fields;
}

function setInstallments(data) {
    if (data) {
        _installments = Helpers.makeInstallments(data);
        _secure = true;
    } else {
        _secure = false;
    }
}

function setHashPagseguro(data) {
    _hash_pagseguro = data;
}

function setSend (data) {
    _send = data;
}

function setFormData(data) {
    _form_data[data.name] = data.value;
}

function setSettingsCard(data) {
    _settings_card = data;
    _cvv_length = data.cvvSize;
    _card_length = data.config.acceptedLengths;
}

var StartStore = assign(EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener (callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    getAmount () {
        return _amount;
    },
    getCartData () {
        return _cart_data;
    },
    getMethodOptions () {
        return _options;
    },
    isMulti () {
        return _multi;
    },
    getSelectedOptions () {
        return _option_selected;
    },
    getSelectedName () {
        return _selected_name;
    },
    getHideCreditForm () {
        return _credit_form;
    },
    getRequiredFields () {
        return _fields;
    },
    getInstallments () {
        return _installments;
    },
    getHashPagseguro() {
        return _hash_pagseguro;
    },
    getSecure () {
        return _secure;
    },
    getSettingsCard () {
        return _settings_card;
    },
    getCvvLength () {
        return _cvv_length;
    },
    getCardLength () {
        return _card_length;
    },
    getSend () {
        return _send;
    },
    getFormData () {
        return _form_data;
    }
});

StartStore.dispatchToken = AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {
        case AppConstants.CART_DATA:
            setCartData(action.data);
            break;
        case AppConstants.HASH_PAGSEGURO:
            setHashPagseguro(action.data);
            break;
        case AppConstants.METHOD_OPTIONS:
            setMethodOptions(action.data);
            break;
        case AppConstants.IS_MULTI:
            setMulti(action.data);
            setSelectOptions(action.options);
            setSelectedName(action.name);
            break;
        case AppConstants.HIDE_FORM_CREDIT_CARD:
            setHideCreditForm(action.data, action.fields);
            break;
        case AppConstants.INSTALLMENTS:
            setInstallments(action.data);
            break;
        case AppConstants.SETTINGS_CARD:
            setSettingsCard(action.data);
            break;
        case AppConstants.SEND:
            setSend(action.data);
            break;
        case AppConstants.FORM_DATA:
            setFormData(action.data);
            break;
    }
    StartStore.emitChange();
    return true;
});


module.exports = StartStore;
