var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants');

module.exports = {

    getOptions () {
        $.ajax({
            url: b + '/fatorcms/teachers.json',
            dataType: 'json',
            method: 'get',
            success: function (res) {
                if (res.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.OPTIONS,
                        data: res.data
                    });
                }
            }
        });
    },

    selectOption(id, partner) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.SELECT,
            data: id,
            partner: partner
        });
    },

    destroy (id) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.DESTROY,
            data: id
        });
    },

    edit (id) {
        $.ajax({
            url: b + '/fatorcms/teachers-team.json',
            dataType: 'json',
            method: 'get',
            data: {id: id},
            success: function (res) {
                if (res.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.EDIT,
                        data: res.data
                    });
                }
            }
        });
    }

};