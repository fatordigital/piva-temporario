var React = require('react'),
    Model = require('../stores/Store'),
    Controller = require('../actions/Actions');

var App = React.createClass({

    getInitialState () {
        return {
            options: {},
            selected: {},
            tt: {}
        };
    },

    componentDidMount () {
        Controller.getOptions();
        if (this.props.edit)
            Controller.edit(this.props.edit);
    },

    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    componentDidUpdate() {
        $('.s-datepicker').datepicker({
            format: 'dd/mm/yyyy',
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });
    },

    _onChange: function () {
        this.setState({
            options: Model.getOptions(),
            selected: Model.getSelects(),
            tt: Model.getTeamTeacher()
        });
    },

    handleSelect(e) {
        Controller.selectOption(e.target.value);
        $(e.target).val("");
    },

    handleDestroy(id, e) {
        swal({
            title: "Você tem certeza?",
            text: "Deseja remover o professor desta turma?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            cancelButtonText: "Não",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                Controller.destroy(id);
                swal("Deletada", "O professor foi removido da turma com sucesso.", "success");
            } else {
                swal("Cancelado", "O professor continua nos registros.", "error");
            }
        });
    },

    render() {
        return (
            <div className="col-lg-12">
                <div className="form-group">
                    <label htmlFor="select">
                        Selecione os professores <strong className="text-success">(Opcional)</strong>
                    </label>
                    <select onChange={this.handleSelect} className="form-control">
                        <option value="">Selecione os professores</option>
                        {Object.keys(this.state.options).map(function (key, row) {
                            return <option key={key} value={this.state.options[row].id}>{this.state.options[row].name}</option>
                        }.bind(this))}
                    </select>
                </div>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <td>Professor</td>
                            <td>E-mail</td>
                            <td>Parceiro?</td>
                            <td width="5%">Opções</td>
                        </tr>
                    </thead>
                    <tbody>
                    {Object.keys(this.state.selected).map(function(key, row){
                        return (
                            <tr key={key}>
                                <td>{this.state.selected[row].name}</td>
                                <td>{this.state.selected[row].email}</td>
                                <td>
                                    {function(){
                                        if (Object.keys(this.state.tt).length > 0 && this.state.tt[this.state.selected[row].id].partner == 1) {
                                            return <input name={"teacher[" + this.state.selected[row].id + "][partner]"} type="checkbox" defaultChecked value="1" />
                                        } else {
                                            return <input name={"teacher[" + this.state.selected[row].id + "][partner]"} type="checkbox" value="1" />
                                        }
                                    }.bind(this)()}
                                </td>
                                <td>
                                    <button type="button" className="btn btn-xs btn-danger" onClick={this.handleDestroy.bind(this, this.state.selected[row].id)}>
                                        <i className="fa fa-trash" />
                                    </button>
                                    <input type="hidden" name={"teacher[" + this.state.selected[row].id + "][id]"} defaultValue={this.state.selected[row].id} />
                                </td>
                            </tr>
                        )
                    }.bind(this))}
                    </tbody>
                </table>
            </div>
        )
    }

});

module.exports = App;
