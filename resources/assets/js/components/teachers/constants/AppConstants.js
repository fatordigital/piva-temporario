var keyMirror = require('keymirror');
module.exports = keyMirror({
    OPTIONS: null,
    SELECT: null,
    DESTROY: null,
    EDIT: null,

    getDiscipline: null,
    getQuestions: null,
    destroyQuestion: null
});
