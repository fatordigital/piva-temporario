var React = require('react'),
    ReactDOM = require('react-dom'),
    App = require('./components/App.react'),
    documentGenerator;
window.react = React;

require('events').EventEmitter.prototype._maxListeners = 100;
documentGenerator = document.getElementById('teachers');
ReactDOM.render(<App edit={documentGenerator.getAttribute('data-edit')} />, documentGenerator);
