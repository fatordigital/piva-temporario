module.exports = {

    removeObject (id, arrayOfObjects, key) {
        for (var i = 0; i < arrayOfObjects.length; i++) {
            var current = arrayOfObjects[i];
            if (current[key] == id) {
                arrayOfObjects.splice(i, 1);
            }
        }
    },

    removeArray (id, array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] == id) {
                array.splice(i, 1);
            }
        }
    },

    uuid: function () {
        var i, random;
        var uuid = '';
        for (i = 0; i < 32; i++) {
            random = Math.random() * 16 | 0;
            if (i === 8 || i === 12 || i === 16 || i === 20) {
                uuid += '-';
            }

            uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
        }
        return uuid;
    },

    getObject (id, arrayOfObjects, key) {
        for (var i = 0; i < arrayOfObjects.length; i++) {
            var current = arrayOfObjects[i];
            if (current[key] == id) {
                return current;
            }
        }
    }

};