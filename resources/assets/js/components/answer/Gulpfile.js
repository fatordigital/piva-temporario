var gulp = require('gulp'),
    browserify = require('browserify'),
    reactify = require('reactify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream');

gulp.task('browserify', function () {
    browserify('./src/js/app.js')
        .transform('babelify', {presets: ['es2015', 'react']})
        .bundle()
        .pipe(source('discipline.min.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('copy', function () {
    gulp.src('src/index.php')
        .pipe(gulp.dest('./'));
    gulp.src('src/assets/**/*.*')
        .pipe(gulp.dest('dist/assets'));
});

gulp.task('dev-copy', function () {
    gulp.src('src/index.php')
        .pipe(gulp.dest('./'));
});

gulp.task('default', ['browserify', 'copy'], function () {
    return gulp.watch('src/**/*.*', ['browserify', 'copy']);
});

gulp.task('dev', ['browserify', 'dev-copy'], function () {
    return gulp.watch('src/**/*.*', ['browserify', 'dev-copy'])
});

