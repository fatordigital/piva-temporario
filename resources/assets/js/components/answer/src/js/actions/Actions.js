var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants');

module.exports = {

    state (data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.STATE,
            data: data
        });
    },

    loadEdit (question_id) {
        if (question_id) {
            var self = this;
            self.state('Verificando se existe respostas vinculada a questão.');
            $.ajax({
                url: b + '/fatorcms/get-answers.json',
                data: {_token: $('meta[name="_token"]').data('content'), question_id: question_id},
                method: 'post',
                success: function (res) {
                    if (res.error == false)
                        self.store(res.data);
                    self.state('');
                }
            });
        }
    },

    store (data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.STORE,
            data: data
        });
    },

    destroy (id) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.DELETE,
            data: id
        });
    },

    edit (id) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.EDIT,
            data: id
        });
    }

};