var keyMirror = require('keymirror');
module.exports = keyMirror({
    STORE: null,
    EDIT: null,
    DELETE: null,
    STATE: null
});
