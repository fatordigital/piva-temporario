var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    assign = require('react/lib/Object.assign'),
    EventEmitter = require('events').EventEmitter,
    Helpers = require('../Helpers/Helpers.react'),
    CHANGE_EVENT = 'change',
    _data = {},
    _edit = {},
    _state = '';

function setData (data) {
    _data = $.merge(data, _data);
}

function destroy (id) {
    Helpers.removeObject(id, _data, 'id');
}

function edit (id) {
    _edit = Helpers.getObject(id, _data, 'id');
    Helpers.removeObject(id, _data, 'id');
}

function state(data) {
    _state = data;
}

var StartStore = assign(EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener (callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    getData () {
        return _data;
    },
    getEdit () {
        return _edit;
    },
    getState () {
        return _state;
    }
});

StartStore.dispatchToken = AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {
        case AppConstants.STORE:
            setData(action.data);
            break;
        case AppConstants.EDIT:
            edit(action.data);
            break;
        case AppConstants.DELETE:
            destroy(action.data);
            break;
        case AppConstants.STATE:
            state(action.data);
            break;
    }
    StartStore.emitChange();
    return true;
});


module.exports = StartStore;
