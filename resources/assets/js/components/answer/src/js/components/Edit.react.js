var React = require('react'),
    Controller = require('../actions/Actions')

var Edit = React.createClass({

    handleSubmit (e) {
        var text = $(this.refs.text);
        var check = this.refs.check.checked;
        if (text.code().length == '<p><br></p>') {
            swal('Professor Piva', 'Antes de adicionar a resposta é necessário que informe um texto', 'error');
            return false;
        }
        Controller.store([{id: this.props.data.id, text: text.code(), check: check}]);
        $(this.refs.btClose).click();
    },

    render () {
        if (this.props.data) {
            var self = this;
            $(self.refs.text).code(self.props.data.text);
            if (self.props.data.check) {
                $('input[name="editAnswer[true]"][value="1"]').prop('checked', 'checked');
            } else {
                $('input[name="editAnswer[true]"][value="0"]').prop('checked', 'checked');
            }
            return (
                <div className="modal fade bs-example-modal-lg" id="edit-answer" tabIndex="-1" role="dialog">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close hide" ref="btClose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title">
                                Editar Resposta
                            </h4>
                        </div>
                        <div className="modal-body">
                            <div className="form-group">
                              <p>
                                <label htmlFor="status">
                                  Resposta correta? <strong className="text-danger">(*)</strong>
                                </label>
                              </p>
                              <div className="radio radio-success radio-inline">
                                  <input ref="check" name="editAnswer[true]" type="radio" defaultValue="1"/>
                                  <label htmlFor="on">
                                      Ativo
                                  </label>
                              </div>
                              <div className="radio radio-danger radio-inline">
                                  <input name="editAnswer[true]" ref="noCheck" type="radio" defaultChecked="checked" defaultValue="0"/>
                                  <label htmlFor="off">
                                      Inativo
                                  </label>
                              </div>
                            </div>

                            <div className="hr-line-dashed"></div>
                            <div className="form-group">
                                <textarea name="editAnswer[text]" ref="text" className="form-control summernote" />
                            </div>
                            <button type="button" onClick={this.handleSubmit} className="btn btn-primary">
                                Salvar Resposta
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            )
        } else {
            return null;
        }
    }
});

module.exports = Edit;