var React = require('react'),
    Model = require('../stores/Store'),
    Controller = require('../actions/Actions'),
    Index = require('./Index.react'),
    Create = require('./Create.react'),
    Edit = require('./Edit.react')

var App = React.createClass({

    getInitialState () {
        return {
            data: {},
            edit: {},
            load: ''
        };
    },

    componentDidMount () {
        Controller.loadEdit(this.props.edit);
    },

    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            data: Model.getData(),
            edit: Model.getEdit(),
            load: Model.getState()
        });
    },

    render() {
        return (
            <div>
                <div id="info-answer">{this.state.load}</div>
                <Index data={this.state.data} />
                <Create />
                <Edit data={this.state.edit} />
            </div>
        )
    }

});

module.exports = App;
