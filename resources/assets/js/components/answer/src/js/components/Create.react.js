var React = require('react'),
    Controller = require('../actions/Actions'),
    Helper = require('../helpers/Helpers.react');

var Create = React.createClass({

    handleSubmit (e) {
        var text = $(this.refs.text);
        var check = this.refs.check.checked;
        if (text.code().length == '<p><br></p>') {
            swal('Professor Piva', 'Antes de adicionar a resposta é necessário que informe um texto', 'error');
            return false;
        }
        Controller.store([{id: Helper.uuid(), text: text.code(), check: check}]);
        text.code('');
        this.refs.check.checked = false;
        this.refs.noCheck.checked = true;
    },

    render () {
        return (
            <div className="modal fade bs-example-modal-lg" id="create-answer" tabIndex="-1" role="dialog">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title">
                                Adicionar resposta
                            </h4>
                        </div>
                        <div className="modal-body">
                            <div className="form-group">
                              <p>
                                <label htmlFor="status">
                                  Resposta correta? <strong className="text-danger">(*)</strong>
                                </label>
                              </p>
                              <div className="radio radio-success radio-inline">
                                  <input ref="check" name="answer[true]" type="radio" defaultValue="1" />
                                  <label htmlFor="on">
                                      Ativo
                                  </label>
                              </div>
                              <div className="radio radio-danger radio-inline">
                                  <input name="answer[true]" ref="noCheck" type="radio" defaultChecked="checked" defaultValue="0" />
                                  <label htmlFor="off">
                                      Inativo
                                  </label>
                              </div>
                            </div>

                            <div className="hr-line-dashed"></div>
                            <div className="form-group">
                                <textarea name="answer[text]" ref="text" className="form-control summernote" />
                            </div>
                            <button type="button" onClick={this.handleSubmit} className="btn btn-primary">
                                Salvar Resposta
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = Create;