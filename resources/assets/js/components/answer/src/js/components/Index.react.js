var React = require('react'),
    Controller = require('../actions/Actions'),
    Helper = require('../helpers/Helpers.react');

var Index = React.createClass({

    handleDestroy(id, e) {
        var self = this;
        swal({
            title: "Você tem certeza?",
            text: "Deseja realmente remover esta resposta?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            cancelButtonText: "Não",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                Controller.destroy(id);
                swal("Deletada", "A questão foi removida do simulado com sucesso.", "success");
            } else {
                swal("Cancelado", "A resposta continua nos registros.", "error");
            }
        });
    },

    handleEdit (id, e) {
        Controller.edit(id);
    },

    render () {
        var self = this;
        return (
            <table className="table table-bordered">
                <thead>
                  <tr>
                      <td>Correta?</td>
                      <td>Resposta</td>
                      <td width="6%">Opções</td>
                  </tr>
                </thead>
                <tbody>
                {Object.keys(this.props.data).map(function(row){
                    return (
                        <tr key={row}>
                            <td>{self.props.data[row].check ? 'Verdadeira' : 'Falsa'}</td>
                            <td>{Helper.stripHtml(self.props.data[row].text)}</td>
                            <td>
                                <button type="button" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#edit-answer" className="btn btn-primary btn-xs margin-right" onClick={self.handleEdit.bind(self, self.props.data[row].id)}>
                                    <i className="fa fa-pencil" />
                                </button>
                                <button type="button" className="btn btn-xs btn-danger" onClick={self.handleDestroy.bind(self, self.props.data[row].id)}>
                                    <i className="fa fa-trash" />
                                </button>
                                <input type="hidden" name="answers[]" value={JSON.stringify(self.props.data[row])} />
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        )
    }
});

module.exports = Index;
