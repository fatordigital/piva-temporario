var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    Model = require('../stores/Store')

module.exports = {

    saveContent (form) {
        $.ajax({
            url: b + '/fatorcms/save-content.json',
            dataType: 'json',
            contentType: false,
            data: form,
            processData: false,
            method: 'post',
            success: function(res) {
                if (res.error == true) {
                    swal('Ops!', 'Não foi possível salvar este conteúdo, atualize a página e tente novamente', 'error');
                } else {
                    this.loadContents('', '', '', 'contents#'+res.data.id);
                    this.getDisciplines();
                }
            }.bind(this)
        });
    },

    getDisciplines () {
        $.ajax({
            url: b + '/fatorcms/get-disciplines.json',
            dataType: 'json',
            method: 'get',
            success: function (res) {
                if (res.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.DISCIPLINES,
                        data: res.data
                    });
                }
            }
        });
    },

    loadContents (discipline, notTests, notContents, idContent) {
        $.ajax({
            url: b + '/fatorcms/content.json',
            data: {_token: $('meta[name="_token"]').data('content'), not_tests: notTests, not_contents: notContents, discipline: discipline, notIn: Model.getNotIn()},
            dataType: 'json',
            method: 'post',
            success: function (res) {
                if (res.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.CONTENTS,
                        data: res.data
                    });
                    if (idContent) {
                        AppDispatcher.handleViewAction({
                            actionType: AppConstants.SELECT,
                            data: idContent
                        });
                    }
                } else {
                    swal('Professor Piva', 'Ops! Não foi possível carregar os conteúdos, atualize a página.', 'error');
                }
            }.bind(this)
        });
    },

    select (id) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.SELECT,
            data: id
        });
    },

    edit (id) {
        $.ajax({
            url: b + '/fatorcms/content-edit.json',
            data: {_token: $('meta[name="_token"]').data('content'), notIn: Model.getNotIn(), id: id},
            dataType: 'json',
            method: 'post',
            success: function (res) {
                if (res.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.EDIT,
                        data: res.data
                    });
                }
            }
        });
    },

    destroy (id) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.DESTROY,
            data: id
        });
    }
};