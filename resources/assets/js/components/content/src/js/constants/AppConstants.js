var keyMirror = require('keymirror');
module.exports = keyMirror({
    CONTENTS: null,

    SELECT: null,
    DISCIPLINES: null,

    DESTROY: null,

    EDIT: null
});
