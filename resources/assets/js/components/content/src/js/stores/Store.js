var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    assign = require('react/lib/Object.assign'),
    EventEmitter = require('events').EventEmitter,
    Helpers = require('../Helpers/Helpers.react'),
    CHANGE_EVENT = 'change',
    _options = {
        tests: {},
        contents: {}
    },
    _selected = {},
    _notIn = [],
    _disciplines = {},
    _test_chooses = [],
    _content_chooses = []

function getExplode(data) {
    var explode = data.split('#');
    return {
        name: explode[0],
        value: explode[1]
    }
}

function setData(data) {
    _options.tests = data.tests;
    _options.contents = data.contents;
}

function setSelect(data) {
    data = getExplode(data);
    if (data.name == 'tests')
        _test_chooses.push(data.value);
    if (data.name == 'contents')
        _content_chooses.push(data.value);
    _options[data.name][Helpers.getObjectKey(data.value, _options[data.name], 'id')].type = data.name;
    _selected = $.merge([Helpers.getObject(data.value, _options[data.name], 'id')], _selected);
    Helpers.removeObject(data.value, _options[data.name], 'id');
}

function setEdit(data) {
    $.each(data, function (key, row) {
        row[row.ContentType].Begin = row.Begin ? row.Begin : '';
        row[row.ContentType].End = row.End;
        row[row.ContentType].order = row.order;
        row[row.ContentType].ContentType = row.ContentType;
        row[row.ContentType].type = row.ContentType;
        _selected = $.merge([row[row.ContentType]], _selected);
        Helpers.removeObject(row[row.ContentType].id, _options[row.ContentType], 'id');
    });
}

function discipline (data) {
    _disciplines = data;
}

function destroy(data) {
    Helpers.removeObject(data.id, _selected, 'id');
    if (data.type == 'tests')
        Helpers.removeArray(data.id, _test_chooses);
    else {
        Helpers.removeArray(data.id, _content_chooses);
    }
}

var StartStore = assign(EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener (callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    getOptions () {
        return _options;
    },
    getNotIn () {
        return _notIn;
    },
    getSelected () {
        return _selected;
    },
    getDisciplines () {
        return _disciplines;
    },
    getTestChoose () {
        return _test_chooses;
    },
    getContentChoose () {
        return _content_chooses;
    }
});

StartStore.dispatchToken = AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {
        case AppConstants.CONTENTS:
            setData(action.data);
            break;
        case AppConstants.SELECT:
            setSelect(action.data);
            break;
        case AppConstants.EDIT:
            setEdit(action.data);
            break;
        case AppConstants.DISCIPLINES:
            discipline(action.data);
            break;
        case AppConstants.DESTROY:
            destroy(action.data);
            break;
    }
    StartStore.emitChange();
    return true;
});


module.exports = StartStore;
