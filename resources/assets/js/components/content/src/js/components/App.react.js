var React = require('react'),
    Form = require('./Form.react'),
    Model = require('../stores/Store'),
    Controller = require('../actions/Actions'),
    Helpers = require('../helpers/Helpers.react')

var App = React.createClass({

    getInitialState () {
        return {
            contentOptions: {},
            selected: {},
            discipline: {},
            test_chooses: [],
            content_chooses: []
        };
    },

    componentDidMount () {
        Controller.getDisciplines();
        Controller.loadContents();
        if (this.props.edit) {
            Controller.edit(this.props.edit);
        }
    },

    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            contentOptions: Model.getOptions(),
            selected: Model.getSelected(),
            discipline: Model.getDisciplines(),
            test_chooses: Model.getTestChoose(),
            content_chooses: Model.getContentChoose()
        });
        this.jquery();
    },

    jquery () {
        $('.input-daterange').datepicker({
            format: 'dd/mm/yyyy',
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true
        });
    },

    handleSelect (e) {
        Controller.select(e.target.value);
        $(e.target).val("");
    },

    getName (key) {
        if (key == 'tests') {
            return 'Simulado';
        } else if (key == 'contents') {
            return 'Conteúdo';
        }
    },

    handleFilter (e) {
        Controller.loadContents(e.target.value, this.state.test_chooses, this.state.content_chooses);
    },

    handleAll (e) {
        var ids = [];
        Object.keys(this.state.contentOptions).map(function (key, row) {
            var state = this.state.contentOptions;
            if (state[key]) {
                var current = state[key];
                Object.keys(current).map(function (k, r) {
                    ids.push(key + "#" + current[r].id);
                }.bind(this));
            }
        }.bind(this));
        for (var i = 0; i < ids.length; i++) {
            Controller.select(ids[i]);
        }
    },

    handleDestroy(id, e) {
        Controller.destroy(id);
        Controller.loadContents('', this.state.test_chooses, this.state.content_chooses);
    },

    handleSave () {

        if ($('#name_modal').val() == '' || ($('#discipline_modal').val() == '' && $('#new_discipline_modal').val() == '')) {
            swal('Ops', 'Para conseguir prosseguir é necessário preencher todos os campos seguidos de (*)', 'error');
            return false;
        }

        var form = new FormData();
        form.append('free', $('input[name="free"]:checked').val());
        form.append('type', $('input[name="type"]:checked').val());
        form.append('name', $('#name_modal').val());
        form.append('content', $('#content_text').code().length > 11 ? $('#content_text').code() : $('#content_video').val());
        form.append('filename', $('#filename')[0].files[0]);
        form.append('discipline_id', $('#discipline_modal').val() ? $('#discipline_modal').val() : $('#new_discipline_modal').val());
        Controller.saveContent(form);

        $('input[name="free"][value="0"]').prop('checked', true);
        $('input[name="type"][value="text"]').prop('checked', true);
        $('#name_modal').val('')
        $('#content_text').code('<p><br /></p>');
        $('#content_video').val('');
        $('#filename').val('');
        $('#discipline_modal').val('');
        $('#new_discipline_modal').val('');
    },

    render() {
        return (
            <div className="row">
                <div className="modal fade bs-example-modal-lg" id="fast-register-content" tabIndex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                  <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1>Novo Conteúdo</h1>
                        </div>
                        <div className="modal-body">
                            <Form discipline={this.state.discipline} />
                        </div>
                        <div className="modal-footer">
                            <div className="row">
                                <div className="col-lg-12">
                                    <button className="btn btn-success" type="button" onClick={this.handleSave}>
                                        Salvar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6">
                    <div className="form-group">
                        <label htmlFor="discipline">
                            [Filtro] Disciplina
                        </label>
                        <select className="form-control"
                                onChange={this.handleFilter}>
                            <option value="">Filtre por disciplina</option>
                            {Object.keys(this.state.discipline).map(function (key, row) {
                                return <option key={key}
                                               value={this.state.discipline[row].id}>{this.state.discipline[row].name}</option>
                            }.bind(this))}
                        </select>
                    </div>
                </div>

                <div className="col-lg-12">
                    <div className="form-group">
                        <label htmlFor="content">
                            Selecione o conteúdo
                        </label>
                        <div className="input-group">
                            <select id="contentsOptions"
                                    style={{width: '100%'}}
                                    className="form-control"
                                    onChange={this.handleSelect}>
                                <option value="">Selecione um conteúdo para adicionar</option>
                                {Object.keys(this.state.contentOptions).map(function (key, row) {
                                    var state = this.state.contentOptions;
                                    if (state[key]) {
                                        var current = state[key];
                                        return Object.keys(current).map(function (k, r) {
                                            if (current[r].disciplines) {
                                                return <option key={key+'-'+current[r].id}
                                                               value={key+"#"+current[r].id}>{current[r].name + ' - ' + current[r].disciplines.name +  ' - ' + this.getName(key)}</option>
                                            } else {
                                                return <option key={key+'-'+current[r].id}
                                                               value={key+"#"+current[r].id}>{current[r].name + ' - ' + this.getName(key)}</option>
                                            }
                                        }.bind(this));
                                    }
                                }.bind(this))}
                            </select>
                            <span className="input-group-addon"
                                  onClick={this.handleAll}>
                                <i className="fa fa-plus"/>
                            </span>
                            <a className="btn input-group-addon btn-success" data-toggle="modal" href="#" data-target="#fast-register-content">
                                Criar Conteúdo
                            </a>
                        </div>
                    </div>
                </div>
                <div className="col-lg-12">
                    <table className="table table-bordered">
                        <tbody>
                        {Object.keys(this.state.selected).map(function (key, row) {
                            var state = this.state.selected;
                            return (
                                <tr key={key}>
                                    <td style={{fontSize: '18px'}}>{state[key].name}</td>
                                    <td width="4%"
                                        data-toggle="title"
                                        title="Ordem de apresentação do conteúdo">
                                        <input type="text"
                                               className="form-control"
                                               defaultValue={ state[key].order ? state[key].order : 1 }
                                               name={"content[" + state[key].id + "][order]"}/>
                                    </td>
                                    <td width="20%">
                                        <div className="input-daterange input-group">
                                            <input type="text"
                                                   autoComplete="off"
                                                   defaultValue={ state[key].Begin ?  state[key].Begin : "" }
                                                   name={"content[" + state[key].id + "][begin_date]"}
                                                   className="input-sm form-control"
                                                   placeholder="dd/mm/AAAA"/>
                                            <span className="input-group-addon">até</span>
                                            <input type="text"
                                                   autoComplete="off"
                                                   defaultValue={ state[key].Begin ?  state[key].Begin : "" }
                                                   className="input-sm form-control"
                                                   name={"content[" + state[key].id + "][end_date]"}
                                                   placeholder="dd/mm/AAAA"/>
                                        </div>
                                        <input type="hidden"
                                               name={"content[" + state[key].id + "][id]"}
                                               value={state[key].id}/>
                                        <input type="hidden"
                                               name={"content[" + state[key].id + "][type]"}
                                               value={state[key].type}/>
                                    </td>
                                    <td width="3%">
                                        <button className="btn btn-xs btn-danger"
                                                onClick={this.handleDestroy.bind(this, state[key])}
                                                type="button">
                                            <i className="fa fa-trash"/>
                                        </button>
                                    </td>
                                </tr>
                            )
                        }.bind(this))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

});

module.exports = App;
