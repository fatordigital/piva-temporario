var React = require('react'),
    Model = require('../stores/Store')

var Form = React.createClass({

    getDefaultProps () {
        return {
            discipline: {}
        }
    },

    getInitialState () {
        return {
            stateDiscipline: true
        }
    },

    handleNewDiscipline (e) {
        var select = $(this.refs.selectDiscipline);
        var input = $(this.refs.addDiscipline);
        if (select.is(':visible')) {
            select.hide();
            select.val('');
            input.show();
            this.setState({stateDiscipline: false});
        } else {
            input.hide();
            input.val('');
            select.show();
            this.setState({stateDiscipline: true});
        }
    },

    handleType (type, e) {
        $('.type').hide();
        $('#' + e.target.value).show();
        $('#filename').prop('disabled', 'disabled');
        $('#content_text').prop('disabled', 'disabled');
        $('#content_video').prop('disabled', 'disabled');
        if (type == 'text') {
            $('#content_text').removeAttr('disabled');
        } else if (type == 'file') {
            $('#filename').removeAttr('disabled');
        } else {
            $('#content_video').removeAttr('disabled');
        }
    },

    render () {
        return (
            <div className="row">
              <div className="col-lg-6">
                <div className="form-group">
                    <label htmlFor="status" className="control-label">Conteúdo Grátis?</label>
                    <br/>
                    <div className="radio radio-success radio-inline">
                        <input type="radio" name="free" value="1" />
                        <label htmlFor="radio4">
                        Sim
                      </label>
                    </div>
                    <div className="radio radio-danger radio-inline">
                        <input type="radio" name="free" value="0" defaultChecked />
                        <label htmlFor="radio4">
                        Não
                      </label>
                    </div>
                </div>

                <div className="form-group">
                  <p>
                    <label htmlFor="status">
                      Tipo de conteúdo <strong className="text-danger">(*)</strong>
                    </label>
                  </p>
                  <div className="radio radio-success radio-inline">
                    <input type="radio" name="type" value="video" onClick={this.handleType.bind(this, 'video')} />
                    <label htmlFor="on">
                        Vídeo
                    </label>
                  </div>
                  <div className="radio radio-danger radio-inline">
                    <input type="radio" name="type" value="text" defaultChecked onClick={this.handleType.bind(this, 'text')} />
                    <label htmlFor="off">
                        Texto
                    </label>
                  </div>
                  <div className="radio radio-primary radio-inline">
                    <input type="radio" name="type" value="file" onClick={this.handleType.bind(this, 'file')} />
                    <label htmlFor="off">
                        Arquivo
                    </label>
                  </div>
                </div>
              </div>

              <div className="col-lg-6">

                <div className="form-group">
                  <label htmlFor="name">
                    Nome <strong className="text-danger">(*)</strong>
                  </label>
                  <input type="text" id="name_modal" className="form-control" maxLength="120"/>
                </div>

                <div className="form-group">
                  <label htmlFor="icon">
                      <i className="fa fa-question-circle" data-toggle="tooltip" title="Caso não exista a disciplina, digite o nome da disciplina e após salvar a aula será criado automaticamente um novo registro." />
                      Selecione/Digite a Disciplina <strong className="text-danger">(*)</strong>
                  </label>
                  <div className="input-group">
                      <div ref="selectDiscipline">
                          <select className="form-control" id="discipline_modal">
                              <option value="">Selecione a disciplina</option>
                              {Object.keys(this.props.discipline).map(function (key, row) {
                                  return <option key={key}
                                                 value={this.props.discipline[row].id}>{this.props.discipline[row].name}</option>
                              }.bind(this))}
                          </select>
                      </div>
                      <div ref="addDiscipline" style={{display: 'none'}}>
                          <input type="text" className="form-control" placeholder="Digite aqui a nova disciplina" id="new_discipline_modal" />
                      </div>
                      <span className="input-group-btn">
                          <button type="button" onClick={this.handleNewDiscipline} className="btn btn-success" title="Nova disciplina"><i className={!this.state.stateDiscipline ? 'fa fa-list' : 'fa fa-plus'} /></button>
                      </span>
                  </div>
                </div>

              </div>

              <div className="col-lg-12">

                <div className="form-group type" id="video" style={{display: 'none'}}>
                  <label htmlFor="content">
                    Conteúdo Vídeo <strong className="text-danger">(*)</strong>
                  </label>
                    <input type="text" id="content_video" disabled className="form-control" />
                </div>

                <div className="form-group type" id="file" style={{display: 'none'}}>
                   <label htmlFor="content">
                    Conteúdo Arquivo <strong className="text-danger">(*)</strong>
                   </label>
                   <input type="file" id="filename" className="form-control" disabled />
                </div>

                <div className="form-group type" id="text">
                  <label htmlFor="description">
                    <i className="fa fa-question-circle" data-toggle="tooltip" title="Você pode inserir imagens, vídeos e arquivos ao montar o conteúdo." /> Conteúdo Texto <strong className="text-danger">(*)</strong>
                  </label>
                  <textarea id="content_text" className="form-control summernote" />
                </div>
              </div>
            </div>
        )
    }

});

module.exports = Form;