var React = require('react'),
    Controller = require('../actions/Actions'),
    StartStore = require('../stores/Store');

var Index = React.createClass({

    getInitialState () {
      return {
        options: {}
      };
    },

    handleSubmit (e) {
      e.preventDefault();
      var id = $(e.target).data('content');
      var begin = this.refs['begin_date['+id+']'].value;
      var end = this.refs['end_date['+id+']'].value;
      Controller.updateDate(begin, end, id);
    },

    componentDidUpdate() {
      $('.s-datepicker').datepicker({
        format: 'dd/mm/yyyy',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
      });
    },

    handleDestroy (e) {
      var target = $(e.target).parents('[data-tl]');
      var id = target.data('tl');
      Controller.destroy(id);
      target.remove();
    },

    render () {
        var self = this;
        var row = this.props.data.map(function(row){
            return (
                <div key={row.id} data-tl={row.id} className="vertical-timeline-block">
                  <div className="vertical-timeline-content" style={{marginLeft: 0}}>
                      <div className="pull-right" onClick={self.handleDestroy}>
                        <button type="button" className="btn btn-xs btn-danger" data-title="title" title="Remover o vinculo desta aula.">
                          <i className="fa fa-trash" />
                        </button>
                      </div>
                      <div className="clearfix" />
                      <h2>{row.lesson.name}</h2>
                      <p>{row.lesson.resume}</p>
                      <span className="vertical-date">
                          <div className="row">
                              <form onSubmit={self.handleSubmit} data-content={row.id}>
                                <div className="col-lg-12" style={{paddingLeft:'0px'}}>
                                    <div className="col-lg-5">
                                      <div className="form-group">
                                        <label>Data início</label>
                                        <input type="text" ref={"begin_date["+row.id+"]"} defaultValue={row.Begin} className="s-datepicker form-control" />
                                      </div>
                                    </div>

                                    <div className="col-lg-5">
                                      <div className="form-group">
                                        <label>Data final</label>
                                        <div className="input-group">
                                          <input type="text" ref={"end_date["+row.id+"]"} defaultValue={row.End} className="s-datepicker form-control" />
                                          <span className="input-group-btn">
                                            <button data-loading="false" type="submit" className="btn btn-primary">
                                            Salvar
                                            </button>
                                          </span>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                              </form>
                          </div>
                      </span>
                    </div>
                </div>
            )
        });
        return (
            <div className="vertical-container dark-timeline" style={{width: '100%'}}>
                {row}
            </div>
        )
    }
});
module.exports = Index;
