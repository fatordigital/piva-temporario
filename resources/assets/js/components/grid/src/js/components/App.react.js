var React = require('react'),
    StartStore = require('../stores/Store'),
    Controller = require('../actions/Actions'),
    Index = require('../components/Index.react'),
    Select = require('../components/Select.react');

var App = React.createClass({

    getInitialState () {
        return {
            settings: {store: '', update: '', delete: ''},
            data: [],
            options: {}
        };
    },

    componentDidMount () {
        Controller.setSettings(this.props.settings);
        Controller.setData(this.props.data);
        Controller.setSlug(this.props.slug);
        Controller.setOptions();
    },

    componentWillMount: function () {
        StartStore.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        StartStore.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            data: StartStore.getData(),
            settings: StartStore.getSettings(),
            options: StartStore.getOptions(),
            slug: StartStore.getSlug()
        });
    },

    render() {
        return (
            <div className="modal inmodal fade" id="grid" tabIndex="-1" role="dialog" aria-hidden="true">
                <div className="modal-dialog modal-lg">
                    <div className="modal-content">
                        <div className="modal-header" style={{borderBottom: '0px !important'}}>
                            <button type="button" className="close" data-dismiss="modal">
                              <span aria-hidden="true">&times;</span>
                              <span className="sr-only">Fechar</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="form-group">
                              <label>
                                Adicionar mais aulas
                              </label>
                              <Select data={this.state.options} />
                            </div>
                            <Index data={this.state.data} />
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-warning" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

});

module.exports = App;
