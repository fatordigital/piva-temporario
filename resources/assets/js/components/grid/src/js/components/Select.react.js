var React = require('react'),
    Controller = require('../actions/Actions'),
    StartStore = require('../stores/Store');

var Options = React.createClass({
  render () {
    return <option value={this.props.id}>{this.props.name}</option>
  }
});

var Select = React.createClass({

    getInitialState () {
      return {
        options: {}
      };
    },

    handleChange (e) {
      e.preventDefault();
      var value = this.refs.lesson;
      if (!value.value)
        return false;
      Controller.store(value.value);
    },

    render () {
        var data = this.props.data;
        return (
            <div className="form-group">
              <select name="lesson" ref="lesson" onChange={this.handleChange} className="form-control">
                <option key="default" defaultValue="">Selecione as aulas que queira adicionar</option>
                {Object.keys(this.props.data).map(function(key, el) {
                  return <Options key={'key' + key} name={data[key]} id={key} />;
                })}
              </select>
            </div>
        )
    }

});
module.exports = Select;
