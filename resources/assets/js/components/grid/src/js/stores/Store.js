var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    assign = require('react/lib/Object.assign'),
    EventEmitter = require('events').EventEmitter,
    CHANGE_EVENT = 'change',
    _slug = null,
    _data = {},
    _options = {},
    _settings = {
        update: '',
        store: '',
        delete: ''
    };

function setData(data) {
  _data = $.merge(data, _data);
}

function setSettings (setting) {
  _settings = setting;
}

function setOptions (options) {
  _options = options;
}

function setSlug (slug) {
  _slug = slug;
}

var StartStore = assign(EventEmitter.prototype, {
    emitChange() {
      this.emit(CHANGE_EVENT);
    },
    addChangeListener (callback) {
      this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener (callback) {
      this.removeListener(CHANGE_EVENT, callback);
    },
    getData () {
      return _data;
    },
    getSettings () {
      return _settings;
    },
    getOptions () {
      return _options;
    },
    getSlug () {
      return _slug;
    },
    uuid: function () {
        var i, random;
        var uuid = '';
        for (i = 0; i < 32; i++) {
            random = Math.random() * 16 | 0;
            if (i === 8 || i === 12 || i === 16 || i === 20) {
                uuid += '-';
            }

            uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
        }
        return uuid;
    }
});

StartStore.dispatchToken = AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {
        case AppConstants.STORE:
            setData(action.data);
            break;
        case AppConstants.SETTINGS:
            setSettings(action.data);
            break;
        case AppConstants.OPTIONS:
            setOptions(action.data);
            break;
        case AppConstants.SLUG:
            setSlug(action.data);
            break;
    }
    StartStore.emitChange();
    return true;
});


module.exports = StartStore;
