var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    StartStore = require('../stores/Store'),
    request = require('superagent');

module.exports = {
    setSlug (slug) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.SLUG,
            data: slug
        });
    },
    setOptions () {
        request.get(b + '/fatorcms/lessons.json')
            .query({type: 'lists', slug: StartStore.getSlug()})
            .set('Accept', 'application/json')
            .end(function (err, response) {
                if (err) return console.log(err);
                AppDispatcher.handleViewAction({
                    actionType: AppConstants.OPTIONS,
                    data: response.body
                });
            });
    },
    setSettings (data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.SETTINGS,
            data: data
        });
    },
    updateDate (begin_date, end_date, id) {
        request.post(b + '/fatorcms/lesson-team-save-date.json')
            .set('Accept', 'application/json')
            .query({
                _token: $('meta[name="_token"]').data('content'),
                begin_date: begin_date,
                end_date: end_date,
                id: id
            })
            .end(function (err, response) {
                if (err) return console.log(err);
                fnToastr('Professor Piva', 'Atualização efetuada com sucesso', 'success');
            });
    },
    store (lesson_id) {
        var self = this;
        request.post(b + '/fatorcms/lesson-team-save.json')
            .set('Accept', 'application/json')
            .query({
                _token: $('meta[name="_token"]').data('content'),
                lesson_id: lesson_id,
                team_id: StartStore.getSlug()
            })
            .end(function (err, response) {
                if (err) return console.log(err);
                AppDispatcher.handleViewAction({
                    actionType: AppConstants.STORE,
                    data: [response.body]
                });
                fnToastr('Professor Piva', 'A Aula foi vinculada com sucesso.', 'success');
                self.setOptions();
            });
    },
    destroy (id) {
        var self = this;
        request.del(b + '/fatorcms/lesson-team-destroy.json')
            .set('Accept', 'application/json')
            .query({_token: $('meta[name="_token"]').data('content'), id: id})
            .end(function (err, response) {
                if (err) return console.log(err);
                fnToastr('Professor Piva', 'O vínculo foi deletado com sucesso', 'success');
                self.setOptions();
            });
    },
    setData (data) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.STORE,
            data: data
        });
    }
};
