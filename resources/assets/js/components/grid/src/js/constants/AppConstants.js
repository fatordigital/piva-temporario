var keyMirror = require('keymirror');
module.exports = keyMirror({
    SETTINGS: null,
    STORE: null,
    OPTIONS: null,
    KEYS: null,
    SLUG: null
});
