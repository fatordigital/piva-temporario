var React = require('react'),
    Controller = require('../actions/Actions');

var Paginate = React.createClass({

    handlePaginate (page, e) {
        e.preventDefault();
        Controller.changePage(page);
    },

    render () {
        if (this.props.data.last_page > 1) {
            var page = [];
            for (var i = 1; i <= this.props.data.last_page; i++) {
                page.push(<li style={{display: 'inline-block'}} key={i}><a href="#" onMouseOver={this.handlePaginate.bind(this, i)}>{i}</a></li>);
            }
            return (
                <ul className="pagination">
                {page}
            </ul>
            )
        } else {
            return null;
        }
    }

});

module.exports = Paginate;