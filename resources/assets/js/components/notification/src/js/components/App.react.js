var React = require('react'),
    Model = require('../stores/Store'),
    Controller = require('../actions/Actions'),
    Hour = require('./City.react'),
    Paginate = require('./Paginate.react')

var App = React.createClass({

    getInitialState () {
        return {
            data: {},
            length: 0,
            label: 'label-primary',
            response: {},
            page: 1
        };
    },

    componentDidMount () {
        Controller.getNotifications(this.state.page);
        setInterval(function(){Controller.getNotifications(this.state.page)}.bind(this) , 10000);
    },

    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            data: Model.getData(),
            length: Model.getLength(),
            label: Model.getLabel(),
            response: Model.getResponse(),
            page: Model.getPage()
        });
    },

    markRead (id, e) {
        e.preventDefault();
        Controller.markRead(id);
    },

    render() {
        return (
            <ul className="nav navbar-top-links navbar-right">
                <li>
                    <span className="m-r-sm text-muted welcome-message">
                        Olá <strong className="text-success">{this.props.userName}</strong> - <Hour />
                    </span>
                </li>
                <li className="dropdown">
                    {function() {
                        if (this.state.length > 0) {
                            return (
                                <ul className="dropdown-menu dropdown-alerts">
                                    {Object.keys(this.state.data).map(function (key, row) {
                                        return (
                                            <li key={key}>
                                                <a onClick={this.markRead.bind(this, this.state.data[key].id)}>
                                                    <div>
                                                        <i className="fa fa-alert"/> {this.state.data[key].title}
                                                        <span className="pull-right text-muted small">{this.state.data[key].DateHour}</span>
                                                    </div>
                                                </a>
                                            </li>
                                        )
                                    }.bind(this))}
                                    <Paginate data={this.state.response}/>
                                </ul>
                            )
                        }
                    }.bind(this)()}
                    <a className="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i className="fa fa-bell-o"/>
                        <span className={"label "+this.state.label}>{this.state.length}</span>
                    </a>
                </li>
                <li>
                    <a href={b + '/ate-logo'}>
                        <i className="fa fa-sign-out"/> Sair!
                    </a>
                </li>
            </ul>
        )
    }

});

module.exports = App;
