var keyMirror = require('keymirror');
module.exports = keyMirror({
    getNotifications: null,
    changePage: null
});
