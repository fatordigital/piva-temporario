var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants')

module.exports = {

    getNotifications (page) {
        $.ajax({
            url: b + '/fatorcms/notifications?page=' + page,
            dataType: 'json',
            method: 'get',
            success: function (res) {
                if (res.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.getNotifications,
                        data: res,
                        length: res.total
                    });
                }
            }
        });
    },

    changePage (page) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.changePage,
            data: page
        });
        this.getNotifications(page);
    },

    markRead (id) {
        $.ajax({
            url: b + '/fatorcms/notifications',
            data: {_token: $('meta[name="_token"]').data('content'), id: id},
            method: 'post',
            dataType: 'json',
            success: function (res) {
                if (res.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.getNotifications,
                        data: res,
                        length: res.total
                    });
                }
            }
        });
    }

};