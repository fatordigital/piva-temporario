var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    assign = require('react/lib/Object.assign'),
    EventEmitter = require('events').EventEmitter,
    Helpers = require('../Helpers/Helpers.react'),
    CHANGE_EVENT = 'change',
    _data = {},
    _length = 0,
    _label = 'default',
    _response = {},
    _page = 1;

function setData (data, length) {
    _data = data.data;
    _response = data;
    _length = length;
    _label = labelPriority();
}

function setPage (page) {
    _page = page;
}

function labelPriority() {
    if (_length == 0)
        return 'label-primary';
    else if (_length > 0 && _length < 10)
        return 'label-success';
    else if (_length > 10 && _length < 20)
        return 'label-warning';
    else if (_length > 20)
        return 'label-danger';
}

var StartStore = assign(EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener (callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    getData () {
        return _data;
    },
    getLabel () {
        return _label;
    },
    getLength () {
        return _length;
    },
    getResponse () {
        return _response;
    },
    getPage() {
        return _page;
    }
});

StartStore.dispatchToken = AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {
        case AppConstants.getNotifications:
            setData(action.data, action.length);
            break;
        case AppConstants.changePage:
            setPage(action.data);
            break;
    }
    StartStore.emitChange();
    return true;
});


module.exports = StartStore;
