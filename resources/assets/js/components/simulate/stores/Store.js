var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    assign = require('react/lib/Object.assign'),
    EventEmitter = require('events').EventEmitter,
    Helpers = require('../Helpers/Helpers.react'),
    CHANGE_EVENT = 'change',
    _simulate = {},
    _questions = {},
    _paginate = {},
    _page = b + 'simulate-data.json',
    _user = {};

function setSimulateAndQuestions(simulate, questions, page, user) {
    _questions = questions.data;
    delete questions.data;
    _paginate = questions;
    _simulate = simulate;
    _page = page;
    _user = user;
}

var StartStore = assign(EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener (callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    getSimulate () {
        return _simulate;
    },
    getQuestions () {
        return _questions;
    },
    getPaginate () {
        return _paginate;
    },
    getCurrentPage () {
        return _page;
    },
    getUserTest () {
        return _user;
    }
});

StartStore.dispatchToken = AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {
        case AppConstants.SIMULATE:
            setSimulateAndQuestions(action.simulate, action.questions, action.page, action.user);
            break;
    }
    StartStore.emitChange();
    return true;
});


module.exports = StartStore;
