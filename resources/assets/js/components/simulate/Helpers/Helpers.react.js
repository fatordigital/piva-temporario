var React = require('react'),
    CryptoJSAesJson = require('./Crypto'),
    CryptoJS = require('crypto-js')

module.exports = {

    addErrorField (target, msg, parent) {
        $('.' + target.attr('name')).remove();
        target.parents(parent ? parent : '.form-group').addClass('has-error');
        target.after('<span class="'+target.attr('name') + ' help-block'+'">'+msg+'</span>')
    },

    removeError (target, parent) {
        target.parents(parent ? parent : '.form-group').removeClass('has-error');
        $('.' + target.attr('name')).remove();
    },

    addSuccessField (target, parent) {
        target.parents(parent ? parent : '.form-group').addClass('has-success');
    },

    removeObject (id, arrayOfObjects, key) {
        for (var i = 0; i < arrayOfObjects.length; i++) {
            var current = arrayOfObjects[i];
            if (current[key] == id) {
                arrayOfObjects.splice(i, 1);
            }
        }
    },

    removeArray (id, array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] == id) {
                array.splice(i, 1);
            }
        }
    },

    uuid: function () {
        var i, random;
        var uuid = '';
        for (i = 0; i < 32; i++) {
            random = Math.random() * 16 | 0;
            if (i === 8 || i === 12 || i === 16 || i === 20) {
                uuid += '-';
            }

            uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
        }
        return uuid;
    },

    getObject (id, arrayOfObjects, key) {
        for (var i = 0; i < arrayOfObjects.length; i++) {
            var current = arrayOfObjects[i];
            if (current[key] == id) {
                return current;
            }
        }
    },

    send(data) {
        return CryptoJS.AES.encrypt(JSON.stringify(data), $f.pass, {format: CryptoJSAesJson}).toString();
    },

    return (res){
        return JSON.parse(CryptoJS.AES.decrypt(JSON.stringify(res), $f.pass, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8))
    },

    methodName (name) {
        switch (name) {
            case 'CREDIT_CARD':
                return "Cartão de Crédito";
                break;
            case 'BOLETO':
                return "Boleto";
                break;
            case 'ONLINE_DEBIT':
                return "Débito Online";
                break;
            case 'BALANCE':
                return "Saldo Pagseguro";
                break;
        }
    },

    markUp (html) {
        return {__html: html};
    },


    getMonths () {
        return {
            '1': '01',
            '2': '02',
            '3': '03',
            '4': '04',
            '5': '05',
            '6': '06',
            '7': '07',
            '8': '08',
            '9': '09',
            '10': '10',
            '11': '11',
            '12': '12'
        };
    },

    getYears (max) {
        max = max ? max : 10;
        var options = [];
        var date = new Date();
        for (var i = date.getFullYear(); i < date.getFullYear() + max; i++) {
            options.push(<option key={i} value={i}>{i}</option>)
        }
        return options;
    },

    validaCPF(strCPF) {
        var Soma;
        var Resto;
        Soma = 0;
        strCPF = strCPF.replace(/[^\d]+/g, '');
        var i = 0;
        if (strCPF == "00000000000") return false;

        for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(strCPF.substring(9, 10))) return false;

        Soma = 0;
        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        return Resto == parseInt(strCPF.substring(10, 11));

    },

    makeInstallments (data) {
        var options = [];
        for (var i = 0; i < data.length; i++) {
            var value = this.send({installment: data[i].quantity + '#' + data[i].installmentAmount + '#' + data[i].totalAmount + '#' + data[i].interestFree});
            var $string = data[i].interestFree ? 'sem/juros' : 'com/juros';
            if (i == 0) {
                options.push(<option key={i} value={value}>{this.valueToCoin(data[i].totalAmount)} à vista</option>);
            } else {
                options.push(<option key={i} value={value}>Parcelar em {data[i].quantity}x de {this.valueToCoin(data[i].installmentAmount)} {$string}</option>);
            }
        }
        return options;
    },

    valueToCoin(num) {
        return "R$ " + num
                .toFixed(2)
                .replace(".", ",")
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")
    },

    compose (value) {
        return value.match(/[a-z][ ][a-z]/ig);
    },

    getToken () {
        return $('#_token').data('content');
    },

    numberToWord (number) {
        number = parseInt(number);
        switch (number) {
            case 0:
                return 'a';
                break;
            case 1:
                return 'b';
                break;
            case 2:
                return 'c';
                break;
            case 3:
                return 'd';
                break;
            case 4:
                return 'e';
                break;
            case 5:
                return 'f';
                break;
            default:
                return number;
                break;
        }
    },

    stripTags (string) {
        return string.replace(/(<([^>]+)>)/ig,"");
    },

    CountDownTimer(dt, id, test_id, correcao)
    {
        var end = new Date(dt);

        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        var Helpers = this;

        function showRemaining(correcao) {
            var now = new Date();
            var distance = end - now;
            if (distance < 0 && !correcao) {

                clearInterval(timer);
                document.getElementById(id).innerHTML = 'ESGOTADO';
                $.ajax({
                    url: b + 'end-simulate.json',
                    dataType: 'json',
                    data: {
                        _token: $('#_token').data('content'),
                        request: Helpers.send({test_id: test_id})
                    },
                    method: 'post',
                    success: function (res) {
                        var r = Helpers.return(res);
                        if (r.error == false) {
                            window.location.href = r.redirectUrl;
                        }
                    }.bind(this)
                });

                return;
            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

            if (hours < 10)
                hours = '0' + hours;
            if (minutes < 10)
                minutes = '0' + minutes;
            if (seconds < 10)
                seconds = '0' + seconds;

            document.getElementById(id).innerHTML = '';
            document.getElementById(id).innerHTML += hours + ':';
            document.getElementById(id).innerHTML += minutes + ':';
            document.getElementById(id).innerHTML += seconds;

            // document.getElementById(id).innerHTML = days + 'days ';
            // document.getElementById(id).innerHTML += hours + 'hrs ';
            // document.getElementById(id).innerHTML += minutes + 'mins ';
            // document.getElementById(id).innerHTML += seconds + 'secs';
        }

        if (correcao == 0)
            timer = setInterval(showRemaining, 1000);
    }

};