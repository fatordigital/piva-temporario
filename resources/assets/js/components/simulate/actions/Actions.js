var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    Helpers = require('../Helpers/Helpers.react');

module.exports = {
    getSimulateData (page) {
        $.ajax({
            url: page,
            dataType: 'json',
            data: {_token: Helpers.getToken()},
            method: 'post',
            success: function (res) {
                var r = Helpers.return(res);
                if (r.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.SIMULATE,
                        simulate: r.simulate,
                        questions: r.questions,
                        page: page,
                        user: r.user
                    });
                }
            },
            complete: function (xhr) {
                if (xhr.responseText.indexOf('ct') == -1)
                    window.location.reload();
            }
        });
    },
    saveDissertative (page, text, question_id, test_id, time) {
        $.ajax({
            url: b + 'save-question.json',
            dataType: 'json',
            data: {
                _token: Helpers.getToken(),
                request: Helpers.send({test_id: test_id, dissertative: text, question_id: question_id, time: time})
            },
            method: 'post',
            success: function (res) {
                var r = Helpers.return(res);
                if (r.error == false) {
                    this.getSimulateData(page);
                }
            }.bind(this)
        });
    },
    saveQuestion (page, answer_id, question_id, test_id, time) {
        $.ajax({
            url: b + 'save-question.json',
            dataType: 'json',
            data: {
                _token: Helpers.getToken(),
                request: Helpers.send({test_id: test_id, answer_id: answer_id, question_id: question_id, time: time})
            },
            method: 'post',
            success: function (res) {
                var r = Helpers.return(res);
                if (r.error == false) {
                    this.getSimulateData(page);
                }
            }.bind(this)
        });
    },
    end (test_id) {
        $.ajax({
            url: b + 'end-simulate.json',
            dataType: 'json',
            data: {
                _token: Helpers.getToken(),
                request: Helpers.send({test_id: test_id})
            },
            method: 'post',
            success: function (res) {
                var r = Helpers.return(res);
                if (r.error == false) {
                    window.location.href = r.redirectUrl;
                } else {
                    swal('Aviso', r.msg, 'error');
                }
            }.bind(this)
        });
    },
    report (text, question_id) {
        $.ajax({
            url: b + 'report-question',
            dataType: 'json',
            data: {_token: Helpers.getToken(), request: Helpers.send({question_id: question_id, report: text})},
            method: 'post',
            success: function (res) {
                var r = Helpers.return(res);
                if (r.error == false) {
                    swal('Professor Piva', r.msg, 'success');
                } else {
                    swal('Professor Piva', r.msg, 'error');
                }
            }
        })
    }
};