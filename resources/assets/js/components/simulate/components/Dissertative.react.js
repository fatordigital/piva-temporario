var React = require('react'),
    Model = require('../stores/Store'),
    Helpers = require('../Helpers/Helpers.react'),
    Paginate = require('./Paginate.react'),
    Controller = require('../actions/Actions');

var Dissertative = React.createClass({

    getInitialState () {
        return {
            time: 0
        }
    },

    getDefaultProps() {
        return {
            paginate: {},
            data: {},
            question: {},
            test: '',
            current_page: b + 'simulate-data.json'
        };
    },

    handleKeyDown (e) {
        var text = e.target.value;
        if (Helpers.stripTags(text).length % 100 == 0) {
            Controller.saveDissertative(this.props.current_page, text, this.props.question.id, this.props.test, this.state.time);
        }
    },


    handleFocusOut (e) {
        var text = e.target.value;
        Controller.saveDissertative(this.props.current_page, text, this.props.question.id, this.props.test, this.state.time);
    },


    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    _onChange: function () {

    },

    render () {
        var choose = this.props.question.user_choose;
        return (
            <div className="col-lg-8 col-md-8 col-sm-8 col-xs-12 questoes">
                <div className="pergunta">
                    <div className="col-xs-1">
                        <h2><big>{this.props.paginate.current_page})</big></h2>
                    </div>
                    <div className="col-xs-10">
                        <h2 dangerouslySetInnerHTML={Helpers.markUp(this.props.question.text)}/>
                    </div>
                </div>
                <div className="alternativas pull-left">
                    <textarea rows="50" onBlur={this.handleFocusOut} className="form-control" onKeyDown={this.handleKeyDown} id="textarea-summernote">
                        {choose ? choose.dissertative : ''}
                    </textarea>
                </div>
                <Paginate question={this.props.question} test={this.props.test} paginate={this.props.paginate}/>
            </div>
        )
    }

});

module.exports = Dissertative;