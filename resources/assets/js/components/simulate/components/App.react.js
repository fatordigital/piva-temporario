var React = require('react'),
    Model = require('../stores/Store'),
    Controller = require('../actions/Actions'),
    Helpers = require('../Helpers/Helpers.react'),
    Objective = require('./Objective.react'),
    Dissertative = require('./Dissertative.react'),
    Modal = require('./Modal.react')

var App = React.createClass({

    defaultProps(){
        return {
            correcao: 0
        }
    },

    getInitialState () {
        return {
            paginate: {},
            simulate: {},
            questions: {},
            current_page: b + 'simulate-data.json',
            user: {}
        };
    },

    componentDidMount () {
        Controller.getSimulateData(b + 'simulate-data.json');
    },

    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            paginate: Model.getPaginate(),
            simulate: Model.getSimulate(),
            questions: Model.getQuestions(),
            current_page: Model.getCurrentPage(),
            user: Model.getUserTest()
        });
    },

    handlePage (page, e) {
        e.preventDefault();
        Controller.getSimulateData(page);
    },

    progressBar () {
        var $current = 0;
        if (this.state.paginate.total) {
            var $total = parseFloat(100 / this.state.paginate.total);
            $current = parseInt(this.state.paginate.current_page) * $total;
        }
        return $current + '%';
    },

    handleReport () {
        $('#reportModal').modal('show');
    },

    render() {
        var page = [];
        if (this.state.paginate.last_page) {
            for (var i = 1; i <= this.state.paginate.last_page; i++) {
                if (this.state.paginate.current_page == i)
                    page.push(<li onClick={this.handlePage.bind(this, b + 'simulate-data.json?page=' + i)} style={{marginRight: '5px'}} className="btn btn-primary" key={i}>{i}</li>);
                else {
                    page.push(<li onClick={this.handlePage.bind(this, b + 'simulate-data.json?page=' + i)} style={{marginRight: '5px'}} className="btn btn-default" key={i}>{i}</li>);
                }
            }
        }
        if (Object.keys(this.state.simulate).length > 1) {
            Helpers.CountDownTimer(this.state.user.end_date, 'time', this.state.simulate.id, this.props.correcao);
        }
        return (
            <main className="simulado gray">
                <div className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            <h1>{this.state.simulate.name}</h1>
                        </div>
                    </div>
                </div>
                <div className="container white">
                    {Object.keys(this.state.questions).map(function(key, row){
                        var question = this.state.questions[key].question;
                        if (question.answers.length) {
                            return <Objective key={key} current_page={this.state.current_page} test={this.state.simulate.id} paginate={this.state.paginate} question={question} data={question.answers} correcao={this.props.correcao} />
                        } else {
                            return <Dissertative key={key} current_page={this.state.current_page} test={this.state.simulate.id} paginate={this.state.paginate} question={question} data={question.answers} />
                        }
                    }.bind(this))}
                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12 sidebar-simulado my-simulado">
                        <div className="row">
                            <div className="col-lg-4 col-xs-6">
                                <img src={b + "images/cronometro.png"} className="img-responsive" id="img-cronometro" />
                            </div>
                            <div className="col-lg-8 col-xs-6">
                                <div className="cronometro text-center">
                                    <p id="horas">{Object.keys(this.state.simulate).length > 1 ? this.state.simulate.broke_duration[0] : '00'}</p>
                                    <p id="minutos">{Object.keys(this.state.simulate).length > 1 ? this.state.simulate.broke_duration[1] : '00'}</p>
                                    <p id="segundos">{Object.keys(this.state.simulate).length > 1 ? this.state.simulate.broke_duration[2] : '00'}}</p>
                                    <h2 id="time">00:00:00</h2>
                                </div>
                            </div>
                        </div>
                        <p className="progress-status">Progresso: <span /></p>
                        <div className="progress">
                          <div className="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style={{width:this.progressBar()}}></div>
                        </div>
                        {function(){
                            if (this.props.correcao == 0) {
                                return (
                                    <ul className="list-group">
                                        <li onClick={this.handleReport} className="list-group-item" style={{cursor: 'pointer'}} key="10">
                                            <strong className="text-danger">Reportar erro</strong>
                                        </li>
                                        {function(){
                                            var rules = [];
                                            if (this.state.simulate) {
                                                if (this.state.simulate.jump_question)
                                                    rules.push(<li className="list-group-item" key="1">Questões "puladas", é considerado questão neutra</li>);
                                                if (this.state.simulate.error_question)
                                                    rules.push(<li className="list-group-item" key="2">Se errar, uma questão correta será descontada</li>);
                                                return rules;
                                            }
                                        }.bind(this)()}
                                    </ul>
                                )
                            }
                        }.bind(this)()}
                        <div className="box-questoes">
                            <p className="text-center">QUESTÕES</p>
                            <ul className="num-questoes">
                                {page}
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <br/><br/>
                        <a href={b} className="pull-left back-link"><span className="glyphicon glyphicon-share-alt" /> Voltar</a>
                    </div>
                </div>
                {function(){
                    if (Object.keys(this.state.questions).length)
                        return <Modal question={this.state.questions[0]} />
                }.bind(this)()}
            </main>
        )
    }
});
module.exports = App;