var React = require('react'),
    Controller = require('../actions/Actions')

var Paginate = React.createClass({

    getDefaultProps() {
        return {
            paginate: {},
            test: '',
            question: {}
        }
    },

    handlePrev (page, e) {
        e.preventDefault();
        Controller.getSimulateData(page);
    },

    handleNext (page, e) {
        e.preventDefault();
        Controller.getSimulateData(page);
    },

    handleEnd (test_id, e) {
        swal({
            title: "Deseja realmente finalizar o simulado?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            cancelButtonText: "Não",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                Controller.end(test_id);
                swal("Professor Piva", "Obrigado, estamos calculando as informações, aguarde...", "success");
            }
        });
    },

    render () {
        return (
            <div className="col-xs-12 buttons">
                {function () {
                    if (this.props.paginate.prev_page_url) {
                        return (
                            <a className="btn btn-primary"
                               onClick={this.handlePrev.bind(this, this.props.paginate.prev_page_url)}>
                                <span className="glyphicon glyphicon-chevron-left"/> Pergunta anterior
                            </a>
                        )
                    }
                }.bind(this)()}
                {function () {
                    if (this.props.paginate.next_page_url) {
                        return (
                            <a className="btn btn-primary pull-right"
                               onClick={this.handleNext.bind(this, this.props.paginate.next_page_url)}>
                                Próxima pergunta  <span className="glyphicon glyphicon-chevron-right"/>
                            </a>
                        )
                    } else {
                        return (
                            <button onClick={this.handleEnd.bind(this, this.props.test)} type="button" className="btn btn-danger pull-right">
                                Finalizar Simulado <span className="glyphicon glyphicon-certificate" />
                            </button>
                        )
                    }
                }.bind(this)()}
            </div>
        )
    }

});

module.exports = Paginate;