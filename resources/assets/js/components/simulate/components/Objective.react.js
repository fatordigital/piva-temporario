var React = require('react'),
    Helpers = require('../Helpers/Helpers.react'),
    Paginate = require('./Paginate.react'),
    Controller = require('../actions/Actions');

var Objective = React.createClass({

    getInitialState () {
        return {
            time: 0
        }
    },

    getDefaultProps() {
        return {
            correcao: 0,
            paginate: {},
            data: {},
            question: {},
            test: '',
            current_page: b + 'simulate-data.json'
        };
    },

    handleSelect (answer, e) {
        if (answer) {
            $('.question-option').prop('checked', false);
            $(e.target).prop('checked', true);
            Controller.saveQuestion(this.props.current_page, answer, this.props.question.id, this.props.test, this.state.time);
        }
    },

    render () {
        console.log(this.props.question);
        var choose = this.props.question.user_choose;
        return (
            <div className="col-lg-8 col-md-8 col-sm-8 col-xs-12 questoes">
                <div className="pergunta">
                    <div className="col-xs-1">
                        <h2><big>{this.props.paginate.current_page})</big></h2>
                    </div>
                    <div className="col-xs-10">
                        <h2 dangerouslySetInnerHTML={Helpers.markUp(this.props.question.text)}/>
                    </div>
                </div>
                <div className="alternativas pull-left">
                    {Object.keys(this.props.data).map(function (key, row) {
                        var data = '';
                        data = this.props.data[key];
                        return (
                            <div className={this.props.correcao == 0 ? "box-alternativa" : (data.check == 1) ? 'box-alternativa correta' : 'box-alternativa errada'}
                                 key={key}>
                                <div className="col-xs-1">
                                    <h3>{Helpers.numberToWord(key)})</h3>
                                </div>
                                <div className="col-xs-1">
                                    {function () {
                                        if (choose && choose.answer_id == data.id) {
                                            return (
                                                <input type="checkbox"
                                                       onClick={this.handleSelect.bind(this, data.id)}
                                                       checked="checked"
                                                       style={{cursor: 'pointer'}}
                                                       className="form-control question-option"/>
                                            )
                                        } else {
                                            return (
                                                <input type="checkbox"
                                                       onClick={this.handleSelect.bind(this, data.id)}
                                                       style={{cursor: 'pointer'}}
                                                       className="form-control question-option"/>
                                            )
                                        }
                                    }.bind(this)()}
                                </div>
                                <div className="col-xs-10" dangerouslySetInnerHTML={Helpers.markUp(data.text)}></div>
                            </div>
                        )
                    }.bind(this))}
                </div>
                <div className="row">
                    {function(){
                        if (this.props.question.justify != "" && this.props.correcao == 1) {
                            return (
                                <div className="col-xs-10" dangerouslySetInnerHTML={Helpers.markUp(this.props.question.justify)}></div>
                            )
                        }
                    }.bind(this)()}
                </div>
                <Paginate test={this.props.test} paginate={this.props.paginate}/>
            </div>
        )
    }

});

module.exports = Objective;