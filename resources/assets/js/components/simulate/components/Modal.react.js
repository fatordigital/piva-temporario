var React = require('react'),
    Controller = require('../actions/Actions')

var Modal = React.createClass({

    getDefaultProps() {
        return {
            question: null
        }
    },

    getInitialState () {
        return {
            submit: 'Enviar'
        }
    },

    handleReport (question_id, e) {
        e.preventDefault();
        this.setState({submit: 'Aguarde...'});
        var value = this.refs.reportText.value;
        if (!value) {
            swal('Professor Piva', 'Por favor, informe o erro antes de enviar', 'error');
            return false;
        }
        if (!question_id)
            return false;
        Controller.report(value, question_id);
        this.refs.reportText.value = '';
        this.setState({submit: 'Enviar'});
    },

    render () {
        return (
            <div className="modal fade" id="reportModal" tabIndex="-1" role="dialog">
              <div className="modal-dialog modal-lg"
                   role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                      <h5 className="modal-title">
                          Reportar erro da questão
                      </h5>
                    </div>
                    <div className="modal-body">
                        <div className="form-group">
                            <textarea ref="reportText" rows="20" placeholder="Digite aqui a observação do erro..." className="form-control" />
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="form-group">
                            <button onClick={this.handleReport.bind(this, this.props.question.question_id)} type="button" className="btn btn-primary">
                                {this.state.submit}
                            </button>
                        </div>
                    </div>
                  </div>
              </div>
          </div>
        )
    }

});

module.exports = Modal;