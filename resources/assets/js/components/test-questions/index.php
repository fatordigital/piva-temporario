<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>FATORDIGITAL - GENERATOR</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
</head>
<body>

<button data-toggle="modal" data-target="#discipline-index">Disciplina</button>

<div id="generator"
     data-settings='{"store": "http://localhost", "update": "http://localhost"}'
     data-return='[{"id":1,"slug":"portugues","name":"Portugu\u00eas","deleted_at":null,"created_at":"2016-07-13 14:02:58","updated_at":"2016-07-13 14:03:00"},{"id":2,"slug":"matematica","name":"Matem\u00e1tica","deleted_at":null,"created_at":"2016-07-13 14:03:08","updated_at":"2016-07-13 14:03:10"},{"id":3,"slug":"direito","name":"Direito","deleted_at":null,"created_at":"2016-07-13 14:03:23","updated_at":"2016-07-13 14:03:24"}]'>
</div>

<script type="text/javascript"
        src="dist/assets/js/jquery.js"></script>
<script type="text/javascript"
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="dist/js/discipline.min.js"></script>

</body>
</html>