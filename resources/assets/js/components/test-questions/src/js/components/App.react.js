var React = require('react'),
    Model = require('../stores/Store'),
    Controller = require('../actions/Actions')

var App = React.createClass({

    getInitialState () {
        return {
            questions: {},
            disciplines: {},
            qtd: 1,
            difficulty: '',
            board: '',
            discipline: '',
            boards: {}
        };
    },

    componentDidMount () {
        Controller.getDisciplines()
        if (this.props.edit != false)
            Controller.loadEdit(this.props.edit);
    },

    componentWillMount: function () {
        Model.addChangeListener(this._onChange);
    },

    componentWillUnmount: function () {
        Model.removeChangeListener(this._onChange);
    },

    _onChange: function () {
        this.setState({
            disciplines: Model.getDisciplines(),
            questions: Model.getQuestions()
        });
    },

    handleChange(name, e) {
        this.setState({[name]: e.target.value});
    },

    handleSubmit (e) {
        e.preventDefault();
        Controller.find({
            _token: $('meta[name="_token"]').data('content'),
            quantity: this.state.qtd,
            difficulty: this.state.difficulty,
            discipline: this.state.discipline,
            board: this.state.board,
            notIn: Model.getIds()
        });
    },

    handleDestroy (data, e) {
        var self = this;
        swal({
            title: "Você tem certeza?",
            text: "Deseja realmente remover a questão: " + data.LimitedText,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim",
            cancelButtonText: "Não",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                Controller.destroy(self.props.edit, data.id);
                swal("Deletada", "A questão foi removida do simulado com sucesso.", "success");
            } else {
                swal("Cancelado", "A questão não sofreu alterações.", "error");
            }
        });
    },

    render() {
        var self = this;
        return (
            <div>
                <div className="col-lg-6">
                    <div className="form-group">
                        <label htmlFor="quantity">
                            Quantidade
                        </label>
                        <input type="text"
                               onChange={this.handleChange.bind(this, 'qtd')}
                               defaultValue={this.state.qtd}
                               className="form-control"/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="difficulty">
                            Dificuldade
                        </label>
                        <select className="form-control"
                                onChange={this.handleChange.bind(this, 'difficulty')}>
                            <option value="">Selecione uma dificuldade</option>
                            <option value="1">Fácil</option>
                            <option value="2">Média</option>
                            <option value="3">Difícil</option>
                        </select>
                    </div>
                </div>
                <div className="col-lg-6">
                    <div className="form-group">
                        <label htmlFor="discipline">
                            Disciplina <strong className="text-danger">(*)</strong>
                        </label>
                        <select className="form-control"
                                onChange={this.handleChange.bind(this, 'discipline')}
                                style={{width: '100%'}}>
                            <option value="">Selecione a disciplina</option>
                            {Object.keys(this.state.disciplines).map(function (row) {
                                return <option key={row}
                                               value={row}>{self.state.disciplines[row]}</option>
                            })}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="discipline">
                            Banca
                        </label>
                        <select className="form-control"
                                onChange={this.handleChange.bind(this, 'board')}
                                style={{width: '100%'}}>
                            <option value="">Selecione a Banca</option>
                            {Object.keys(this.props.boards).map(function (row) {
                                return <option key={row}
                                               value={row}>{self.props.boards[row]}</option>
                            })}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="">
                            &nbsp;
                        </label>
                        <button type="button"
                                onClick={this.handleSubmit}
                                className="btn btn-primary btn-block">
                            Adicionar questões
                        </button>
                    </div>
                </div>
                <div className="col-lg-12">
                    <table className="table table-bordered">
                        <thead>
                        <tr>
                            <td>Questão</td>
                            <td width="10%">Disciplina</td>
                            <td>Opções</td>
                        </tr>
                        </thead>
                        <tbody>
                            {Object.keys(this.state.questions).map(function(key, row) {
                                return (
                                    <tr key={key}>
                                        <td>{self.state.questions[key].LimitedText}</td>
                                        <td>{self.state.questions[key].discipline.name}</td>
                                        <td width="5%">
                                            <button onClick={self.handleDestroy.bind(self, self.state.questions[key])} type="button" className="btn btn-xs btn-danger">
                                                <i className="fa fa-trash" />
                                            </button>
                                            <input type="hidden" name="TestQuestion[question_id][]" value={self.state.questions[key].id}/>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

});

module.exports = App;
