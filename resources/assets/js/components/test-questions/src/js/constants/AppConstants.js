var keyMirror = require('keymirror');
module.exports = keyMirror({
    getDiscipline: null,
    getQuestions: null,
    destroyQuestion: null
});
