var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants')

module.exports = {
    loadEdit (id) {
        $.ajax({
            url: b + '/fatorcms/load-edit-test.json',
            dataType: 'json',
            method: 'post',
            data: {_token: $('meta[name="_token"]').data('content'), id: id},
            success: function (res) {
                if (res.error == false) {
                    AppDispatcher.handleViewAction({
                        actionType: AppConstants.getQuestions,
                        data: res.data
                    });
                }
            }
        });
    },
    getDisciplines () {
        $.ajax({
            url: b + '/fatorcms/disciplines.json',
            dataType: 'json',
            method: 'get',
            success: function (res) {
                AppDispatcher.handleViewAction({
                    actionType: AppConstants.getDiscipline,
                    data: res.data
                });
            }
        });
    },
    find (form) {
        $.ajax({
            url: b + '/fatorcms/get-questions.json',
            dataType: 'json',
            method: 'post',
            data: form,
            success: function (res) {
                if (res.error == true) {
                    swal('Professor Piva', res.msg, 'error');
                    return false;
                }
                AppDispatcher.handleViewAction({
                    actionType: AppConstants.getQuestions,
                    data: res.data
                });
            }
        });
    },
    destroy (test_id, question_id) {
        $.ajax({
            url: b + '/fatorcms/destroy-question.json',
            dataType: 'json',
            method: 'post',
            data: {
                _token: $('meta[name="_token"]').data('content'),
                test_id: test_id,
                question_id: question_id,
                '_method': 'DELETE'
            },
            success: function (res) {
                if (res.error == true) {
                    swal('Ops!', 'Um erro inesperado aconteceu, tente novamente ou atualize a sua página.', 'error');
                    return false;
                }
                AppDispatcher.handleViewAction({
                    actionType: AppConstants.destroyQuestion,
                    data: question_id
                });
            }
        })
    }
};