var AppDispatcher = require('../dispatchers/AppDispatcher'),
    AppConstants = require('../constants/AppConstants'),
    assign = require('react/lib/Object.assign'),
    EventEmitter = require('events').EventEmitter,
    Helpers = require('../Helpers/Helpers.react'),
    CHANGE_EVENT = 'change',
    _disciplines = {},
    _questions = {},
    _notIn = [];

function setQuestions(data) {
    _questions = $.merge(data, _questions);
    setNotIn(_questions);
}

function setDisciplines (data) {
    _disciplines = data;
}

function setNotIn (data) {
    $.each(data, function(key, row) {
        if (_notIn.indexOf(row.id) === -1)
            _notIn.push(row.id);
    });
}

function destroyQuestion (id) {
    Helpers.removeObject(id, _questions, 'id');
    Helpers.removeArray(id, _notIn);
}

var StartStore = assign(EventEmitter.prototype, {
    emitChange() {
        this.emit(CHANGE_EVENT);
    },
    addChangeListener (callback) {
        this.on(CHANGE_EVENT, callback);
    },
    removeChangeListener (callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },
    getDisciplines() {
        return _disciplines;
    },
    getQuestions () {
        return _questions;
    },
    getIds () {
        return _notIn;
    }
});

StartStore.dispatchToken = AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {
        case AppConstants.getDiscipline:
            setDisciplines(action.data);
            break;
        case AppConstants.getQuestions:
            setQuestions(action.data);
            break;
        case AppConstants.destroyQuestion:
            destroyQuestion(action.data);
            break;
    }
    StartStore.emitChange();
    return true;
});


module.exports = StartStore;
