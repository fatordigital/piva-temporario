<?php
    $return = [
        'login'  => [
            'welcome' => 'Olá, seja bem-vindo',
            'logout'  => 'Sair'
        ],
        'form'   => [
            'alert' => [
                'fields_required' => 'Os campos com <strong class="text-danger">(*)</strong> são campos obrigatórios.'
            ]
        ],
        'insert' => [
            'success' => 'O registro foi inserido com sucesso.'
        ],
        'update' => [
            'success' => ':name foi atualizado com sucesso.'
        ],
        'delete' => [
            'coupon'  => [
                'order' => 'Desculpe, mas este cupom está sendo usado no pedido :id'
            ],
            'success' => 'O registro foi removido com sucesso.',
            'alert'   => 'Este item tem outros menus relacionados.'
        ],
        'status' => [
            'success' => 'Situação foi alterada para :status com sucesso.'
        ],
        'errors' => [
            'logon' => 'O usuário :user, está logado no momento.'
        ],
        'fd' => [
            'hello' => 'Olá :name. Agora são <span class="font-bold" id="hour-top">:time</span>'
        ]
    ];

    return $return;