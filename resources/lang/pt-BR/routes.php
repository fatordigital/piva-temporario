<?php
    $return = [
        'language'  => 'mudar/para/idioma/',
        'logout'    => 'ate-logo',
        'login'     => 'autenticar',
        'auth'      => 'servico/de/autenticacao',
        'dashboard' => 'painel'
    ];
    return $return;