<?php

    $return = [
        'username'        => 'Usuário/E-mail',
        'password'        => 'Senha',
        'failed'          => 'Ops, não encontramos os dados digitados.',
        'throttle'        => 'Falha em várias tentaivas. Por favor, tente novamente em :seconds segundos',
        'forgot_password' => 'Esqueceu a senha?'
    ];

    return $return;