<?php
    $return = [
        'language'  => 'change/lang/to/',
        'logout'    => 'see-ya',
        'login'     => 'login',
        'auth'      => 'service/auth',
        'dashboard' => 'dashboard'
    ];
    return $return;