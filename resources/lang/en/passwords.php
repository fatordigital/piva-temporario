<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A senha deve ter no minímo 6 caracteres.',
    'reset' => 'Sua senha foi resetada!',
    'sent' => 'Você recebeu nosso e-mail com um link para resetar a senha!',
    'token' => 'A chave de segurança para resetar sua senha não é válida.',
    'user' => "Nós não encontramos este e-mail no nossos registros.",

];
