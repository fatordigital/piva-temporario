<?php

    $return = [
        'username'        => 'User/E-mail',
        'password'        => 'Password',
        'failed'          => 'These credentials do not match our records.',
        'throttle'        => 'Too many login attempts. Please try again in :seconds seconds.',
        'forgot_password' => 'Forgot Password?'
    ];

    return $return;