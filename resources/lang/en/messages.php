<?php
    $return = [
        'login'  => [
            'welcome' => 'Welcome, please signin',
            'logout'  => 'See ya'
        ],
        'form'   => [
            'alert' => [
                'fields_required' => 'The fields with <strong class="text-danger">(*)</strong> is required fields.'
            ]
        ],
        'insert' => [
            'success' => 'Data has been save with success.'
        ],
        'update' => [
            'success' => ':name has been updated with success.'
        ],
        'delete' => [
            'coupon' => [
                'order' => 'Sorry, but this coupon has been used in order :id'
            ],
            'success' => 'The data has been removed with success.',
            'alert'   => 'This item have other menus relations'
        ],
        'status' => [
            'success' => 'Status has been changed to :status with success.'
        ],
        'errors' => [
            'logon' => 'The user :user, on in this moment.'
        ],
        'fd' => [
            'hello' => 'Hello :name. Now is <span class="font-bold" id="hour-top">:time</span>'
        ]
    ];

    return $return;