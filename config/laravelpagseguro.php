<?php

return [
    'sandbox' => TRUE,//DEFINI SE SERÁ UTILIZADO O AMBIENTE DE TESTES
    
    'credentials' => [//SETA AS CREDENCIAIS DE SUA LOJA
        'email' => 'fatordigital@fatordigital.com.br',
        'token' => 'FF8DE4E9D4614178BBA0777B92B17EC5',
    ],
    'currency' => [//MOEDA QUE SERÁ UTILIZADA COMO MEIO DE PAGAMENTO
        'type' => 'BRL'
    ],
    'reference' => [//CRIAR UMA REFERENCIA PARA OS PRODUTOS VENDIDOS
        'idReference' => NULL
    ],
    'proxy' => [//CONFIGURAÇÃO PARA PROXY
        'user'     => NULL,
        'password' => NULL,
        'url'      => NULL,
        'port'     => NULL,
        'protocol' => NULL
    ],
    'url' => '',
];
