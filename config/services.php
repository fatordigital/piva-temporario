<?php
    return [

        /*
        |--------------------------------------------------------------------------
        | Third Party Services
        |--------------------------------------------------------------------------
        |
        | This file is for storing the credentials for third party services such
        | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
        | default location for this type of information, allowing packages
        | to have a conventional place to find your various credentials.
        |
        */

        'mailgun' => [
            'domain' => env('MAILGUN_DOMAIN'),
            'secret' => env('MAILGUN_SECRET'),
        ],

        'mandrill' => [
            'secret' => env('zBFyUxGFVPrq0NNXW9nxLQ'),
        ],

        'ses' => [
            'key'    => env('SES_KEY'),
            'secret' => env('SES_SECRET'),
            'region' => 'us-east-1',
        ],

        'stripe' => [
            'model'  => App\User::class,
            'key'    => env('STRIPE_KEY'),
            'secret' => env('STRIPE_SECRET'),
        ],

        'facebook' => [
//            'client_id'     => env('FACEBOOK_CLIENT_ID', '1559224034396009'),
//            'client_secret' => env('FACEBOOK_CLIENT_SECRET', '3f9952c1e07b4de1ca2e5a0273226780'),
//            'redirect'      => env('FACEBOOK_REDIRECT', 'http://operacaodelta.com.br/temp/public/facebook')
            'client_id'     => env('FACEBOOK_CLIENT_ID', '1120068371339557'),
            'client_secret' => env('FACEBOOK_CLIENT_SECRET', '6126f76a4eb4e02b4ae54afc78bd549c'),
            'redirect'      => env('FACEBOOK_REDIRECT', 'http://localhost/operacaodelta//facebook')
        ]

    ];
