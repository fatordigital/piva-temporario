var elixir = require('laravel-elixir'),
    gulp = require('gulp'),
    browserify = require('browserify'),
    reactify = require('reactify'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream');

elixir.config.js.browserify.transformers.push({
    name: 'babelify',
    options: {presets: ['es2015', 'react']}
});

elixir.config.js.browserify.watchify = {
    enabled: true,
    options: {
        poll: true
    }
};

elixir(function (mix) {
    mix.browserify([
        'components/teachers/app.js'
    ], 'public/js/cms/components/teachers/teachers.min.js');
    mix.browserify([
        'components/notification/src/js/app.js'
    ], 'public/js/cms/components/notification/notification.min.js');
    mix.browserify([
        'components/discipline/src/js/app.js'
    ], 'public/js/cms/components/discipline/discipline.min.js');
    mix.browserify([
        'components/team-lessons/app.js'
    ], 'public/js/cms/components/team-lessons/grid.min.js');
    mix.browserify([
        'components/answer/src/js/app.js'
    ], 'public/js/cms/components/answer/answer.min.js');
    mix.browserify([
        'components/test-questions/src/js/app.js'
    ], 'public/js/cms/components/test-questions/test-questions.min.js');
    mix.browserify([
        'components/content/src/js/app.js'
    ], 'public/js/cms/components/content/content.min.js');
    mix.browserify([
        'components/about-team/app.js'
    ], 'public/js/cms/components/about-team/about-team.min.js');
    mix.browserify([
        'components/pagseguro/app.js'
    ], 'public/js/components/pagseguro/pagseguro.min.js');
    mix.browserify([
        'components/simulate/app.js'
    ], 'public/js/components/simulate/simulate.min.js');
    mix.browserify([
        'components/study/app.js'
    ], 'public/js/components/study/study.min.js');
    mix.version([
        'public/js/cms/components/discipline/discipline.min.js',
        'public/js/cms/components/answer/answer.min.js',
        'public/js/cms/components/test-questions/test-questions.min.js',
        'public/js/cms/components/notification/notification.min.js',
        'public/js/cms/components/content/content.min.js',
        'public/js/cms/components/team-lessons/grid.min.js',
        'public/js/cms/components/teachers/teachers.min.js',
        'public/js/cms/components/about-team/about-team.min.js',
        'public/js/components/pagseguro/pagseguro.min.js',
        'public/js/components/simulate/simulate.min.js',
        'public/js/components/study/study.min.js'
    ]);
});