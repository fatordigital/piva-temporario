<?php

    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateUsersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('group_id')->nullable();
                $table->string('name');
                $table->string('email')->unique();
                $table->string('password', 60);
                $table->string('filename')->nullable();
                $table->timestamp('last_activity')->nullable();
                $table->boolean('status');
                $table->rememberToken();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            //Schema::drop('users');
        }
    }
