<?php

    use Illuminate\Database\Seeder;
    use Illuminate\Database\Eloquent\Model;
    use Modules\Acl\Database\Seeders\AclDatabaseSeeder;

    class DatabaseSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            Model::unguard();

            // $this->call(UserTableSeeder::class);
            $this->call(UserTableSeeder::class);
            $this->call(AclDatabaseSeeder::class);

            Model::reguard();
        }
    }
