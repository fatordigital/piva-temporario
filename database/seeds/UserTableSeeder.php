<?php

    use App\User;
    use Illuminate\Database\Seeder;

    class UserTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            User::firstOrCreate([
                'group_id' => 1,
                'name'     => 'Developer',
                'email'    => 'dev@fd.com.br',
                'password' => bcrypt(123),
                'filename' => '',
                'status'   => true
            ]);
        }
    }
